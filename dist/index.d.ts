/// <reference path="../node_modules/@types/lodash/common/common.d.ts" />
/// <reference path="../node_modules/@types/lodash/common/array.d.ts" />
/// <reference path="../node_modules/@types/lodash/common/collection.d.ts" />
/// <reference path="../node_modules/@types/lodash/common/date.d.ts" />
/// <reference path="../node_modules/@types/lodash/common/function.d.ts" />
/// <reference path="../node_modules/@types/lodash/common/lang.d.ts" />
/// <reference path="../node_modules/@types/lodash/common/math.d.ts" />
/// <reference path="../node_modules/@types/lodash/common/number.d.ts" />
/// <reference path="../node_modules/@types/lodash/common/object.d.ts" />
/// <reference path="../node_modules/@types/lodash/common/seq.d.ts" />
/// <reference path="../node_modules/@types/lodash/common/string.d.ts" />
/// <reference path="../node_modules/@types/lodash/common/util.d.ts" />
/// <reference types="node" />
/// <reference types="klaw" />
import events, { EventEmitter } from 'events';
import * as npmlog from 'npmlog';
import { Logger } from 'npmlog';
import { Server as Server$1 } from 'ws';
import http, { Server } from 'http';
import AndroidDriver, { AndroidDriverCaps } from 'armor-android-driver';
import { Writable } from 'stream';
import * as child_process from 'child_process';
import semver from 'semver';
import https from 'https';
import * as fs_promises from 'fs/promises';
import * as _fs from 'fs';
import { promises, constants, createWriteStream, createReadStream } from 'fs';
import { Path, PathScurry, FSOption } from 'path-scurry';
import * as ncp from 'ncp';
import ncp__default from 'ncp';
import * as which from 'which';
import which__default from 'which';
import * as klaw from 'klaw';
import klaw__default from 'klaw';
import * as mv$1 from 'mv';
import { v1, v3, v4, v5 } from 'uuid';
import { parse } from 'shell-quote';
import yauzl from 'yauzl';
import * as util$1 from 'util';

function _mergeNamespaces(n, m) {
    m.forEach(function (e) {
        e && typeof e !== 'string' && !Array.isArray(e) && Object.keys(e).forEach(function (k) {
            if (k !== 'default' && !(k in n)) {
                var d = Object.getOwnPropertyDescriptor(e, k);
                Object.defineProperty(n, k, d.get ? d : {
                    enumerable: true,
                    get: function () { return e[k]; }
                });
            }
        });
    });
    return Object.freeze(n);
}

interface AppConfigs {
    root: string;
    appDataPath: string;
    modulesDir: string;
    extensionsDir: string;
    [key: string]: any;
}

declare class AITModuleBase extends EventEmitter {
    constructor();
    install: () => Promise<void> | void;
    uninstall: () => void;
    update: () => void;
    login: () => void;
    logout: () => void;
}
declare abstract class AITModule extends AITModuleBase {
    static newInstance: (configs: AppConfigs) => AITModule;
}

declare global {
	// eslint-disable-next-line @typescript-eslint/consistent-type-definitions -- It has to be an `interface` so that it can be merged.
	interface SymbolConstructor {
		readonly observable: symbol;
	}
}

declare const emptyObjectSymbol: unique symbol;

/**
Represents a strictly empty plain object, the `{}` value.

When you annotate something as the type `{}`, it can be anything except `null` and `undefined`. This means that you cannot use `{}` to represent an empty plain object ([read more](https://stackoverflow.com/questions/47339869/typescript-empty-object-and-any-difference/52193484#52193484)).

@example
```
import type {EmptyObject} from 'type-fest';

// The following illustrates the problem with `{}`.
const foo1: {} = {}; // Pass
const foo2: {} = []; // Pass
const foo3: {} = 42; // Pass
const foo4: {} = {a: 1}; // Pass

// With `EmptyObject` only the first case is valid.
const bar1: EmptyObject = {}; // Pass
const bar2: EmptyObject = 42; // Fail
const bar3: EmptyObject = []; // Fail
const bar4: EmptyObject = {a: 1}; // Fail
```

Unfortunately, `Record<string, never>`, `Record<keyof any, never>` and `Record<never, never>` do not work. See {@link https://github.com/sindresorhus/type-fest/issues/395 #395}.

@category Object
*/
type EmptyObject = {[emptyObjectSymbol]?: never};

/**
Useful to flatten the type output to improve type hints shown in editors. And also to transform an interface into a type to aide with assignability.

@example
```
import type {Simplify} from 'type-fest';

type PositionProps = {
	top: number;
	left: number;
};

type SizeProps = {
	width: number;
	height: number;
};

// In your editor, hovering over `Props` will show a flattened object with all the properties.
type Props = Simplify<PositionProps & SizeProps>;
```

Sometimes it is desired to pass a value as a function argument that has a different type. At first inspection it may seem assignable, and then you discover it is not because the `value`'s type definition was defined as an interface. In the following example, `fn` requires an argument of type `Record<string, unknown>`. If the value is defined as a literal, then it is assignable. And if the `value` is defined as type using the `Simplify` utility the value is assignable.  But if the `value` is defined as an interface, it is not assignable because the interface is not sealed and elsewhere a non-string property could be added to the interface.

If the type definition must be an interface (perhaps it was defined in a third-party npm package), then the `value` can be defined as `const value: Simplify<SomeInterface> = ...`. Then `value` will be assignable to the `fn` argument.  Or the `value` can be cast as `Simplify<SomeInterface>` if you can't re-declare the `value`.

@example
```
import type {Simplify} from 'type-fest';

interface SomeInterface {
	foo: number;
	bar?: string;
	baz: number | undefined;
}

type SomeType = {
	foo: number;
	bar?: string;
	baz: number | undefined;
};

const literal = {foo: 123, bar: 'hello', baz: 456};
const someType: SomeType = literal;
const someInterface: SomeInterface = literal;

function fn(object: Record<string, unknown>): void {}

fn(literal); // Good: literal object type is sealed
fn(someType); // Good: type is sealed
fn(someInterface); // Error: Index signature for type 'string' is missing in type 'someInterface'. Because `interface` can be re-opened
fn(someInterface as Simplify<SomeInterface>); // Good: transform an `interface` into a `type`
```

@link https://github.com/microsoft/TypeScript/issues/15300

@category Object
*/
type Simplify<T> = {[KeyType in keyof T]: T[KeyType]} & {};

/**
Omit any index signatures from the given object type, leaving only explicitly defined properties.

This is the counterpart of `PickIndexSignature`.

Use-cases:
- Remove overly permissive signatures from third-party types.

This type was taken from this [StackOverflow answer](https://stackoverflow.com/a/68261113/420747).

It relies on the fact that an empty object (`{}`) is assignable to an object with just an index signature, like `Record<string, unknown>`, but not to an object with explicitly defined keys, like `Record<'foo' | 'bar', unknown>`.

(The actual value type, `unknown`, is irrelevant and could be any type. Only the key type matters.)

```
const indexed: Record<string, unknown> = {}; // Allowed

const keyed: Record<'foo', unknown> = {}; // Error
// => TS2739: Type '{}' is missing the following properties from type 'Record<"foo" | "bar", unknown>': foo, bar
```

Instead of causing a type error like the above, you can also use a [conditional type](https://www.typescriptlang.org/docs/handbook/2/conditional-types.html) to test whether a type is assignable to another:

```
type Indexed = {} extends Record<string, unknown>
	? '✅ `{}` is assignable to `Record<string, unknown>`'
	: '❌ `{}` is NOT assignable to `Record<string, unknown>`';
// => '✅ `{}` is assignable to `Record<string, unknown>`'

type Keyed = {} extends Record<'foo' | 'bar', unknown>
	? "✅ `{}` is assignable to `Record<'foo' | 'bar', unknown>`"
	: "❌ `{}` is NOT assignable to `Record<'foo' | 'bar', unknown>`";
// => "❌ `{}` is NOT assignable to `Record<'foo' | 'bar', unknown>`"
```

Using a [mapped type](https://www.typescriptlang.org/docs/handbook/2/mapped-types.html#further-exploration), you can then check for each `KeyType` of `ObjectType`...

```
import type {OmitIndexSignature} from 'type-fest';

type OmitIndexSignature<ObjectType> = {
	[KeyType in keyof ObjectType // Map each key of `ObjectType`...
	]: ObjectType[KeyType]; // ...to its original value, i.e. `OmitIndexSignature<Foo> == Foo`.
};
```

...whether an empty object (`{}`) would be assignable to an object with that `KeyType` (`Record<KeyType, unknown>`)...

```
import type {OmitIndexSignature} from 'type-fest';

type OmitIndexSignature<ObjectType> = {
	[KeyType in keyof ObjectType
		// Is `{}` assignable to `Record<KeyType, unknown>`?
		as {} extends Record<KeyType, unknown>
			? ... // ✅ `{}` is assignable to `Record<KeyType, unknown>`
			: ... // ❌ `{}` is NOT assignable to `Record<KeyType, unknown>`
	]: ObjectType[KeyType];
};
```

If `{}` is assignable, it means that `KeyType` is an index signature and we want to remove it. If it is not assignable, `KeyType` is a "real" key and we want to keep it.

```
import type {OmitIndexSignature} from 'type-fest';

type OmitIndexSignature<ObjectType> = {
	[KeyType in keyof ObjectType
		as {} extends Record<KeyType, unknown>
			? never // => Remove this `KeyType`.
			: KeyType // => Keep this `KeyType` as it is.
	]: ObjectType[KeyType];
};
```

@example
```
import type {OmitIndexSignature} from 'type-fest';

interface Example {
	// These index signatures will be removed.
	[x: string]: any
	[x: number]: any
	[x: symbol]: any
	[x: `head-${string}`]: string
	[x: `${string}-tail`]: string
	[x: `head-${string}-tail`]: string
	[x: `${bigint}`]: string
	[x: `embedded-${number}`]: string

	// These explicitly defined keys will remain.
	foo: 'bar';
	qux?: 'baz';
}

type ExampleWithoutIndexSignatures = OmitIndexSignature<Example>;
// => { foo: 'bar'; qux?: 'baz' | undefined; }
```

@see PickIndexSignature
@category Object
*/
type OmitIndexSignature<ObjectType> = {
	[KeyType in keyof ObjectType as {} extends Record<KeyType, unknown>
		? never
		: KeyType]: ObjectType[KeyType];
};

/**
Pick only index signatures from the given object type, leaving out all explicitly defined properties.

This is the counterpart of `OmitIndexSignature`.

When you use a type that will iterate through an object that has indexed keys and explicitly defined keys you end up with a type where only the indexed keys are kept. This is because `keyof` of an indexed type always returns `string | number | symbol`, because every key is possible in that object. With this type, you can save the indexed keys and reinject them later, like in the second example below.

@example
```
import type {PickIndexSignature} from 'type-fest';

declare const symbolKey: unique symbol;

type Example = {
	// These index signatures will remain.
	[x: string]: unknown;
	[x: number]: unknown;
	[x: symbol]: unknown;
	[x: `head-${string}`]: string;
	[x: `${string}-tail`]: string;
	[x: `head-${string}-tail`]: string;
	[x: `${bigint}`]: string;
	[x: `embedded-${number}`]: string;

	// These explicitly defined keys will be removed.
	['snake-case-key']: string;
	[symbolKey]: string;
	foo: 'bar';
	qux?: 'baz';
};

type ExampleIndexSignature = PickIndexSignature<Example>;
// {
// 	[x: string]: unknown;
// 	[x: number]: unknown;
// 	[x: symbol]: unknown;
// 	[x: `head-${string}`]: string;
// 	[x: `${string}-tail`]: string;
// 	[x: `head-${string}-tail`]: string;
// 	[x: `${bigint}`]: string;
// 	[x: `embedded-${number}`]: string;
// }
```

@example
```
import type {OmitIndexSignature, PickIndexSignature, Simplify} from 'type-fest';

type Foo = {
	[x: string]: string;
	foo: string;
	bar: number;
};

// Imagine that you want a new type `Bar` that comes from `Foo`.
// => {
// 	[x: string]: string;
// 	bar: number;
// };

type Bar = Omit<Foo, 'foo'>;
// This is not working because `Omit` returns only indexed keys.
// => {
// 	[x: string]: string;
// 	[x: number]: string;
// }

// One solution is to save the indexed signatures to new type.
type FooIndexSignature = PickIndexSignature<Foo>;
// => {
// 	[x: string]: string;
// }

// Get a new type without index signatures.
type FooWithoutIndexSignature = OmitIndexSignature<Foo>;
// => {
// 	foo: string;
// 	bar: number;
// }

// At this point we can use Omit to get our new type.
type BarWithoutIndexSignature = Omit<FooWithoutIndexSignature, 'foo'>;
// => {
// 	bar: number;
// }

// And finally we can merge back the indexed signatures.
type BarWithIndexSignature = Simplify<BarWithoutIndexSignature & FooIndexSignature>;
// => {
// 	[x: string]: string;
// 	bar: number;
// }
```

@see OmitIndexSignature
@category Object
*/
type PickIndexSignature<ObjectType> = {
	[KeyType in keyof ObjectType as {} extends Record<KeyType, unknown>
		? KeyType
		: never]: ObjectType[KeyType];
};

// Returns `never` if the key is optional otherwise return the key type.
type RequiredFilter<Type, Key extends keyof Type> = undefined extends Type[Key]
	? Type[Key] extends undefined
		? Key
		: never
	: Key;

// Returns `never` if the key is required otherwise return the key type.
type OptionalFilter<Type, Key extends keyof Type> = undefined extends Type[Key]
	? Type[Key] extends undefined
		? never
		: Key
	: never;

/**
Enforce optional keys (by adding the `?` operator) for keys that have a union with `undefined`.

@example
```
import type {EnforceOptional} from 'type-fest';

type Foo = {
	a: string;
	b?: string;
	c: undefined;
	d: number | undefined;
};

type FooBar = EnforceOptional<Foo>;
// => {
// 	a: string;
// 	b?: string;
// 	c: undefined;
// 	d?: number;
// }
```

@internal
@category Object
*/
type EnforceOptional<ObjectType> = Simplify<{
	[Key in keyof ObjectType as RequiredFilter<ObjectType, Key>]: ObjectType[Key]
} & {
	[Key in keyof ObjectType as OptionalFilter<ObjectType, Key>]?: Exclude<ObjectType[Key], undefined>
}>;

// Merges two objects without worrying about index signatures.
type SimpleMerge<Destination, Source> = {
	[Key in keyof Destination as Key extends keyof Source ? never : Key]: Destination[Key];
} & Source;

/**
Merge two types into a new type. Keys of the second type overrides keys of the first type.

@example
```
import type {Merge} from 'type-fest';

interface Foo {
	[x: string]: unknown;
	[x: number]: unknown;
	foo: string;
	bar: symbol;
}

type Bar = {
	[x: number]: number;
	[x: symbol]: unknown;
	bar: Date;
	baz: boolean;
};

export type FooBar = Merge<Foo, Bar>;
// => {
// 	[x: string]: unknown;
// 	[x: number]: number;
// 	[x: symbol]: unknown;
// 	foo: string;
// 	bar: Date;
// 	baz: boolean;
// }
```

@category Object
*/
type Merge<Destination, Source> = EnforceOptional<
SimpleMerge<PickIndexSignature<Destination>, PickIndexSignature<Source>>
& SimpleMerge<OmitIndexSignature<Destination>, OmitIndexSignature<Source>>>;

/**
Create a union of the given object's values, and optionally specify which keys to get the values from.

Please upvote [this issue](https://github.com/microsoft/TypeScript/issues/31438) if you want to have this type as a built-in in TypeScript.

@example
```
// data.json
{
	'foo': 1,
	'bar': 2,
	'biz': 3
}

// main.ts
import type {ValueOf} from 'type-fest';
import data = require('./data.json');

export function getData(name: string): ValueOf<typeof data> {
	return data[name];
}

export function onlyBar(name: string): ValueOf<typeof data, 'bar'> {
	return data[name];
}

// file.ts
import {getData, onlyBar} from './main';

getData('foo');
//=> 1

onlyBar('foo');
//=> TypeError ...

onlyBar('bar');
//=> 2
```

@category Object
*/
type ValueOf<ObjectType, ValueType extends keyof ObjectType = keyof ObjectType> = ObjectType[ValueType];

/**
 * Utility type for a object with string-only props
 */
type StringRecord<T = any> = Record<string, T>;
/**
 * Converts a kebab-cased string into a camel-cased string.
 */
type KebabToCamel<S extends string> = S extends `${infer P1}-${infer P2}${infer P3}` ? `${Lowercase<P1>}${Uppercase<P2>}${KebabToCamel<P3>}` : Lowercase<S>;
/**
 * Object `B` has all the keys as object `A` (even if those keys in `A` are otherwise optional).
 */
type Associated<A extends object, B extends {
    [key in keyof Required<A>]: unknown;
}> = {
    [Prop in keyof Required<A>]: B[Prop];
};
/**
 * Given `string` `T`, this is a case-insensitive version of `T`.
 */
type AnyCase<T extends string> = string extends T ? string : T extends `${infer F1}${infer F2}${infer R}` ? `${Uppercase<F1> | Lowercase<F1>}${Uppercase<F2> | Lowercase<F2>}${AnyCase<R>}` : T extends `${infer F}${infer R}` ? `${Uppercase<F> | Lowercase<F>}${AnyCase<R>}` : '';
/**
 * A W3C element.
 * @see https://www.w3.org/TR/webdriver1/#elements
 */
interface Element<Id extends string = string> {
    /**
     * For backwards compatibility with JSONWP only.
     * @deprecated Use {@linkcode element-6066-11e4-a52e-4f735466cecf} instead.
     */
    ELEMENT?: Id;
    /**
     * This property name is the string constant W3C element identifier used to identify an object as
     * a W3C element.
     */
    'element-6066-11e4-a52e-4f735466cecf': Id;
}
/**
 * A W3C find element result.
 * @see https://www.w3.org/TR/webdriver1/#elements
 */
interface ElementFind extends Element {
    /**
     * This property name is the string constant W3C element identifier used to identify an object as
     * a W3C element.
     */
    selector: string;
}

/**
 * Portions Copyright (c) 2017 Igor Muchychka
 * @see https://github.com/w3c-webdriver/w3c-webdriver
 * @module
 */

/**
 * @group Actions
 */
type PauseAction = {
    type: 'pause';
    duration: number;
};
/**
 * @group Actions
 */
type KeyDownAction = {
    type: 'keyDown';
    value: Key | string;
};
/**
 * @group Actions
 */
type KeyUpAction = {
    type: 'keyUp';
    value: Key | string;
};
/**
 * @group Actions
 */
type PointerMoveAction = {
    type: 'pointerMove';
    x: number;
    y: number;
    duration?: number;
    origin?: 'viewport' | 'pointer' | Element;
};
/**
 * @group Actions
 */
type PointerUpAction = {
    type: 'pointerUp';
    button: number;
};
/**
 * @group Actions
 */
type PointerDownAction = {
    type: 'pointerDown';
    button: number;
};
/**
 * @group Actions
 */
type NullAction = PauseAction;
/**
 * @group Actions
 */
type KeyAction = PauseAction | KeyDownAction | KeyUpAction;
/**
 * @group Actions
 */
type PointerAction = PauseAction | PointerMoveAction | PointerUpAction | PointerDownAction;
/**
 * @group Actions
 */
type NullActionSequence = {
    type: 'none';
    id: string;
    actions: NullAction[];
};
/**
 * @group Actions
 */
type KeyActionSequence = {
    type: 'key';
    id: string;
    actions: KeyAction[];
};
/**
 * @group Actions
 */
type PointerParameters = {
    pointerType: 'mouse' | 'pen' | 'touch';
};
/**
 * @group Actions
 */
type PointerActionSequence = {
    type: 'pointer';
    id: string;
    actions: PointerAction[];
    parameters?: PointerParameters;
};
/**
 * @group Actions
 */
type ActionSequence = NullActionSequence | KeyActionSequence | PointerActionSequence;
/**
 * @group Actions
 */
declare enum Key {
    NULL = "\uE000",
    CANCEL = "\uE001",
    HELP = "\uE002",
    BACKSPACE = "\uE003",
    TAB = "\uE004",
    CLEAR = "\uE005",
    RETURN = "\uE006",
    ENTER = "\uE007",
    SHIFT = "\uE008",
    CONTROL = "\uE009",
    ALT = "\uE00A",
    PAUSE = "\uE00B",
    ESCAPE = "\uE00C",
    SPACE = "\uE00D",
    PAGE_UP = "\uE00E",
    PAGE_DOWN = "\uE00F",
    END = "\uE010",
    HOME = "\uE011",
    LEFT = "\uE012",
    UP = "\uE013",
    RIGHT = "\uE014",
    DOWN = "\uE015",
    INSERT = "\uE016",
    DELETE = "\uE017",
    SEMICOLON = "\uE018",
    EQUALS = "\uE019",
    NUMPAD0 = "\uE01A",
    NUMPAD1 = "\uE01B",
    NUMPAD2 = "\uE01C",
    NUMPAD3 = "\uE01D",
    NUMPAD4 = "\uE01E",
    NUMPAD5 = "\uE01F",
    NUMPAD6 = "\uE020",
    NUMPAD7 = "\uE021",
    NUMPAD8 = "\uE022",
    NUMPAD9 = "\uE023",
    MULTIPLY = "\uE024",
    ADD = "\uE025",
    SEPARATOR = "\uE026",
    SUBTRACT = "\uE027",
    DECIMAL = "\uE028",
    DIVIDE = "\uE029",
    F1 = "\uE031",
    F2 = "\uE032",
    F3 = "\uE033",
    F4 = "\uE034",
    F5 = "\uE035",
    F6 = "\uE036",
    F7 = "\uE037",
    F8 = "\uE038",
    F9 = "\uE039",
    F10 = "\uE03A",
    F11 = "\uE03B",
    F12 = "\uE03C",
    META = "\uE03D",
    ZENKAKUHANKAKU = "\uE040",
    R_SHIFT = "\uE050",
    R_CONTROL = "\uE051",
    R_ALT = "\uE052",
    R_META = "\uE053",
    R_PAGEUP = "\uE054",
    R_PAGEDOWN = "\uE055",
    R_END = "\uE056",
    R_HOME = "\uE057",
    R_ARROWLEFT = "\uE058",
    R_ARROWUP = "\uE059",
    R_ARROWRIGHT = "\uE05A",
    R_ARROWDOWN = "\uE05B",
    R_INSERT = "\uE05C",
    R_DELETE = "\uE05D"
}

declare const BASE_DESIRED_CAP_CONSTRAINTS: {
    readonly platformName: {
        readonly presence: true;
        readonly isString: true;
    };
    readonly app: {
        readonly isString: true;
    };
    readonly platformVersion: {
        readonly isString: true;
    };
    readonly webSocketUrl: {
        readonly isBoolean: true;
    };
    readonly newCommandTimeout: {
        readonly isNumber: true;
    };
    readonly automationName: {
        readonly isString: true;
    };
    readonly autoLaunch: {
        readonly isBoolean: true;
    };
    readonly udid: {
        readonly isString: true;
    };
    readonly orientation: {
        readonly inclusion: readonly ["LANDSCAPE", "PORTRAIT"];
    };
    readonly autoWebview: {
        readonly isBoolean: true;
    };
    readonly noReset: {
        readonly isBoolean: true;
    };
    readonly fullReset: {
        readonly isBoolean: true;
    };
    readonly language: {
        readonly isString: true;
    };
    readonly locale: {
        readonly isString: true;
    };
    readonly eventTimings: {
        readonly isBoolean: true;
    };
    readonly printPageSourceOnFindFailure: {
        readonly isBoolean: true;
    };
};
type BaseDriverCapConstraints = typeof BASE_DESIRED_CAP_CONSTRAINTS;

type PageLoadingStrategy = 'none' | 'eager' | 'normal';
type ProxyTypes = 'pac' | 'noproxy' | 'autodetect' | 'system' | 'manual';
interface ProxyObject {
    proxyType?: ProxyTypes;
    proxyAutoconfigUrl?: string;
    ftpProxy?: string;
    ftpProxyPort?: number;
    httpProxy?: string;
    httpProxyPort?: number;
    sslProxy?: string;
    sslProxyPort?: number;
    socksProxy?: string;
    socksProxyPort?: number;
    socksVersion?: string;
    socksUsername?: string;
    socksPassword?: string;
}
type Timeouts = Record<'script' | 'pageLoad' | 'implicit', number>;
interface StandardCapabilities {
    /**
     * Identifies the user agent.
     */
    browserName?: string;
    /**
     * Identifies the version of the user agent.
     */
    browserVersion?: string;
    /**
     * Identifies the operating system of the endpoint node.
     */
    platformName?: string;
    /**
     * Indicates whether untrusted and self-signed TLS certificates are implicitly trusted on navigation for the duration of the session.
     */
    acceptInsecureCerts?: boolean;
    /**
     * Defines the current session’s page load strategy.
     */
    pageLoadStrategy?: PageLoadingStrategy;
    /**
     * Defines the current session’s proxy configuration.
     */
    proxy?: ProxyObject;
    /**
     * Indicates whether the remote end supports all of the resizing and repositioning commands.
     */
    setWindowRect?: boolean;
    /**
     * Describes the timeouts imposed on certain session operations.
     */
    timeouts?: Timeouts;
    /**
     * Defines the current session’s strict file interactability.
     */
    strictFileInteractability?: boolean;
    /**
     * Describes the current session’s user prompt handler. Defaults to the dismiss and notify state.
     */
    unhandledPromptBehavior?: string;
    /**
     * WebDriver clients opt in to a bidirectional connection by requesting a capability with the name "webSocketUrl" and value true.
     */
    webSocketUrl?: boolean;
}

type W3C_ARMOR_PREFIX = 'armor';
/**
 * Base capabilities as derived from {@linkcode BaseDriverCapConstraints}.
 */
type BaseCapabilities = Capabilities<BaseDriverCapConstraints>;
/**
 * Like {@linkcode NSBaseCapabilities}, except W3C-style.
 * @see {@linkcode W3CCapabilities}
 */
type BaseW3CCapabilities = W3CCapabilities<BaseDriverCapConstraints>;
/**
 * Given a {@linkcode Constraint} `C` and a type `T`, see if `inclusion`/`inclusionCaseInsensitive` is present, and create a union of its allowed literals; otherwise just use `T`.
 */
type ConstraintChoice<C extends Constraint, T> = C['inclusionCaseInsensitive'] extends T[] ? AnyCase<C['inclusionCaseInsensitive'][number]> : C['inclusion'] extends ReadonlyArray<T> ? C['inclusion'][number] : T;
/**
 * Given {@linkcode Constraint} `C`, determine the associated type of the capability.
 *
 * Notes:
 *
 * - Only `number` and `string` values can have "choices" (`inclusion`/`inclusionCaseInesnsitive`) associated with them.
 * - If `isArray` is `true`, the type is always of type `string[]`. If this is incorrect, then it will be `any[]`.
 * - There is no way to express the shape of an object if `ifObject` is `true`.
 */
type ConstraintToCapKind<C extends Constraint> = C['isString'] extends true ? ConstraintChoice<C, string> : C['isNumber'] extends true ? ConstraintChoice<C, number> : C['isBoolean'] extends true ? boolean : C['isArray'] extends true ? string[] : C['isObject'] extends true ? StringRecord : unknown;
/**
 * Given {@linkcode Constraint} `C`, determine if it is required or optional.
 *
 * In practice, _all_ capabilities are considered optional per types, but various errors might be thrown if some are not present.
 */
type ConstraintToCap<C extends Constraint> = C['presence'] extends true | {
    allowEmpty: boolean;
} ? ConstraintToCapKind<C> : ConstraintToCapKind<C> | undefined;
/**
 * Given {@linkcode StringRecord} `T` and namespace string `NS`, a type with the key names prefixed by `${NS}:` _except_ for standard capabilities.  `NS` defaults to `armor`.
 *
 * If `T` is already namespaced, well, it'll get _double_-namespaced.
 */
type CapsToNSCaps<T extends StringRecord, NS extends string = W3C_ARMOR_PREFIX> = {
    [K in keyof T as K extends keyof StandardCapabilities ? K : NamespacedString<K & string, NS>]: T[K];
};
/**
 * A namespaced string of the format `<NS>:<S>` where `NS` defaults to the value of
 * {@linkcode W3C_ARMOR_PREFIX} and `S` is a string.
 */
type NamespacedString<S extends string, NS extends string = W3C_ARMOR_PREFIX> = `${NS}:${S}`;
/**
 * Converts {@linkcode Constraint} `C` to a {@linkcode Capabilities} object.
 * @privateRemarks I would like to figure out how to simplify this type
 */
type ConstraintsToCaps<C extends Constraints> = {
    -readonly [K in keyof C]: ConstraintToCap<C[K]>;
};
/**
 * Given some constraints, return the entire set of supported capabilities it supports (including whatever is in its desired caps).
 *
 * Does not contain {@linkcode BaseCapabilities}; see {@linkcode DriverCaps}.
 */
type Capabilities<C extends Constraints> = ConstraintsToCaps<C>;
/**
 * Like {@linkcode Capabilities}, except W3C-style.
 *
 * Does not contain {@linkcode BaseCapabilities}; see {@linkcode W3CDriverCaps}.
 */
interface W3CCapabilities<C extends Constraints> {
    alwaysMatch: NSCapabilities<C>;
    firstMatch: NSCapabilities<C>[];
}
/**
 * Namespaced caps (where appropriate).
 *
 * Does not contain {@linkcode BaseCapabilities}; see {@linkcode NSDriverCaps}.
 */
type NSCapabilities<C extends Constraints, NS extends string = W3C_ARMOR_PREFIX> = Partial<CapsToNSCaps<ConstraintsToCaps<C>, NS>>;
/**
 * Capabilities for drivers extending `BaseDriver`.
 *
 * Includes {@linkcode BaseCapabilities}.
 *
 * @example
 * ```ts
 * class MyDriver extends BaseDriver<MyDriverConstraints> {
 *   async createSession (w3ccaps: W3CDriverCaps<MyDriverConstraints>, ...args: any[]) {
 *     const [
 *       sessionId: string,
 *       caps: DriverCaps<MyDriverConstraints>
 *     ] = await super.createSession(w3ccaps, ...args);
 *     // ...
 *   }
 * }
 * ```
 */
/**
 * Normalized capabilities for drivers extending `BaseDriver`.
 * Includes {@linkcode BaseCapabilities}.
 */
type DriverCaps<C extends Constraints = Constraints> = BaseCapabilities & Capabilities<C>;
/**
 * W3C-style capabilities for drivers extending `BaseDriver`.
 *
 * Includes {@linkcode BaseW3CCapabilities}.
 *
 * @example
 * ```ts
 * class MyDriver extends BaseDriver<MyDriverConstraints> {
 *   async createSession (w3ccaps: W3CDriverCaps<MyDriverConstraints>, ...args: any[]) {
 *     // ...
 *   }
 * }
 * ```
 */
type W3CDriverCaps<C extends Constraints = Constraints> = BaseW3CCapabilities & W3CCapabilities<C>;

// ==================================================================================================
// JSON Schema Draft 07
// ==================================================================================================
// https://tools.ietf.org/html/draft-handrews-json-schema-validation-01
// --------------------------------------------------------------------------------------------------

/**
 * Primitive type
 * @see https://tools.ietf.org/html/draft-handrews-json-schema-validation-01#section-6.1.1
 */
type JSONSchema7TypeName =
    | "string" //
    | "number"
    | "integer"
    | "boolean"
    | "object"
    | "array"
    | "null";

/**
 * Primitive type
 * @see https://tools.ietf.org/html/draft-handrews-json-schema-validation-01#section-6.1.1
 */
type JSONSchema7Type =
    | string //
    | number
    | boolean
    | JSONSchema7Object
    | JSONSchema7Array
    | null;

// Workaround for infinite type recursion
interface JSONSchema7Object {
    [key: string]: JSONSchema7Type;
}

// Workaround for infinite type recursion
// https://github.com/Microsoft/TypeScript/issues/3496#issuecomment-128553540
interface JSONSchema7Array extends Array<JSONSchema7Type> {}

/**
 * Meta schema
 *
 * Recommended values:
 * - 'http://json-schema.org/schema#'
 * - 'http://json-schema.org/hyper-schema#'
 * - 'http://json-schema.org/draft-07/schema#'
 * - 'http://json-schema.org/draft-07/hyper-schema#'
 *
 * @see https://tools.ietf.org/html/draft-handrews-json-schema-validation-01#section-5
 */
type JSONSchema7Version = string;

/**
 * JSON Schema v7
 * @see https://tools.ietf.org/html/draft-handrews-json-schema-validation-01
 */
type JSONSchema7Definition = JSONSchema7 | boolean;
interface JSONSchema7 {
    $id?: string | undefined;
    $ref?: string | undefined;
    $schema?: JSONSchema7Version | undefined;
    $comment?: string | undefined;

    /**
     * @see https://datatracker.ietf.org/doc/html/draft-bhutton-json-schema-00#section-8.2.4
     * @see https://datatracker.ietf.org/doc/html/draft-bhutton-json-schema-validation-00#appendix-A
     */
    $defs?: {
        [key: string]: JSONSchema7Definition;
    } | undefined;

    /**
     * @see https://tools.ietf.org/html/draft-handrews-json-schema-validation-01#section-6.1
     */
    type?: JSONSchema7TypeName | JSONSchema7TypeName[] | undefined;
    enum?: JSONSchema7Type[] | undefined;
    const?: JSONSchema7Type | undefined;

    /**
     * @see https://tools.ietf.org/html/draft-handrews-json-schema-validation-01#section-6.2
     */
    multipleOf?: number | undefined;
    maximum?: number | undefined;
    exclusiveMaximum?: number | undefined;
    minimum?: number | undefined;
    exclusiveMinimum?: number | undefined;

    /**
     * @see https://tools.ietf.org/html/draft-handrews-json-schema-validation-01#section-6.3
     */
    maxLength?: number | undefined;
    minLength?: number | undefined;
    pattern?: string | undefined;

    /**
     * @see https://tools.ietf.org/html/draft-handrews-json-schema-validation-01#section-6.4
     */
    items?: JSONSchema7Definition | JSONSchema7Definition[] | undefined;
    additionalItems?: JSONSchema7Definition | undefined;
    maxItems?: number | undefined;
    minItems?: number | undefined;
    uniqueItems?: boolean | undefined;
    contains?: JSONSchema7Definition | undefined;

    /**
     * @see https://tools.ietf.org/html/draft-handrews-json-schema-validation-01#section-6.5
     */
    maxProperties?: number | undefined;
    minProperties?: number | undefined;
    required?: string[] | undefined;
    properties?: {
        [key: string]: JSONSchema7Definition;
    } | undefined;
    patternProperties?: {
        [key: string]: JSONSchema7Definition;
    } | undefined;
    additionalProperties?: JSONSchema7Definition | undefined;
    dependencies?: {
        [key: string]: JSONSchema7Definition | string[];
    } | undefined;
    propertyNames?: JSONSchema7Definition | undefined;

    /**
     * @see https://tools.ietf.org/html/draft-handrews-json-schema-validation-01#section-6.6
     */
    if?: JSONSchema7Definition | undefined;
    then?: JSONSchema7Definition | undefined;
    else?: JSONSchema7Definition | undefined;

    /**
     * @see https://tools.ietf.org/html/draft-handrews-json-schema-validation-01#section-6.7
     */
    allOf?: JSONSchema7Definition[] | undefined;
    anyOf?: JSONSchema7Definition[] | undefined;
    oneOf?: JSONSchema7Definition[] | undefined;
    not?: JSONSchema7Definition | undefined;

    /**
     * @see https://tools.ietf.org/html/draft-handrews-json-schema-validation-01#section-7
     */
    format?: string | undefined;

    /**
     * @see https://tools.ietf.org/html/draft-handrews-json-schema-validation-01#section-8
     */
    contentMediaType?: string | undefined;
    contentEncoding?: string | undefined;

    /**
     * @see https://tools.ietf.org/html/draft-handrews-json-schema-validation-01#section-9
     */
    definitions?: {
        [key: string]: JSONSchema7Definition;
    } | undefined;

    /**
     * @see https://tools.ietf.org/html/draft-handrews-json-schema-validation-01#section-10
     */
    title?: string | undefined;
    description?: string | undefined;
    default?: JSONSchema7Type | undefined;
    readOnly?: boolean | undefined;
    writeOnly?: boolean | undefined;
    examples?: JSONSchema7Type | undefined;
}

declare namespace ArmorConfigJsonSchema {
    export let $schema: "http://json-schema.org/draft-07/schema";
    export let additionalProperties: false;
    export let description: "A schema for Armor configuration files";
    export namespace properties {
        export namespace $schema_1 {
            let description_1: "The JSON schema for this file";
            export { description_1 as description };
            let _default: "https://raw.githubusercontent.com/armor/armor/master/packages/schema/lib/armor-config.schema.json";
            export { _default as default };
            export let type: "string";
            export let format: "uri";
        }
        export { $schema_1 as $schema };
        export namespace server {
            let additionalProperties_1: false;
            export { additionalProperties_1 as additionalProperties };
            let description_2: "Configuration when running Armor as a server";
            export { description_2 as description };
            let properties_1: {
                readonly address: {
                    readonly armorCliAliases: readonly ["a"];
                    readonly default: "0.0.0.0";
                    readonly description: "IPv4/IPv6 address or a hostname to listen on";
                    readonly title: "address config";
                    readonly type: "string";
                    readonly anyOf: readonly [{
                        readonly type: "string";
                        readonly format: "hostname";
                    }, {
                        readonly type: "string";
                        readonly format: "ipv6";
                    }];
                };
                readonly 'allow-cors': {
                    readonly description: "Whether the Armor server should allow web browser connections from any host";
                    readonly title: "allow-cors config";
                    readonly type: "boolean";
                    readonly default: false;
                };
                readonly 'allow-insecure': {
                    readonly armorCliTransformer: "csv";
                    readonly default: readonly [];
                    readonly description: "Set which insecure features are allowed to run in this server's sessions. Features are defined on a driver level; see documentation for more details. Note that features defined via \"deny-insecure\" will be disabled, even if also listed here. If string, a path to a text file containing policy or a comma-delimited list.";
                    readonly items: {
                        readonly type: "string";
                    };
                    readonly title: "allow-insecure config";
                    readonly type: "array";
                    readonly uniqueItems: true;
                };
                readonly 'base-path': {
                    readonly armorCliAliases: readonly ["pa"];
                    readonly default: "";
                    readonly description: "Base path to use as the prefix for all webdriver routes running on the server";
                    readonly title: "base-path config";
                    readonly type: "string";
                };
                readonly 'callback-address': {
                    readonly armorCliAliases: readonly ["ca"];
                    readonly description: "Callback IP address (default: same as \"address\")";
                    readonly title: "callback-address config";
                    readonly type: "string";
                };
                readonly 'callback-port': {
                    readonly armorCliAliases: readonly ["cp"];
                    readonly default: 4723;
                    readonly description: "Callback port (default: same as \"port\")";
                    readonly maximum: 65535;
                    readonly minimum: 1;
                    readonly title: "callback-port config";
                    readonly type: "integer";
                };
                readonly 'debug-log-spacing': {
                    readonly default: false;
                    readonly description: "Add exaggerated spacing in logs to help with visual inspection";
                    readonly title: "debug-log-spacing config";
                    readonly type: "boolean";
                };
                readonly 'default-capabilities': {
                    readonly $comment: "TODO";
                    readonly armorCliAliases: readonly ["dc"];
                    readonly description: "Set the default desired capabilities, which will be set on each session unless overridden by received capabilities. If a string, a path to a JSON file containing the capabilities, or raw JSON.";
                    readonly title: "default-capabilities config";
                    readonly type: "object";
                };
                readonly 'deny-insecure': {
                    readonly $comment: "Allowed values are defined by drivers";
                    readonly armorCliTransformer: "csv";
                    readonly default: readonly [];
                    readonly description: "Set which insecure features are not allowed to run in this server's sessions. Features are defined on a driver level; see documentation for more details. Features listed here will not be enabled even if also listed in \"allow-insecure\", and even if \"relaxed-security\" is enabled. If string, a path to a text file containing policy or a comma-delimited list.";
                    readonly items: {
                        readonly type: "string";
                    };
                    readonly title: "deny-insecure config";
                    readonly type: "array";
                    readonly uniqueItems: true;
                };
                readonly driver: {
                    readonly description: "Driver-specific configuration. Keys should correspond to driver package names";
                    readonly properties: Record<string, JSONSchema7>;
                    readonly title: "driver config";
                    readonly type: "object";
                };
                readonly 'keep-alive-timeout': {
                    readonly armorCliAliases: readonly ["ka"];
                    readonly default: 600;
                    readonly description: "Number of seconds the Armor server should apply as both the keep-alive timeout and the connection timeout for all requests. A value of 0 disables the timeout.";
                    readonly minimum: 0;
                    readonly title: "keep-alive-timeout config";
                    readonly type: "integer";
                };
                readonly 'local-timezone': {
                    readonly default: false;
                    readonly description: "Use local timezone for timestamps";
                    readonly title: "local-timezone config";
                    readonly type: "boolean";
                };
                readonly log: {
                    readonly armorCliAliases: readonly ["g"];
                    readonly armorCliDest: "logFile";
                    readonly description: "Also send log output to this file";
                    readonly title: "log config";
                    readonly type: "string";
                };
                readonly 'log-filters': {
                    readonly description: "One or more log filtering rules";
                    readonly title: "log-filters config";
                    readonly type: "array";
                    readonly items: {
                        readonly $ref: "#/$defs/logFilter";
                    };
                    readonly armorCliTransformer: "json";
                };
                readonly 'log-level': {
                    readonly armorCliDest: "loglevel";
                    readonly default: "debug";
                    readonly description: "Log level (console[:file])";
                    readonly enum: readonly ["info", "info:debug", "info:info", "info:warn", "info:error", "warn", "warn:debug", "warn:info", "warn:warn", "warn:error", "error", "error:debug", "error:info", "error:warn", "error:error", "debug", "debug:debug", "debug:info", "debug:warn", "debug:error"];
                    readonly title: "log-level config";
                    readonly type: "string";
                };
                readonly 'log-no-colors': {
                    readonly default: false;
                    readonly description: "Do not use color in console output";
                    readonly title: "log-no-colors config";
                    readonly type: "boolean";
                };
                readonly 'log-timestamp': {
                    readonly default: false;
                    readonly description: "Show timestamps in console output";
                    readonly title: "log-timestamp config";
                    readonly type: "boolean";
                };
                readonly 'long-stacktrace': {
                    readonly default: false;
                    readonly description: "Add long stack traces to log entries. Recommended for debugging only.";
                    readonly title: "long-stacktrace config";
                    readonly type: "boolean";
                };
                readonly 'no-perms-check': {
                    readonly default: false;
                    readonly description: "Do not check that needed files are readable and/or writable";
                    readonly title: "no-perms-check config";
                    readonly type: "boolean";
                };
                readonly nodeconfig: {
                    readonly $comment: "Selenium Grid 3 is unmaintained and Selenium Grid 4 no longer supports this file.";
                    readonly description: "Path to configuration JSON file to register Armor as a node with Selenium Grid 3; otherwise the configuration itself";
                    readonly title: "nodeconfig config";
                    readonly type: "object";
                };
                readonly plugin: {
                    readonly description: "Plugin-specific configuration. Keys should correspond to plugin package names";
                    readonly properties: Record<string, JSONSchema7>;
                    readonly title: "plugin config";
                    readonly type: "object";
                };
                readonly port: {
                    readonly armorCliAliases: readonly ["p"];
                    readonly default: 4723;
                    readonly description: "Port to listen on";
                    readonly maximum: 65535;
                    readonly minimum: 1;
                    readonly title: "port config";
                    readonly type: "integer";
                };
                readonly 'relaxed-security': {
                    readonly default: false;
                    readonly description: "Disable additional security checks, so it is possible to use some advanced features, provided by drivers supporting this option. Only enable it if all the clients are in the trusted network and it's not the case if a client could potentially break out of the session sandbox. Specific features can be overridden by using \"deny-insecure\"";
                    readonly title: "relaxed-security config";
                    readonly type: "boolean";
                    readonly armorCliDest: "relaxedSecurityEnabled";
                };
                readonly 'session-override': {
                    readonly default: false;
                    readonly description: "Enables session override (clobbering)";
                    readonly title: "session-override config";
                    readonly type: "boolean";
                };
                readonly 'strict-caps': {
                    readonly default: false;
                    readonly description: "Cause sessions to fail if desired caps are sent in that Armor does not recognize as valid for the selected device";
                    readonly title: "strict-caps config";
                    readonly type: "boolean";
                };
                readonly tmp: {
                    readonly armorCliDest: "tmpDir";
                    readonly description: "Absolute path to directory Armor can use to manage temp files. Defaults to C:\\Windows\\Temp on Windows and /tmp otherwise.";
                    readonly title: "tmp config";
                    readonly type: "string";
                };
                readonly 'trace-dir': {
                    readonly description: "Absolute path to directory Armor can use to save iOS instrument traces; defaults to <tmp>/armor-instruments";
                    readonly title: "trace-dir config";
                    readonly type: "string";
                };
                readonly 'use-drivers': {
                    readonly armorCliDescription: "A list of drivers to activate. Can be a comma-delimited string or path to CSV file. By default, all installed drivers will be activated. Windows environments may require wrapping the comma-delimited string with quotes to escape the comma.";
                    readonly default: readonly [];
                    readonly description: "A list of drivers to activate. By default, all installed drivers will be activated.";
                    readonly items: {
                        readonly type: "string";
                    };
                    readonly title: "use-drivers config";
                    readonly type: "array";
                    readonly uniqueItems: true;
                };
                readonly 'use-plugins': {
                    readonly armorCliDescription: "A list of plugins to activate. Can be a comma-delimited string, path to CSV file, or the string \"all\" to use all installed plugins. Windows environments may require wrapping the comma-delimited string with quotes to escape the comma.";
                    readonly default: readonly [];
                    readonly description: "A list of plugins to activate. To activate all plugins, the value should be an array with a single item \"all\".";
                    readonly items: {
                        readonly type: "string";
                    };
                    readonly title: "use-plugins config";
                    readonly type: "array";
                    readonly uniqueItems: true;
                };
                readonly webhook: {
                    readonly $comment: "This should probably use a uri-template format to restrict the protocol to http/https";
                    readonly armorCliAliases: readonly ["G"];
                    readonly description: "Also send log output to this http listener";
                    readonly format: "uri";
                    readonly title: "webhook config";
                    readonly type: "string";
                };
                readonly 'ssl-cert-path': {
                    readonly description: "Full path to the .cert file if TLS is used. Must be provided together with \"ssl-key-path\"";
                    readonly title: ".cert file path";
                    readonly armorCliDest: "sslCertificatePath";
                    readonly type: "string";
                };
                readonly 'ssl-key-path': {
                    readonly description: "Full path to the .key file if TLS is used. Must be provided together with \"ssl-cert-path\"";
                    readonly title: ".key file path";
                    readonly armorCliDest: "sslKeyPath";
                    readonly type: "string";
                };
            };
            export { properties_1 as properties };
            export let title: "server config";
            let type_1: "object";
            export { type_1 as type };
        }
    }
    let title_1: "Armor Configuration";
    export { title_1 as title };
    let type_2: "object";
    export { type_2 as type };
    export namespace $defs {
        namespace logFilterText {
            let type_3: "object";
            export { type_3 as type };
            let description_3: "Log filter with plain text";
            export { description_3 as description };
            export namespace properties_2 {
                namespace text {
                    let description_4: "Text to match";
                    export { description_4 as description };
                    let type_4: "string";
                    export { type_4 as type };
                }
            }
            export { properties_2 as properties };
            export let required: readonly ["text"];
            export namespace not {
                let required_1: readonly ["pattern"];
                export { required_1 as required };
            }
        }
        namespace logFilterRegex {
            let type_5: "object";
            export { type_5 as type };
            let description_5: "Log filter with regular expression";
            export { description_5 as description };
            export namespace properties_3 {
                namespace pattern {
                    let description_6: "Regex pattern to match";
                    export { description_6 as description };
                    let type_6: "string";
                    export { type_6 as type };
                    let format_1: "regex";
                    export { format_1 as format };
                }
            }
            export { properties_3 as properties };
            let required_2: readonly ["pattern"];
            export { required_2 as required };
            export namespace not_1 {
                let required_3: readonly ["text"];
                export { required_3 as required };
            }
            export { not_1 as not };
        }
        namespace logFilter {
            let type_7: "object";
            export { type_7 as type };
            let description_7: "Log filtering rule";
            export { description_7 as description };
            export let allOf: readonly [{
                readonly type: "object";
                readonly properties: {
                    readonly replacer: {
                        readonly description: "Replacement string for matched text";
                        readonly type: "string";
                        readonly default: "**SECURE**";
                    };
                    readonly flags: {
                        readonly description: "Matching flags; see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_Expressions#advanced_searching_with_flags";
                        readonly type: "string";
                        readonly pattern: "^[igmsduy](,[igmsduy])*$";
                    };
                };
            }, {
                readonly anyOf: readonly [{
                    readonly $ref: "#/$defs/logFilterText";
                }, {
                    readonly $ref: "#/$defs/logFilterRegex";
                }];
            }];
        }
    }
}

/**
 * This file was automatically generated by json-schema-to-typescript.
 * DO NOT MODIFY IT BY HAND. Instead, modify the source JSONSchema file,
 * and run json-schema-to-typescript to regenerate this file.
 */
/**
 * IPv4/IPv6 address or a hostname to listen on
 */
type AddressConfig = AddressConfig1 & AddressConfig2;
type AddressConfig1 = string;
type AddressConfig2 = string;
/**
 * Whether the Armor server should allow web browser connections from any host
 */
type AllowCorsConfig = boolean;
/**
 * Set which insecure features are allowed to run in this server's sessions. Features are defined on a driver level; see documentation for more details. Note that features defined via "deny-insecure" will be disabled, even if also listed here. If string, a path to a text file containing policy or a comma-delimited list.
 */
type AllowInsecureConfig = string[];
/**
 * Base path to use as the prefix for all webdriver routes running on the server
 */
type BasePathConfig = string;
/**
 * Callback IP address (default: same as "address")
 */
type CallbackAddressConfig = string;
/**
 * Callback port (default: same as "port")
 */
type CallbackPortConfig = number;
/**
 * Add exaggerated spacing in logs to help with visual inspection
 */
type DebugLogSpacingConfig = boolean;
/**
 * Set which insecure features are not allowed to run in this server's sessions. Features are defined on a driver level; see documentation for more details. Features listed here will not be enabled even if also listed in "allow-insecure", and even if "relaxed-security" is enabled. If string, a path to a text file containing policy or a comma-delimited list.
 */
type DenyInsecureConfig = string[];
/**
 * Number of seconds the Armor server should apply as both the keep-alive timeout and the connection timeout for all requests. A value of 0 disables the timeout.
 */
type KeepAliveTimeoutConfig = number;
/**
 * Use local timezone for timestamps
 */
type LocalTimezoneConfig = boolean;
/**
 * Also send log output to this file
 */
type LogConfig = string;
/**
 * Log filtering rule
 */
type LogFilter = {
    /**
     * Replacement string for matched text
     */
    replacer?: string;
    /**
     * Matching flags; see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_Expressions#advanced_searching_with_flags
     */
    flags?: string;
    [k: string]: unknown;
} & (LogFilterText | LogFilterRegex);
/**
 * One or more log filtering rules
 */
type LogFiltersConfig = LogFilter[];
/**
 * Log level (console[:file])
 */
type LogLevelConfig = "info" | "info:debug" | "info:info" | "info:warn" | "info:error" | "warn" | "warn:debug" | "warn:info" | "warn:warn" | "warn:error" | "error" | "error:debug" | "error:info" | "error:warn" | "error:error" | "debug" | "debug:debug" | "debug:info" | "debug:warn" | "debug:error";
/**
 * Do not use color in console output
 */
type LogNoColorsConfig = boolean;
/**
 * Show timestamps in console output
 */
type LogTimestampConfig = boolean;
/**
 * Add long stack traces to log entries. Recommended for debugging only.
 */
type LongStacktraceConfig = boolean;
/**
 * Do not check that needed files are readable and/or writable
 */
type NoPermsCheckConfig = boolean;
/**
 * Port to listen on
 */
type PortConfig = number;
/**
 * Disable additional security checks, so it is possible to use some advanced features, provided by drivers supporting this option. Only enable it if all the clients are in the trusted network and it's not the case if a client could potentially break out of the session sandbox. Specific features can be overridden by using "deny-insecure"
 */
type RelaxedSecurityConfig = boolean;
/**
 * Enables session override (clobbering)
 */
type SessionOverrideConfig = boolean;
/**
 * Cause sessions to fail if desired caps are sent in that Armor does not recognize as valid for the selected device
 */
type StrictCapsConfig = boolean;
/**
 * Absolute path to directory Armor can use to manage temp files. Defaults to C:\Windows\Temp on Windows and /tmp otherwise.
 */
type TmpConfig = string;
/**
 * Absolute path to directory Armor can use to save iOS instrument traces; defaults to <tmp>/armor-instruments
 */
type TraceDirConfig = string;
/**
 * A list of drivers to activate. By default, all installed drivers will be activated.
 */
type UseDriversConfig = string[];
/**
 * A list of plugins to activate. To activate all plugins, the value should be an array with a single item "all".
 */
type UsePluginsConfig = string[];
/**
 * Also send log output to this http listener
 */
type WebhookConfig = string;
/**
 * Full path to the .cert file if TLS is used. Must be provided together with "ssl-key-path"
 */
type CertFilePath = string;
/**
 * Full path to the .key file if TLS is used. Must be provided together with "ssl-cert-path"
 */
type KeyFilePath = string;
/**
 * Configuration when running Armor as a server
 */
interface ServerConfig {
    address?: AddressConfig;
    "allow-cors"?: AllowCorsConfig;
    "allow-insecure"?: AllowInsecureConfig;
    "base-path"?: BasePathConfig;
    "callback-address"?: CallbackAddressConfig;
    "callback-port"?: CallbackPortConfig;
    "debug-log-spacing"?: DebugLogSpacingConfig;
    "default-capabilities"?: DefaultCapabilitiesConfig;
    "deny-insecure"?: DenyInsecureConfig;
    driver?: DriverConfig;
    "keep-alive-timeout"?: KeepAliveTimeoutConfig;
    "local-timezone"?: LocalTimezoneConfig;
    log?: LogConfig;
    "log-filters"?: LogFiltersConfig;
    "log-level"?: LogLevelConfig;
    "log-no-colors"?: LogNoColorsConfig;
    "log-timestamp"?: LogTimestampConfig;
    "long-stacktrace"?: LongStacktraceConfig;
    "no-perms-check"?: NoPermsCheckConfig;
    nodeconfig?: NodeconfigConfig;
    plugin?: PluginConfig;
    port?: PortConfig;
    "relaxed-security"?: RelaxedSecurityConfig;
    "session-override"?: SessionOverrideConfig;
    "strict-caps"?: StrictCapsConfig;
    tmp?: TmpConfig;
    "trace-dir"?: TraceDirConfig;
    "use-drivers"?: UseDriversConfig;
    "use-plugins"?: UsePluginsConfig;
    webhook?: WebhookConfig;
    "ssl-cert-path"?: CertFilePath;
    "ssl-key-path"?: KeyFilePath;
}
/**
 * Set the default desired capabilities, which will be set on each session unless overridden by received capabilities. If a string, a path to a JSON file containing the capabilities, or raw JSON.
 */
interface DefaultCapabilitiesConfig {
    [k: string]: unknown;
}
/**
 * Driver-specific configuration. Keys should correspond to driver package names
 */
interface DriverConfig {
    [k: string]: unknown;
}
/**
 * Log filter with plain text
 */
interface LogFilterText {
    /**
     * Text to match
     */
    text: string;
    [k: string]: unknown;
}
/**
 * Log filter with regular expression
 */
interface LogFilterRegex {
    /**
     * Regex pattern to match
     */
    pattern: string;
    [k: string]: unknown;
}
/**
 * Path to configuration JSON file to register Armor as a node with Selenium Grid 3; otherwise the configuration itself
 */
interface NodeconfigConfig {
    [k: string]: unknown;
}
/**
 * Plugin-specific configuration. Keys should correspond to plugin package names
 */
interface PluginConfig {
    [k: string]: unknown;
}

/**
 * Derive the "constant" type of the server properties from the schema.
 */
type ArmorServerJsonSchema = (typeof ArmorConfigJsonSchema)['properties']['server']['properties'];
/**
 * This type associates the types generated from the schema ({@linkcode ArmorConfiguration})
 * with the schema itself (beginning with the `server` prop).
 */
type ServerConfigMapping = Associated<ServerConfig, ArmorServerJsonSchema>;
/**
 * This type checks if `armorCliDest` is present in the object via
 * {@linkcode WithDest}, and uses the _value_ of that property for the key name;
 * otherwise uses the camel-cased value of the key name.
 */
type SetKeyForProp<Prop extends keyof ServerConfigMapping> = ArmorServerJsonSchema[Prop] extends WithDest ? ArmorServerJsonSchema[Prop]['armorCliDest'] : KebabToCamel<Prop>;
/**
 * Checks for the existence of default values, and ensures those properties will
 * always be defined (eliminates `| undefined` from the type).
 * If no default value, just a type.
 */
type KeyOrDefaultForProp<Prop extends keyof ServerConfigMapping> = ArmorServerJsonSchema[Prop] extends WithDefault ? NonNullable<ServerConfig[Prop]> : ServerConfig[Prop];
/**
 * The final shape of the parsed CLI arguments.
 *
 * These will be camel-cased unless overridden by `armorCliDest` field in schema(s).
 */
type ServerArgs = {
    [Prop in keyof ServerConfigMapping as SetKeyForProp<Prop>]: KeyOrDefaultForProp<Prop>;
};
/**
 * Certain properties have an `armorCliDest` prop, which affects the shape of
 * `ParsedArgs`. This type helps recognize these properties.
 *
 * See `armor/lib/schema/keywords` for definition of `armorCliDest`.
 */
interface WithDest {
    armorCliDest: string;
}
/**
 * Some properties have a `default` prop, which means practically they will not
 * be `undefined` upon parsing.
 *
 * We use this to ensure that the `ParsedArgs` makes guarantees
 * about the presence of properties.
 */
interface WithDefault<T = any> {
    default: T;
}

/**
 * An object of HTTP headers.
 */
type HTTPHeaders = Record<string, string | string[] | number | boolean | null>;
/**
 * Possible HTTP methods, as stolen from `axios`.
 *
 * @see https://npm.im/axios
 */
type HTTPMethod = 'get' | 'GET' | 'delete' | 'DELETE' | 'head' | 'HEAD' | 'options' | 'OPTIONS' | 'post' | 'POST' | 'put' | 'PUT' | 'patch' | 'PATCH' | 'purge' | 'PURGE' | 'link' | 'LINK' | 'unlink' | 'UNLINK';

/**
 * A log prefix for {@linkcode ArmorLogger}
 *
 * If a function, the function will return the prefix.  Log messages will be prefixed with this value.
 */
type ArmorLoggerPrefix$1 = string | (() => string);
/**
 * Possible "log levels" for {@linkcode ArmorLogger}.
 *
 * Extracted from `npmlog`.
 */
type ArmorLoggerLevel$1 = 'silly' | 'verbose' | 'debug' | 'info' | 'http' | 'warn' | 'error';
/**
 * Describes the `npmlog`-based internal logger.
 *
 * @see https://npm.im/npmlog
 */
interface ArmorLogger$1 {
    /**
     * Returns the underlying `npmlog` {@link Logger}.
     */
    unwrap(): Logger;
    level: ArmorLoggerLevel$1;
    levels: ArmorLoggerLevel$1[];
    /**
     * Log prefix, if applicable.
     */
    prefix?: ArmorLoggerPrefix$1;
    debug(...args: any[]): void;
    info(...args: any[]): void;
    warn(...args: any[]): void;
    error(...args: any[]): void;
    verbose(...args: any[]): void;
    silly(...args: any[]): void;
    http(...args: any[]): void;
    errorAndThrow(...args: any[]): never;
}

/**
 * Armor's slightly-modified {@linkcode HTTPServer http.Server}.
 */
type ArmorServer = Omit<Server, 'close'> & ArmorServerExtension;
interface ArmorServerExtension {
    close(): Promise<void>;
    /**
     * Adds websocket handler to an {@linkcode ArmorServer}.
     * @param handlerPathname - Web socket endpoint path starting with a single slash character. It is recommended to always prepend `/ws` to all web socket pathnames.
     * @param handlerServer - WebSocket server instance. See https://github.com/websockets/ws/pull/885 for more details on how to configure the handler properly.
     */
    addWebSocketHandler(this: ArmorServer, handlerPathname: string, handlerServer: Server$1): Promise<void>;
    /**
     * Removes existing WebSocket handler from the server instance.
     *
     * The call is ignored if the given `handlerPathname` handler is not present in the handlers list.
     * @param handlerPathname - WebSocket endpoint path
     * @returns `true` if the `handlerPathname` was found and deleted; `false` otherwise.
     */
    removeWebSocketHandler(this: ArmorServer, handlerPathname: string): Promise<boolean>;
    /**
     * Removes all existing WebSocket handlers from the server instance.
     * @returns `true` if at least one handler was deleted; `false` otherwise.
     */
    removeAllWebSocketHandlers(this: ArmorServer): Promise<boolean>;
    /**
     * Returns web socket handlers registered for the given server
     * instance.
     * @param keysFilter - Only include pathnames with given value if set. All pairs will be included by default.
     * @returns Pathnames to WS server instances mapping matching the search criteria, if any found.
     */
    getWebSocketHandlers(this: ArmorServer, keysFilter?: string | null): Promise<Record<string, Server$1>>;
    webSocketsMapping: Record<string, Server$1>;
}

/**
 * Interface implemented by the `DeviceSettings` class in `@armor/base-driver`
 */
interface IDeviceSettings<T extends StringRecord> {
    update(newSettings: T): Promise<void>;
    getSettings(): T;
}
interface ITimeoutCommands {
    /**
     * Set the various timeouts associated with a session
     * @see {@link https://w3c.github.io/webdriver/#set-timeouts}
     *
     * @param type - used only for the old (JSONWP) command, the type of the timeout
     * @param ms - used only for the old (JSONWP) command, the ms for the timeout
     * @param script - the number in ms for the script timeout, used for the W3C command
     * @param pageLoad - the number in ms for the pageLoad timeout, used for the W3C command
     * @param implicit - the number in ms for the implicit wait timeout, used for the W3C command
     */
    timeouts(type: string, ms: number | string, script?: number, pageLoad?: number, implicit?: number | string): Promise<void>;
    /**
     * Set the new command timeout
     *
     * @param ms - the timeout in ms
     */
    setNewCommandTimeout(ms: number): void;
    /**
     * Set the implicit wait timeout
     *
     * @param ms - the timeout in ms
     *
     * @deprecated Use `timeouts` instead
     *
     */
    implicitWait(ms: number | string): Promise<void>;
    /**
     * A helper method (not a command) used to set the implicit wait value
     *
     * @param ms - the implicit wait in ms
     */
    setImplicitWait(ms: number): void;
    /**
     * Periodically retry an async function up until the currently set implicit wait timeout
     *
     * @param condition - the behaviour to retry until it returns truthy
     *
     * @returns The return value of the condition
     */
    implicitWaitForCondition(condition: (...args: any[]) => Promise<any>): Promise<unknown>;
    /**
     * Get the current timeouts
     * @see {@link https://w3c.github.io/webdriver/#get-timeouts}
     *
     * @returns A map of timeout names to ms values
     */
    getTimeouts(): Promise<Record<string, number>>;
    /**
     * Set the implicit wait value that was sent in via the W3C protocol
     *
     * @param ms - the timeout in ms
     */
    implicitWaitW3C(ms: number): Promise<void>;
    /**
     * Set the implicit wait value that was sent in via the JSONWP
     *
     * @param ms - the timeout in ms
     * @deprecated
     */
    implicitWaitMJSONWP(ms: number): Promise<void>;
    /**
     * Set the page load timeout value that was sent in via the W3C protocol
     *
     * @param ms - the timeout in ms
     */
    pageLoadTimeoutW3C(ms: number): Promise<void>;
    /**
     * Set the page load timeout value that was sent in via the JSONWP
     *
     * @param ms - the timeout in ms
     * @deprecated
     */
    pageLoadTimeoutMJSONWP(ms: number): Promise<void>;
    /**
     * Set the script timeout value that was sent in via the W3C protocol
     *
     * @param ms - the timeout in ms
     */
    scriptTimeoutW3C(ms: number): Promise<void>;
    /**
     * Set the script timeout value that was sent in via the JSONWP
     *
     * @param ms - the timeout in ms
     * @deprecated
     */
    scriptTimeoutMJSONWP(ms: number): Promise<void>;
    /**
     * Set Armor's new command timeout
     *
     * @param ms - the timeout in ms
     */
    newCommandTimeout(ms: number): Promise<void>;
    /**
     * Get a timeout value from a number or a string
     *
     * @param ms - the timeout value as a number or a string
     *
     * @returns The timeout as a number in ms
     */
    parseTimeoutArgument(ms: number | string): number;
}
interface IEventCommands {
    /**
     * Add a custom-named event to the Armor event log
     *
     * @param vendor - the name of the vendor or tool the event belongs to, to namespace the event
     * @param event - the name of the event itself
     */
    logCustomEvent(vendor: string, event: string): Promise<void>;
    /**
     * Get a list of events that have occurred in the current session
     *
     * @param type - filter the returned events by including one or more types
     *
     * @returns The event history for the session
     */
    getLogEvents(type?: string | string[]): Promise<EventHistory | Record<string, number>>;
}
interface IExecuteCommands {
    /**
     * Call an `Execute Method` by its name with the given arguments. This method will check that the
     * driver has registered the method matching the name, and send it the arguments.
     *
     * @param script - the name of the Execute Method
     * @param args - a singleton array containing an arguments object
     *
     * @returns The result of calling the Execute Method
     */
    executeMethod<TArgs extends readonly any[] | readonly [StringRecord<unknown>] = unknown[], TReturn = unknown>(script: string, args: TArgs): Promise<TReturn>;
}
interface MultiSessionData<C extends Constraints = Constraints> {
    id: string;
    capabilities: DriverCaps<C>;
}
/**
 * Data returned by {@linkcode ISessionCommands.getSession}.
 *
 * @typeParam C - The driver's constraints
 * @typeParam T - Any extra data the driver stuffs in here
 * @privateRemarks The content of this object looks implementation-specific and in practice is not well-defined.  It's _possible_ to fully type this in the future.
 */
type SingularSessionData<C extends Constraints = Constraints, T extends StringRecord = StringRecord> = DriverCaps<C> & {
    events?: EventHistory;
    error?: string;
} & T;
interface IFindCommands {
    /**
     * Find a UI element given a locator strategy and a selector, erroring if it can't be found
     * @see {@link https://w3c.github.io/webdriver/#find-element}
     *
     * @param strategy - the locator strategy
     * @param selector - the selector to combine with the strategy to find the specific element
     *
     * @returns The element object encoding the element id which can be used in element-related
     * commands
     */
    findElement(strategy: string, selector: string): Promise<Element>;
    /**
     * Find a a list of all UI elements matching a given a locator strategy and a selector
     * @see {@link https://w3c.github.io/webdriver/#find-elements}
     *
     * @param strategy - the locator strategy
     * @param selector - the selector to combine with the strategy to find the specific elements
     *
     * @returns A possibly-empty list of element objects
     */
    findElements(strategy: string, selector: string): Promise<Element[]>;
    /**
     * Find a UI element given a locator strategy and a selector, erroring if it can't be found. Only
     * look for elements among the set of descendants of a given element
     * @see {@link https://w3c.github.io/webdriver/#find-element-from-element}
     *
     * @param strategy - the locator strategy
     * @param selector - the selector to combine with the strategy to find the specific element
     * @param elementId - the id of the element to use as the search basis
     *
     * @returns The element object encoding the element id which can be used in element-related
     * commands
     */
    findElementFromElement(strategy: string, selector: string, elementId: string): Promise<Element>;
    /**
     * Find a a list of all UI elements matching a given a locator strategy and a selector. Only
     * look for elements among the set of descendants of a given element
     * @see {@link https://w3c.github.io/webdriver/#find-elements-from-element}
     *
     * @param strategy - the locator strategy
     * @param selector - the selector to combine with the strategy to find the specific elements
     * @param elementId - the id of the element to use as the search basis
     *
     * @returns A possibly-empty list of element objects
     */
    findElementsFromElement(strategy: string, selector: string, elementId: string): Promise<Element[]>;
    /**
     * Find elements from selectors
     * @param strategy - the locator strategy
     * @param selector - the selector to combine with the strategy to find the specific elements
     *
     * @returns A possibly-empty list of element objects
     */
    findElementBySelectors(strategy: string, selector: string[]): Promise<ElementFind>;
    /**
     * Find an element from a shadow root
     * @see {@link https://w3c.github.io/webdriver/#find-element-from-shadow-root}
     * @param strategy - the locator strategy
     * @param selector - the selector to combine with the strategy to find the specific elements
     * @param shadowId - the id of the element to use as the search basis
     *
     * @returns The element inside the shadow root matching the selector
     */
    findElementFromShadowRoot?(strategy: string, selector: string, shadowId: string): Promise<Element>;
    /**
     * Find elements from a shadow root
     * @see {@link https://w3c.github.io/webdriver/#find-element-from-shadow-root}
     * @param strategy - the locator strategy
     * @param selector - the selector to combine with the strategy to find the specific elements
     * @param shadowId - the id of the element to use as the search basis
     *
     * @returns A possibly empty list of elements inside the shadow root matching the selector
     */
    findElementsFromShadowRoot?(strategy: string, selector: string, shadowId: string): Promise<Element[]>;
    /**
     * A helper method that returns one or more UI elements based on the search criteria
     *
     * @param strategy - the locator strategy
     * @param selector - the selector
     * @param mult - whether or not we want to find multiple elements
     * @param context - the element to use as the search context basis if desiredCapabilities
     *
     * @returns A single element or list of elements
     */
    findElOrEls(strategy: string, selector: string, mult: true, context?: any): Promise<Element[]>;
    findElOrEls(strategy: string, selector: string, mult: false, context?: any): Promise<Element>;
    /**
     * This is a wrapper for {@linkcode findElOrEls} that validates locator strategies
     * and implements the `armor:printPageSourceOnFindFailure` capability
     *
     * @param strategy - the locator strategy
     * @param selector - the selector
     * @param mult - whether or not we want to find multiple elements
     * @param context - the element to use as the search context basis if desiredCapabilities
     *
     * @returns A single element or list of elements
     */
    findElOrElsWithProcessing(strategy: string, selector: string, mult: true, context?: any): Promise<Element[]>;
    findElOrElsWithProcessing(strategy: string, selector: string, mult: false, context?: any): Promise<Element>;
    /**
     * Get the current page/app source as HTML/XML
     * @see {@link https://w3c.github.io/webdriver/#get-page-source}
     *
     * @returns The UI hierarchy in a platform-appropriate format (e.g., HTML for a web page)
     */
    getPageSource(): Promise<string>;
}
interface ILogCommands {
    /**
     * Definition of the available log types
     */
    supportedLogTypes: Readonly<LogDefRecord>;
    /**
     * Get available log types as a list of strings
     */
    getLogTypes(): Promise<string[]>;
    /**
     * Get the log for a given log type.
     *
     * @param logType - Name/key of log type as defined in {@linkcode ILogCommands.supportedLogTypes}.
     */
    getLog(logType: string): Promise<any>;
}
/**
 * A record of {@linkcode LogDef} objects, keyed by the log type name.
 * Used in {@linkcode ILogCommands.supportedLogTypes}
 */
type LogDefRecord = Record<string, LogDef>;
/**
 * A definition of a log type
 */
interface LogDef {
    /**
     * Description of the log type.
     *
     * The only place this is used is in error messages if the client provides an invalid log type
     * via {@linkcode ILogCommands.getLog}.
     */
    description: string;
    /**
     * Returns all the log data for the given type
     *
     * This implementation *should* drain, truncate or otherwise reset the log buffer.
     */
    getter: (driver: any) => Promise<unknown> | unknown;
}
interface ISettingsCommands<T extends object = object> {
    /**
     * Update the session's settings dictionary with a new settings object
     *
     * @param settings - A key-value map of setting names to values. Settings not named in the map
     * will not have their value adjusted.
     */
    updateSettings: (settings: T) => Promise<void>;
    /**
     * Get the current settings for the session
     *
     * @returns The settings object
     */
    getSettings(): Promise<T>;
}
/**
 * @see {@linkcode ISessionHandler}
 */
type DefaultCreateSessionResult<C extends Constraints> = [
    sessionId: string,
    capabilities: DriverCaps<C>
];
/**
 * @see {@linkcode ISessionHandler}
 */
type DefaultDeleteSessionResult = void;
/**
 * An interface which creates and deletes sessions.
 */
interface ISessionHandler<C extends Constraints = Constraints, CreateResult = DefaultCreateSessionResult<C>, DeleteResult = DefaultDeleteSessionResult, SessionData extends StringRecord = StringRecord> {
    /**
     * Start a new automation session
     * @see {@link https://w3c.github.io/webdriver/#new-session}
     *
     * @privateRemarks
     * The shape of this method is strange because it used to support both JSONWP and W3C
     * capabilities. This will likely change in the future to simplify.
     *
     * @param w3cCaps1 - the new session capabilities
     * @param w3cCaps2 - another place the new session capabilities could be sent (typically left undefined)
     * @param w3cCaps3 - another place the new session capabilities could be sent (typically left undefined)
     * @param driverData - a list of DriverData objects representing other sessions running for this
     * driver on the same Armor server. This information can be used to help ensure no conflict of
     * resources
     *
     * @returns The capabilities object representing the created session
     */
    createSession(w3cCaps1: W3CDriverCaps<C>, w3cCaps2?: W3CDriverCaps<C>, w3cCaps3?: W3CDriverCaps<C>, driverData?: DriverData[]): Promise<CreateResult>;
    /**
     * Stop an automation session
     * @see {@link https://w3c.github.io/webdriver/#delete-session}
     *
     * @param sessionId - the id of the session that is to be deleted
     * @param driverData - the driver data for other currently-running sessions
     */
    deleteSession(sessionId?: string, driverData?: DriverData[]): Promise<DeleteResult | void>;
    /**
     * Get data for all sessions running on an Armor server
     *
     * @returns A list of session data objects
     */
    getSessions(): Promise<MultiSessionData[]>;
    /**
     * Get the data for the current session
     *
     * @returns A session data object
     */
    getSession(): Promise<SingularSessionData<C, SessionData>>;
}
/**
 * Custom session data for a driver.
 */
type DriverData = Record<string, unknown>;
interface Constraint {
    readonly presence?: boolean | Readonly<{
        allowEmpty: boolean;
    }>;
    readonly isString?: boolean;
    readonly isNumber?: boolean;
    readonly isBoolean?: boolean;
    readonly isObject?: boolean;
    readonly isArray?: boolean;
    readonly deprecated?: boolean;
    readonly inclusion?: Readonly<[string, ...string[]]>;
    readonly inclusionCaseInsensitive?: Readonly<[string, ...string[]]>;
}
/**
 * A collection of constraints describing the allowed capabilities for a driver.
 */
type Constraints = {
    readonly [name: string]: Constraint;
};
interface DriverHelpers {
    configureApp: (app: string, supportedAppExtensions?: string | string[] | ConfigureAppOptions) => Promise<string>;
    isPackageOrBundle: (app: string) => boolean;
    duplicateKeys: <T>(input: T, firstKey: string, secondKey: string) => T;
    parseCapsArray: (cap: string | string[]) => string[];
    generateDriverLogPrefix: <C extends Constraints>(obj: Core<C>, sessionId?: string) => string;
}
interface Rect {
    x: number;
    y: number;
    width: number;
    height: number;
}
type NewWindowType = 'tab' | 'window';
interface NewWindow {
    handle: string;
    type: NewWindowType;
}
interface Cookie {
    name: string;
    value: string;
    path?: string;
    domain?: string;
    secure?: boolean;
    httpOnly?: boolean;
    expiry?: number;
    sameSite?: 'Lax' | 'Strict';
}
interface Location$1 {
    latitude: number;
    longitude: number;
    altitude?: number;
}
interface Rotation {
    x: number;
    y: number;
    z: number;
}
interface Credential {
    credentialId: string;
    isResidentCredential: boolean;
    rpId: string;
    privateKey: string;
    userHandle?: string;
    signCount: number;
    largeBlob?: string;
}
interface EventHistory {
    commands: EventHistoryCommand[];
    [key: string]: any;
}
interface EventHistoryCommand {
    cmd: string;
    startTime: number;
    endTime: number;
}
type Protocol = 'MJSONWP' | 'W3C';
/**
 * Methods and properties which both `ArmorDriver` and `BaseDriver` inherit.
 *
 * This should not be used directly by external code.
 */
interface Core<C extends Constraints, Settings extends StringRecord = StringRecord> {
    shouldValidateCaps: boolean;
    sessionId: string | null;
    opts: DriverOpts<C>;
    initialOpts: InitialOpts;
    protocol?: Protocol;
    helpers: DriverHelpers;
    basePath: string;
    relaxedSecurityEnabled: boolean;
    allowInsecure: string[];
    denyInsecure: string[];
    newCommandTimeoutMs: number;
    implicitWaitMs: number;
    locatorStrategies: string[];
    webLocatorStrategies: string[];
    eventEmitter: EventEmitter;
    settings: IDeviceSettings<Settings>;
    log: ArmorLogger$1;
    driverData: DriverData;
    isCommandsQueueEnabled: boolean;
    eventHistory: EventHistory;
    onUnexpectedShutdown(handler: () => any): void;
    /**
     * @summary Retrieve the server's current status.
     * @description
     * Returns information about whether a remote end is in a state in which it can create new sessions and can additionally include arbitrary meta information that is specific to the implementation.
     *
     * The readiness state is represented by the ready property of the body, which is false if an attempt to create a session at the current time would fail. However, the value true does not guarantee that a New Session command will succeed.
     *
     * Implementations may optionally include additional meta information as part of the body, but the top-level properties ready and message are reserved and must not be overwritten.
     *
     * @example
     * ```js
     * // webdriver.io example
     * await driver.status();
     * ```
     *
     * ```python
     * driver.get_status()
     * ```
     *
     * ```java
     * driver.getStatus();
     * ```
     *
     * ```ruby
     * # ruby_lib example
     * remote_status
     *
     * # ruby_lib_core example
     * @driver.remote_status
     * ```
     */
    getStatus(): Promise<any>;
    sessionExists(sessionId?: string): boolean;
    isW3CProtocol(): boolean;
    isMjsonwpProtocol(): boolean;
    isFeatureEnabled(name: string): boolean;
    assertFeatureEnabled(name: string): void;
    validateLocatorStrategy(strategy: string, webContext?: boolean): void;
    proxyActive(sessionId?: string): boolean;
    getProxyAvoidList(sessionId?: string): RouteMatcher[];
    canProxy(sessionId?: string): boolean;
    proxyRouteIsAvoided(sessionId: string, method: string, url: string, body?: any): boolean;
    addManagedDriver(driver: Driver): void;
    getManagedDrivers(): Driver<Constraints>[];
    clearNewCommandTimeout(): Promise<void>;
    logEvent(eventName: string): void;
    driverForSession(sessionId: string): Core<Constraints> | null;
}
/**
 * `BaseDriver` implements this.  It contains default behavior;
 * external drivers are expected to implement {@linkcode ExternalDriver} instead.
 *
 * `C` should be the constraints of the driver.
 * `CArgs` would be the shape of `cliArgs`.
 * `Settings` is the shape of the raw device settings object (see {@linkcode IDeviceSettings})
 */
interface Driver<C extends Constraints = Constraints, CArgs extends StringRecord = StringRecord, Settings extends StringRecord = StringRecord, CreateResult = DefaultCreateSessionResult<C>, DeleteResult = DefaultDeleteSessionResult, SessionData extends StringRecord = StringRecord> extends ILogCommands, IFindCommands, ISettingsCommands<Settings>, ITimeoutCommands, IEventCommands, IExecuteCommands, ISessionHandler<C, CreateResult, DeleteResult, SessionData>, Core<C, Settings> {
    /**
     * The set of command line arguments set for this driver
     */
    cliArgs: CArgs;
    server?: ArmorServer;
    serverHost?: string;
    serverPort?: number;
    serverPath?: string;
    /**
     * Execute a driver (WebDriver-protocol) command by its name as defined in the routes file
     *
     * @param cmd - the name of the command
     * @param args - arguments to pass to the command
     *
     * @returns The result of running the command
     */
    executeCommand(cmd: string, ...args: any[]): Promise<any>;
    /**
     * Signify to any owning processes that this driver encountered an error which should cause the
     * session to terminate immediately (for example an upstream service failed)
     *
     * @param err - the Error object which is causing the shutdown
     */
    startUnexpectedShutdown(err?: Error): Promise<void>;
    /**
     * Start the timer for the New Command Timeout, which when it runs out, will stop the current
     * session
     */
    startNewCommandTimeout(): Promise<void>;
    /**
     * Reset the current session (run the delete session and create session subroutines)
     *
     * @deprecated Use explicit session management commands instead
     * @privateRemarks This is implemented by `BaseDriver` and is used within `@armor/driver-test-support`
     */
    reset(): Promise<void>;
    /**
     * The processed capabilities used to start the session represented by the current driver instance
     */
    caps?: Capabilities<C>;
    /**
     * The original capabilities used to start the session represented by the current driver instance
     */
    originalCaps?: W3CCapabilities<C>;
    /**
     * The constraints object used to validate capabilities
     */
    desiredCapConstraints: C;
    /**
     * A helper function used to assign server information to the driver instance so the driver knows
     * where the server is Running
     *
     * @param server - the server object
     * @param host - the server hostname
     * @param port - the server port
     * @param path - the server base url
     */
    assignServer?(server: ArmorServer, host: string, port: number, path: string): void;
}
/**
 * External drivers must subclass `BaseDriver`, and can implement any of these methods.
 * None of these are implemented within Armor itself.
 */
interface ExternalDriver<C extends Constraints = Constraints, Ctx = string, CArgs extends StringRecord = StringRecord, Settings extends StringRecord = StringRecord, CreateResult = DefaultCreateSessionResult<C>, DeleteResult = DefaultDeleteSessionResult, SessionData extends StringRecord = StringRecord> extends Driver<C, CArgs, Settings, CreateResult, DeleteResult, SessionData> {
    /**
     * Navigate to a given url
     * @see {@link https://w3c.github.io/webdriver/#navigate-to}
     *
     * @param url - the url
     */
    setUrl?(url: string): Promise<void>;
    /**
     * Get the current url
     * @see {@link https://w3c.github.io/webdriver/#get-current-url}
     *
     * @returns The url
     */
    getUrl?(): Promise<string>;
    /**
     * Navigate back in the page history
     * @see {@link https://w3c.github.io/webdriver/#back}
     */
    back?(): Promise<void>;
    /**
     * Navigate forward in the page history
     * @see {@link https://w3c.github.io/webdriver/#forward}
     */
    forward?(): Promise<void>;
    /**
     * Refresh the page
     * @see {@link https://w3c.github.io/webdriver/#refresh}
     */
    refresh?(): Promise<void>;
    /**
     * Get the current page title
     * @see {@link https://w3c.github.io/webdriver/#get-title}
     *
     * @returns The title
     *
     * @example
     * ```js
     * await driver.getTitle()
     * ```
     * ```py
     * driver.title
     * ```
     * ```java
     * driver.getTitle();
     * ```
     */
    title?(): Promise<string>;
    /**
     * Get the handle (id) associated with the current browser window
     * @see {@link https://w3c.github.io/webdriver/#get-window-handle}
     *
     * @returns The handle string
     */
    getWindowHandle?(): Promise<string>;
    /**
     * Close the current browsing context (window)
     * @see {@link https://w3c.github.io/webdriver/#close-window}
     *
     * @returns An array of window handles representing currently-open windows
     */
    closeWindow?(): Promise<string[]>;
    /**
     * Switch to a specified window
     * @see {@link https://w3c.github.io/webdriver/#switch-to-window}
     *
     * @param handle - the window handle of the window to make active
     */
    setWindow?(handle: string): Promise<void>;
    /**
     * Get a set of handles representing open browser windows
     * @see {@link https://w3c.github.io/webdriver/#get-window-handles}
     *
     * @returns An array of window handles representing currently-open windows
     */
    getWindowHandles?(): Promise<string[]>;
    /**
     * Create a new browser window
     * @see {@link https://w3c.github.io/webdriver/#new-window}
     *
     * @param type - a hint to the driver whether to create a "tab" or "window"
     *
     * @returns An object containing the handle of the newly created window and its type
     */
    createNewWindow?(type?: NewWindowType): Promise<NewWindow>;
    /**
     * Switch the current browsing context to a frame
     * @see {@link https://w3c.github.io/webdriver/#switch-to-frame}
     *
     * @param id - the frame id, index, or `null` (indicating the top-level context)
     */
    setFrame?(id: null | number | string): Promise<void>;
    /**
     * Set the current browsing context to the parent of the current context
     * @see {@link https://w3c.github.io/webdriver/#switch-to-parent-frame}
     */
    switchToParentFrame?(): Promise<void>;
    /**
     * Get the size and position of the current window
     * @see {@link https://w3c.github.io/webdriver/#get-window-rect}
     *
     * @returns A `Rect` JSON object with x, y, width, and height properties
     */
    getWindowRect?(): Promise<Rect>;
    /**
     * Set the current window's size and position
     * @see {@link https://w3c.github.io/webdriver/#set-window-rect}
     *
     * @param x - the screen coordinate for the new left edge of the window
     * @param y - the screen coordinate for the new top edge of the window
     * @param width - the width in pixels to resize the window to
     * @param height - the height in pixels to resize the window to
     *
     * @returns The actual `Rect` of the window after running the command
     */
    setWindowRect?(x: number, y: number, width: number, height: number): Promise<Rect>;
    /**
     * Run the window-manager specific 'maximize' operation on the current window
     * @see {@link https://w3c.github.io/webdriver/#maximize-window}
     *
     * @returns The actual `Rect` of the window after running the command
     */
    maximizeWindow?(): Promise<Rect>;
    /**
     * Run the window-manager specific 'minimize' operation on the current window
     * @see {@link https://w3c.github.io/webdriver/#minimize-window}
     *
     * @returns The actual `Rect` of the window after running the command
     */
    minimizeWindow?(): Promise<Rect>;
    /**
     * Put the current window into full screen mode
     * @see {@link https://w3c.github.io/webdriver/#fullscreen-window}
     *
     * @returns The actual `Rect` of the window after running the command
     */
    fullScreenWindow?(): Promise<Rect>;
    /**
     * Get the active element
     * @see {@link https://w3c.github.io/webdriver/#get-active-element}
     *
     * @returns The JSON object encapsulating the active element reference
     */
    active?(): Promise<Element>;
    /**
     * Get the shadow root of an element
     * @see {@link https://w3c.github.io/webdriver/#get-element-shadow-root}
     *
     * @param elementId - the id of the element to retrieve the shadow root for
     *
     * @returns The shadow root for an element, as an element
     */
    elementShadowRoot?(elementId: string): Promise<Element>;
    /**
     * Determine if the reference element is selected or not
     * @see {@link https://w3c.github.io/webdriver/#is-element-selected}
     *
     * @param elementId - the id of the element
     *
     * @returns True if the element is selected, False otherwise
     */
    elementSelected?(elementId: string): Promise<boolean>;
    /**
     * Retrieve the value of an element's attribute
     * @see {@link https://w3c.github.io/webdriver/#get-element-attribute}
     *
     * @param name - the attribute name
     * @param elementId - the id of the element
     *
     * @returns The attribute value
     */
    getAttribute?(name: string, elementId: string): Promise<string | null>;
    /**
     * Retrieve the value of a named property of an element's JS object
     * @see {@link https://w3c.github.io/webdriver/#get-element-property}
     *
     * @param name - the object property name
     * @param elementId - the id of the element
     *
     * @returns The property value
     */
    getProperty?(name: string, elementId: string): Promise<string | null>;
    /**
     * Retrieve the value of a CSS property of an element
     * @see {@link https://w3c.github.io/webdriver/#get-element-css-value}
     *
     * @param name - the CSS property name
     * @param elementId - the id of the element
     *
     * @returns The property value
     */
    getCssProperty?(name: string, elementId: string): Promise<string>;
    /**
     * Get the text of an element as rendered
     * @see {@link https://w3c.github.io/webdriver/#get-element-text}
     *
     * @param elementId - the id of the element
     *
     * @returns The text rendered for the element
     */
    getText?(elementId: string): Promise<string>;
    /**
     * Get the tag name of an element
     * @see {@link https://w3c.github.io/webdriver/#get-element-tag-name}
     *
     * @param elementId - the id of the element
     *
     * @returns The tag name
     */
    getName?(elementId: string): Promise<string>;
    /**
     * Get the dimensions and position of an element
     * @see {@link https://w3c.github.io/webdriver/#get-element-rect}
     *
     * @param elementId - the id of the element
     *
     * @returns The Rect object containing x, y, width, and height properties
     */
    getElementRect?(elementId: string): Promise<Rect>;
    /**
     * Determine whether an element is enabled
     * @see {@link https://w3c.github.io/webdriver/#is-element-enabled}
     *
     * @param elementId - the id of the element
     *
     * @returns True if the element is enabled, False otherwise
     */
    elementEnabled?(elementId: string): Promise<boolean>;
    /**
     * Get the WAI-ARIA role of an element
     * @see {@link https://w3c.github.io/webdriver/#get-computed-role}
     *
     * @param elementId - the id of the element
     *
     * @returns The role
     */
    getComputedRole?(elementId: string): Promise<string | null>;
    /**
     * Get the accessible name/label of an element
     * @see {@link https://w3c.github.io/webdriver/#get-computed-label}
     *
     * @param elementId - the id of the element
     *
     * @returns The accessible name
     */
    getComputedLabel?(elementId: string): Promise<string | null>;
    /**
     * Determine whether an element is displayed
     * @see {@link https://w3c.github.io/webdriver/#element-displayedness}
     *
     * @param elementId - the id of the element
     *
     * @returns True if any part of the element is rendered within the viewport, False otherwise
     */
    elementDisplayed?(elementId: string): Promise<boolean>;
    /**
     * Click/tap an element
     * @see {@link https://w3c.github.io/webdriver/#element-click}
     *
     * @param elementId - the id of the element
     */
    click?(elementId: string): Promise<void>;
    /**
     * Clear the text/value of an editable element
     * @see {@link https://w3c.github.io/webdriver/#element-clear}
     *
     * @param elementId - the id of the element
     */
    clear?(elementId: string): Promise<void>;
    /**
     * Send keystrokes to an element (or otherwise set its value)
     * @see {@link https://w3c.github.io/webdriver/#element-send-keys}
     *
     * @param text - the text to send to the element
     * @param elementId - the id of the element
     */
    setValue?(text: string, elementId: string): Promise<void>;
    /**
     * Execute JavaScript (or some other kind of script) in the browser/app context
     * @see {@link https://w3c.github.io/webdriver/#execute-script}
     *
     * @param script - the string to be evaluated as the script, which will be made the body of an
     * anonymous function in the case of JS
     * @param args - the list of arguments to be applied to the script as a function
     *
     * @returns The return value of the script execution
     */
    execute?(script: string, args: unknown[]): Promise<unknown>;
    /**
     * Execute JavaScript (or some other kind of script) in the browser/app context, asynchronously
     * @see {@link https://w3c.github.io/webdriver/#execute-async-script}
     *
     * @param script - the string to be evaluated as the script, which will be made the body of an
     * anonymous function in the case of JS
     * @param args - the list of arguments to be applied to the script as a function
     *
     * @returns The promise resolution of the return value of the script execution (or an error
     * object if the promise is rejected)
     */
    executeAsync?(script: string, args: unknown[]): Promise<unknown>;
    /**
     * Get all cookies known to the browsing context
     * @see {@link https://w3c.github.io/webdriver/#get-all-cookies}
     *
     * @returns A list of serialized cookies
     */
    getCookies?(): Promise<Cookie[]>;
    /**
     * Get a cookie by name
     * @see {@link https://w3c.github.io/webdriver/#get-named-cookie}
     *
     * @param name - the name of the cookie
     *
     * @returns A serialized cookie
     */
    getCookie?(name: string): Promise<Cookie>;
    /**
     * Add a cookie to the browsing context
     * @see {@link https://w3c.github.io/webdriver/#add-cookie}
     *
     * @param cookie - the cookie data including properties like name, value, path, domain,
     * secure, httpOnly, expiry, and samesite
     */
    setCookie?(cookie: Cookie): Promise<void>;
    /**
     * Delete a named cookie
     * @see {@link https://w3c.github.io/webdriver/#delete-cookie}
     *
     * @param name - the name of the cookie to delete
     */
    deleteCookie?(name: string): Promise<void>;
    /**
     * Delete all cookies
     * @see {@link https://w3c.github.io/webdriver/#delete-all-cookies}
     */
    deleteCookies?(): Promise<void>;
    /**
     * Perform touch or keyboard actions
     * @see {@link https://w3c.github.io/webdriver/#perform-actions}
     *
     * @param actions - the action sequence
     */
    performActions?(actions: ActionSequence[]): Promise<void>;
    /**
     * Release all keys or buttons that are currently pressed
     * @see {@link https://w3c.github.io/webdriver/#release-actions}
     */
    releaseActions?(): Promise<void>;
    /**
     * Dismiss a simple dialog/alert
     * @see {@link https://w3c.github.io/webdriver/#dismiss-alert}
     */
    postDismissAlert?(): Promise<void>;
    /**
     * Accept a simple dialog/alert
     * @see {@link https://w3c.github.io/webdriver/#accept-alert}
     */
    postAcceptAlert?(): Promise<void>;
    /**
     * Get the text of the displayed alert
     * @see {@link https://w3c.github.io/webdriver/#get-alert-text}
     *
     * @returns The text of the alert
     */
    getAlertText?(): Promise<string | null>;
    /**
     * Set the text field of an alert prompt
     * @see {@link https://w3c.github.io/webdriver/#send-alert-text}
     *
     * @param text - the text to send to the prompt
     */
    setAlertText?(text: string): Promise<void>;
    /**
     * Get a screenshot of the current document as rendered
     * @see {@link https://w3c.github.io/webdriver/#take-screenshot}
     *
     * @returns A base64-encoded string representing the PNG image data
     */
    getScreenshot?(): Promise<string>;
    /**
     * Get an image of a single element as rendered on screen
     * @see {@link https://w3c.github.io/webdriver/#take-element-screenshot}
     *
     * @param elementId - the id of the element
     *
     * @returns A base64-encoded string representing the PNG image data for the element rect
     */
    getElementScreenshot?(elementId: string): Promise<string>;
    /**
     * Get the current time on the device under timeouts
     *
     * @param format - the date/time format you would like the response into
     *
     * @returns The formatted time
     */
    getDeviceTime?(format?: string): Promise<string>;
    /**
     * List the performance data types supported by this driver, which can be used in a call to get
     * the performance data by type.
     *
     * @returns The list of types
     *
     * @deprecated
     */
    getPerformanceDataTypes?(): Promise<string[]>;
    /**
     * Get the list of performance data associated with a given type
     *
     * @param packageName - the package name / id of the app to retrieve data for
     * @param dataType - the performance data type; one of those retrieved in a call to
     * getPerformanceDataTypes
     * @param dataReadTimeout - how long to wait for data before timing out
     *
     * @returns A list of performance data strings
     *
     * @deprecated
     */
    getPerformanceData?(packageName: string, dataType: string, dataReadTimeout?: number): Promise<any>;
    /**
     * Press a device hardware key by its code for the default duration
     *
     * @param keycode - the keycode
     * @param metastate - the code denoting the simultaneous pressing of any meta keys (shift etc)
     * @param flags - the code denoting the combination of extra flags
     *
     * @deprecated
     */
    pressKeyCode?(keycode: number, metastate?: number, flags?: number): Promise<void>;
    /**
     * Press a device hardware key by its code for a longer duration
     *
     * @param keycode - the keycode
     * @param metastate - the code denoting the simultaneous pressing of any meta keys (shift etc)
     * @param flags - the code denoting the combination of extra flags
     *
     * @deprecated
     *
     */
    longPressKeyCode?(keycode: number, metastate?: number, flags?: number): Promise<void>;
    /**
     * Apply a synthetic fingerprint to the fingerprint detector of the device
     *
     * @param fingerprintId - the numeric ID of the fingerprint to use
     *
     * @deprecated
     */
    fingerprint?(fingerprintId: number): Promise<void>;
    /**
     * Simulate sending an SMS message from a certain phone number to the device
     *
     * @param phoneNumber - the number to pretend the message is from
     * @param message - the SMS text
     *
     * @deprecated
     */
    sendSMS?(phoneNumber: string, message: string): Promise<void>;
    /**
     * Simulate triggering a phone call from a phone number and having the device take an action in
     * response
     *
     * @param phoneNumber - the number to pretend the call is from
     * @param action - the action to take in response (accept, reject, etc...)
     *
     * @deprecated
     */
    gsmCall?(phoneNumber: string, action: string): Promise<void>;
    /**
     * Simulate setting the GSM signal strength for a cell phone
     *
     * @param singalStrength - the strength in a driver-appropriate string
     *
     * @deprecated
     */
    gsmSignal?(signalStrength: string | number): Promise<void>;
    /**
     * Do something with GSM voice (unclear; this should not be implemented anywhere)
     *
     * @param state - the state
     *
     * @deprecated
     * @privateRemarks Not implemented in `armor-xcuitest-driver`
     */
    gsmVoice?(state: string): Promise<void>;
    /**
     * Set the simulated power capacity of the device
     *
     * @param percent - how full the battery should become
     *
     * @deprecated
     */
    powerCapacity?(percent: number): Promise<void>;
    /**
     * Set the AC-connected power state of the device
     *
     * @param state - whether the device is connected to power or not
     *
     * @deprecated
     */
    powerAC?(state: string): Promise<void>;
    /**
     * Set the network speed of the device
     *
     * @param netspeed - the speed as a string, like '3G'
     *
     * @deprecated
     */
    networkSpeed?(netspeed: string): Promise<void>;
    /**
     * Simulate a keyevent on the device
     *
     * @param keycode - the manufacturer defined keycode
     * @param metastate - the combination of meta startUnexpectedShutdown
     *
     * @deprecated
     */
    keyevent?(keycode: string | number, metastate?: string | number): Promise<void>;
    /**
     * Get the current activity name
     *
     * @returns The activity name
     *
     * @deprecated
     * @privateRemarks Not implemented in `armor-xcuitest-driver`
     */
    getCurrentActivity?(): Promise<string>;
    /**
     * Get the current active app package name/id
     *
     * @returns The package name
     *
     * @deprecated
     * @privateRemarks Not implemented in `armor-xcuitest-driver`
     */
    getCurrentPackage?(): Promise<string>;
    /**
     * Install an app on a device
     *
     * @param appPath - the absolute path to a local app or a URL of a downloadable app bundle
     * @param options - driver-specific install options
     */
    installApp?(appPath: string, options?: unknown): Promise<void>;
    /**
     * Launch an app
     *
     * @param appId - the package or bundle ID of the application
     * @param options - driver-specific launch options
     */
    activateApp?(appId: string, options?: unknown): Promise<void>;
    /**
     * Remove / uninstall an app
     *
     * @param appId - the package or bundle ID of the application
     * @param options - driver-specific launch options
     *
     * @returns `true` if successful
     */
    removeApp?(appId: string, options?: unknown): Promise<boolean>;
    /**
     * Quit / terminate / stop a running application
     *
     * @param appId - the package or bundle ID of the application
     * @param options - driver-specific launch options
     */
    terminateApp?(appId: string, options?: unknown): Promise<boolean>;
    /**
     * Determine whether an app is installed
     *
     * @param appId - the package or bundle ID of the application
     */
    isAppInstalled?(appId: string): Promise<boolean>;
    /**
     * Get the running state of an app
     *
     * @param appId - the package or bundle ID of the application
     *
     * @returns A number representing the state. `0` means not installed, `1` means not running, `2`
     * means running in background but suspended, `3` means running in the background, and `4` means
     * running in the foreground
     */
    queryAppState?(appId: string): Promise<0 | 1 | 2 | 3 | 4>;
    /**
     * Attempt to hide a virtual keyboard
     *
     * @param strategy - the driver-specific name of a hiding strategy to follow
     * @param key - the text of a key to use to hide the keyboard
     * @param keyCode - a key code to trigger to hide the keyboard
     * @param keyName - the name of a key to use to hide the keyboard
     *
     * @returns Whether the keyboard was successfully hidden. May never return `false` on some platforms
     */
    hideKeyboard?(strategy?: string, key?: string, keyCode?: string, keyName?: string): Promise<boolean>;
    /**
     * Determine whether the keyboard is shown
     *
     * @returns Whether the keyboard is shown
     */
    isKeyboardShown?(): Promise<boolean>;
    /**
     * Push data to a file at a remote path on the device
     *
     * @param path - the remote path on the device to create the file at
     * @param data - the base64-encoded data which will be decoded and written to `path`
     */
    pushFile?(path: string, data: string): Promise<void>;
    /**
     * Retrieve the data from a file on the device at a given path
     *
     * @param path - the remote path on the device to pull file data from
     *
     * @returns The base64-encoded file data
     */
    pullFile?(path: string): Promise<string>;
    /**
     * Retrieve the data from a folder on the device at a given path
     *
     * @param path - the remote path of a directory on the device
     *
     * @returns The base64-encoded zip file of the directory contents
     */
    pullFolder?(path: string): Promise<string>;
    /**
     * Toggle airplane/flight mode for the device
     *
     * @deprecated
     * @privateRemarks Not implemented in `armor-xcuitest-driver`
     */
    toggleFlightMode?(): Promise<void>;
    /**
     * Toggle cell network data
     *
     * @deprecated
     * @privateRemarks Not implemented in `armor-xcuitest-driver`
     */
    toggleData?(): Promise<void>;
    /**
     * Toggle WiFi radio status
     *
     * @deprecated
     * @privateRemarks Not implemented in `armor-xcuitest-driver`
     */
    toggleWiFi?(): Promise<void>;
    /**
     * Toggle location services for the device
     *
     * @deprecated
     * @privateRemarks Not implemented in `armor-xcuitest-driver`
     */
    toggleLocationServices?(): Promise<void>;
    /**
     * Open the notifications shade/screen
     *
     * @deprecated
     * @privateRemarks Not implemented in `armor-xcuitest-driver`
     */
    openNotifications?(): Promise<void>;
    /**
     * Start an Android activity within an app
     *
     * @param appPackage - the app package id
     * @param appActivity - the activity name
     * @param appWaitPackage - the package id to wait for if different from the app package
     * @param appWaitActivity - the activity name to wait for being active if different from
     * appActivity
     * @param intentAction - the action for the intent to use to start the activity
     * @param intentCategory - the category for the intent
     * @param flags - the flags for the intent
     * @param optionalIntentArguments - additional arguments to be passed to launching the intent
     * @param dontStopAppOnReset - set to true to not stop the current app before launching the
     * activity
     *
     * @deprecated
     * @privateRemarks Not implemented in `armor-xcuitest-driver`
     */
    startActivity?(appPackage: string, appActivity: string, appWaitPackage?: string, appWaitActivity?: string, intentAction?: string, intentCategory?: string, intentFlags?: string, optionalIntentArguments?: string, dontStopAppOnReset?: boolean): Promise<void>;
    /**
     * Get information from the system bars of a device
     *
     * @returns An array of information objects of driver-specific shape
     *
     * @deprecated
     */
    getSystemBars?(): Promise<unknown>;
    /**
     * Get the display's pixel density
     *
     * @returns The density
     *
     * @deprecated
     * @privateRemarks Not implemented in `armor-xcuitest-driver`
     */
    getDisplayDensity?(): Promise<number>;
    /**
     * End platform-specific code coverage tracing
     *
     * @param intent - the Android intent for the coverage activity
     * @param path - the path to place the results
     *
     * @deprecated
     * @privateRemarks Not implemented in `armor-xcuitest-driver`
     */
    endCoverage?(intent: string, path: string): Promise<void>;
    /**
     * Set the value of a text field but ensure the current value is replace and not appended
     *
     * @param value - the text to set
     * @param elementId - the element to set it in
     *
     * @deprecated
     * @privateRemarks Not implemented in `armor-xcuitest-driver`
     */
    replaceValue?(value: string, elementId: string): Promise<void>;
    /**
     * Check whether two elements are identical
     *
     * @param elementId - the first element's ID
     * @param otherElementId - the second element's ID
     *
     * @returns True if the elements are equal, false otherwise
     *
     * @deprecated
     * @privateRemarks Not implemented in `armor-xcuitest-driver`
     */
    equalsElement?(elementId: string, otherElementId: string): Promise<boolean>;
    /**
     * Get the list of IME engines
     *
     * @returns The list of IME engines
     *
     * @deprecated
     * @privateRemarks Not implemented in `armor-xcuitest-driver`
     */
    availableIMEEngines?(): Promise<string[]>;
    /**
     * Get the active IME engine
     *
     * @returns The name of the active engine
     *
     * @deprecated
     */
    getActiveIMEEngine?(): Promise<string>;
    /**
     * Determine whether an IME is active
     *
     * @returns True if the IME is activated
     *
     * @deprecated
     */
    isIMEActivated?(): Promise<boolean>;
    /**
     * Deactivate an IME engine
     *
     * @deprecated
     */
    deactivateIMEEngine?(): Promise<void>;
    /**
     * Activate an IME engine
     *
     * @param engine - the name of the engine
     *
     * @deprecated
     */
    activateIMEEngine?(engine: string): Promise<void>;
    /**
     * Get the device orientation
     *
     * @returns The orientation string
     */
    getOrientation?(): Promise<string>;
    /**
     * Set the device orientation
     *
     * @param orientation - the orientation string
     */
    setOrientation?(orientation: string): Promise<void>;
    /**
     * Trigger a mouse button down
     *
     * @param button - the button ID
     *
     * @deprecated Use the Actions API instead
     * @privateRemarks Not implemented in `armor-xcuitest-driver`
     */
    buttonDown?(button?: number): Promise<void>;
    /**
     * Trigger a mouse button up
     *
     * @param button - the button ID
     *
     * @deprecated Use the Actions API instead
     * @privateRemarks Not implemented in `armor-xcuitest-driver`
     */
    buttonUp?(button?: number): Promise<void>;
    /**
     * Click the current mouse location
     *
     * @param button - the button ID
     *
     * @deprecated Use the Actions API instead
     * @privateRemarks Not implemented in `armor-xcuitest-driver`
     */
    clickCurrent?(button?: number): Promise<void>;
    /**
     * Double-click the current mouse location
     *
     * @deprecated Use the Actions API instead
     * @privateRemarks Not implemented in `armor-xcuitest-driver`
     */
    doubleClick?(): Promise<void>;
    /**
     * Perform a touch down event at the location specified
     *
     * @param x - the x coordinate
     * @param y - the y coordinate
     *
     * @deprecated Use the Actions API instead
     */
    touchDown?(element: string, x: number, y: number): Promise<void>;
    /**
     * Perform a touch up event at the location specified
     *
     * @param x - the x coordinate
     * @param y - the y coordinate
     *
     * @deprecated Use the Actions API instead
     */
    touchUp?(element: string, x: number, y: number): Promise<void>;
    /**
     * Perform a touch move event at the location specified
     *
     * @param x - the x coordinate
     * @param y - the y coordinate
     *
     * @deprecated Use the Actions API instead
     */
    touchMove?(element: string, x: number, y: number): Promise<void>;
    /**
     * Perform a long touch down event at the location specified
     *
     * @param elementId - the id of the element to long touch
     *
     * @deprecated Use the Actions API instead
     */
    touchLongClick?(element: string, x: number, y: number, duration: number): Promise<void>;
    /**
     * Perform a flick event at the location specified
     *
     * @param element - the element to make coordinates relative to
     * @param xSpeed - the horizontal flick speed (in driver-specific units)
     * @param ySpeed - the vertical flick speed (in driver-specific units)
     * @param xOffset - the x coordinate
     * @param yOffset - the y coordinate
     * @param speed - the speed (unclear how this relates to xSpeed and ySpeed)
     *
     * @deprecated Use the Actions API instead
     * @privateRemarks Not implemented in `armor-xcuitest-driver`
     */
    flick?(element?: string, xSpeed?: number, ySpeed?: number, xOffset?: number, yOffset?: number, speed?: number): Promise<void>;
    /**
     * Get the virtual or real geographical location of a device
     *
     * @returns The location
     */
    getGeoLocation?(): Promise<Location$1>;
    /**
     * Set the virtual geographical location of a device
     *
     * @param location - the location including latitude and longitude
     * @returns The complete location
     */
    setGeoLocation?(location: Partial<Location$1>): Promise<Location$1>;
    /**
     * Get the currently active context
     * @see {@link https://github.com/SeleniumHQ/mobile-spec/blob/master/spec-draft.md#webviews-and-other-contexts}
     *
     * @returns The context name
     */
    getCurrentContext?(): Promise<Ctx | null>;
    /**
     * Switch to a context by name
     * @see {@link https://github.com/SeleniumHQ/mobile-spec/blob/master/spec-draft.md#webviews-and-other-contexts}
     *
     * @param name - the context name
     */
    setContext?(name: string, ...args: any[]): Promise<void>;
    /**
     * Get the list of available contexts
     * @see {@link https://github.com/SeleniumHQ/mobile-spec/blob/master/spec-draft.md#webviews-and-other-contexts}
     *
     * @returns The list of context names
     */
    getContexts?(): Promise<Ctx[]>;
    /**
     * Get the index of an element on the page
     *
     * @param elementId - the element id
     *
     * @returns The page index
     *
     * @deprecated
     * @privateRemarks Not implemented in `armor-xcuitest-driver`
     */
    getPageIndex?(elementId: string): Promise<string>;
    /**
     * Get the network connection state of a device
     * @see {@link https://github.com/SeleniumHQ/mobile-spec/blob/master/spec-draft.md#device-modes}
     *
     * @returns A number which is a bitmask representing categories like Data, Wifi, and Airplane
     * mode status
     */
    getNetworkConnection?(): Promise<number>;
    /**
     * Set the network connection of the device
     * @see {@link https://github.com/SeleniumHQ/mobile-spec/blob/master/spec-draft.md#device-modes}
     *
     * @param type - the bitmask representing network state
     * @returns A number which is a bitmask representing categories like Data, Wifi, and Airplane
     * mode status
     */
    setNetworkConnection?(type: number): Promise<number>;
    /**
     * Get the current rotation state of the device
     * @see {@link https://github.com/SeleniumHQ/mobile-spec/blob/master/spec-draft.md#device-rotation}
     *
     * @returns The Rotation object consisting of x, y, and z rotation values (0 <= n <= 360)
     */
    getRotation?(): Promise<Rotation>;
    /**
     * Set the device rotation state
     * @see {@link https://github.com/SeleniumHQ/mobile-spec/blob/master/spec-draft.md#device-rotation}
     *
     * @param x - the degree to which the device is rotated around the x axis (0 <= x <= 360)
     * @param y - the degree to which the device is rotated around the y axis (0 <= y <= 360)
     * @param z - the degree to which the device is rotated around the z axis (0 <= z <= 360)
     */
    setRotation?(x: number, y: number, z: number): Promise<void>;
    /**
     * Execute a devtools command
     *
     * @param cmd - the command
     * @param params - any command-specific command parameters
     *
     * @returns The result of the command execution
     */
    executeCdp?(cmd: string, params: unknown): Promise<unknown>;
    /**
     * Add a virtual authenticator to a browser
     * @see {@link https://www.w3.org/TR/webauthn-2/#sctn-automation-add-virtual-authenticator}
     *
     * @param protocol  - the protocol
     * @param transport - a valid AuthenticatorTransport value
     * @param hasResidentKey - whether there is a resident key
     * @param hasUserVerification - whether the authenticator has user verification
     * @param isUserConsenting - whether it is a user consenting authenticator
     * @param isUserVerified - whether the user is verified
     *
     * @returns The authenticator ID
     */
    addVirtualAuthenticator?(protocol: 'ctap/u2f' | 'ctap2' | 'ctap2_1', transport: string, hasResidentKey?: boolean, hasUserVerification?: boolean, isUserConsenting?: boolean, isUserVerified?: boolean): Promise<string>;
    /**
     * Remove a virtual authenticator
     * @see {@link https://www.w3.org/TR/webauthn-2/#sctn-automation-remove-virtual-authenticator}
     *
     * @param authenticatorId - the ID returned in the call to add the authenticator
     */
    removeVirtualAuthenticator?(authenticatorId: string): Promise<void>;
    /**
     * Inject a public key credential source into a virtual authenticator
     * @see {@link https://www.w3.org/TR/webauthn-2/#sctn-automation-add-credential}
     *
     * @param credentialId - the base64 encoded credential ID
     * @param isResidentCredential - if true, a client-side credential, otherwise a server-side
     * credential
     * @param rpId - the relying party ID the credential is scoped to
     * @param privateKey - the base64 encoded private key package
     * @param userHandle - the base64 encoded user handle
     * @param signCount - the initial value for a signature counter
     */
    addAuthCredential?(credentialId: string, isResidentCredential: boolean, rpId: string, privateKey: string, userHandle: string, signCount: number, authenticatorId: string): Promise<void>;
    /**
     * Get the list of public key credential sources
     * @see {@link https://www.w3.org/TR/webauthn-2/#sctn-automation-get-credentials}
     *
     * @returns The list of Credentials
     */
    getAuthCredential?(): Promise<Credential[]>;
    /**
     * Remove all auth credentials
     * @see {@link https://www.w3.org/TR/webauthn-2/#sctn-automation-remove-all-credentials}
     */
    removeAllAuthCredentials?(): Promise<void>;
    /**
     * Remove a specific auth credential
     *
     * @param credentialId - the credential ID
     * @param authenticatorId - the authenticator ID
     */
    removeAuthCredential?(credentialId: string, authenticatorId: string): Promise<void>;
    /**
     * Set the isUserVerified property of an authenticator
     * @see {@link https://www.w3.org/TR/webauthn-2/#sctn-automation-set-user-verified}
     *
     * @param isUserVerified - the value of the isUserVerified property
     * @param authenticatorId - the authenticator id
     */
    setUserAuthVerified?(isUserVerified: boolean, authenticatorId: string): Promise<void>;
    /**
     * Proxy a command to a connected WebDriver server
     *
     * @typeParam TReq - the type of the incoming body
     * @typeParam TRes - the type of the return value
     * @param url - the incoming URL
     * @param method - the incoming HTTP method
     * @param body - the incoming HTTP body
     *
     * @returns The return value of the proxied command
     */
    proxyCommand?<TReq = any, TRes = unknown>(url: string, method: HTTPMethod, body?: TReq): Promise<TRes>;
}
interface ExtraDriverOpts {
    fastReset?: boolean;
    skipUninstall?: boolean;
}
/**
 * Options as set within {@linkcode ExternalDriver.createSession}, which is a union of {@linkcode InitialOpts} and {@linkcode DriverCaps}.
 */
type DriverOpts<C extends Constraints> = InitialOpts & DriverCaps<C>;
/**
 * Options as provided to the {@linkcode Driver} constructor.
 */
type InitialOpts = Merge<ServerArgs, ExtraDriverOpts>;
/**
 * Tuple of an HTTP method with a regex matching a request path
 */
type RouteMatcher = [HTTPMethod, RegExp];
/**
 * Result of the {@linkcode onPostProcess ConfigureAppOptions.onPostProcess} callback.
 */
interface PostProcessResult {
    /**
     * The full past to the post-processed application package on the local file system .
     *
     * This might be a file or a folder path.
     */
    appPath: string;
}
/**
 * Information about a cached app instance.
 */
interface CachedAppInfo {
    /**
     * SHA1 hash of the package if it is a file (and not a folder)
     */
    packageHash: string;
    /**
     * Date instance; the value of the file's `Last-Modified` header
     */
    lastModified?: Date;
    /**
     * The value of the file's `Etag` header
     */
    etag?: string;
    /**
     * `true` if the file contains an `immutable` mark in `Cache-control` header
     */
    immutable?: boolean;
    /**
     * Integer representation of `maxAge` parameter in `Cache-control` header
     */
    maxAge?: number;
    /**
     * The timestamp this item has been added to the cache (measured in Unix epoch milliseconds)
     */
    timestamp?: number;
    /**
     * An object containing either `file` property with SHA1 hash of the file or `folder` property
     * with total amount of cached files and subfolders
     */
    integrity?: {
        file?: string;
    } | {
        folder?: number;
    };
    /**
     * The full path to the cached app
     */
    fullPath?: string;
}
/**
 * Options for the post-processing step
 *
 * The generic can be supplied if using `axios`, where `headers` is a fancy object.
 */
interface PostProcessOptions<Headers = HTTPHeaders> {
    /**
     * The information about the previously cached app instance (if exists)
     */
    cachedAppInfo?: CachedAppInfo;
    /**
     * Whether the app has been downloaded from a remote URL
     */
    isUrl?: boolean;
    /**
     * Optional headers object.
     *
     * Only present if `isUrl` is `true` and if the server responds to `HEAD` requests. All header names are normalized to lowercase.
     */
    headers?: Headers;
    /**
     * A string containing full path to the preprocessed application package (either downloaded or a local one)
     */
    appPath?: string;
}
interface ConfigureAppOptions {
    /**
     *
     * Optional function, which should be applied to the application after it is
     * downloaded/preprocessed.
     *
     * This function may be async and is expected to accept single object parameter. The function is
     * expected to either return a falsy value, which means the app must not be cached and a fresh
     * copy of it is downloaded each time, _or_ if this function returns an object containing an
     * `appPath` property, then the integrity of it will be verified and stored into the cache.
     * @returns
     */
    onPostProcess?: (obj: PostProcessOptions) => Promise<PostProcessResult | undefined> | PostProcessResult | undefined;
    supportedExtensions: string[];
}

/// <reference lib="esnext"/>

// TODO: This can just be `export type Primitive = not object` when the `not` keyword is out.
/**
Matches any [primitive value](https://developer.mozilla.org/en-US/docs/Glossary/Primitive).
*/
type Primitive =
	| null
	| undefined
	| string
	| number
	| boolean
	| symbol
	| bigint;

/**
Matches a JSON object.

This type can be useful to enforce some input to be JSON-compatible or as a super-type to be extended from. Don't use this as a direct return type as the user would have to double-cast it: `jsonObject as unknown as CustomResponse`. Instead, you could extend your CustomResponse type from it to ensure your type only uses JSON-compatible types: `interface CustomResponse extends JsonObject { … }`.
*/
type JsonObject = {[key: string]: JsonValue};

/**
Matches a JSON array.
*/
interface JsonArray extends Array<JsonValue> {}

/**
Matches any valid JSON value.
*/
type JsonValue = string | number | boolean | null | JsonObject | JsonArray;

declare global {
	interface SymbolConstructor {
		readonly observable: symbol;
	}
}

/**
Allows creating a union type by combining primitive types and literal types without sacrificing auto-completion in IDEs for the literal type part of the union.

Currently, when a union type of a primitive type is combined with literal types, TypeScript loses all information about the combined literals. Thus, when such type is used in an IDE with autocompletion, no suggestions are made for the declared literals.

This type is a workaround for [Microsoft/TypeScript#29729](https://github.com/Microsoft/TypeScript/issues/29729). It will be removed as soon as it's not needed anymore.

@example
```
import {LiteralUnion} from 'type-fest';

// Before

type Pet = 'dog' | 'cat' | string;

const pet: Pet = '';
// Start typing in your TypeScript-enabled IDE.
// You **will not** get auto-completion for `dog` and `cat` literals.

// After

type Pet2 = LiteralUnion<'dog' | 'cat', string>;

const pet: Pet2 = '';
// You **will** get auto-completion for `dog` and `cat` literals.
```
 */
type LiteralUnion<
	LiteralType extends BaseType,
	BaseType extends Primitive
> = LiteralType | (BaseType & {_?: never});

declare namespace PackageJson {
	/**
	A person who has been involved in creating or maintaining the package.
	*/
	export type Person =
		| string
		| {
			name: string;
			url?: string;
			email?: string;
		};

	export type BugsLocation =
		| string
		| {
			/**
			The URL to the package's issue tracker.
			*/
			url?: string;

			/**
			The email address to which issues should be reported.
			*/
			email?: string;
		};

	export interface DirectoryLocations {
		/**
		Location for executable scripts. Sugar to generate entries in the `bin` property by walking the folder.
		*/
		bin?: string;

		/**
		Location for Markdown files.
		*/
		doc?: string;

		/**
		Location for example scripts.
		*/
		example?: string;

		/**
		Location for the bulk of the library.
		*/
		lib?: string;

		/**
		Location for man pages. Sugar to generate a `man` array by walking the folder.
		*/
		man?: string;

		/**
		Location for test files.
		*/
		test?: string;

		[directoryType: string]: unknown;
	}

	export type Scripts = {
		/**
		Run **before** the package is published (Also run on local `npm install` without any arguments).
		*/
		prepublish?: string;

		/**
		Run both **before** the package is packed and published, and on local `npm install` without any arguments. This is run **after** `prepublish`, but **before** `prepublishOnly`.
		*/
		prepare?: string;

		/**
		Run **before** the package is prepared and packed, **only** on `npm publish`.
		*/
		prepublishOnly?: string;

		/**
		Run **before** a tarball is packed (on `npm pack`, `npm publish`, and when installing git dependencies).
		*/
		prepack?: string;

		/**
		Run **after** the tarball has been generated and moved to its final destination.
		*/
		postpack?: string;

		/**
		Run **after** the package is published.
		*/
		publish?: string;

		/**
		Run **after** the package is published.
		*/
		postpublish?: string;

		/**
		Run **before** the package is installed.
		*/
		preinstall?: string;

		/**
		Run **after** the package is installed.
		*/
		install?: string;

		/**
		Run **after** the package is installed and after `install`.
		*/
		postinstall?: string;

		/**
		Run **before** the package is uninstalled and before `uninstall`.
		*/
		preuninstall?: string;

		/**
		Run **before** the package is uninstalled.
		*/
		uninstall?: string;

		/**
		Run **after** the package is uninstalled.
		*/
		postuninstall?: string;

		/**
		Run **before** bump the package version and before `version`.
		*/
		preversion?: string;

		/**
		Run **before** bump the package version.
		*/
		version?: string;

		/**
		Run **after** bump the package version.
		*/
		postversion?: string;

		/**
		Run with the `npm test` command, before `test`.
		*/
		pretest?: string;

		/**
		Run with the `npm test` command.
		*/
		test?: string;

		/**
		Run with the `npm test` command, after `test`.
		*/
		posttest?: string;

		/**
		Run with the `npm stop` command, before `stop`.
		*/
		prestop?: string;

		/**
		Run with the `npm stop` command.
		*/
		stop?: string;

		/**
		Run with the `npm stop` command, after `stop`.
		*/
		poststop?: string;

		/**
		Run with the `npm start` command, before `start`.
		*/
		prestart?: string;

		/**
		Run with the `npm start` command.
		*/
		start?: string;

		/**
		Run with the `npm start` command, after `start`.
		*/
		poststart?: string;

		/**
		Run with the `npm restart` command, before `restart`. Note: `npm restart` will run the `stop` and `start` scripts if no `restart` script is provided.
		*/
		prerestart?: string;

		/**
		Run with the `npm restart` command. Note: `npm restart` will run the `stop` and `start` scripts if no `restart` script is provided.
		*/
		restart?: string;

		/**
		Run with the `npm restart` command, after `restart`. Note: `npm restart` will run the `stop` and `start` scripts if no `restart` script is provided.
		*/
		postrestart?: string;
	} & {
		[scriptName: string]: string;
	};

	/**
	Dependencies of the package. The version range is a string which has one or more space-separated descriptors. Dependencies can also be identified with a tarball or Git URL.
	*/
	export interface Dependency {
		[packageName: string]: string;
	}

	export interface NonStandardEntryPoints {
		/**
		An ECMAScript module ID that is the primary entry point to the program.
		*/
		module?: string;

		/**
		A module ID with untranspiled code that is the primary entry point to the program.
		*/
		esnext?:
		| string
		| {
			main?: string;
			browser?: string;
			[moduleName: string]: string | undefined;
		};

		/**
		A hint to JavaScript bundlers or component tools when packaging modules for client side use.
		*/
		browser?:
		| string
		| {
			[moduleName: string]: string | false;
		};
	}

	export interface TypeScriptConfiguration {
		/**
		Location of the bundled TypeScript declaration file.
		*/
		types?: string;

		/**
		Location of the bundled TypeScript declaration file. Alias of `types`.
		*/
		typings?: string;
	}

	export interface YarnConfiguration {
		/**
		If your package only allows one version of a given dependency, and you’d like to enforce the same behavior as `yarn install --flat` on the command line, set this to `true`.

		Note that if your `package.json` contains `"flat": true` and other packages depend on yours (e.g. you are building a library rather than an application), those other packages will also need `"flat": true` in their `package.json` or be installed with `yarn install --flat` on the command-line.
		*/
		flat?: boolean;

		/**
		Selective version resolutions. Allows the definition of custom package versions inside dependencies without manual edits in the `yarn.lock` file.
		*/
		resolutions?: Dependency;
	}

	export interface JSPMConfiguration {
		/**
		JSPM configuration.
		*/
		jspm?: PackageJson;
	}
}

/**
Type for [npm's `package.json` file](https://docs.npmjs.com/creating-a-package-json-file). Also includes types for fields used by other popular projects, like TypeScript and Yarn.
*/
type PackageJson = {
	/**
	The name of the package.
	*/
	name?: string;

	/**
	Package version, parseable by [`node-semver`](https://github.com/npm/node-semver).
	*/
	version?: string;

	/**
	Package description, listed in `npm search`.
	*/
	description?: string;

	/**
	Keywords associated with package, listed in `npm search`.
	*/
	keywords?: string[];

	/**
	The URL to the package's homepage.
	*/
	homepage?: LiteralUnion<'.', string>;

	/**
	The URL to the package's issue tracker and/or the email address to which issues should be reported.
	*/
	bugs?: PackageJson.BugsLocation;

	/**
	The license for the package.
	*/
	license?: string;

	/**
	The licenses for the package.
	*/
	licenses?: Array<{
		type?: string;
		url?: string;
	}>;

	author?: PackageJson.Person;

	/**
	A list of people who contributed to the package.
	*/
	contributors?: PackageJson.Person[];

	/**
	A list of people who maintain the package.
	*/
	maintainers?: PackageJson.Person[];

	/**
	The files included in the package.
	*/
	files?: string[];

	/**
	The module ID that is the primary entry point to the program.
	*/
	main?: string;

	/**
	The executable files that should be installed into the `PATH`.
	*/
	bin?:
	| string
	| {
		[binary: string]: string;
	};

	/**
	Filenames to put in place for the `man` program to find.
	*/
	man?: string | string[];

	/**
	Indicates the structure of the package.
	*/
	directories?: PackageJson.DirectoryLocations;

	/**
	Location for the code repository.
	*/
	repository?:
	| string
	| {
		type: string;
		url: string;
	};

	/**
	Script commands that are run at various times in the lifecycle of the package. The key is the lifecycle event, and the value is the command to run at that point.
	*/
	scripts?: PackageJson.Scripts;

	/**
	Is used to set configuration parameters used in package scripts that persist across upgrades.
	*/
	config?: {
		[configKey: string]: unknown;
	};

	/**
	The dependencies of the package.
	*/
	dependencies?: PackageJson.Dependency;

	/**
	Additional tooling dependencies that are not required for the package to work. Usually test, build, or documentation tooling.
	*/
	devDependencies?: PackageJson.Dependency;

	/**
	Dependencies that are skipped if they fail to install.
	*/
	optionalDependencies?: PackageJson.Dependency;

	/**
	Dependencies that will usually be required by the package user directly or via another dependency.
	*/
	peerDependencies?: PackageJson.Dependency;

	/**
	Package names that are bundled when the package is published.
	*/
	bundledDependencies?: string[];

	/**
	Alias of `bundledDependencies`.
	*/
	bundleDependencies?: string[];

	/**
	Engines that this package runs on.
	*/
	engines?: {
		[EngineName in 'npm' | 'node' | string]: string;
	};

	/**
	@deprecated
	*/
	engineStrict?: boolean;

	/**
	Operating systems the module runs on.
	*/
	os?: Array<LiteralUnion<
		| 'aix'
		| 'darwin'
		| 'freebsd'
		| 'linux'
		| 'openbsd'
		| 'sunos'
		| 'win32'
		| '!aix'
		| '!darwin'
		| '!freebsd'
		| '!linux'
		| '!openbsd'
		| '!sunos'
		| '!win32',
		string
	>>;

	/**
	CPU architectures the module runs on.
	*/
	cpu?: Array<LiteralUnion<
		| 'arm'
		| 'arm64'
		| 'ia32'
		| 'mips'
		| 'mipsel'
		| 'ppc'
		| 'ppc64'
		| 's390'
		| 's390x'
		| 'x32'
		| 'x64'
		| '!arm'
		| '!arm64'
		| '!ia32'
		| '!mips'
		| '!mipsel'
		| '!ppc'
		| '!ppc64'
		| '!s390'
		| '!s390x'
		| '!x32'
		| '!x64',
		string
	>>;

	/**
	If set to `true`, a warning will be shown if package is installed locally. Useful if the package is primarily a command-line application that should be installed globally.

	@deprecated
	*/
	preferGlobal?: boolean;

	/**
	If set to `true`, then npm will refuse to publish it.
	*/
	private?: boolean;

	/**
	 * A set of config values that will be used at publish-time. It's especially handy to set the tag, registry or access, to ensure that a given package is not tagged with 'latest', published to the global public registry or that a scoped module is private by default.
	 */
	publishConfig?: {
		[config: string]: unknown;
	};
} &
PackageJson.NonStandardEntryPoints &
PackageJson.TypeScriptConfiguration &
PackageJson.YarnConfiguration &
PackageJson.JSPMConfiguration & {
	[key: string]: unknown;
};

declare function normalize(data: normalize.Input, warn?: normalize.WarnFn, strict?: boolean): void;
declare function normalize(data: normalize.Input, strict?: boolean): void;

declare namespace normalize {
    type WarnFn = (msg: string) => void;
    interface Input {
        [k: string]: any;
    }

    interface Person {
        name?: string | undefined;
        email?: string | undefined;
        url?: string | undefined;
    }

    interface Package {
        [k: string]: any;
        name: string;
        version: string;
        files?: string[] | undefined;
        bin?: { [k: string]: string } | undefined;
        man?: string[] | undefined;
        keywords?: string[] | undefined;
        author?: Person | undefined;
        maintainers?: Person[] | undefined;
        contributors?: Person[] | undefined;
        bundleDependencies?: { [name: string]: string } | undefined;
        dependencies?: { [name: string]: string } | undefined;
        devDependencies?: { [name: string]: string } | undefined;
        optionalDependencies?: { [name: string]: string } | undefined;
        description?: string | undefined;
        engines?: { [type: string]: string } | undefined;
        license?: string | undefined;
        repository?: { type: string; url: string } | undefined;
        bugs?: { url: string; email?: string | undefined } | { url?: string | undefined; email: string } | undefined;
        homepage?: string | undefined;
        scripts?: { [k: string]: string } | undefined;
        readme: string;
        _id: string;
    }
}

declare namespace readPkg {
	interface Options {
		/**
		[Normalize](https://github.com/npm/normalize-package-data#what-normalization-currently-entails) the package data.

		@default true
		*/
		readonly normalize?: boolean;

		/**
		Current working directory.

		@default process.cwd()
		*/
		readonly cwd?: string;
	}

	interface NormalizeOptions extends Options {
		readonly normalize?: true;
	}

	type NormalizedPackageJson = PackageJson & normalize.Package;
	type PackageJson = PackageJson;
}

declare const readPkg: {
	/**
	@returns The parsed JSON.

	@example
	```
	import readPkg = require('read-pkg');

	(async () => {
		console.log(await readPkg());
		//=> {name: 'read-pkg', …}

		console.log(await readPkg({cwd: 'some-other-directory'});
		//=> {name: 'unicorn', …}
	})();
	```
	*/
	(options?: readPkg.NormalizeOptions): Promise<readPkg.NormalizedPackageJson>;
	(options: readPkg.Options): Promise<readPkg.PackageJson>;

	/**
	@returns The parsed JSON.

	@example
	```
	import readPkg = require('read-pkg');

	console.log(readPkg.sync());
	//=> {name: 'read-pkg', …}

	console.log(readPkg.sync({cwd: 'some-other-directory'});
	//=> {name: 'unicorn', …}
	```
	*/
	sync(options?: readPkg.NormalizeOptions): readPkg.NormalizedPackageJson;
	sync(options: readPkg.Options): readPkg.PackageJson;
};

interface IgnoreLike {
    ignored?: (p: Path) => boolean;
    childrenIgnored?: (p: Path) => boolean;
}

/**
 * A `GlobOptions` object may be provided to any of the exported methods, and
 * must be provided to the `Glob` constructor.
 *
 * All options are optional, boolean, and false by default, unless otherwise
 * noted.
 *
 * All resolved options are added to the Glob object as properties.
 *
 * If you are running many `glob` operations, you can pass a Glob object as the
 * `options` argument to a subsequent operation to share the previously loaded
 * cache.
 */
interface GlobOptions {
    /**
     * Set to `true` to always receive absolute paths for
     * matched files. Set to `false` to always return relative paths.
     *
     * When this option is not set, absolute paths are returned for patterns
     * that are absolute, and otherwise paths are returned that are relative
     * to the `cwd` setting.
     *
     * This does _not_ make an extra system call to get
     * the realpath, it only does string path resolution.
     *
     * Conflicts with {@link withFileTypes}
     */
    absolute?: boolean;
    /**
     * Set to false to enable {@link windowsPathsNoEscape}
     *
     * @deprecated
     */
    allowWindowsEscape?: boolean;
    /**
     * The current working directory in which to search. Defaults to
     * `process.cwd()`.
     *
     * May be eiher a string path or a `file://` URL object or string.
     */
    cwd?: string | URL;
    /**
     * Include `.dot` files in normal matches and `globstar`
     * matches. Note that an explicit dot in a portion of the pattern
     * will always match dot files.
     */
    dot?: boolean;
    /**
     * Prepend all relative path strings with `./` (or `.\` on Windows).
     *
     * Without this option, returned relative paths are "bare", so instead of
     * returning `'./foo/bar'`, they are returned as `'foo/bar'`.
     *
     * Relative patterns starting with `'../'` are not prepended with `./`, even
     * if this option is set.
     */
    dotRelative?: boolean;
    /**
     * Follow symlinked directories when expanding `**`
     * patterns. This can result in a lot of duplicate references in
     * the presence of cyclic links, and make performance quite bad.
     *
     * By default, a `**` in a pattern will follow 1 symbolic link if
     * it is not the first item in the pattern, or none if it is the
     * first item in the pattern, following the same behavior as Bash.
     */
    follow?: boolean;
    /**
     * string or string[], or an object with `ignore` and `ignoreChildren`
     * methods.
     *
     * If a string or string[] is provided, then this is treated as a glob
     * pattern or array of glob patterns to exclude from matches. To ignore all
     * children within a directory, as well as the entry itself, append `'/**'`
     * to the ignore pattern.
     *
     * **Note** `ignore` patterns are _always_ in `dot:true` mode, regardless of
     * any other settings.
     *
     * If an object is provided that has `ignored(path)` and/or
     * `childrenIgnored(path)` methods, then these methods will be called to
     * determine whether any Path is a match or if its children should be
     * traversed, respectively.
     */
    ignore?: string | string[] | IgnoreLike;
    /**
     * Treat brace expansion like `{a,b}` as a "magic" pattern. Has no
     * effect if {@link nobrace} is set.
     *
     * Only has effect on the {@link hasMagic} function.
     */
    magicalBraces?: boolean;
    /**
     * Add a `/` character to directory matches. Note that this requires
     * additional stat calls in some cases.
     */
    mark?: boolean;
    /**
     * Perform a basename-only match if the pattern does not contain any slash
     * characters. That is, `*.js` would be treated as equivalent to
     * `**\/*.js`, matching all js files in all directories.
     */
    matchBase?: boolean;
    /**
     * Limit the directory traversal to a given depth below the cwd.
     * Note that this does NOT prevent traversal to sibling folders,
     * root patterns, and so on. It only limits the maximum folder depth
     * that the walk will descend, relative to the cwd.
     */
    maxDepth?: number;
    /**
     * Do not expand `{a,b}` and `{1..3}` brace sets.
     */
    nobrace?: boolean;
    /**
     * Perform a case-insensitive match. This defaults to `true` on macOS and
     * Windows systems, and `false` on all others.
     *
     * **Note** `nocase` should only be explicitly set when it is
     * known that the filesystem's case sensitivity differs from the
     * platform default. If set `true` on case-sensitive file
     * systems, or `false` on case-insensitive file systems, then the
     * walk may return more or less results than expected.
     */
    nocase?: boolean;
    /**
     * Do not match directories, only files. (Note: to match
     * _only_ directories, put a `/` at the end of the pattern.)
     */
    nodir?: boolean;
    /**
     * Do not match "extglob" patterns such as `+(a|b)`.
     */
    noext?: boolean;
    /**
     * Do not match `**` against multiple filenames. (Ie, treat it as a normal
     * `*` instead.)
     *
     * Conflicts with {@link matchBase}
     */
    noglobstar?: boolean;
    /**
     * Defaults to value of `process.platform` if available, or `'linux'` if
     * not. Setting `platform:'win32'` on non-Windows systems may cause strange
     * behavior.
     */
    platform?: NodeJS.Platform;
    /**
     * Set to true to call `fs.realpath` on all of the
     * results. In the case of an entry that cannot be resolved, the
     * entry is omitted. This incurs a slight performance penalty, of
     * course, because of the added system calls.
     */
    realpath?: boolean;
    /**
     *
     * A string path resolved against the `cwd` option, which
     * is used as the starting point for absolute patterns that start
     * with `/`, (but not drive letters or UNC paths on Windows).
     *
     * Note that this _doesn't_ necessarily limit the walk to the
     * `root` directory, and doesn't affect the cwd starting point for
     * non-absolute patterns. A pattern containing `..` will still be
     * able to traverse out of the root directory, if it is not an
     * actual root directory on the filesystem, and any non-absolute
     * patterns will be matched in the `cwd`. For example, the
     * pattern `/../*` with `{root:'/some/path'}` will return all
     * files in `/some`, not all files in `/some/path`. The pattern
     * `*` with `{root:'/some/path'}` will return all the entries in
     * the cwd, not the entries in `/some/path`.
     *
     * To start absolute and non-absolute patterns in the same
     * path, you can use `{root:''}`. However, be aware that on
     * Windows systems, a pattern like `x:/*` or `//host/share/*` will
     * _always_ start in the `x:/` or `//host/share` directory,
     * regardless of the `root` setting.
     */
    root?: string;
    /**
     * A [PathScurry](http://npm.im/path-scurry) object used
     * to traverse the file system. If the `nocase` option is set
     * explicitly, then any provided `scurry` object must match this
     * setting.
     */
    scurry?: PathScurry;
    /**
     * Call `lstat()` on all entries, whether required or not to determine
     * if it's a valid match. When used with {@link withFileTypes}, this means
     * that matches will include data such as modified time, permissions, and
     * so on.  Note that this will incur a performance cost due to the added
     * system calls.
     */
    stat?: boolean;
    /**
     * An AbortSignal which will cancel the Glob walk when
     * triggered.
     */
    signal?: AbortSignal;
    /**
     * Use `\\` as a path separator _only_, and
     *  _never_ as an escape character. If set, all `\\` characters are
     *  replaced with `/` in the pattern.
     *
     *  Note that this makes it **impossible** to match against paths
     *  containing literal glob pattern characters, but allows matching
     *  with patterns constructed using `path.join()` and
     *  `path.resolve()` on Windows platforms, mimicking the (buggy!)
     *  behavior of Glob v7 and before on Windows. Please use with
     *  caution, and be mindful of [the caveat below about Windows
     *  paths](#windows). (For legacy reasons, this is also set if
     *  `allowWindowsEscape` is set to the exact value `false`.)
     */
    windowsPathsNoEscape?: boolean;
    /**
     * Return [PathScurry](http://npm.im/path-scurry)
     * `Path` objects instead of strings. These are similar to a
     * NodeJS `Dirent` object, but with additional methods and
     * properties.
     *
     * Conflicts with {@link absolute}
     */
    withFileTypes?: boolean;
    /**
     * An fs implementation to override some or all of the defaults.  See
     * http://npm.im/path-scurry for details about what can be overridden.
     */
    fs?: FSOption;
    /**
     * Just passed along to Minimatch.  Note that this makes all pattern
     * matching operations slower and *extremely* noisy.
     */
    debug?: boolean;
    /**
     * Return `/` delimited paths, even on Windows.
     *
     * On posix systems, this has no effect.  But, on Windows, it means that
     * paths will be `/` delimited, and absolute paths will be their full
     * resolved UNC forms, eg instead of `'C:\\foo\\bar'`, it would return
     * `'//?/C:/foo/bar'`
     */
    posix?: boolean;
}

declare function sanitize(
  input: string,
  options?: {
    replacement?: string | ((substring: string) => string);
  }
): string;

declare namespace sanitize_filename {
  export {
    sanitize as default,
  };
}

/**
 * The callback function which will be called during the directory walking
 */
type WalkDirCallback = (itemPath: string, isDirectory: boolean) => boolean | void;
type mv = typeof mv;
type ReadFn<TBuffer extends NodeJS.ArrayBufferView> = (fd: number, offset?: number | undefined, length?: number | undefined, position?: number | null | undefined) => B<{
    bytesRead: number;
    buffer: TBuffer;
}>;
declare namespace fs {
    /**
     * Resolves `true` if `path` is _readable_, which differs from Node.js' default behavior of "can we see it?"
     *
     * On Windows, ACLs are not supported, so this becomes a simple check for existence.
     *
     * This function will never reject.
     * @param {PathLike} path
     * @returns {Promise<boolean>}
     */
    export function hasAccess(path: _fs.PathLike): Promise<boolean>;
    /**
     * Resolves `true` if `path` is executable; `false` otherwise.
     *
     * On Windows, this function delegates to {@linkcode fs.hasAccess}.
     *
     * This function will never reject.
     * @param {PathLike} path
     * @returns {Promise<boolean>}
     */
    export function isExecutable(path: _fs.PathLike): Promise<boolean>;
    /**
     * Alias for {@linkcode fs.hasAccess}
     * @param {PathLike} path
     */
    export function exists(path: _fs.PathLike): Promise<boolean>;
    /**
     * Remove a directory and all its contents, recursively
     * @param {PathLike} filepath
     * @returns Promise<void>
     * @see https://nodejs.org/api/fs.html#fspromisesrmpath-options
     */
    export function rimraf(filepath: _fs.PathLike): Promise<void>;
    /**
     * Remove a directory and all its contents, recursively in sync
     * @param {PathLike} filepath
     * @returns undefined
     * @see https://nodejs.org/api/fs.html#fsrmsyncpath-options
     */
    export function rimrafSync(filepath: _fs.PathLike): void;
    /**
     * Like Node.js' `fsPromises.mkdir()`, but will _not_ reject if the directory already exists.
     *
     * @param {string|Buffer|URL} filepath
     * @param {import('fs').MakeDirectoryOptions} [opts]
     * @returns {Promise<string|undefined>}
     * @see https://nodejs.org/api/fs.html#fspromisesmkdirpath-options
     */
    export function mkdir(filepath: string | Buffer | URL, opts?: _fs.MakeDirectoryOptions | undefined): Promise<string | undefined>;
    /**
     * Copies files _and entire directories_
     * @param {string} source - Source to copy
     * @param {string} destination - Destination to copy to
     * @param {ncp.Options} [opts] - Additional arguments to pass to `ncp`
     * @see https://npm.im/ncp
     * @returns {Promise<void>}
     */
    export function copyFile(source: string, destination: string, opts?: ncp__default.Options | undefined): Promise<void>;
    /**
     * Create an MD5 hash of a file.
     * @param {PathLike} filePath
     * @returns {Promise<string>}
     */
    export function md5(filePath: _fs.PathLike): Promise<string>;
    export function mv_1(from: string, to: string, opts?: mv.Options | undefined): B<void>;
    export { mv_1 as mv };
    export { which__default as which };
    export function glob(pattern: string, opts?: GlobOptions | undefined): B<string[]>;
    export { sanitize as sanitizeName };
    /**
     * Create a hex digest of some file at `filePath`
     * @param {PathLike} filePath
     * @param {string} [algorithm]
     * @returns {Promise<string>}
     */
    export function hash(filePath: _fs.PathLike, algorithm?: string | undefined): Promise<string>;
    /**
     * Returns an `Walker` instance, which is a readable stream (and thusly an async iterator).
     *
     * @param {string} dir - Dir to start walking at
     * @param {import('klaw').Options} [opts]
     * @returns {import('klaw').Walker}
     * @see https://www.npmjs.com/package/klaw
     */
    export function walk(dir: string, opts?: klaw__default.Options | undefined): klaw__default.Walker;
    /**
     * Recursively create a directory.
     * @param {PathLike} dir
     * @returns {Promise<string|undefined>}
     */
    export function mkdirp(dir: _fs.PathLike): Promise<string | undefined>;
    /**
     * Walks a directory given according to the parameters given. The callback will be invoked with a path joined with the dir parameter
     * @param {string} dir Directory path where we will start walking
     * @param {boolean} recursive Set it to true if you want to continue walking sub directories
     * @param {WalkDirCallback} callback The callback to be called when a new path is found
     * @throws {Error} If the `dir` parameter contains a path to an invalid folder
     * @returns {Promise<string?>} returns the found path or null if the item was not found
     */
    export function walkDir(dir: string, recursive: boolean, callback: WalkDirCallback): Promise<string | null>;
    /**
     * Reads the closest `package.json` file from absolute path `dir`.
     * @param {string} dir - Directory to search from
     * @throws {Error} If there were problems finding or reading a `package.json` file
     */
    export function readPackageJsonFrom(dir: string, opts?: {}): readPkg.NormalizedPackageJson;
    /**
     * Finds the project root directory from `dir`.
     * @param {string} dir - Directory to search from
     * @throws {TypeError} If `dir` is not a nonempty string or relative path
     * @throws {Error} If there were problems finding the project root
     * @returns {string} The closeset parent dir containing `package.json`
     */
    export function findRoot(dir: string): string;
    export let access: typeof promises.access;
    export let appendFile: typeof promises.appendFile;
    export let chmod: typeof promises.chmod;
    export let close: any;
    export { constants };
    export { createWriteStream };
    export { createReadStream };
    export let lstat: typeof promises.lstat;
    export let open: (path: _fs.PathLike, flags: _fs.OpenMode, mode?: _fs.Mode | undefined) => Promise<number>;
    export let openFile: typeof promises.open;
    export let readdir: typeof promises.readdir;
    export function read(fd: number, offset?: number | undefined, length?: number | undefined, position?: number | null | undefined): B<{
        bytesRead: number;
        buffer: TBuffer;
    }>;
    export let readFile: typeof promises.readFile;
    export let readlink: typeof promises.readlink;
    export let realpath: typeof promises.realpath;
    export let rename: typeof promises.rename;
    export let stat: typeof promises.stat;
    export let symlink: typeof promises.symlink;
    export let unlink: typeof promises.unlink;
    export let write: any;
    export let writeFile: typeof promises.writeFile;
    export let F_OK: number;
    export let R_OK: number;
    export let W_OK: number;
    export let X_OK: number;
}

type Affixes = {
    /**
     * - prefix of the temp directory name
     */
    prefix?: string | undefined;
    /**
     * - suffix of the temp directory name
     */
    suffix?: string | undefined;
};
type OpenedAffixes = {
    /**
     * - The path to file
     */
    path: string;
    /**
     * - The file descriptor opened
     */
    fd: number;
};
/**
 * @typedef OpenedAffixes
 * @property {string} path - The path to file
 * @property {number} fd - The file descriptor opened
 */
/**
 * Generate a temporary directory in os.tempdir() or process.env.ARMOR_TMP_DIR
 * with arbitrary prefix/suffix for the directory name and return it as open.
 *
 * @param {Affixes} affixes
 * @returns {Promise<OpenedAffixes>}
 */
declare function open(affixes: Affixes): Promise<OpenedAffixes>;
/**
 * @typedef Affixes
 * @property {string} [prefix] - prefix of the temp directory name
 * @property {string} [suffix] - suffix of the temp directory name
 */
/**
 * Generate a temporary directory in os.tempdir() or process.env.ARMOR_TMP_DIR
 * with arbitrary prefix/suffix for the directory name.
 *
 * @param {string|Affixes} rawAffixes
 * @param {string} [defaultPrefix]
 * @returns {Promise<string>}  A path to the temporary directory with rawAffixes and defaultPrefix
 */
declare function path(rawAffixes: string | Affixes, defaultPrefix?: string | undefined): Promise<string>;
/**
 * Generate a temporary directory in os.tempdir() or process.env.ARMOR_TMP_DIR.
 * e.g.
 * - No `process.env.ARMOR_TMP_DIR`: `/var/folders/34/2222sh8n27d6rcp7jqlkw8km0000gn/T/xxxxxxxx.yyyy`
 * - With `process.env.ARMOR_TMP_DIR = '/path/to/root'`: `/path/to/root/xxxxxxxx.yyyy`
 *
 * @returns {Promise<string>} A path to the temporary directory
 */
declare function openDir(): Promise<string>;
/**
 * Returns a path to a temporary directory whcih is defined as static in the same process
 *
 * @returns {Promise<string>} A temp directory path whcih is defined as static in the same process
 */
declare function staticDir(): Promise<string>;

type tempDir_Affixes = Affixes;
type tempDir_OpenedAffixes = OpenedAffixes;
declare const tempDir_open: typeof open;
declare const tempDir_openDir: typeof openDir;
declare const tempDir_path: typeof path;
declare const tempDir_staticDir: typeof staticDir;
declare namespace tempDir {
  export {
    tempDir_Affixes as Affixes,
    tempDir_OpenedAffixes as OpenedAffixes,
    tempDir_open as open,
    tempDir_openDir as openDir,
    tempDir_path as path,
    tempDir_staticDir as staticDir,
  };
}

declare function isWindows(): boolean;
declare function isMac(): boolean;
declare function isLinux(): boolean;
declare function isOSWin64(): boolean;
declare function arch(): Promise<"32" | "64" | undefined>;
declare function macOsxVersion(): Promise<string>;

declare const system_arch: typeof arch;
declare const system_isLinux: typeof isLinux;
declare const system_isMac: typeof isMac;
declare const system_isOSWin64: typeof isOSWin64;
declare const system_isWindows: typeof isWindows;
declare const system_macOsxVersion: typeof macOsxVersion;
declare namespace system {
  export {
    system_arch as arch,
    system_isLinux as isLinux,
    system_isMac as isMac,
    system_isOSWin64 as isOSWin64,
    system_isWindows as isWindows,
    system_macOsxVersion as macOsxVersion,
  };
}

/**
 * @template {string} T
 * @param {T} val
 * @returns {val is NonEmptyString<T>}
 */
declare function hasContent<T extends string>(val: T): val is NonEmptyString<T>;
type PluralizeOptions = {
    /**
     * - Whether to prefix with the number (e.g., 3 ducks)
     */
    inclusive?: boolean | undefined;
};
type EncodingOptions = {
    /**
     * The maximum size of
     * the resulting buffer in bytes. This is set to 1GB by default, because
     * Armor limits the maximum HTTP body size to 1GB. Also, the NodeJS heap
     * size must be enough to keep the resulting object (usually this size is
     * limited to 1.4 GB)
     */
    maxSize?: number | undefined;
};
type LockFileOptions = {
    /**
     * The max time in seconds to wait for the lock
     */
    timeout?: number | undefined;
    /**
     * Whether to try lock recovery if
     * the first attempt to acquire it timed out.
     */
    tryRecovery?: boolean | undefined;
};
/**
 * A `string` which is never `''`.
 */
type NonEmptyString<T extends string> = T extends '' ? never : T;
/**
 * return true if the the value is not `undefined`, `null`, or `NaN`.
 *
 * XXX: `NaN` is not expressible in TypeScript.
 * @template T
 * @param {T} val
 * @returns {val is NonNullable<T>}
 */
declare function hasValue<T>(val: T): val is NonNullable<T>;
declare function escapeSpace(str: any): any;
declare function escapeSpecialChars(str: any, quoteEscape: any): any;
declare function localIp(): any;
declare function cancellableDelay$1(ms: any): any;
declare function multiResolve(roots: any, ...args: any[]): any;
declare function safeJsonParse(obj: any): any;
/**
 *
 * @param {string} elementId
 * @returns {import('armor-types').Element}
 */
declare function wrapElement(elementId: string): Element;
/**
 * Removes the wrapper from element, if it exists.
 *   { ELEMENT: 4 } becomes 4
 *   { element-6066-11e4-a52e-4f735466cecf: 5 } becomes 5
 * @param {import('armor-types').Element|string} el
 * @returns {string}
 */
declare function unwrapElement(el: Element | string): string;
declare function filterObject(obj: any, predicate: any): any;
/**
 * Converts number of bytes to a readable size string.
 *
 * @param {number|string} bytes - The actual number of bytes.
 * @returns {string} The actual string representation, for example
 *                   '1.00 KB' for '1024 B'
 * @throws {Error} If bytes count cannot be converted to an integer or
 *                 if it is less than zero.
 */
declare function toReadableSizeString(bytes: number | string): string;
/**
 * Checks whether the given path is a subpath of the
 * particular root folder. Both paths can include .. and . specifiers
 *
 * @param {string} originalPath The absolute file/folder path
 * @param {string} root The absolute root folder path
 * @param {?boolean} forcePosix Set it to true if paths must be interpreted in POSIX format
 * @returns {boolean} true if the given original path is the subpath of the root folder
 * @throws {Error} if any of the given paths is not absolute
 */
declare function isSubPath(originalPath: string, root: string, forcePosix?: boolean | null): boolean;
declare const W3C_WEB_ELEMENT_IDENTIFIER: "element-6066-11e4-a52e-4f735466cecf";
/**
 * Checks whether the given paths are pointing to the same file system
 * destination.
 *
 * @param {string} path1 - Absolute or relative path to a file/folder
 * @param {string} path2 - Absolute or relative path to a file/folder
 * @param {...string} pathN - Zero or more absolute or relative paths to files/folders
 * @returns {Promise<boolean>} true if all paths are pointing to the same file system item
 */
declare function isSameDestination(path1: string, path2: string, ...pathN: string[]): Promise<boolean>;
/**
 * Compares two version strings
 *
 * @param {string} ver1 - The first version number to compare. Should be a valid
 * version number supported by semver parser.
 * @param {string} ver2 - The second version number to compare. Should be a valid
 * version number supported by semver parser.
 * @param {string} operator - One of supported version number operators:
 * ==, !=, >, <, <=, >=, =
 * @returns {boolean} true or false depending on the actual comparison result
 * @throws {Error} if an unsupported operator is supplied or any of the supplied
 * version strings cannot be coerced
 */
declare function compareVersions(ver1: string, operator: string, ver2: string): boolean;
/**
 * Coerces the given number/string to a valid version string
 *
 * @template {boolean} [Strict=true]
 * @param {string} ver - Version string to coerce
 * @param {Strict} [strict] - If `true` then an exception will be thrown
 * if `ver` cannot be coerced
 * @returns {Strict extends true ? string : string|null} Coerced version number or null if the string cannot be
 * coerced and strict mode is disabled
 * @throws {Error} if strict mode is enabled and `ver` cannot be coerced
 */
declare function coerceVersion<Strict extends boolean = true>(ver: string, strict?: Strict | undefined): Strict extends true ? string : string | null;
/**
 * Add appropriate quotes to command arguments. See https://github.com/substack/node-shell-quote
 * for more details
 *
 * @param {string|string[]} args - The arguments that will be parsed
 * @returns {string} - The arguments, quoted
 */
declare function quote(args: string | string[]): string;
/**
 * This function is necessary to workaround unexpected memory leaks
 * caused by NodeJS string interning
 * behavior described in https://bugs.chromium.org/p/v8/issues/detail?id=2869
 *
 * @param {*} s - The string to unleak
 * @return {string} Either the unleaked string or the original object converted to string
 */
declare function unleakString(s: any): string;
/**
 * Stringifies the object passed in, converting Buffers into Strings for better
 * display. This mimics JSON.stringify (see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/JSON/stringify)
 * except the `replacer` argument can only be a function.
 *
 * @param {any} obj - the object to be serialized
 * @param {((key:any, value:any) => any)?} replacer - function to transform the properties added to the
 *                               serialized object
 * @param {number|string|undefined} space - used to insert white space into the output JSON
 *                                 string for readability purposes. Defaults to 2
 * @returns {string} - the JSON object serialized as a string
 */
declare function jsonStringify(obj: any, replacer?: ((key: any, value: any) => any) | null, space?: number | string | undefined): string;
/**
 * @typedef PluralizeOptions
 * @property {boolean} [inclusive=false] - Whether to prefix with the number (e.g., 3 ducks)
 */
/**
 * Get the form of a word appropriate to the count
 *
 * @param {string} word - The word to pluralize
 * @param {number} count - How many of the word exist
 * @param {PluralizeOptions|boolean} options - options for word pluralization,
 *   or a boolean indicating the options.inclusive property
 * @returns {string} The word pluralized according to the number
 */
declare function pluralize(word: string, count: number, options?: PluralizeOptions | boolean): string;
declare const GiB: number;
declare const MiB: number;
declare const KiB: 1024;
/**
 * @typedef EncodingOptions
 * @property {number} [maxSize=1073741824] The maximum size of
 * the resulting buffer in bytes. This is set to 1GB by default, because
 * Armor limits the maximum HTTP body size to 1GB. Also, the NodeJS heap
 * size must be enough to keep the resulting object (usually this size is
 * limited to 1.4 GB)
 */
/**
 * Converts contents of a local file to an in-memory base-64 encoded buffer.
 * The operation is memory-usage friendly and should be used while encoding
 * large files to base64
 *
 * @param {string} srcPath The full path to the file being encoded
 * @param {EncodingOptions} opts
 * @returns {Promise<Buffer>} base64-encoded content of the source file as memory buffer
 * @throws {Error} if there was an error while reading the source file
 * or the source file is too
 */
declare function toInMemoryBase64(srcPath: string, opts?: EncodingOptions): Promise<Buffer>;

/**
 * @typedef LockFileOptions
 * @property {number} [timeout=120] The max time in seconds to wait for the lock
 * @property {boolean} [tryRecovery=false] Whether to try lock recovery if
 * the first attempt to acquire it timed out.
 */
/**
 * Create an async function which, when called, will not proceed until a certain file is no
 * longer present on the system. This allows for preventing concurrent behavior across processes
 * using a known lockfile path.
 *
 * @template T
 * @param {string} lockFile The full path to the file used for the lock
 * @param {LockFileOptions} opts
 * @returns async function that takes another async function defining the locked
 * behavior
 */
declare function getLockFileGuard<T>(lockFile: string, opts?: LockFileOptions): {
    (behavior: (...args: any[]) => T): Promise<T>;
    check(): Promise<any>;
};

type util_EncodingOptions = EncodingOptions;
declare const util_GiB: typeof GiB;
declare const util_KiB: typeof KiB;
type util_LockFileOptions = LockFileOptions;
declare const util_MiB: typeof MiB;
type util_NonEmptyString<T extends string> = NonEmptyString<T>;
type util_PluralizeOptions = PluralizeOptions;
declare const util_W3C_WEB_ELEMENT_IDENTIFIER: typeof W3C_WEB_ELEMENT_IDENTIFIER;
declare const util_coerceVersion: typeof coerceVersion;
declare const util_compareVersions: typeof compareVersions;
declare const util_escapeSpace: typeof escapeSpace;
declare const util_escapeSpecialChars: typeof escapeSpecialChars;
declare const util_filterObject: typeof filterObject;
declare const util_getLockFileGuard: typeof getLockFileGuard;
declare const util_hasContent: typeof hasContent;
declare const util_hasValue: typeof hasValue;
declare const util_isSameDestination: typeof isSameDestination;
declare const util_isSubPath: typeof isSubPath;
declare const util_jsonStringify: typeof jsonStringify;
declare const util_localIp: typeof localIp;
declare const util_multiResolve: typeof multiResolve;
declare const util_pluralize: typeof pluralize;
declare const util_quote: typeof quote;
declare const util_safeJsonParse: typeof safeJsonParse;
declare const util_toInMemoryBase64: typeof toInMemoryBase64;
declare const util_toReadableSizeString: typeof toReadableSizeString;
declare const util_unleakString: typeof unleakString;
declare const util_unwrapElement: typeof unwrapElement;
declare const util_wrapElement: typeof wrapElement;
declare namespace util {
  export {
    util_EncodingOptions as EncodingOptions,
    util_GiB as GiB,
    util_KiB as KiB,
    util_LockFileOptions as LockFileOptions,
    util_MiB as MiB,
    util_NonEmptyString as NonEmptyString,
    util_PluralizeOptions as PluralizeOptions,
    util_W3C_WEB_ELEMENT_IDENTIFIER as W3C_WEB_ELEMENT_IDENTIFIER,
    cancellableDelay$1 as cancellableDelay,
    util_coerceVersion as coerceVersion,
    util_compareVersions as compareVersions,
    util_escapeSpace as escapeSpace,
    util_escapeSpecialChars as escapeSpecialChars,
    util_filterObject as filterObject,
    util_getLockFileGuard as getLockFileGuard,
    util_hasContent as hasContent,
    util_hasValue as hasValue,
    util_isSameDestination as isSameDestination,
    util_isSubPath as isSubPath,
    util_jsonStringify as jsonStringify,
    util_localIp as localIp,
    util_multiResolve as multiResolve,
    util_pluralize as pluralize,
    util_quote as quote,
    util_safeJsonParse as safeJsonParse,
    parse as shellParse,
    util_toInMemoryBase64 as toInMemoryBase64,
    util_toReadableSizeString as toReadableSizeString,
    util_unleakString as unleakString,
    util_unwrapElement as unwrapElement,
    v1 as uuidV1,
    v3 as uuidV3,
    v4 as uuidV4,
    v5 as uuidV5,
    util_wrapElement as wrapElement,
  };
}

type Method =
  | 'get' | 'GET'
  | 'delete' | 'DELETE'
  | 'head' | 'HEAD'
  | 'options' | 'OPTIONS'
  | 'post' | 'POST'
  | 'put' | 'PUT'
  | 'patch' | 'PATCH'
  | 'purge' | 'PURGE'
  | 'link' | 'LINK'
  | 'unlink' | 'UNLINK';

/**
 * Common options for {@linkcode uploadFile } and {@linkcode downloadFile }.
 */
type NetOptions = {
    /**
     * - Whether to log the actual download performance
     * (e.g. timings and speed)
     */
    isMetered?: boolean | undefined;
    /**
     * - Authentication credentials
     */
    auth?: AuthCredentials | undefined;
};
/**
 * Specific options for {@linkcode downloadFile }.
 */
type DownloadOptions = {
    /**
     * - The actual request timeout in milliseconds; defaults to {@linkcode DEFAULT_TIMEOUT_MS }
     */
    timeout?: number | undefined;
    /**
     * - Request headers mapping
     */
    headers?: Record<string, any> | undefined;
};
/**
 * Basic auth credentials; used by {@linkcode NetOptions }.
 */
type AuthCredentials = {
    /**
     * - Non-empty user name
     */
    user: string;
    /**
     * - Non-empty password
     */
    pass: string;
};
/**
 * This type is used in {@linkcode uploadFile } if the remote location uses the `ftp` protocol, and distinguishes the type from {@linkcode HttpUploadOptions }.
 */
type NotHttpUploadOptions = {
    headers: never;
    method: never;
    timeout: never;
    fileFieldName: never;
    formFields: never;
};
/**
 * Specific options for {@linkcode uploadFile } if the remote location uses the `http(s)` protocol
 */
type HttpUploadOptions = {
    /**
     * - Additional request headers mapping
     */
    headers?: HTTPHeaders | undefined;
    /**
     * - The HTTP method used for file upload
     */
    method?: Method | undefined;
    /**
     * - The actual request timeout in milliseconds; defaults to {@linkcode DEFAULT_TIMEOUT_MS }
     */
    timeout?: number | undefined;
    /**
     * - The name of the form field containing the file
     * content to be uploaded. Any falsy value make the request to use non-multipart upload
     */
    fileFieldName?: string | undefined;
    /**
     * - The additional form fields
     * to be included into the upload request. This property is only considered if
     * `fileFieldName` is set
     */
    formFields?: Record<string, any> | [string, any][] | undefined;
};
/**
 * Uploads the given file to a remote location. HTTP(S) and FTP
 * protocols are supported.
 *
 * @param {string} localPath - The path to a file on the local storage.
 * @param {string} remoteUri - The remote URI to upload the file to.
 * @param {(HttpUploadOptions|NotHttpUploadOptions) & NetOptions} [uploadOptions]
 * @returns {Promise<void>}
 */
declare function uploadFile(localPath: string, remoteUri: string, uploadOptions?: ((HttpUploadOptions | NotHttpUploadOptions) & NetOptions) | undefined): Promise<void>;
/**
 * Downloads the given file via HTTP(S)
 *
 * @param {string} remoteUrl - The remote url
 * @param {string} dstPath - The local path to download the file to
 * @param {DownloadOptions & NetOptions} [downloadOptions]
 * @throws {Error} If download operation fails
 */
declare function downloadFile(remoteUrl: string, dstPath: string, downloadOptions?: (DownloadOptions & NetOptions) | undefined): Promise<void>;

type net_AuthCredentials = AuthCredentials;
type net_DownloadOptions = DownloadOptions;
type net_HttpUploadOptions = HttpUploadOptions;
type net_NetOptions = NetOptions;
type net_NotHttpUploadOptions = NotHttpUploadOptions;
declare const net_downloadFile: typeof downloadFile;
declare const net_uploadFile: typeof uploadFile;
declare namespace net {
  export {
    net_AuthCredentials as AuthCredentials,
    net_DownloadOptions as DownloadOptions,
    net_HttpUploadOptions as HttpUploadOptions,
    net_NetOptions as NetOptions,
    net_NotHttpUploadOptions as NotHttpUploadOptions,
    net_downloadFile as downloadFile,
    net_uploadFile as uploadFile,
  };
}

/**
 * Parses a file in xml or binary format of plist
 * @param {string} plist The plist file path
 * @param {boolean} mustExist If set to false, this method will return an empty object when the file doesn't exist
 * @param {boolean} quiet If set to false, the plist path will be logged in debug level
 * @returns {Promise<any>} parsed plist JS Object
 */
declare function parsePlistFile(plist: string, mustExist?: boolean, quiet?: boolean): Promise<any>;
/**
 * Parses an buffer or a string to a JS object a plist from an object
 * @param {string|Buffer} data The plist in the form of string or Buffer
 * @returns {Object} parsed plist JS Object
 * @throws Will throw an error if the plist type is unknown
 */
declare function parsePlist(data: string | Buffer): any;
/**
 * Creates a plist from an object
 * @param {Object} object The JS object to be turned into a plist
 * @param {boolean} binary Set it to true for a binary plist
 * @returns {string|Buffer} returns a buffer or a string in respect to the binary parameter
 */
declare function createPlist(object: any, binary?: boolean): string | Buffer;
/**
 * Updates a plist file with the given fields
 * @param {string} plist The plist file path
 * @param {Object} updatedFields The updated fields-value pairs
 * @param {boolean} binary If set to false, the file will be created as a xml plist
 * @param {boolean} mustExist If set to false, this method will update an empty plist
 * @param {boolean} quiet If set to false, the plist path will be logged in debug level
 */
declare function updatePlistFile(plist: string, updatedFields: any, binary?: boolean, mustExist?: boolean, quiet?: boolean): Promise<void>;
/**
 * Creates a binary plist Buffer from an object
 * @param {Object} data The object to be turned into a binary plist
 * @returns {Buffer} plist in the form of a binary buffer
 */
declare function createBinaryPlist(data: any): Buffer;
/**
 * Parses a Buffer into an Object
 * @param {Buffer} data The beffer of a binary plist
 */
declare function parseBinaryPlist(data: Buffer): [any];

declare const plist_createBinaryPlist: typeof createBinaryPlist;
declare const plist_createPlist: typeof createPlist;
declare const plist_parseBinaryPlist: typeof parseBinaryPlist;
declare const plist_parsePlist: typeof parsePlist;
declare const plist_parsePlistFile: typeof parsePlistFile;
declare const plist_updatePlistFile: typeof updatePlistFile;
declare namespace plist {
  export {
    plist_createBinaryPlist as createBinaryPlist,
    plist_createPlist as createPlist,
    plist_parseBinaryPlist as parseBinaryPlist,
    plist_parsePlist as parsePlist,
    plist_parsePlistFile as parsePlistFile,
    plist_updatePlistFile as updatePlistFile,
  };
}

declare const mkdirp: (dir: _fs.PathLike) => Promise<string | undefined>;

type SecureValuePreprocessingRule = {
    /**
     * The parsed pattern which is going to be used for replacement
     */
    pattern: RegExp;
    /**
     * The replacer value to use. By default
     * equals to `DEFAULT_SECURE_REPLACER`
     */
    replacer?: string | undefined;
};

/** @type {import('armor-types').ArmorLoggerLevel[]} */
declare const LEVELS: ArmorLoggerLevel$1[];

type LoadResult = {
    /**
     * The list of rule parsing issues (one item per rule).
     * Rules with issues are skipped. An empty list is returned if no parsing issues exist.
     */
    issues: string[];
    /**
     * The list of successfully loaded
     * replacement rules. The list could be empty if no rules were loaded.
     */
    rules: SecureValuePreprocessingRule[];
};
type ArmorLoggerPrefix = ArmorLoggerPrefix$1;
type ArmorLogger = ArmorLogger$1;
type ArmorLoggerLevel = ArmorLoggerLevel$1;
declare const log: ArmorLogger$1;
/**
 *
 * @param {import('npmlog').Logger} logger
 */
declare function patchLogger(logger: npmlog.Logger): void;
/**
 *
 * @param {ArmorLoggerPrefix?} prefix
 * @returns {ArmorLogger}
 */
declare function getLogger(prefix?: ArmorLoggerPrefix | null): ArmorLogger;
/**
 * @typedef LoadResult
 * @property {string[]} issues The list of rule parsing issues (one item per rule).
 * Rules with issues are skipped. An empty list is returned if no parsing issues exist.
 * @property {import('./log-internal').SecureValuePreprocessingRule[]} rules The list of successfully loaded
 * replacement rules. The list could be empty if no rules were loaded.
 */
/**
 * Loads the JSON file containing secure values replacement rules.
 * This might be necessary to hide sensitive values that may possibly
 * appear in Armor logs.
 * Each call to this method replaces the previously loaded rules if any existed.
 *
 * @param {string|string[]|import('armor-types').LogFiltersConfig} rulesJsonPath The full path to the JSON file containing
 * the replacement rules. Each rule could either be a string to be replaced
 * or an object with predefined properties.
 * @throws {Error} If the given file cannot be loaded
 * @returns {Promise<LoadResult>}
 */
declare function loadSecureValuesPreprocessingRules(rulesJsonPath: string | string[] | LogFiltersConfig): Promise<LoadResult>;

type logger_ArmorLogger = ArmorLogger;
type logger_ArmorLoggerLevel = ArmorLoggerLevel;
type logger_ArmorLoggerPrefix = ArmorLoggerPrefix;
declare const logger_LEVELS: typeof LEVELS;
type logger_LoadResult = LoadResult;
declare const logger_getLogger: typeof getLogger;
declare const logger_loadSecureValuesPreprocessingRules: typeof loadSecureValuesPreprocessingRules;
declare const logger_log: typeof log;
declare const logger_patchLogger: typeof patchLogger;
declare namespace logger {
  export {
    logger_ArmorLogger as ArmorLogger,
    logger_ArmorLoggerLevel as ArmorLoggerLevel,
    logger_ArmorLoggerPrefix as ArmorLoggerPrefix,
    logger_LEVELS as LEVELS,
    logger_LoadResult as LoadResult,
    log as default,
    logger_getLogger as getLogger,
    logger_loadSecureValuesPreprocessingRules as loadSecureValuesPreprocessingRules,
    logger_log as log,
    logger_patchLogger as patchLogger,
  };
}

declare function getProcessIds(appName: any): Promise<any[]>;
declare function killProcess(appName: any, force?: boolean): Promise<void>;

declare const process$1_getProcessIds: typeof getProcessIds;
declare const process$1_killProcess: typeof killProcess;
declare namespace process$1 {
  export {
    process$1_getProcessIds as getProcessIds,
    process$1_killProcess as killProcess,
  };
}

declare namespace _default$1 {
    export { extractAllTo };
    export { readEntries };
    export { toInMemoryZip };
    export { assertValidZip };
    export { toArchive };
}

type ExtractAllOptions = {
    /**
     * The encoding to use for extracted file names.
     * For ZIP archives created on MacOS it is usually expected to be `utf8`.
     * By default it is autodetected based on the entry metadata and is only needed to be set explicitly
     * if the particular archive does not comply to the standards, which leads to corrupted file names
     * after extraction. Only applicable if system unzip binary is NOT being used.
     */
    fileNamesEncoding?: string | undefined;
    /**
     * If true, attempt to use system unzip; if this fails,
     * fallback to the JS unzip implementation.
     */
    useSystemUnzip?: boolean | undefined;
};
type ZipEntry = {
    /**
     * The actual entry instance
     */
    entry: yauzl.Entry;
    /**
     * An async function, which accepts one parameter.
     * This parameter contains the destination folder path to which this function is going to extract the entry.
     */
    extractEntryTo: Function;
};
type ZipOptions = {
    /**
     * Whether to encode
     * the resulting archive to a base64-encoded string
     */
    encodeToBase64?: boolean | undefined;
    /**
     * Whether to log the actual
     * archiver performance
     */
    isMetered?: boolean | undefined;
    /**
     * The maximum size of
     * the resulting archive in bytes. This is set to 1GB by default, because
     * Armor limits the maximum HTTP body size to 1GB. Also, the NodeJS heap
     * size must be enough to keep the resulting object (usually this size is
     * limited to 1.4 GB)
     */
    maxSize?: number | undefined;
    /**
     * The compression level. The maximum
     * level is 9 (the best compression, worst performance). The minimum
     * compression level is 0 (no compression).
     */
    level?: number | undefined;
};
type ZipCompressionOptions = {
    /**
     * [9] - Compression level in range 0..9
     * (greater numbers mean better compression, but longer processing time)
     */
    level: number;
};
type ZipSourceOptions = {
    /**
     * - GLOB pattern for compression
     */
    pattern?: string | undefined;
    /**
     * - The source root folder (the parent folder of
     * the destination file by default)
     */
    cwd?: string | undefined;
    /**
     * - The list of ignored patterns
     */
    ignore?: string[] | undefined;
};
/**
 * @typedef ExtractAllOptions
 * @property {string} [fileNamesEncoding] The encoding to use for extracted file names.
 * For ZIP archives created on MacOS it is usually expected to be `utf8`.
 * By default it is autodetected based on the entry metadata and is only needed to be set explicitly
 * if the particular archive does not comply to the standards, which leads to corrupted file names
 * after extraction. Only applicable if system unzip binary is NOT being used.
 * @property {boolean} [useSystemUnzip] If true, attempt to use system unzip; if this fails,
 * fallback to the JS unzip implementation.
 */
/**
 * Extract zipfile to a directory
 *
 * @param {string} zipFilePath The full path to the source ZIP file
 * @param {string} destDir The full path to the destination folder
 * @param {ExtractAllOptions} [opts]
 */
declare function extractAllTo(zipFilePath: string, destDir: string, opts?: ExtractAllOptions | undefined): Promise<void>;
/**
 * @typedef ZipEntry
 * @property {yauzl.Entry} entry The actual entry instance
 * @property {function} extractEntryTo An async function, which accepts one parameter.
 * This parameter contains the destination folder path to which this function is going to extract the entry.
 */
/**
 * Get entries for a zip folder
 *
 * @param {string} zipFilePath The full path to the source ZIP file
 * @param {function} onEntry Callback when entry is read.
 * The callback is expected to accept one argument of ZipEntry type.
 * The iteration through the source zip file will bi terminated as soon as
 * the result of this function equals to `false`.
 */
declare function readEntries(zipFilePath: string, onEntry: Function): Promise<any>;
/**
 * @typedef ZipOptions
 * @property {boolean} [encodeToBase64=false] Whether to encode
 * the resulting archive to a base64-encoded string
 * @property {boolean} [isMetered=true] Whether to log the actual
 * archiver performance
 * @property {number} [maxSize=1073741824] The maximum size of
 * the resulting archive in bytes. This is set to 1GB by default, because
 * Armor limits the maximum HTTP body size to 1GB. Also, the NodeJS heap
 * size must be enough to keep the resulting object (usually this size is
 * limited to 1.4 GB)
 * @property {number} [level=9] The compression level. The maximum
 * level is 9 (the best compression, worst performance). The minimum
 * compression level is 0 (no compression).
 */
/**
 * Converts contents of local directory to an in-memory .zip buffer
 *
 * @param {string} srcPath The full path to the folder or file being zipped
 * @param {ZipOptions} opts Zipping options
 * @returns {Promise<Buffer>} Zipped (and encoded if `encodeToBase64` is truthy)
 * content of the source path as memory buffer
 * @throws {Error} if there was an error while reading the source
 * or the source is too big
 */
declare function toInMemoryZip(srcPath: string, opts?: ZipOptions): Promise<Buffer>;
/**
 * Extract a single zip entry to a directory
 *
 * @param {yauzl.ZipFile} zipFile The source ZIP stream
 * @param {yauzl.Entry} entry The entry instance
 * @param {string} destDir The full path to the destination folder
 */
declare function _extractEntryTo(zipFile: yauzl.ZipFile, entry: yauzl.Entry, destDir: string): Promise<any>;
/**
 * Verifies whether the given file is a valid ZIP archive
 *
 * @param {string} filePath - Full path to the file
 * @throws {Error} If the file does not exist or is not a valid ZIP archive
 */
declare function assertValidZip(filePath: string): Promise<boolean>;
/**
 * @typedef ZipCompressionOptions
 * @property {number} level [9] - Compression level in range 0..9
 * (greater numbers mean better compression, but longer processing time)
 */
/**
 * @typedef ZipSourceOptions
 * @property {string} [pattern='**\/*'] - GLOB pattern for compression
 * @property {string} [cwd] - The source root folder (the parent folder of
 * the destination file by default)
 * @property {string[]} [ignore] - The list of ignored patterns
 */
/**
 * Creates an archive based on the given glob pattern
 *
 * @param {string} dstPath - The resulting archive path
 * @param {ZipSourceOptions} src - Source options
 * @param {ZipCompressionOptions} opts - Compression options
 * @throws {Error} If there was an error while creating the archive
 */
declare function toArchive(dstPath: string, src?: ZipSourceOptions, opts?: ZipCompressionOptions): Promise<any>;

type zip_ExtractAllOptions = ExtractAllOptions;
type zip_ZipCompressionOptions = ZipCompressionOptions;
type zip_ZipEntry = ZipEntry;
type zip_ZipOptions = ZipOptions;
type zip_ZipSourceOptions = ZipSourceOptions;
declare const zip__extractEntryTo: typeof _extractEntryTo;
declare const zip_assertValidZip: typeof assertValidZip;
declare const zip_extractAllTo: typeof extractAllTo;
declare const zip_readEntries: typeof readEntries;
declare const zip_toArchive: typeof toArchive;
declare const zip_toInMemoryZip: typeof toInMemoryZip;
declare namespace zip {
  export {
    zip_ExtractAllOptions as ExtractAllOptions,
    zip_ZipCompressionOptions as ZipCompressionOptions,
    zip_ZipEntry as ZipEntry,
    zip_ZipOptions as ZipOptions,
    zip_ZipSourceOptions as ZipSourceOptions,
    zip__extractEntryTo as _extractEntryTo,
    zip_assertValidZip as assertValidZip,
    _default$1 as default,
    zip_extractAllTo as extractAllTo,
    zip_readEntries as readEntries,
    zip_toArchive as toArchive,
    zip_toInMemoryZip as toInMemoryZip,
  };
}

//# sourceMappingURL=image-util.d.ts.map

declare namespace imageUtil {
  export {
  };
}

/** Class which stores the last bit of data streamed into it */
declare class MJpegStream extends Writable {
    /**
     * Create an MJpegStream
     * @param {string} mJpegUrl - URL of MJPEG-over-HTTP stream
     * @param {function} [errorHandler=noop] - additional function that will be
     * called in the case of any errors.
     * @param {object} [options={}] - Options to pass to the Writable constructor
     */
    constructor(mJpegUrl: string, errorHandler?: Function | undefined, options?: object);
    /**
     * @type {number}
     */
    updateCount: number;
    errorHandler: Function;
    url: string;
    /**
     * Get the base64-encoded version of the JPEG
     *
     * @returns {?string} base64-encoded JPEG image data
     * or `null` if no image can be parsed
     */
    get lastChunkBase64(): string | null;
    /**
     * Get the PNG version of the JPEG buffer
     *
     * @returns {Promise<Buffer?>} PNG image data or `null` if no PNG
     * image can be parsed
     */
    lastChunkPNG(): Promise<Buffer | null>;
    /**
     * Get the base64-encoded version of the PNG
     *
     * @returns {Promise<string?>} base64-encoded PNG image data
     * or `null` if no image can be parsed
     */
    lastChunkPNGBase64(): Promise<string | null>;
    /**
     * Reset internal state
     */
    clear(): void;
    registerStartSuccess: any;
    registerStartFailure: any;
    responseStream: any;
    consumer: any;
    lastChunk: Buffer | null | undefined;
    /**
     * Start reading the MJpeg stream and storing the last image
     */
    start(serverTimeout?: number): Promise<void>;
    /**
     * Stop reading the MJpeg stream. Ensure we disconnect all the pipes and stop
     * the HTTP request itself. Then reset the state.
     */
    stop(): void;
    /**
     * Override the Writable write() method in order to save the last image and
     * log the number of images we have received
     * @override
     * @param {Buffer} data - binary data streamed from the MJpeg consumer
     */
    override write(data: Buffer): boolean;
}

type mjpeg_MJpegStream = MJpegStream;
declare const mjpeg_MJpegStream: typeof MJpegStream;
declare namespace mjpeg {
  export {
    mjpeg_MJpegStream as MJpegStream,
  };
}

/**
 * Utility function to extend node functionality, allowing us to require
 * modules that are installed globally. If the package cannot be required,
 * this will attempt to link the package and then re-require it
 *
 * @param {string} packageName - the name of the package to be required
 * @returns {Promise<unknown>} - the package object
 * @throws {Error} If the package is not found locally or globally
 */
declare function requirePackage(packageName: string): Promise<unknown>;
/**
 * Calculate the in-depth size in memory of the provided object.
 * The original implementation is borrowed from https://github.com/miktam/sizeof.
 *
 * @param {*} obj An object whose size should be calculated
 * @returns {number} Object size in bytes.
 */
declare function getObjectSize(obj: any): number;
/**
 * Calculates a unique object identifier
 *
 * @param {object} object Any valid ECMA object
 * @returns {string} A uuidV4 string that uniquely identifies given object
 */
declare function getObjectId(object: object): string;
/**
 * Perform deep freeze of the given object (e. g.
 * all nested objects also become immutable).
 * If the passed object is of a plain type
 * then no change is done and the same object
 * is returned.
 * ! This function changes the given object,
 * so it becomes immutable.
 *
 * @param {*} object Any valid ECMA object
 * @returns {*} The same object that was passed to the
 * function after it was made immutable.
 */
declare function deepFreeze(object: any): any;
/**
 * Tries to synchronously detect the absolute path to the folder
 * where the given `moduleName` is located.
 *
 * @param {string} moduleName The name of the module as it is written in package.json
 * @param {string} filePath Full path to any of files that `moduleName` contains. Use
 * `__filename` to find the root of the module where this helper is called.
 * @returns {string?} Full path to the module root
 */
declare function getModuleRootSync(moduleName: string, filePath: string): string | null;

declare const node_deepFreeze: typeof deepFreeze;
declare const node_getModuleRootSync: typeof getModuleRootSync;
declare const node_getObjectId: typeof getObjectId;
declare const node_getObjectSize: typeof getObjectSize;
declare const node_requirePackage: typeof requirePackage;
declare namespace node {
  export {
    node_deepFreeze as deepFreeze,
    node_getModuleRootSync as getModuleRootSync,
    node_getObjectId as getObjectId,
    node_getObjectSize as getObjectSize,
    node_requirePackage as requirePackage,
  };
}

declare class Timer {
    _startTime: bigint | [number, number] | null;
    get startTime(): bigint | [number, number] | null;
    /**
     * Start the timer
     *
     * @return {Timer} The current instance, for chaining
     */
    start(): Timer;
    /**
     * Get the duration since the timer was started
     *
     * @return {Duration} the duration
     */
    getDuration(): Duration;
    toString(): string;
}
/**
 * Class representing a duration, encapsulating the number and units.
 */
declare class Duration {
    constructor(nanos: any);
    _nanos: any;
    get nanos(): any;
    /**
     * Get the duration as nanoseconds
     *
     * @returns {number} The duration as nanoseconds
     */
    get asNanoSeconds(): number;
    /**
     * Get the duration converted into milliseconds
     *
     * @returns {number} The duration as milliseconds
     */
    get asMilliSeconds(): number;
    /**
     * Get the duration converted into seconds
     *
     * @returns {number} The duration fas seconds
     */
    get asSeconds(): number;
    toString(): string;
}

type timing_Duration = Duration;
declare const timing_Duration: typeof Duration;
type timing_Timer = Timer;
declare const timing_Timer: typeof Timer;
declare namespace timing {
  export {
    timing_Duration as Duration,
    timing_Timer as Timer,
    Timer as default,
  };
}

declare const _: _.LoDashStatic;
declare namespace _ {
    // eslint-disable-next-line @typescript-eslint/no-empty-interface -- (This will be augmented)
    interface LoDashStatic {}
}

/**
 * Resolves `true` if an `armor` dependency can be found somewhere in the given `cwd`.
 *
 * @param {string} cwd
 * @returns {Promise<boolean>}
 */
declare function hasArmorDependency(cwd: string): Promise<boolean>;
/**
 * Path to the default `ARMOR_HOME` dir (`~/.armor`).
 * @type {string}
 */
declare const DEFAULT_ARMOR_HOME: string;
/**
 * Basename of extension manifest file.
 * @type {string}
 */
declare const MANIFEST_BASENAME: string;
/**
 * Relative path to extension manifest file from `ARMOR_HOME`.
 * @type {string}
 */
declare const MANIFEST_RELATIVE_PATH: string;
/**
 * Given `cwd`, use `npm` to find the closest package _or workspace root_, and return the path if the root depends upon `armor`.
 *
 * Looks at `dependencies` and `devDependencies` for `armor`.
 */
declare const findArmorDependencyPackage: ((cwd?: string | undefined) => Promise<string | undefined>) & _.MemoizedFunction;
/**
 * Read a `package.json` in dir `cwd`.  If none found, return `undefined`.
 */
declare const readPackageInDir: ((cwd: string) => Promise<readPkg.NormalizedPackageJson>) & _.MemoizedFunction;
/**
 * Determines location of Armor's "home" dir
 *
 * - If `ARMOR_HOME` is set in the environment, use that
 * - If we find a `package.json` in or above `cwd` and it has an `armor` dependency, use that.
 *
 * All returned paths will be absolute.
 */
declare const resolveArmorHome: ((cwd?: string | undefined) => Promise<string>) & _.MemoizedFunction;
/**
 * Figure out manifest path based on `armorHome`.
 *
 * The assumption is that, if `armorHome` has been provided, it was resolved via {@link resolveArmorHome `resolveArmorHome()`}!  If unsure,
 * don't pass a parameter and let `resolveArmorHome()` handle it.
 */
declare const resolveManifestPath: ((armorHome?: string | undefined) => Promise<string>) & _.MemoizedFunction;

declare const env_DEFAULT_ARMOR_HOME: typeof DEFAULT_ARMOR_HOME;
declare const env_MANIFEST_BASENAME: typeof MANIFEST_BASENAME;
declare const env_MANIFEST_RELATIVE_PATH: typeof MANIFEST_RELATIVE_PATH;
declare const env_findArmorDependencyPackage: typeof findArmorDependencyPackage;
declare const env_hasArmorDependency: typeof hasArmorDependency;
declare const env_readPackageInDir: typeof readPackageInDir;
declare const env_resolveArmorHome: typeof resolveArmorHome;
declare const env_resolveManifestPath: typeof resolveManifestPath;
declare namespace env {
  export {
    env_DEFAULT_ARMOR_HOME as DEFAULT_ARMOR_HOME,
    env_MANIFEST_BASENAME as MANIFEST_BASENAME,
    env_MANIFEST_RELATIVE_PATH as MANIFEST_RELATIVE_PATH,
    env_findArmorDependencyPackage as findArmorDependencyPackage,
    env_hasArmorDependency as hasArmorDependency,
    env_readPackageInDir as readPackageInDir,
    env_resolveArmorHome as resolveArmorHome,
    env_resolveManifestPath as resolveManifestPath,
  };
}

// Type definitions for @colors/colors 1.4+
// Project: https://github.com/Marak/colors.js
// Definitions by: Bart van der Schoor <https://github.com/Bartvds>, Staffan Eketorp <https://github.com/staeke>
// Definitions: https://github.com/DABH/colors.js

interface Color {
    (text: string): string;

    strip: Color;
    stripColors: Color;

    black: Color;
    red: Color;
    green: Color;
    yellow: Color;
    blue: Color;
    magenta: Color;
    cyan: Color;
    white: Color;
    gray: Color;
    grey: Color;

    brightRed: Color;
    brightGreen: Color;
    brightYellow: Color;
    brightBlue: Color;
    brightMagenta: Color;
    brightCyan: Color;
    brightWhite: Color;

    bgBlack: Color;
    bgRed: Color;
    bgGreen: Color;
    bgYellow: Color;
    bgBlue: Color;
    bgMagenta: Color;
    bgCyan: Color;
    bgWhite: Color;

    bgBrightRed: Color;
    bgBrightGreen: Color;
    bgBrightYellow: Color;
    bgBrightBlue: Color;
    bgBrightMagenta: Color;
    bgBrightCyan: Color;
    bgBrightWhite: Color;

    reset: Color;
    bold: Color;
    dim: Color;
    italic: Color;
    underline: Color;
    inverse: Color;
    hidden: Color;
    strikethrough: Color;

    rainbow: Color;
    zebra: Color;
    america: Color;
    trap: Color;
    random: Color;
    zalgo: Color;
}

declare global {
    interface String {
        strip: string;
        stripColors: string;

        black: string;
        red: string;
        green: string;
        yellow: string;
        blue: string;
        magenta: string;
        cyan: string;
        white: string;
        gray: string;
        grey: string;

        brightRed: string;
        brightGreen: string;
        brightYellow: string;
        brightBlue: string;
        brightMagenta: string;
        brightCyan: string;
        brightWhite: string;

        bgBlack: string;
        bgRed: string;
        bgGreen: string;
        bgYellow: string;
        bgBlue: string;
        bgMagenta: string;
        bgCyan: string;
        bgWhite: string;

        bgBrightRed: string;
        bgBrightGreen: string;
        bgBrightYellow: string;
        bgBrightBlue: string;
        bgBrightMagenta: string;
        bgBrightCyan: string;
        bgBrightWhite: string;

        reset: string;
        // @ts-ignore
        bold: string;
        dim: string;
        italic: string;
        underline: string;
        inverse: string;
        hidden: string;
        strikethrough: string;

        rainbow: string;
        zebra: string;
        america: string;
        trap: string;
        random: string;
        zalgo: string;
    }
}

/**
Colored symbols for various log levels.

Includes fallbacks for Windows CMD which only supports a [limited character set](https://en.wikipedia.org/wiki/Code_page_437).

@example
```
import logSymbols = require('log-symbols');

console.log(logSymbols.success, 'Finished successfully!');
// Terminals with Unicode support:     ✔ Finished successfully!
// Terminals without Unicode support:  √ Finished successfully!
```
*/
declare const logSymbols: {
	readonly info: string;

	readonly success: string;

	readonly warning: string;

	readonly error: string;
};

/**
 * A particular console/logging class for Armor's CLI.
 *
 * - By default, uses some fancy symbols
 * - Writes to `STDERR`, generally.
 * - In "JSON mode", `STDERR` is squelched. Use {@linkcode Console.json} to write the JSON.
 *
 * DO NOT extend this to do anything other than what it already does. Download a library or something.
 */
declare class CliConsole {
    /**
     * @type {Record<keyof typeof symbols,keyof Extract<import('@colors/colors').Color, 'string'>>}
     */
    static symbolToColor: Record<keyof typeof logSymbols, keyof Extract<Color, 'string'>>;
    /**
     *
     * @param {ConsoleOpts} opts
     */
    constructor({ jsonMode, useSymbols, useColor }?: ConsoleOpts);
    /**
     * Wraps a message string in breathtaking fanciness
     *
     * Returns `undefined` if `msg` is `undefined`.
     * @param {string} [msg] - Message to decorate, if anything
     * @param {keyof typeof CliConsole['symbolToColor']} [symbol] - Symbol to use
     * @returns {string|undefined}
     */
    decorate(msg?: string | undefined, symbol?: "info" | "success" | "warning" | "error" | undefined): string | undefined;
    /**
     * Writes to `STDOUT`.  Must be stringifyable.
     *
     * You probably don't want to call this more than once before exiting (since that will output invalid JSON).
     * @param {import('type-fest').JsonValue} value
     */
    json(value: JsonValue): void;
    /**
     * General logging function.
     * @param {string} [message]
     * @param {...any} args
     */
    log(message?: string | undefined, ...args: any[]): void;
    /**
     * A "success" message
     * @param {string} [message]
     * @param {...any} args
     */
    ok(message?: string | undefined, ...args: any[]): void;
    /**
     * Alias for {@linkcode Console.log}
     * @param {string} [message]
     * @param {...any} args
     */
    debug(message?: string | undefined, ...args: any[]): void;
    /**
     * Wraps {@link console.dir}
     * @param {any} item
     * @param {import('util').InspectOptions} [opts]
     */
    dump(item: any, opts?: util$1.InspectOptions | undefined): void;
    /**
     * An "info" message
     * @param {string} [message]
     * @param {...any} args
     */
    info(message?: string | undefined, ...args: any[]): void;
    /**
     * A "warning" message
     * @param {string} [message]
     * @param {...any} args
     */
    warn(message?: string | undefined, ...args: any[]): void;
    /**
     * An "error" message
     * @param {string} [message]
     * @param {...any} args
     */
    error(message?: string | undefined, ...args: any[]): void;
    #private;
}
/**
 * Options for {@linkcode CliConsole}.
 *
 * @typedef ConsoleOpts
 * @property {boolean} [jsonMode] - If _truthy_, supress all output except JSON (use {@linkcode CliConsole#json}), which writes to `STDOUT`.
 * @property {boolean} [useSymbols] - If _falsy_, do not use fancy symbols.
 * @property {boolean} [useColor] - If _falsy_, do not use color output. If _truthy_, forces color output. By default, checks terminal/TTY for support via pkg `supports-color`. Ignored if `useSymbols` is `false`.
 * @see https://npm.im/supports-color
 */
declare const console: CliConsole;

/**
 * Options for {@linkcode CliConsole }.
 */
type ConsoleOpts = {
    /**
     * - If _truthy_, supress all output except JSON (use {@linkcode CliConsolejson }), which writes to `STDOUT`.
     */
    jsonMode?: boolean | undefined;
    /**
     * - If _falsy_, do not use fancy symbols.
     */
    useSymbols?: boolean | undefined;
    /**
     * - If _falsy_, do not use color output. If _truthy_, forces color output. By default, checks terminal/TTY for support via pkg `supports-color`. Ignored if `useSymbols` is `false`.
     */
    useColor?: boolean | undefined;
};

type console$1_CliConsole = CliConsole;
declare const console$1_CliConsole: typeof CliConsole;
type console$1_ConsoleOpts = ConsoleOpts;
declare const console$1_console: typeof console;
declare namespace console$1 {
  export {
    console$1_CliConsole as CliConsole,
    console$1_ConsoleOpts as ConsoleOpts,
    console$1_console as console,
    logSymbols as symbols,
  };
}

/**
 * Options on top of `SpawnOptions`, unique to `ait-process.`
 */
type AITProcessProps = {
    /**
     * - Ignore & discard all output
     */
    ignoreOutput?: boolean | undefined;
    /**
     * - Return output as a Buffer
     */
    isBuffer?: boolean | undefined;
    /**
     * - Logger to use for debugging
     */
    logger?: AITProcessLogger | undefined;
    /**
     * - Maximum size of `stdout` buffer
     */
    maxStdoutBufferSize?: number | undefined;
    /**
     * - Maximum size of `stderr` buffer
     */
    maxStderrBufferSize?: number | undefined;
    /**
     * - Encoding to use for output
     */
    encoding?: BufferEncoding | undefined;
};
/**
 * A logger object understood by {@link exec ait-process.exec}.
 */
type AITProcessLogger = {
    debug: (...args: any[]) => void;
};
/**
 * Options for {@link exec ait-process.exec}.
 */
type AITProcessExecOptions = child_process.SpawnOptions & AITProcessProps;
/**
 * The value {@link exec ait-process.exec} resolves to when `isBuffer` is `false`
 */
type AITProcessExecStringResult = {
    /**
     * - Stdout
     */
    stdout: string;
    /**
     * - Stderr
     */
    stderr: string;
    /**
     * - Exit code
     */
    code: number | null;
};
/**
 * The value {@link exec ait-process.exec} resolves to when `isBuffer` is `true`
 */
type AITProcessExecBufferResult = {
    /**
     * - Stdout
     */
    stdout: Buffer;
    /**
     * - Stderr
     */
    stderr: Buffer;
    /**
     * - Exit code
     */
    code: number | null;
};
/**
 * Extra props {@link exec ait-process.exec} adds to its error objects
 */
type AITProcessExecErrorProps = {
    /**
     * - STDOUT
     */
    stdout: string;
    /**
     * - STDERR
     */
    stderr: string;
    /**
     * - Exit code
     */
    code: number | null;
};
/**
 * Error thrown by {@link exec ait-process.exec}
 */
type AITProcessExecError = Error & AITProcessExecErrorProps;
type BufferProp<MaybeBuffer extends {
    isBuffer?: boolean | undefined;
}> = MaybeBuffer['isBuffer'];
/**
 * Spawns a process
 * @template {AITProcessExecOptions} T
 * @param {string} cmd - Program to execute
 * @param {string[]} [args] - Arguments to pass to the program
 * @param {T} [opts] - Options
 * @returns {Promise<BufferProp<T> extends true ? AITProcessExecBufferResult : AITProcessExecStringResult>}
 */
declare function exec$1<T extends AITProcessExecOptions>(cmd: string, args?: string[] | undefined, opts?: T | undefined): Promise<BufferProp<T> extends true ? AITProcessExecBufferResult : AITProcessExecStringResult>;

type StartDetector$1 = (stdout: string, stderr?: string | undefined) => any;
declare class SubProcess$2 extends events {
    /**
     * @param {string} cmd
     * @param {string[]} [args]
     * @param {any} [opts]
     */
    constructor(cmd: string, args?: string[] | undefined, opts?: any);
    /**
     * @type { {stdout: string, stderr: string} }
     */
    lastLinePortion: {
        stdout: string;
        stderr: string;
    };
    /** @type {import('child_process').ChildProcess?} */
    proc: child_process.ChildProcess | null;
    /** @type {string[]} */
    args: string[];
    /**
     * @type {string}
     */
    cmd: string;
    /**
     * @type {any}
    */
    opts: any;
    /**
     * @type {boolean}
     */
    expectingExit: boolean;
    /**
     * @type {string}
     */
    rep: string;
    get isRunning(): boolean;
    /**
     *
     * @param {string} stream
     * @param {Iterable<string>} lines
     */
    emitLines(stream: string, lines: Iterable<string>): void;
    /**
     *
     * @param {StartDetector|number?} startDetector
     * @param {number?} timeoutMs
     * @param {boolean} detach
     * @returns {Promise<void>}
     */
    start(startDetector?: StartDetector$1 | (number | null), timeoutMs?: number | null, detach?: boolean): Promise<void>;
    handleLastLines(): void;
    /**
     *
     * @param {NodeJS.Signals} signal
     * @param {number} timeout
     * @returns {Promise<void>}
     */
    stop(signal?: NodeJS.Signals, timeout?: number): Promise<void>;
    join(allowedExitCodes?: number[]): Promise<any>;
    detachProcess(): void;
    get pid(): number | null | undefined;
}

declare const exec: typeof exec$1;
declare const spawn: typeof child_process.spawn;
declare const SubProcess$1: typeof SubProcess$2;

var process = /*#__PURE__*/_mergeNamespaces({
    __proto__: null,
    AITProcessExecBufferResult: AITProcessExecBufferResult,
    AITProcessExecError: AITProcessExecError,
    AITProcessExecErrorProps: AITProcessExecErrorProps,
    AITProcessExecOptions: AITProcessExecOptions,
    AITProcessExecStringResult: AITProcessExecStringResult,
    AITProcessLogger: AITProcessLogger,
    AITProcessProps: AITProcessProps,
    BufferProp: BufferProp,
    StartDetector: StartDetector$1,
    SubProcess: SubProcess$1,
    exec: exec,
    spawn: spawn
}, [child_process]);

/**
 * XXX: This should probably be a singleton, but it isn't.  Maybe this module should just export functions?
 */
declare class NPM {
    /**
     * Returns path to "install" lockfile
     * @private
     * @param {string} cwd
     */
    private _getInstallLockfilePath;
    /**
     * Execute `npm` with given args.
     *
     * If the process exits with a nonzero code, the contents of `STDOUT` and `STDERR` will be in the
     * `message` of any rejected error.
     * @param {string} cmd
     * @param {string[]} args
     * @param {ExecOpts} opts
     * @param {Omit<AITProcessExecOptions, 'cwd'>} [execOpts]
     */
    exec(cmd: string, args: string[], opts: ExecOpts, execOpts?: Omit<AITProcessExecOptions, "cwd"> | undefined): Promise<AITProcessExecStringResult & {
        json?: any;
    }>;
    /**
     * @param {string} cwd
     * @param {string} pkg
     * @returns {Promise<string?>}
     */
    getLatestVersion(cwd: string, pkg: string): Promise<string | null>;
    /**
     * @param {string} cwd
     * @param {string} pkg
     * @param {string} curVersion
     * @returns {Promise<string?>}
     */
    getLatestSafeUpgradeVersion(cwd: string, pkg: string, curVersion: string): Promise<string | null>;
    /**
     * Runs `npm ls`, optionally for a particular package.
     * @param {string} cwd
     * @param {string} [pkg]
     */
    list(cwd: string, pkg?: string | undefined): Promise<any>;
    /**
     * Given a current version and a list of all versions for a package, return the version which is
     * the highest safely-upgradable version (meaning not crossing any major revision boundaries, and
     * not including any alpha/beta/rc versions)
     *
     * @param {string} curVersion - the current version of a package
     * @param {Array<string>} allVersions - a list of version strings
     *
     * @return {string|null} - the highest safely-upgradable version, or null if there isn't one
     */
    getLatestSafeUpgradeFromVersions(curVersion: string, allVersions: Array<string>): string | null;
    /**
     * Installs a package w/ `npm`
     * @param {string} cwd
     * @param {string} pkgName
     * @param {InstallPackageOpts} opts
     * @returns {Promise<NpmInstallReceipt>}
     */
    installPackage(cwd: string, pkgName: string, { pkgVer, installType }?: InstallPackageOpts): Promise<NpmInstallReceipt>;
    /**
     * @param {string} cwd
     * @param {string} pkg
     */
    uninstallPackage(cwd: string, pkg: string): Promise<void>;
}
declare const npm: NPM;
/**
 * Options for {@link NPM.installPackage }
 */
type InstallPackageOpts = {
    /**
     * - whether to install from a local path or from npm
     */
    installType?: LiteralUnion<"local", string> | undefined;
    /**
     * - the version of the package to install
     */
    pkgVer?: string | undefined;
};
/**
 * Options for {@link NPM.exec }
 */
type ExecOpts = {
    /**
     * - Current working directory
     */
    cwd: string;
    /**
     * - If `true`, supply `--json` flag to npm and resolve w/ parsed JSON
     */
    json?: boolean | undefined;
    /**
     * - Path to lockfile to use
     */
    lockFile?: string | undefined;
};
type NpmInstallReceipt = {
    /**
     * - Path to installed package
     */
    installPath: string;
    /**
     * - Package data
     */
    pkg: PackageJson;
};

declare const cancellableDelay: typeof cancellableDelay$1;

declare const _default: {
    tempDir: typeof tempDir;
    system: typeof system;
    util: typeof util;
    fs: {
        hasAccess(path: fs.PathLike): Promise<boolean>;
        isExecutable(path: fs.PathLike): Promise<boolean>;
        exists(path: fs.PathLike): Promise<boolean>;
        rimraf(filepath: fs.PathLike): Promise<void>;
        rimrafSync(filepath: fs.PathLike): void;
        mkdir(filepath: string | Buffer | URL, opts?: fs.MakeDirectoryOptions | undefined): Promise<string | undefined>;
        copyFile(source: string, destination: string, opts?: ncp.Options | undefined): Promise<void>;
        md5(filePath: fs.PathLike): Promise<string>;
        mv: (from: string, to: string, opts?: mv$1.Options | undefined) => B<void>;
        which: typeof which;
        glob: (pattern: string, opts?: GlobOptions | undefined) => B<string[]>;
        sanitizeName: typeof sanitize_filename;
        hash(filePath: fs.PathLike, algorithm?: string | undefined): Promise<string>;
        walk(dir: string, opts?: klaw.Options | undefined): klaw.Walker;
        mkdirp(dir: fs.PathLike): Promise<string | undefined>;
        walkDir(dir: string, recursive: boolean, callback: WalkDirCallback): Promise<string | null>;
        readPackageJsonFrom(dir: string, opts?: {}): undefined;
        findRoot(dir: string): string;
        access: typeof fs_promises.access;
        appendFile: typeof fs_promises.appendFile;
        chmod: typeof fs_promises.chmod;
        close: any;
        constants: typeof fs.constants;
        createWriteStream: typeof fs.createWriteStream;
        createReadStream: typeof fs.createReadStream;
        lstat: typeof fs_promises.lstat;
        open: (path: fs.PathLike, flags: fs.OpenMode, mode?: fs.Mode | undefined) => Promise<number>;
        openFile: typeof fs_promises.open;
        readdir: typeof fs_promises.readdir;
        read: ReadFn<NodeJS.ArrayBufferView>;
        readFile: typeof fs_promises.readFile;
        readlink: typeof fs_promises.readlink;
        realpath: typeof fs_promises.realpath;
        rename: typeof fs_promises.rename;
        stat: typeof fs_promises.stat;
        symlink: typeof fs_promises.symlink;
        unlink: typeof fs_promises.unlink;
        write: any;
        writeFile: typeof fs_promises.writeFile;
        F_OK: number;
        R_OK: number;
        W_OK: number;
        X_OK: number;
    };
    cancellableDelay: typeof cancellableDelay$1;
    plist: typeof plist;
    mkdirp: (dir: fs.PathLike) => Promise<string | undefined>;
    logger: typeof logger;
    process: typeof process$1;
    zip: typeof zip;
    imageUtil: typeof imageUtil;
    net: typeof net;
    mjpeg: typeof mjpeg;
    node: typeof node;
    timing: typeof timing;
    env: typeof env;
    console: typeof console$1;
};
//# sourceMappingURL=index.d.ts.map

type support_Affixes = Affixes;
type support_AuthCredentials = AuthCredentials;
type support_ConsoleOpts = ConsoleOpts;
type support_DownloadOptions = DownloadOptions;
type support_EncodingOptions = EncodingOptions;
type support_ExecOpts = ExecOpts;
type support_ExtractAllOptions = ExtractAllOptions;
type support_HttpUploadOptions = HttpUploadOptions;
type support_InstallPackageOpts = InstallPackageOpts;
type support_LockFileOptions = LockFileOptions;
type support_NetOptions = NetOptions;
type support_NonEmptyString<T extends string> = NonEmptyString<T>;
type support_NotHttpUploadOptions = NotHttpUploadOptions;
type support_NpmInstallReceipt = NpmInstallReceipt;
type support_OpenedAffixes = OpenedAffixes;
type support_PluralizeOptions = PluralizeOptions;
type support_ReadFn<TBuffer extends NodeJS.ArrayBufferView> = ReadFn<TBuffer>;
type support_SecureValuePreprocessingRule = SecureValuePreprocessingRule;
type support_WalkDirCallback = WalkDirCallback;
type support_ZipCompressionOptions = ZipCompressionOptions;
type support_ZipEntry = ZipEntry;
type support_ZipOptions = ZipOptions;
type support_ZipSourceOptions = ZipSourceOptions;
declare const support_cancellableDelay: typeof cancellableDelay;
declare const support_env: typeof env;
declare const support_fs: typeof fs;
declare const support_imageUtil: typeof imageUtil;
declare const support_logger: typeof logger;
declare const support_mjpeg: typeof mjpeg;
declare const support_mkdirp: typeof mkdirp;
declare const support_net: typeof net;
declare const support_node: typeof node;
declare const support_npm: typeof npm;
declare const support_plist: typeof plist;
declare const support_system: typeof system;
declare const support_tempDir: typeof tempDir;
declare const support_timing: typeof timing;
declare const support_util: typeof util;
declare const support_zip: typeof zip;
declare namespace support {
  export {
    support_Affixes as Affixes,
    support_AuthCredentials as AuthCredentials,
    support_ConsoleOpts as ConsoleOpts,
    support_DownloadOptions as DownloadOptions,
    support_EncodingOptions as EncodingOptions,
    support_ExecOpts as ExecOpts,
    support_ExtractAllOptions as ExtractAllOptions,
    support_HttpUploadOptions as HttpUploadOptions,
    support_InstallPackageOpts as InstallPackageOpts,
    support_LockFileOptions as LockFileOptions,
    support_NetOptions as NetOptions,
    support_NonEmptyString as NonEmptyString,
    support_NotHttpUploadOptions as NotHttpUploadOptions,
    support_NpmInstallReceipt as NpmInstallReceipt,
    support_OpenedAffixes as OpenedAffixes,
    support_PluralizeOptions as PluralizeOptions,
    support_ReadFn as ReadFn,
    support_SecureValuePreprocessingRule as SecureValuePreprocessingRule,
    support_WalkDirCallback as WalkDirCallback,
    support_ZipCompressionOptions as ZipCompressionOptions,
    support_ZipEntry as ZipEntry,
    support_ZipOptions as ZipOptions,
    support_ZipSourceOptions as ZipSourceOptions,
    support_cancellableDelay as cancellableDelay,
    console$1 as console,
    _default as default,
    support_env as env,
    support_fs as fs,
    support_imageUtil as imageUtil,
    support_logger as logger,
    support_mjpeg as mjpeg,
    support_mkdirp as mkdirp,
    support_net as net,
    support_node as node,
    support_npm as npm,
    support_plist as plist,
    process$1 as process,
    support_system as system,
    support_tempDir as tempDir,
    support_timing as timing,
    support_util as util,
    support_zip as zip,
  };
}

declare const UIAUTOMATOR2_CONSTRAINTS: {
    readonly platformName: {
        readonly isString: true;
        readonly inclusionCaseInsensitive: readonly ["Android"];
        readonly presence: true;
    };
    readonly deviceName: {
        readonly isString: true;
    };
    readonly appActivity: {
        readonly isString: true;
    };
    readonly appPackage: {
        readonly isString: true;
    };
    readonly appWaitActivity: {
        readonly isString: true;
    };
    readonly appWaitPackage: {
        readonly isString: true;
    };
    readonly appWaitDuration: {
        readonly isNumber: true;
    };
    readonly deviceReadyTimeout: {
        readonly isNumber: true;
    };
    readonly androidDeviceReadyTimeout: {
        readonly isNumber: true;
    };
    readonly androidDeviceSocket: {
        readonly isString: true;
    };
    readonly androidInstallTimeout: {
        readonly isNumber: true;
    };
    readonly adbPort: {
        readonly isNumber: true;
    };
    readonly remoteAdbHost: {
        readonly isString: true;
    };
    readonly adbExecTimeout: {
        readonly isNumber: true;
    };
    readonly avd: {
        readonly isString: true;
    };
    readonly avdLaunchTimeout: {
        readonly isNumber: true;
    };
    readonly avdReadyTimeout: {
        readonly isNumber: true;
    };
    readonly avdArgs: {};
    readonly avdEnv: {
        readonly isObject: true;
    };
    readonly useKeystore: {
        readonly isBoolean: true;
    };
    readonly keystorePath: {
        readonly isString: true;
    };
    readonly keystorePassword: {
        readonly isString: true;
    };
    readonly keyAlias: {
        readonly isString: true;
    };
    readonly keyPassword: {
        readonly isString: true;
    };
    readonly webviewDevtoolsPort: {
        readonly isNumber: true;
    };
    readonly ensureWebviewsHavePages: {
        readonly isBoolean: true;
    };
    readonly enableWebviewDetailsCollection: {
        readonly isBoolean: true;
    };
    readonly chromedriverPort: {
        readonly isNumber: true;
    };
    readonly chromedriverPorts: {
        readonly isArray: true;
    };
    readonly chromedriverArgs: {
        readonly isObject: true;
    };
    readonly chromedriverExecutable: {
        readonly isString: true;
    };
    readonly chromedriverExecutableDir: {
        readonly isString: true;
    };
    readonly chromedriverChromeMappingFile: {
        readonly isString: true;
    };
    readonly chromedriverUseSystemExecutable: {
        readonly isBoolean: true;
    };
    readonly chromedriverDisableBuildCheck: {
        readonly isBoolean: true;
    };
    readonly chromeLoggingPrefs: {
        readonly isObject: true;
    };
    readonly autoWebviewTimeout: {
        readonly isNumber: true;
    };
    readonly autoWebviewName: {
        readonly isString: true;
    };
    readonly intentAction: {
        readonly isString: true;
    };
    readonly intentCategory: {
        readonly isString: true;
    };
    readonly intentFlags: {
        readonly isString: true;
    };
    readonly optionalIntentArguments: {
        readonly isString: true;
    };
    readonly dontStopAppOnReset: {
        readonly isBoolean: true;
    };
    readonly unicodeKeyboard: {
        readonly isBoolean: true;
    };
    readonly hideKeyboard: {
        readonly isBoolean: true;
    };
    readonly noSign: {
        readonly isBoolean: true;
    };
    readonly recreateChromeDriverSessions: {
        readonly isBoolean: false;
    };
    readonly autoLaunch: {
        readonly isBoolean: true;
    };
    readonly nativeWebScreenshot: {
        readonly isBoolean: true;
    };
    readonly clearSystemFiles: {
        readonly isBoolean: true;
    };
    readonly extractChromeAndroidPackageFromContextName: {
        readonly isBoolean: true;
    };
    readonly autoGrantPermissions: {
        readonly isBoolean: true;
    };
    readonly networkSpeed: {
        readonly isString: true;
    };
    readonly gpsEnabled: {
        readonly isBoolean: true;
    };
    readonly isHeadless: {
        readonly isBoolean: true;
    };
    readonly showChromedriverLog: {
        readonly isBoolean: true;
    };
    readonly skipUnlock: {
        readonly isBoolean: true;
    };
    readonly clearDeviceLogsOnStart: {
        readonly isBoolean: true;
    };
    readonly unlockType: {
        readonly isString: true;
    };
    readonly unlockKey: {
        readonly isString: true;
    };
    readonly unlockStrategy: {
        readonly isString: true;
        readonly inclusionCaseInsensitive: readonly ["locksettings", "uiautomator"];
    };
    readonly otherApps: {
        readonly isString: true;
    };
    readonly uninstallOtherPackages: {
        readonly isString: true;
    };
    readonly allowTestPackages: {
        readonly isBoolean: true;
    };
    readonly pageLoadStrategy: {
        readonly isString: true;
    };
    readonly localeScript: {
        readonly isString: true;
    };
    readonly skipDeviceInitialization: {
        readonly isBoolean: true;
    };
    readonly remoteAppsCacheLimit: {
        readonly isNumber: true;
    };
    readonly buildToolsVersion: {
        readonly isString: true;
    };
    readonly skipLogcatCapture: {
        readonly isBoolean: true;
    };
    readonly chromeOptions: {
        readonly isObject: true;
    };
    readonly enablePerformanceLogging: {
        readonly isBoolean: true;
    };
    readonly userProfile: {
        readonly isNumber: true;
    };
    readonly browserName: {
        readonly isString: true;
    };
    readonly enforceAppInstall: {
        readonly isBoolean: true;
    };
    readonly suppressKillServer: {
        readonly isBoolean: true;
    };
    readonly allowOfflineDevices: {
        readonly isBoolean: true;
    };
    readonly ignoreHiddenApiPolicyError: {
        readonly isBoolean: true;
    };
    readonly unlockSuccessTimeout: {
        readonly isNumber: true;
    };
    readonly mockLocationApp: {
        readonly isString: true;
    };
    readonly logcatFormat: {
        readonly isString: true;
    };
    readonly logcatFilterSpecs: {
        readonly isArray: true;
    };
    readonly allowDelayAdb: {
        readonly isBoolean: true;
    };
    readonly ignoreUnimportantViews: {
        readonly isBoolean: true;
    };
    readonly disableWindowAnimation: {
        readonly isBoolean: true;
    };
    readonly appWaitForLaunch: {
        readonly isBoolean: true;
    };
    readonly launchTimeout: {
        readonly isNumber: true;
    };
    readonly uiautomator2ServerLaunchTimeout: {
        readonly isNumber: true;
    };
    readonly uiautomator2ServerInstallTimeout: {
        readonly isNumber: true;
    };
    readonly uiautomator2ServerReadTimeout: {
        readonly isNumber: true;
    };
    readonly systemPort: {
        readonly isNumber: true;
    };
    readonly mjpegServerPort: {
        readonly isNumber: true;
    };
    readonly mjpegScreenshotUrl: {
        readonly isString: true;
    };
    readonly skipServerInstallation: {
        readonly isBoolean: true;
    };
    readonly disableSuppressAccessibilityService: {
        readonly isBoolean: true;
    };
    readonly forceAppLaunch: {
        readonly isBoolean: true;
    };
    readonly shouldTerminateApp: {
        readonly isBoolean: true;
    };
};

type Uiautomator2Constraints = typeof UIAUTOMATOR2_CONSTRAINTS;

type RelativeRect = Pick<Rect, 'width' | 'height'> & {
    left: Rect['x'];
    top: Rect['y'];
};

type Uiautomator2DriverOpts = DriverOpts<Uiautomator2Constraints>;
type Uiautomator2DriverCaps = DriverCaps<Uiautomator2Constraints>;
type W3CUiautomator2DriverCaps = W3CDriverCaps<Uiautomator2Constraints>;
interface Uiautomator2SessionInfo {
    deviceName: string;
    deviceUDID: string;
}
interface Uiautomator2DeviceDetails {
    pixelRatio: string;
    statBarHeight: number;
    viewportRect: RelativeRect;
    deviceApiLevel: number;
    deviceScreenSize: string;
    deviceScreenDensity: string;
    deviceModel: string;
    deviceManufacturer: string;
    platformVersion: string;
}
interface Uiautomator2ServerInfo {
    platform: 'LINUX';
    webStorageEnabled: false;
    takesScreenshot: true;
    javascriptEnabled: true;
    databaseEnabled: false;
    networkConnectionEnabled: true;
    locationContextEnabled: false;
    warnings: EmptyObject;
    desired: Uiautomator2DriverCaps;
}
interface Uiautomator2StartSessionOpts extends Uiautomator2DriverCaps, Uiautomator2ServerInfo {
}
interface Uiautomator2SessionCaps extends Uiautomator2ServerInfo, Uiautomator2SessionInfo, Partial<Uiautomator2DeviceDetails> {
}
interface Uiautomator2Settings {
    ignoreUnimportantViews: boolean;
    allowInvisibleElements: boolean;
}

type StartDetector = (stdout: string, stderr?: string | undefined) => any;
declare class SubProcess extends events {
    /**
     * @param {string} cmd
     * @param {string[]} [args]
     * @param {any} [opts]
     */
    constructor(cmd: string, args?: string[] | undefined, opts?: any);
    /**
     * @type { {stdout: string, stderr: string} }
     */
    lastLinePortion: {
        stdout: string;
        stderr: string;
    };
    /** @type {import('child_process').ChildProcess?} */
    proc: child_process.ChildProcess | null;
    /** @type {string[]} */
    args: string[];
    /**
     * @type {string}
     */
    cmd: string;
    /**
     * @type {any}
    */
    opts: any;
    /**
     * @type {boolean}
     */
    expectingExit: boolean;
    /**
     * @type {string}
     */
    rep: string;
    get isRunning(): boolean;
    /**
     *
     * @param {string} stream
     * @param {Iterable<string>} lines
     */
    emitLines(stream: string, lines: Iterable<string>): void;
    /**
     *
     * @param {StartDetector|number?} startDetector
     * @param {number?} timeoutMs
     * @param {boolean} detach
     * @returns {Promise<void>}
     */
    start(startDetector?: StartDetector | (number | null), timeoutMs?: number | null, detach?: boolean): Promise<void>;
    handleLastLines(): void;
    /**
     *
     * @param {NodeJS.Signals} signal
     * @param {number} timeout
     * @returns {Promise<void>}
     */
    stop(signal?: NodeJS.Signals, timeout?: number): Promise<void>;
    join(allowedExitCodes?: number[]): Promise<any>;
    detachProcess(): void;
    get pid(): number | null | undefined;
}

type LogcatOpts = {
    /**
     * The log print format, where <format> is one of:
     * brief process tag thread raw time threadtime long
     * `threadtime` is the default value.
     */
    format?: string | undefined;
    /**
     * Series of `<tag>[:priority]`
     * where `<tag>` is a log component tag (or `*` for all) and priority is:
     * V    Verbose
     * D    Debug
     * I    Info
     * W    Warn
     * E    Error
     * F    Fatal
     * S    Silent (supress all output)
     *
     * `'*'` means `'*:d'` and `<tag>` by itself means `<tag>:v`
     *
     * If not specified on the commandline, filterspec is set from `ANDROID_LOG_TAGS`.
     * If no filterspec is found, filter defaults to `'*:I'`
     */
    filterSpecs?: string[] | undefined;
};
declare class Logcat extends EventEmitter {
    constructor(opts?: {});
    adb: any;
    clearLogs: any;
    debug: any;
    debugTrace: any;
    maxBufferSize: any;
    logs: any[];
    logIdxSinceLastRequest: number;
    startCapture(opts?: {}): Promise<any>;
    proc: SubProcess$2 | null | undefined;
    outputHandler(output: any, prefix?: string): void;
    stopCapture(): Promise<void>;
    getLogs(): any[];
    getAllLogs(): any[];
    clear(): Promise<void>;
}

interface ADBOptions {
    sdkRoot?: string;
    udid?: string;
    appDeviceReadyTimeout?: number;
    useKeystore?: boolean;
    keystorePath?: string;
    keystorePassword?: string;
    keyAlias?: string;
    keyPassword?: string;
    executable: ADBExecutable;
    tmpDir?: string;
    curDeviceId?: string;
    emulatorPort?: number;
    logcat?: Logcat;
    suppressKillServer?: boolean;
    adbPort?: number;
    adbHost?: string;
    adbExecTimeout?: number;
    remoteAppsCacheLimit?: number;
    buildToolsVersion?: string;
    allowOfflineDevices?: boolean;
    allowDelayAdb?: boolean;
    remoteAdbHost?: string;
    remoteAdbPort?: number;
    clearDeviceLogsOnStart?: boolean;
}
interface ADBExecutable {
    path: string;
    defaultArgs: string[];
}

/**
 * @module LRUCache
 */
declare const TYPE: unique symbol;
type Index = number & {
    [TYPE]: 'LRUCache Index';
};
type UintArray = Uint8Array | Uint16Array | Uint32Array;
type NumberArray = UintArray | number[];
declare class ZeroArray extends Array<number> {
    constructor(size: number);
}

type StackLike = Stack | Index[];
declare class Stack {
    #private;
    heap: NumberArray;
    length: number;
    static create(max: number): StackLike;
    constructor(max: number, HeapCls: {
        new (n: number): NumberArray;
    });
    push(n: Index): void;
    pop(): Index;
}
/**
 * Promise representing an in-progress {@link LRUCache#fetch} call
 */
type BackgroundFetch<V> = Promise<V | undefined> & {
    __returned: BackgroundFetch<V> | undefined;
    __abortController: AbortController;
    __staleWhileFetching: V | undefined;
};
declare namespace LRUCache {
    /**
     * An integer greater than 0, reflecting the calculated size of items
     */
    type Size = number;
    /**
     * Integer greater than 0, representing some number of milliseconds, or the
     * time at which a TTL started counting from.
     */
    type Milliseconds = number;
    /**
     * An integer greater than 0, reflecting a number of items
     */
    type Count = number;
    /**
     * The reason why an item was removed from the cache, passed
     * to the {@link Disposer} methods.
     */
    type DisposeReason = 'evict' | 'set' | 'delete';
    /**
     * A method called upon item removal, passed as the
     * {@link OptionsBase.dispose} and/or
     * {@link OptionsBase.disposeAfter} options.
     */
    type Disposer<K, V> = (value: V, key: K, reason: DisposeReason) => void;
    /**
     * A function that returns the effective calculated size
     * of an entry in the cache.
     */
    type SizeCalculator<K, V> = (value: V, key: K) => Size;
    /**
     * Options provided to the
     * {@link OptionsBase.fetchMethod} function.
     */
    interface FetcherOptions<K, V, FC = unknown> {
        signal: AbortSignal;
        options: FetcherFetchOptions<K, V, FC>;
        /**
         * Object provided in the {@link FetchOptions.context} option to
         * {@link LRUCache#fetch}
         */
        context: FC;
    }
    /**
     * Status object that may be passed to {@link LRUCache#fetch},
     * {@link LRUCache#get}, {@link LRUCache#set}, and {@link LRUCache#has}.
     */
    interface Status<V> {
        /**
         * The status of a set() operation.
         *
         * - add: the item was not found in the cache, and was added
         * - update: the item was in the cache, with the same value provided
         * - replace: the item was in the cache, and replaced
         * - miss: the item was not added to the cache for some reason
         */
        set?: 'add' | 'update' | 'replace' | 'miss';
        /**
         * the ttl stored for the item, or undefined if ttls are not used.
         */
        ttl?: Milliseconds;
        /**
         * the start time for the item, or undefined if ttls are not used.
         */
        start?: Milliseconds;
        /**
         * The timestamp used for TTL calculation
         */
        now?: Milliseconds;
        /**
         * the remaining ttl for the item, or undefined if ttls are not used.
         */
        remainingTTL?: Milliseconds;
        /**
         * The calculated size for the item, if sizes are used.
         */
        entrySize?: Size;
        /**
         * The total calculated size of the cache, if sizes are used.
         */
        totalCalculatedSize?: Size;
        /**
         * A flag indicating that the item was not stored, due to exceeding the
         * {@link OptionsBase.maxEntrySize}
         */
        maxEntrySizeExceeded?: true;
        /**
         * The old value, specified in the case of `set:'update'` or
         * `set:'replace'`
         */
        oldValue?: V;
        /**
         * The results of a {@link LRUCache#has} operation
         *
         * - hit: the item was found in the cache
         * - stale: the item was found in the cache, but is stale
         * - miss: the item was not found in the cache
         */
        has?: 'hit' | 'stale' | 'miss';
        /**
         * The status of a {@link LRUCache#fetch} operation.
         * Note that this can change as the underlying fetch() moves through
         * various states.
         *
         * - inflight: there is another fetch() for this key which is in process
         * - get: there is no fetchMethod, so {@link LRUCache#get} was called.
         * - miss: the item is not in cache, and will be fetched.
         * - hit: the item is in the cache, and was resolved immediately.
         * - stale: the item is in the cache, but stale.
         * - refresh: the item is in the cache, and not stale, but
         *   {@link FetchOptions.forceRefresh} was specified.
         */
        fetch?: 'get' | 'inflight' | 'miss' | 'hit' | 'stale' | 'refresh';
        /**
         * The {@link OptionsBase.fetchMethod} was called
         */
        fetchDispatched?: true;
        /**
         * The cached value was updated after a successful call to
         * {@link OptionsBase.fetchMethod}
         */
        fetchUpdated?: true;
        /**
         * The reason for a fetch() rejection.  Either the error raised by the
         * {@link OptionsBase.fetchMethod}, or the reason for an
         * AbortSignal.
         */
        fetchError?: Error;
        /**
         * The fetch received an abort signal
         */
        fetchAborted?: true;
        /**
         * The abort signal received was ignored, and the fetch was allowed to
         * continue.
         */
        fetchAbortIgnored?: true;
        /**
         * The fetchMethod promise resolved successfully
         */
        fetchResolved?: true;
        /**
         * The fetchMethod promise was rejected
         */
        fetchRejected?: true;
        /**
         * The status of a {@link LRUCache#get} operation.
         *
         * - fetching: The item is currently being fetched.  If a previous value
         *   is present and allowed, that will be returned.
         * - stale: The item is in the cache, and is stale.
         * - hit: the item is in the cache
         * - miss: the item is not in the cache
         */
        get?: 'stale' | 'hit' | 'miss';
        /**
         * A fetch or get operation returned a stale value.
         */
        returnedStale?: true;
    }
    /**
     * options which override the options set in the LRUCache constructor
     * when calling {@link LRUCache#fetch}.
     *
     * This is the union of {@link GetOptions} and {@link SetOptions}, plus
     * {@link OptionsBase.noDeleteOnFetchRejection},
     * {@link OptionsBase.allowStaleOnFetchRejection},
     * {@link FetchOptions.forceRefresh}, and
     * {@link FetcherOptions.context}
     *
     * Any of these may be modified in the {@link OptionsBase.fetchMethod}
     * function, but the {@link GetOptions} fields will of course have no
     * effect, as the {@link LRUCache#get} call already happened by the time
     * the fetchMethod is called.
     */
    interface FetcherFetchOptions<K, V, FC = unknown> extends Pick<OptionsBase<K, V, FC>, 'allowStale' | 'updateAgeOnGet' | 'noDeleteOnStaleGet' | 'sizeCalculation' | 'ttl' | 'noDisposeOnSet' | 'noUpdateTTL' | 'noDeleteOnFetchRejection' | 'allowStaleOnFetchRejection' | 'ignoreFetchAbort' | 'allowStaleOnFetchAbort'> {
        status?: Status<V>;
        size?: Size;
    }
    /**
     * Options that may be passed to the {@link LRUCache#fetch} method.
     */
    interface FetchOptions<K, V, FC> extends FetcherFetchOptions<K, V, FC> {
        /**
         * Set to true to force a re-load of the existing data, even if it
         * is not yet stale.
         */
        forceRefresh?: boolean;
        /**
         * Context provided to the {@link OptionsBase.fetchMethod} as
         * the {@link FetcherOptions.context} param.
         *
         * If the FC type is specified as unknown (the default),
         * undefined or void, then this is optional.  Otherwise, it will
         * be required.
         */
        context?: FC;
        signal?: AbortSignal;
        status?: Status<V>;
    }
    /**
     * Options provided to {@link LRUCache#fetch} when the FC type is something
     * other than `unknown`, `undefined`, or `void`
     */
    interface FetchOptionsWithContext<K, V, FC> extends FetchOptions<K, V, FC> {
        context: FC;
    }
    /**
     * Options provided to {@link LRUCache#fetch} when the FC type is
     * `undefined` or `void`
     */
    interface FetchOptionsNoContext<K, V> extends FetchOptions<K, V, undefined> {
        context?: undefined;
    }
    /**
     * Options that may be passed to the {@link LRUCache#has} method.
     */
    interface HasOptions<K, V, FC> extends Pick<OptionsBase<K, V, FC>, 'updateAgeOnHas'> {
        status?: Status<V>;
    }
    /**
     * Options that may be passed to the {@link LRUCache#get} method.
     */
    interface GetOptions<K, V, FC> extends Pick<OptionsBase<K, V, FC>, 'allowStale' | 'updateAgeOnGet' | 'noDeleteOnStaleGet'> {
        status?: Status<V>;
    }
    /**
     * Options that may be passed to the {@link LRUCache#peek} method.
     */
    interface PeekOptions<K, V, FC> extends Pick<OptionsBase<K, V, FC>, 'allowStale'> {
    }
    /**
     * Options that may be passed to the {@link LRUCache#set} method.
     */
    interface SetOptions<K, V, FC> extends Pick<OptionsBase<K, V, FC>, 'sizeCalculation' | 'ttl' | 'noDisposeOnSet' | 'noUpdateTTL'> {
        /**
         * If size tracking is enabled, then setting an explicit size
         * in the {@link LRUCache#set} call will prevent calling the
         * {@link OptionsBase.sizeCalculation} function.
         */
        size?: Size;
        /**
         * If TTL tracking is enabled, then setting an explicit start
         * time in the {@link LRUCache#set} call will override the
         * default time from `performance.now()` or `Date.now()`.
         *
         * Note that it must be a valid value for whichever time-tracking
         * method is in use.
         */
        start?: Milliseconds;
        status?: Status<V>;
    }
    /**
     * The type signature for the {@link OptionsBase.fetchMethod} option.
     */
    type Fetcher<K, V, FC = unknown> = (key: K, staleValue: V | undefined, options: FetcherOptions<K, V, FC>) => Promise<V | undefined | void> | V | undefined | void;
    /**
     * Options which may be passed to the {@link LRUCache} constructor.
     *
     * Most of these may be overridden in the various options that use
     * them.
     *
     * Despite all being technically optional, the constructor requires that
     * a cache is at minimum limited by one or more of {@link OptionsBase.max},
     * {@link OptionsBase.ttl}, or {@link OptionsBase.maxSize}.
     *
     * If {@link OptionsBase.ttl} is used alone, then it is strongly advised
     * (and in fact required by the type definitions here) that the cache
     * also set {@link OptionsBase.ttlAutopurge}, to prevent potentially
     * unbounded storage.
     */
    interface OptionsBase<K, V, FC> {
        /**
         * The maximum number of items to store in the cache before evicting
         * old entries. This is read-only on the {@link LRUCache} instance,
         * and may not be overridden.
         *
         * If set, then storage space will be pre-allocated at construction
         * time, and the cache will perform significantly faster.
         *
         * Note that significantly fewer items may be stored, if
         * {@link OptionsBase.maxSize} and/or {@link OptionsBase.ttl} are also
         * set.
         */
        max?: Count;
        /**
         * Max time in milliseconds for items to live in cache before they are
         * considered stale.  Note that stale items are NOT preemptively removed
         * by default, and MAY live in the cache long after they have expired.
         *
         * Also, as this cache is optimized for LRU/MRU operations, some of
         * the staleness/TTL checks will reduce performance, as they will incur
         * overhead by deleting items.
         *
         * Must be an integer number of ms. If set to 0, this indicates "no TTL"
         *
         * @default 0
         */
        ttl?: Milliseconds;
        /**
         * Minimum amount of time in ms in which to check for staleness.
         * Defaults to 1, which means that the current time is checked
         * at most once per millisecond.
         *
         * Set to 0 to check the current time every time staleness is tested.
         * (This reduces performance, and is theoretically unnecessary.)
         *
         * Setting this to a higher value will improve performance somewhat
         * while using ttl tracking, albeit at the expense of keeping stale
         * items around a bit longer than their TTLs would indicate.
         *
         * @default 1
         */
        ttlResolution?: Milliseconds;
        /**
         * Preemptively remove stale items from the cache.
         * Note that this may significantly degrade performance,
         * especially if the cache is storing a large number of items.
         * It is almost always best to just leave the stale items in
         * the cache, and let them fall out as new items are added.
         *
         * Note that this means that {@link OptionsBase.allowStale} is a bit
         * pointless, as stale items will be deleted almost as soon as they
         * expire.
         *
         * @default false
         */
        ttlAutopurge?: boolean;
        /**
         * Update the age of items on {@link LRUCache#get}, renewing their TTL
         *
         * Has no effect if {@link OptionsBase.ttl} is not set.
         *
         * @default false
         */
        updateAgeOnGet?: boolean;
        /**
         * Update the age of items on {@link LRUCache#has}, renewing their TTL
         *
         * Has no effect if {@link OptionsBase.ttl} is not set.
         *
         * @default false
         */
        updateAgeOnHas?: boolean;
        /**
         * Allow {@link LRUCache#get} and {@link LRUCache#fetch} calls to return
         * stale data, if available.
         */
        allowStale?: boolean;
        /**
         * Function that is called on items when they are dropped from the cache.
         * This can be handy if you want to close file descriptors or do other
         * cleanup tasks when items are no longer accessible. Called with `key,
         * value`.  It's called before actually removing the item from the
         * internal cache, so it is *NOT* safe to re-add them.
         *
         * Use {@link OptionsBase.disposeAfter} if you wish to dispose items after
         * they have been full removed, when it is safe to add them back to the
         * cache.
         */
        dispose?: Disposer<K, V>;
        /**
         * The same as {@link OptionsBase.dispose}, but called *after* the entry
         * is completely removed and the cache is once again in a clean state.
         * It is safe to add an item right back into the cache at this point.
         * However, note that it is *very* easy to inadvertently create infinite
         * recursion this way.
         */
        disposeAfter?: Disposer<K, V>;
        /**
         * Set to true to suppress calling the
         * {@link OptionsBase.dispose} function if the entry key is
         * still accessible within the cache.
         * This may be overridden by passing an options object to
         * {@link LRUCache#set}.
         */
        noDisposeOnSet?: boolean;
        /**
         * Boolean flag to tell the cache to not update the TTL when
         * setting a new value for an existing key (ie, when updating a value
         * rather than inserting a new value).  Note that the TTL value is
         * _always_ set (if provided) when adding a new entry into the cache.
         *
         * Has no effect if a {@link OptionsBase.ttl} is not set.
         */
        noUpdateTTL?: boolean;
        /**
         * If you wish to track item size, you must provide a maxSize
         * note that we still will only keep up to max *actual items*,
         * if max is set, so size tracking may cause fewer than max items
         * to be stored.  At the extreme, a single item of maxSize size
         * will cause everything else in the cache to be dropped when it
         * is added.  Use with caution!
         *
         * Note also that size tracking can negatively impact performance,
         * though for most cases, only minimally.
         */
        maxSize?: Size;
        /**
         * The maximum allowed size for any single item in the cache.
         *
         * If a larger item is passed to {@link LRUCache#set} or returned by a
         * {@link OptionsBase.fetchMethod}, then it will not be stored in the
         * cache.
         */
        maxEntrySize?: Size;
        /**
         * A function that returns a number indicating the item's size.
         *
         * If not provided, and {@link OptionsBase.maxSize} or
         * {@link OptionsBase.maxEntrySize} are set, then all
         * {@link LRUCache#set} calls **must** provide an explicit
         * {@link SetOptions.size} or sizeCalculation param.
         */
        sizeCalculation?: SizeCalculator<K, V>;
        /**
         * Method that provides the implementation for {@link LRUCache#fetch}
         */
        fetchMethod?: Fetcher<K, V, FC>;
        /**
         * Set to true to suppress the deletion of stale data when a
         * {@link OptionsBase.fetchMethod} returns a rejected promise.
         */
        noDeleteOnFetchRejection?: boolean;
        /**
         * Do not delete stale items when they are retrieved with
         * {@link LRUCache#get}.
         *
         * Note that the `get` return value will still be `undefined`
         * unless {@link OptionsBase.allowStale} is true.
         */
        noDeleteOnStaleGet?: boolean;
        /**
         * Set to true to allow returning stale data when a
         * {@link OptionsBase.fetchMethod} throws an error or returns a rejected
         * promise.
         *
         * This differs from using {@link OptionsBase.allowStale} in that stale
         * data will ONLY be returned in the case that the
         * {@link LRUCache#fetch} fails, not any other times.
         */
        allowStaleOnFetchRejection?: boolean;
        /**
         * Set to true to return a stale value from the cache when the
         * `AbortSignal` passed to the {@link OptionsBase.fetchMethod} dispatches an `'abort'`
         * event, whether user-triggered, or due to internal cache behavior.
         *
         * Unless {@link OptionsBase.ignoreFetchAbort} is also set, the underlying
         * {@link OptionsBase.fetchMethod} will still be considered canceled, and
         * any value it returns will be ignored and not cached.
         *
         * Caveat: since fetches are aborted when a new value is explicitly
         * set in the cache, this can lead to fetch returning a stale value,
         * since that was the fallback value _at the moment the `fetch()` was
         * initiated_, even though the new updated value is now present in
         * the cache.
         *
         * For example:
         *
         * ```ts
         * const cache = new LRUCache<string, any>({
         *   ttl: 100,
         *   fetchMethod: async (url, oldValue, { signal }) =>  {
         *     const res = await fetch(url, { signal })
         *     return await res.json()
         *   }
         * })
         * cache.set('https://example.com/', { some: 'data' })
         * // 100ms go by...
         * const result = cache.fetch('https://example.com/')
         * cache.set('https://example.com/', { other: 'thing' })
         * console.log(await result) // { some: 'data' }
         * console.log(cache.get('https://example.com/')) // { other: 'thing' }
         * ```
         */
        allowStaleOnFetchAbort?: boolean;
        /**
         * Set to true to ignore the `abort` event emitted by the `AbortSignal`
         * object passed to {@link OptionsBase.fetchMethod}, and still cache the
         * resulting resolution value, as long as it is not `undefined`.
         *
         * When used on its own, this means aborted {@link LRUCache#fetch} calls are not
         * immediately resolved or rejected when they are aborted, and instead
         * take the full time to await.
         *
         * When used with {@link OptionsBase.allowStaleOnFetchAbort}, aborted
         * {@link LRUCache#fetch} calls will resolve immediately to their stale
         * cached value or `undefined`, and will continue to process and eventually
         * update the cache when they resolve, as long as the resulting value is
         * not `undefined`, thus supporting a "return stale on timeout while
         * refreshing" mechanism by passing `AbortSignal.timeout(n)` as the signal.
         *
         * **Note**: regardless of this setting, an `abort` event _is still
         * emitted on the `AbortSignal` object_, so may result in invalid results
         * when passed to other underlying APIs that use AbortSignals.
         *
         * This may be overridden in the {@link OptionsBase.fetchMethod} or the
         * call to {@link LRUCache#fetch}.
         */
        ignoreFetchAbort?: boolean;
    }
    interface OptionsMaxLimit<K, V, FC> extends OptionsBase<K, V, FC> {
        max: Count;
    }
    interface OptionsTTLLimit<K, V, FC> extends OptionsBase<K, V, FC> {
        ttl: Milliseconds;
        ttlAutopurge: boolean;
    }
    interface OptionsSizeLimit<K, V, FC> extends OptionsBase<K, V, FC> {
        maxSize: Size;
    }
    /**
     * The valid safe options for the {@link LRUCache} constructor
     */
    type Options<K, V, FC> = OptionsMaxLimit<K, V, FC> | OptionsSizeLimit<K, V, FC> | OptionsTTLLimit<K, V, FC>;
    /**
     * Entry objects used by {@link LRUCache#load} and {@link LRUCache#dump},
     * and returned by {@link LRUCache#info}.
     */
    interface Entry<V> {
        value: V;
        ttl?: Milliseconds;
        size?: Size;
        start?: Milliseconds;
    }
}
/**
 * Default export, the thing you're using this module to get.
 *
 * All properties from the options object (with the exception of
 * {@link OptionsBase.max} and {@link OptionsBase.maxSize}) are added as
 * normal public members. (`max` and `maxBase` are read-only getters.)
 * Changing any of these will alter the defaults for subsequent method calls,
 * but is otherwise safe.
 */
declare class LRUCache<K extends {}, V extends {}, FC = unknown> {
    #private;
    /**
     * {@link LRUCache.OptionsBase.ttl}
     */
    ttl: LRUCache.Milliseconds;
    /**
     * {@link LRUCache.OptionsBase.ttlResolution}
     */
    ttlResolution: LRUCache.Milliseconds;
    /**
     * {@link LRUCache.OptionsBase.ttlAutopurge}
     */
    ttlAutopurge: boolean;
    /**
     * {@link LRUCache.OptionsBase.updateAgeOnGet}
     */
    updateAgeOnGet: boolean;
    /**
     * {@link LRUCache.OptionsBase.updateAgeOnHas}
     */
    updateAgeOnHas: boolean;
    /**
     * {@link LRUCache.OptionsBase.allowStale}
     */
    allowStale: boolean;
    /**
     * {@link LRUCache.OptionsBase.noDisposeOnSet}
     */
    noDisposeOnSet: boolean;
    /**
     * {@link LRUCache.OptionsBase.noUpdateTTL}
     */
    noUpdateTTL: boolean;
    /**
     * {@link LRUCache.OptionsBase.maxEntrySize}
     */
    maxEntrySize: LRUCache.Size;
    /**
     * {@link LRUCache.OptionsBase.sizeCalculation}
     */
    sizeCalculation?: LRUCache.SizeCalculator<K, V>;
    /**
     * {@link LRUCache.OptionsBase.noDeleteOnFetchRejection}
     */
    noDeleteOnFetchRejection: boolean;
    /**
     * {@link LRUCache.OptionsBase.noDeleteOnStaleGet}
     */
    noDeleteOnStaleGet: boolean;
    /**
     * {@link LRUCache.OptionsBase.allowStaleOnFetchAbort}
     */
    allowStaleOnFetchAbort: boolean;
    /**
     * {@link LRUCache.OptionsBase.allowStaleOnFetchRejection}
     */
    allowStaleOnFetchRejection: boolean;
    /**
     * {@link LRUCache.OptionsBase.ignoreFetchAbort}
     */
    ignoreFetchAbort: boolean;
    /**
     * Do not call this method unless you need to inspect the
     * inner workings of the cache.  If anything returned by this
     * object is modified in any way, strange breakage may occur.
     *
     * These fields are private for a reason!
     *
     * @internal
     */
    static unsafeExposeInternals<K extends {}, V extends {}, FC extends unknown = unknown>(c: LRUCache<K, V, FC>): {
        starts: ZeroArray | undefined;
        ttls: ZeroArray | undefined;
        sizes: ZeroArray | undefined;
        keyMap: Map<K, number>;
        keyList: (K | undefined)[];
        valList: (V | BackgroundFetch<V> | undefined)[];
        next: NumberArray;
        prev: NumberArray;
        readonly head: Index;
        readonly tail: Index;
        free: StackLike;
        isBackgroundFetch: (p: any) => boolean;
        backgroundFetch: (k: K, index: number | undefined, options: LRUCache.FetchOptions<K, V, FC>, context: any) => BackgroundFetch<V>;
        moveToTail: (index: number) => void;
        indexes: (options?: {
            allowStale: boolean;
        }) => Generator<Index, void, unknown>;
        rindexes: (options?: {
            allowStale: boolean;
        }) => Generator<Index, void, unknown>;
        isStale: (index: number | undefined) => boolean;
    };
    /**
     * {@link LRUCache.OptionsBase.max} (read-only)
     */
    get max(): LRUCache.Count;
    /**
     * {@link LRUCache.OptionsBase.maxSize} (read-only)
     */
    get maxSize(): LRUCache.Count;
    /**
     * The total computed size of items in the cache (read-only)
     */
    get calculatedSize(): LRUCache.Size;
    /**
     * The number of items stored in the cache (read-only)
     */
    get size(): LRUCache.Count;
    /**
     * {@link LRUCache.OptionsBase.fetchMethod} (read-only)
     */
    get fetchMethod(): LRUCache.Fetcher<K, V, FC> | undefined;
    /**
     * {@link LRUCache.OptionsBase.dispose} (read-only)
     */
    get dispose(): LRUCache.Disposer<K, V> | undefined;
    /**
     * {@link LRUCache.OptionsBase.disposeAfter} (read-only)
     */
    get disposeAfter(): LRUCache.Disposer<K, V> | undefined;
    constructor(options: LRUCache.Options<K, V, FC> | LRUCache<K, V, FC>);
    /**
     * Return the remaining TTL time for a given entry key
     */
    getRemainingTTL(key: K): number;
    /**
     * Return a generator yielding `[key, value]` pairs,
     * in order from most recently used to least recently used.
     */
    entries(): Generator<(K | V | BackgroundFetch<V> | undefined)[], void, unknown>;
    /**
     * Inverse order version of {@link LRUCache.entries}
     *
     * Return a generator yielding `[key, value]` pairs,
     * in order from least recently used to most recently used.
     */
    rentries(): Generator<(K | V | BackgroundFetch<V> | undefined)[], void, unknown>;
    /**
     * Return a generator yielding the keys in the cache,
     * in order from most recently used to least recently used.
     */
    keys(): Generator<K, void, unknown>;
    /**
     * Inverse order version of {@link LRUCache.keys}
     *
     * Return a generator yielding the keys in the cache,
     * in order from least recently used to most recently used.
     */
    rkeys(): Generator<K, void, unknown>;
    /**
     * Return a generator yielding the values in the cache,
     * in order from most recently used to least recently used.
     */
    values(): Generator<V | BackgroundFetch<V> | undefined, void, unknown>;
    /**
     * Inverse order version of {@link LRUCache.values}
     *
     * Return a generator yielding the values in the cache,
     * in order from least recently used to most recently used.
     */
    rvalues(): Generator<V | BackgroundFetch<V> | undefined, void, unknown>;
    /**
     * Iterating over the cache itself yields the same results as
     * {@link LRUCache.entries}
     */
    [Symbol.iterator](): Generator<(K | V | BackgroundFetch<V> | undefined)[], void, unknown>;
    /**
     * Find a value for which the supplied fn method returns a truthy value,
     * similar to Array.find().  fn is called as fn(value, key, cache).
     */
    find(fn: (v: V, k: K, self: LRUCache<K, V, FC>) => boolean, getOptions?: LRUCache.GetOptions<K, V, FC>): V | undefined;
    /**
     * Call the supplied function on each item in the cache, in order from
     * most recently used to least recently used.  fn is called as
     * fn(value, key, cache).  Does not update age or recenty of use.
     * Does not iterate over stale values.
     */
    forEach(fn: (v: V, k: K, self: LRUCache<K, V, FC>) => any, thisp?: any): void;
    /**
     * The same as {@link LRUCache.forEach} but items are iterated over in
     * reverse order.  (ie, less recently used items are iterated over first.)
     */
    rforEach(fn: (v: V, k: K, self: LRUCache<K, V, FC>) => any, thisp?: any): void;
    /**
     * Delete any stale entries. Returns true if anything was removed,
     * false otherwise.
     */
    purgeStale(): boolean;
    /**
     * Get the extended info about a given entry, to get its value, size, and
     * TTL info simultaneously. Like {@link LRUCache#dump}, but just for a
     * single key. Always returns stale values, if their info is found in the
     * cache, so be sure to check for expired TTLs if relevant.
     */
    info(key: K): LRUCache.Entry<V> | undefined;
    /**
     * Return an array of [key, {@link LRUCache.Entry}] tuples which can be
     * passed to cache.load()
     */
    dump(): [K, LRUCache.Entry<V>][];
    /**
     * Reset the cache and load in the items in entries in the order listed.
     * Note that the shape of the resulting cache may be different if the
     * same options are not used in both caches.
     */
    load(arr: [K, LRUCache.Entry<V>][]): void;
    /**
     * Add a value to the cache.
     *
     * Note: if `undefined` is specified as a value, this is an alias for
     * {@link LRUCache#delete}
     */
    set(k: K, v: V | BackgroundFetch<V> | undefined, setOptions?: LRUCache.SetOptions<K, V, FC>): this;
    /**
     * Evict the least recently used item, returning its value or
     * `undefined` if cache is empty.
     */
    pop(): V | undefined;
    /**
     * Check if a key is in the cache, without updating the recency of use.
     * Will return false if the item is stale, even though it is technically
     * in the cache.
     *
     * Will not update item age unless
     * {@link LRUCache.OptionsBase.updateAgeOnHas} is set.
     */
    has(k: K, hasOptions?: LRUCache.HasOptions<K, V, FC>): boolean;
    /**
     * Like {@link LRUCache#get} but doesn't update recency or delete stale
     * items.
     *
     * Returns `undefined` if the item is stale, unless
     * {@link LRUCache.OptionsBase.allowStale} is set.
     */
    peek(k: K, peekOptions?: LRUCache.PeekOptions<K, V, FC>): V | undefined;
    /**
     * Make an asynchronous cached fetch using the
     * {@link LRUCache.OptionsBase.fetchMethod} function.
     *
     * If multiple fetches for the same key are issued, then they will all be
     * coalesced into a single call to fetchMethod.
     *
     * Note that this means that handling options such as
     * {@link LRUCache.OptionsBase.allowStaleOnFetchAbort},
     * {@link LRUCache.FetchOptions.signal},
     * and {@link LRUCache.OptionsBase.allowStaleOnFetchRejection} will be
     * determined by the FIRST fetch() call for a given key.
     *
     * This is a known (fixable) shortcoming which will be addresed on when
     * someone complains about it, as the fix would involve added complexity and
     * may not be worth the costs for this edge case.
     */
    fetch(k: K, fetchOptions: unknown extends FC ? LRUCache.FetchOptions<K, V, FC> : FC extends undefined | void ? LRUCache.FetchOptionsNoContext<K, V> : LRUCache.FetchOptionsWithContext<K, V, FC>): Promise<undefined | V>;
    fetch(k: unknown extends FC ? K : FC extends undefined | void ? K : never, fetchOptions?: unknown extends FC ? LRUCache.FetchOptions<K, V, FC> : FC extends undefined | void ? LRUCache.FetchOptionsNoContext<K, V> : never): Promise<undefined | V>;
    /**
     * Return a value from the cache. Will update the recency of the cache
     * entry found.
     *
     * If the key is not found, get() will return `undefined`.
     */
    get(k: K, getOptions?: LRUCache.GetOptions<K, V, FC>): V | undefined;
    /**
     * Deletes a key out of the cache.
     * Returns true if the key was deleted, false otherwise.
     */
    delete(k: K): boolean;
    /**
     * Clear the cache entirely, throwing away all values.
     */
    clear(): void;
}

declare class ADB$1 {
    adbHost?: string;
    adbPort?: number;
    _apiLevel: number | undefined;
    _logcatStartupParams: LogcatOpts | undefined;
    _doesPsSupportAOption: boolean | undefined;
    _isPgrepAvailable: boolean | undefined;
    _canPgrepUseFullCmdLineSearch: boolean | undefined;
    _isPidofAvailable: boolean | undefined;
    _memoizedFeatures: (() => Promise<string>) | undefined;
    _areExtendedLsOptionsSupported: boolean | undefined;
    remoteAppsCache: LRUCache<string, string> | undefined;
    _isLockManagementSupported: boolean | undefined;
    executable: ADBExecutable;
    constructor(opts?: Partial<ADBOptions>);
    /**
     * Create a new instance of `ADB` that inherits configuration from this `ADB` instance.
     * This avoids the need to call `ADB.createADB()` multiple times.
     * @param opts - Additional options mapping to pass to the `ADB` constructor.
     * @returns The resulting class instance.
     */
    clone(opts?: Partial<ADBOptions>): ADB$1;
    static createADB(opts: Partial<ADBOptions>): Promise<ADB$1>;
}

type ResolveActivityOptions = {
    /**
     * Whether to prefer `cmd` tool usage for
     * launchable activity name detection. It might be useful to disable it if
     * `cmd package resolve-activity` returns 'android/com.android.internal.app.ResolverActivity',
     * which means the app has no default handler set in system settings.
     * This option has no effect if the target Android version is below 24 as there
     * the corresponding `cmd` subcommand is not implemented and dumpsys usage is the only
     * possible way to detect the launchable activity name.
     */
    preferCmd?: boolean | undefined;
};
/**
 * Listener function, which accepts one argument.
 *
 * The argument is a log record object with `timestamp`, `level` and `message` properties.
 */
type LogcatListener = (record: LogcatRecord) => any;
type LogcatRecord = {
    timestamp: number;
    level: string;
    message: string;
};
type SetPropOpts = {
    /**
     * - Do we run setProp as a privileged command? Default true.
     */
    privileged?: boolean | undefined;
};
type ScreenrecordOptions = {
    /**
     * - The format is widthxheight.
     * The default value is the device's native display resolution (if supported),
     * 1280x720 if not. For best results,
     * use a size supported by your device's Advanced Video Coding (AVC) encoder.
     * For example, "1280x720"
     */
    videoSize?: string | undefined;
    /**
     * - Set it to `true` in order to display additional information on the video overlay,
     *   such as a timestamp, that is helpful in videos captured to illustrate bugs.
     *   This option is only supported since API level 27 (Android P).
     */
    bugReport?: boolean | undefined;
    /**
     * - The maximum recording time, in seconds.
     *   The default (and maximum) value is 180 (3 minutes).
     */
    timeLimit?: string | number | undefined;
    /**
     * - The video bit rate for the video, in megabits per second.
     * The default value is 4. You can increase the bit rate to improve video quality,
     * but doing so results in larger movie files.
     */
    bitRate?: string | number | undefined;
};
type ADBCommands = typeof methods;
declare namespace methods {
    /**
     * Creates chunks for the given arguments and executes them in `adb shell`.
     * This is faster than calling `adb shell` separately for each arg, however
     * there is a limit for a maximum length of a single adb command. that is why
     * we need all this complicated logic.
     *
     * @this {import('../adb.js').ADB}
     * @param {(x: string) => string[]} argTransformer A function, that receives single argument
     * from the `args` array and transforms it into a shell command. The result
     * of the function must be an array, where each item is a part of a single command.
     * The last item of the array could be ';'. If this is not a semicolon then it is going to
     * be added automatically.
     * @param {string[]} args Array of argument values to create chunks for
     * @throws {Error} If any of the chunks returns non-zero exit code after being executed
     */
    function shellChunks(this: ADB$1, argTransformer: (x: string) => string[], args: string[]): Promise<void>;
    /**
     * Get the path to adb executable amd assign it
     * to this.executable.path and this.binaries.adb properties.
     *
     * @this {import('../adb.js').ADB}
     * @return {Promise<import('../adb.js').ADB>} ADB instance.
     */
    function getAdbWithCorrectAdbPath(this: ADB$1): Promise<ADB$1>;
    /**
     * Get the full path to aapt tool and assign it to
     * this.binaries.aapt property
     * @this {import('../adb.js').ADB}
     */
    function initAapt(this: ADB$1): Promise<void>;
    /**
     * Get the full path to aapt2 tool and assign it to
     * this.binaries.aapt2 property
     * @this {import('../adb.js').ADB}
     */
    function initAapt2(this: ADB$1): Promise<void>;
    /**
     * Get the full path to zipalign tool and assign it to
     * this.binaries.zipalign property
     * @this {import('../adb.js').ADB}
     */
    function initZipAlign(this: ADB$1): Promise<void>;
    /**
     * Get the full path to bundletool binary and assign it to
     * this.binaries.bundletool property
     */
    function initBundletool(): Promise<void>;
    /**
     * Retrieve the API level of the device under test.
     *
     * @this {import('../adb.js').ADB}
     * @return {Promise<number>} The API level as integer number, for example 21 for
     *                  Android Lollipop. The result of this method is cached, so all the further
     * calls return the same value as the first one.
     */
    function getApiLevel(this: ADB$1): Promise<number>;
    /**
     * Retrieve the platform version of the device under test.
     *
     * @this {import('../adb.js').ADB}
     * @return {Promise<string>} The platform version as a string, for example '5.0' for
     * Android Lollipop.
     */
    function getPlatformVersion(this: ADB$1): Promise<string>;
    /**
     * Verify whether a device is connected.
     *
     * @this {import('../adb.js').ADB}
     * @return {Promise<boolean>} True if at least one device is visible to adb.
     */
    function isDeviceConnected(this: ADB$1): Promise<boolean>;
    /**
     * Recursively create a new folder on the device under test.
     *
     * @this {import('../adb.js').ADB}
     * @param {string} remotePath - The new path to be created.
     * @return {Promise<string>} mkdir command output.
     */
    function mkdir(this: ADB$1, remotePath: string): Promise<string>;
    /**
     * Verify whether the given argument is a
     * valid class name.
     *
     * @this {import('../adb.js').ADB}
     * @param {string} classString - The actual class name to be verified.
     * @return {boolean} The result of Regexp.exec operation
     * or _null_ if no matches are found.
     */
    function isValidClass(this: ADB$1, classString: string): boolean;
    /**
     * @typedef {Object} ResolveActivityOptions
     * @property {boolean} [preferCmd=true] Whether to prefer `cmd` tool usage for
     * launchable activity name detection. It might be useful to disable it if
     * `cmd package resolve-activity` returns 'android/com.android.internal.app.ResolverActivity',
     * which means the app has no default handler set in system settings.
     * This option has no effect if the target Android version is below 24 as there
     * the corresponding `cmd` subcommand is not implemented and dumpsys usage is the only
     * possible way to detect the launchable activity name.
     */
    /**
     * Fetches the fully qualified name of the launchable activity for the
     * given package. It is expected the package is already installed on
     * the device under test.
     *
     * @this {import('../adb.js').ADB}
     * @param {string} pkg - The target package identifier
     * @param {ResolveActivityOptions} opts
     * @return {Promise<string>} Fully qualified name of the launchable activity
     * @throws {Error} If there was an error while resolving the activity name
     */
    function resolveLaunchableActivity(this: ADB$1, pkg: string, opts?: ResolveActivityOptions): Promise<string>;
    /**
     * Force application to stop on the device under test.
     *
     * @this {import('../adb.js').ADB}
     * @param {string} pkg - The package name to be stopped.
     * @return {Promise<string>} The output of the corresponding adb command.
     */
    function forceStop(this: ADB$1, pkg: string): Promise<string>;
    /**
     * Kill application
     *
     * @this {import('../adb.js').ADB}
     * @param {string} pkg - The package name to be stopped.
     * @return {Promise<string>} The output of the corresponding adb command.
     */
    function killPackage(this: ADB$1, pkg: string): Promise<string>;
    /**
     * Clear the user data of the particular application on the device
     * under test.
     *
     * @this {import('../adb.js').ADB}
     * @param {string} pkg - The package name to be cleared.
     * @return {Promise<string>} The output of the corresponding adb command.
     */
    function clear(this: ADB$1, pkg: string): Promise<string>;
    /**
     * Grant all permissions requested by the particular package.
     * This method is only useful on Android 6.0+ and for applications
     * that support components-based permissions setting.
     *
     * @this {import('../adb.js').ADB}
     * @param {string} pkg - The package name to be processed.
     * @param {string} [apk] - The path to the actual apk file.
     * @throws {Error} If there was an error while granting permissions
     */
    function grantAllPermissions(this: ADB$1, pkg: string, apk?: string | undefined): Promise<void>;
    /**
     * Grant multiple permissions for the particular package.
     * This call is more performant than `grantPermission` one, since it combines
     * multiple `adb shell` calls into a single command.
     *
     * @this {import('../adb.js').ADB}
     * @param {string} pkg - The package name to be processed.
     * @param {Array<string>} permissions - The list of permissions to be granted.
     * @throws {Error} If there was an error while changing permissions.
     */
    function grantPermissions(this: ADB$1, pkg: string, permissions: string[]): Promise<void>;
    /**
     * Grant single permission for the particular package.
     *
     * @this {import('../adb.js').ADB}
     * @param {string} pkg - The package name to be processed.
     * @param {string} permission - The full name of the permission to be granted.
     * @throws {Error} If there was an error while changing permissions.
     */
    function grantPermission(this: ADB$1, pkg: string, permission: string): Promise<void>;
    /**
     * Revoke single permission from the particular package.
     *
     * @this {import('../adb.js').ADB}
     * @param {string} pkg - The package name to be processed.
     * @param {string} permission - The full name of the permission to be revoked.
     * @throws {Error} If there was an error while changing permissions.
     */
    function revokePermission(this: ADB$1, pkg: string, permission: string): Promise<void>;
    /**
     * Retrieve the list of granted permissions for the particular package.
     *
     * @this {import('../adb.js').ADB}
     * @param {string} pkg - The package name to be processed.
     * @param {string?} [cmdOutput=null] - Optional parameter containing command output of
     * _dumpsys package_ command. It may speed up the method execution.
     * @return {Promise<string[]>} The list of granted permissions or an empty list.
     * @throws {Error} If there was an error while changing permissions.
     */
    function getGrantedPermissions(this: ADB$1, pkg: string, cmdOutput?: string | null | undefined): Promise<string[]>;
    /**
     * Retrieve the list of denied permissions for the particular package.
     *
     * @this {import('../adb.js').ADB}
     * @param {string} pkg - The package name to be processed.
     * @param {string?} [cmdOutput=null] - Optional parameter containing command output of
     * _dumpsys package_ command. It may speed up the method execution.
     * @return {Promise<string[]>} The list of denied permissions or an empty list.
     */
    function getDeniedPermissions(this: ADB$1, pkg: string, cmdOutput?: string | null | undefined): Promise<string[]>;
    /**
     * Retrieve the list of requested permissions for the particular package.
     *
     * @this {import('../adb.js').ADB}
     * @param {string} pkg - The package name to be processed.
     * @param {string?} [cmdOutput=null] - Optional parameter containing command output of
     *                                    _dumpsys package_ command. It may speed up the method execution.
     * @return {Promise<string[]>} The list of requested permissions or an empty list.
     */
    function getReqPermissions(this: ADB$1, pkg: string, cmdOutput?: string | null | undefined): Promise<string[]>;
    /**
     * Retrieve the list of location providers for the device under test.
     *
     * @this {import('../adb.js').ADB}
     * @return {Promise<string[]>} The list of available location providers or an empty list.
     */
    function getLocationProviders(this: ADB$1): Promise<string[]>;
    /**
     * Toggle the state of GPS location provider.
     *
     * @this {import('../adb.js').ADB}
     * @param {boolean} enabled - Whether to enable (true) or disable (false) the GPS provider.
     */
    function toggleGPSLocationProvider(this: ADB$1, enabled: boolean): Promise<void>;
    /**
     * Set hidden api policy to manage access to non-SDK APIs.
     * https://developer.android.com/preview/restrictions-non-sdk-interfaces
     *
     * @this {import('../adb.js').ADB}
     * @param {number|string} value - The API enforcement policy.
     *     For Android P
     *     0: Disable non-SDK API usage detection. This will also disable logging, and also break the strict mode API,
     *        detectNonSdkApiUsage(). Not recommended.
     *     1: "Just warn" - permit access to all non-SDK APIs, but keep warnings in the log.
     *        The strict mode API will keep working.
     *     2: Disallow usage of dark grey and black listed APIs.
     *     3: Disallow usage of blacklisted APIs, but allow usage of dark grey listed APIs.
     *
     *     For Android Q
     *     https://developer.android.com/preview/non-sdk-q#enable-non-sdk-access
     *     0: Disable all detection of non-SDK interfaces. Using this setting disables all log messages for non-SDK interface usage
     *        and prevents you from testing your app using the StrictMode API. This setting is not recommended.
     *     1: Enable access to all non-SDK interfaces, but print log messages with warnings for any non-SDK interface usage.
     *        Using this setting also allows you to test your app using the StrictMode API.
     *     2: Disallow usage of non-SDK interfaces that belong to either the black list
     *        or to a restricted greylist for your target API level.
     *
     * @param {boolean} [ignoreError=false] Whether to ignore an exception in 'adb shell settings put global' command
     * @throws {error} If there was an error and ignoreError was true while executing 'adb shell settings put global'
     *                 command on the device under test.
     */
    function setHiddenApiPolicy(this: ADB$1, value: string | number, ignoreError?: boolean | undefined): Promise<void>;
    /**
     * Reset access to non-SDK APIs to its default setting.
     * https://developer.android.com/preview/restrictions-non-sdk-interfaces
     *
     * @this {import('../adb.js').ADB}
     * @param {boolean} [ignoreError=false] Whether to ignore an exception in 'adb shell settings delete global' command
     * @throws {error} If there was an error and ignoreError was true while executing 'adb shell settings delete global'
     *                 command on the device under test.
     */
    function setDefaultHiddenApiPolicy(this: ADB$1, ignoreError?: boolean | undefined): Promise<void>;
    /**
     * Stop the particular package if it is running and clears its application data.
     *
     * @this {import('../adb.js').ADB}
     * @param {string} pkg - The package name to be processed.
     */
    function stopAndClear(this: ADB$1, pkg: string): Promise<void>;
    /**
     * Retrieve the list of available input methods (IMEs) for the device under test.
     *
     * @this {import('../adb.js').ADB}
     * @return {Promise<string[]>} The list of IME names or an empty list.
     */
    function availableIMEs(this: ADB$1): Promise<string[]>;
    /**
     * Retrieve the list of enabled input methods (IMEs) for the device under test.
     *
     * @this {import('../adb.js').ADB}
     * @return {Promise<string[]>} The list of enabled IME names or an empty list.
     */
    function enabledIMEs(this: ADB$1): Promise<string[]>;
    /**
     * Enable the particular input method on the device under test.
     *
     * @this {import('../adb.js').ADB}
     * @param {string} imeId - One of existing IME ids.
     */
    function enableIME(this: ADB$1, imeId: string): Promise<void>;
    /**
     * Disable the particular input method on the device under test.
     *
     * @this {import('../adb.js').ADB}
     * @param {string} imeId - One of existing IME ids.
     */
    function disableIME(this: ADB$1, imeId: string): Promise<void>;
    /**
     * Set the particular input method on the device under test.
     *
     * @this {import('../adb.js').ADB}
     * @param {string} imeId - One of existing IME ids.
     */
    function setIME(this: ADB$1, imeId: string): Promise<void>;
    /**
     * Get the default input method on the device under test.
     *
     * @this {import('../adb.js').ADB}
     * @return {Promise<string|null>} The name of the default input method
     */
    function defaultIME(this: ADB$1): Promise<string | null>;
    /**
     * Send the particular keycode to the device under test.
     *
     * @this {import('../adb.js').ADB}
     * @param {string|number} keycode - The actual key code to be sent.
     */
    function keyevent(this: ADB$1, keycode: string | number): Promise<void>;
    /**
     * Send the particular text or a number to the device under test.
     * The text gets properly escaped before being passed to ADB.
     * Noop if the text is empty.
     *
     * @this {import('../adb.js').ADB}
     * @param {string|number} text - The actual text to be sent.
     * @throws {Error} If it is impossible to escape the given string
     */
    function inputText(this: ADB$1, text: string | number): Promise<void>;
    /**
     * Clear the active text field on the device under test by sending
     * special keyevents to it.
     *
     * @this {import('../adb.js').ADB}
     * @param {number} [length=100] - The maximum length of the text in the field to be cleared.
     */
    function clearTextField(this: ADB$1, length?: number | undefined): Promise<void>;
    /**
     * Send the special keycode to the device under test in order to lock it.
     * @this {import('../adb.js').ADB}
     */
    function lock(this: ADB$1): Promise<void>;
    /**
     * Send the special keycode to the device under test in order to emulate
     * Back button tap.
     * @this {import('../adb.js').ADB}
     */
    function back(this: ADB$1): Promise<void>;
    /**
     * Send the special keycode to the device under test in order to emulate
     * Home button tap.
     * @this {import('../adb.js').ADB}
     */
    function goToHome(this: ADB$1): Promise<void>;
    /**
     * @this {import('../adb.js').ADB}
     * @return {string} the actual path to adb executable.
     */
    function getAdbPath(this: ADB$1): string;
    /**
     * Retrieve current screen orientation of the device under test.
     *
     * @this {import('../adb.js').ADB}
     * @return {Promise<number?>} The current orientation encoded as an integer number.
     */
    function getScreenOrientation(this: ADB$1): Promise<number | null>;
    /**
     * Send an arbitrary Telnet command to the device under test.
     *
     * @this {import('../adb.js').ADB}
     * @param {string} command - The command to be sent.
     * @return {Promise<string>} The actual output of the given command.
     */
    function sendTelnetCommand(this: ADB$1, command: string): Promise<string>;
    /**
     * Check the state of Airplane mode on the device under test.
     *
     * @this {import('../adb.js').ADB}
     * @return {Promise<boolean>} True if Airplane mode is enabled.
     */
    function isAirplaneModeOn(this: ADB$1): Promise<boolean>;
    /**
     * Change the state of Airplane mode in Settings on the device under test.
     *
     * @this {import('../adb.js').ADB}
     * @param {boolean} on - True to enable the Airplane mode in Settings and false to disable it.
     */
    function setAirplaneMode(this: ADB$1, on: boolean): Promise<void>;
    /**
     * Broadcast the state of Airplane mode on the device under test.
     * This method should be called after {@link #setAirplaneMode}, otherwise
     * the mode change is not going to be applied for the device.
     * ! This API requires root since Android API 24. Since API 30
     * there is a dedicated adb command to change airplane mode state, which
     * does not require to call this one afterwards.
     *
     * @this {import('../adb.js').ADB}
     * @param {boolean} on - True to broadcast enable and false to broadcast disable.
     */
    function broadcastAirplaneMode(this: ADB$1, on: boolean): Promise<void>;
    /**
     * Check the state of WiFi on the device under test.
     *
     * @this {import('../adb.js').ADB}
     * @return {Promise<boolean>} True if WiFi is enabled.
     */
    function isWifiOn(this: ADB$1): Promise<boolean>;
    /**
     * Check the state of Data transfer on the device under test.
     *
     * @this {import('../adb.js').ADB}
     * @return {Promise<boolean>} True if Data transfer is enabled.
     */
    function isDataOn(this: ADB$1): Promise<boolean>;
    /**
     * Change the state of WiFi and/or Data transfer on the device under test.
     *
     * @this {import('../adb.js').ADB}
     * @param {{wifi?: boolean, data?: boolean}} opts - True to enable wifi and data transfer
     * @param {boolean} [isEmulator] - Set it to true if the device under test is an emulator rather than a real device.
     */
    function setWifiAndData(this: ADB$1, { wifi, data }: {
        wifi?: boolean | undefined;
        data?: boolean | undefined;
    }, isEmulator?: boolean | undefined): Promise<void>;
    /**
     * Check the state of animation on the device under test.
     *
     * @this {import('../adb.js').ADB}
     * @return {Promise<boolean>} True if at least one of animation scale settings
     *                   is not equal to '0.0'.
     */
    function isAnimationOn(this: ADB$1): Promise<boolean>;
    /**
     * Forcefully recursively remove a path on the device under test.
     * Be careful while calling this method.
     *
     * @this {import('../adb.js').ADB}
     * @param {string} path - The path to be removed recursively.
     */
    function rimraf(this: ADB$1, path: string): Promise<void>;
    /**
     * Send a file to the device under test.
     *
     * @this {import('../adb.js').ADB}
     * @param {string} localPath - The path to the file on the local file system.
     * @param {string} remotePath - The destination path on the remote device.
     * @param {object} [opts] - Additional options mapping. See
     *                        _exec_ method options, for more information about available
     *                        options.
     */
    function push(this: ADB$1, localPath: string, remotePath: string, opts?: any): Promise<void>;
    /**
     * Receive a file from the device under test.
     *
     * @this {import('../adb.js').ADB}
     * @param {string} remotePath - The source path on the remote device.
     * @param {string} localPath - The destination path to the file on the local file system.
     * @param {import('ait-process').AITProcessExecOptions} [opts={}] - Additional options mapping. See
     * _exec_ method options, for more information about available
     * options.
     */
    function pull(this: ADB$1, remotePath: string, localPath: string, opts?: AITProcessExecOptions | undefined): Promise<void>;
    /**
     * Check whether the process with the particular name is running on the device
     * under test.
     *
     * @this {import('../adb.js').ADB}
     * @param {string} processName - The name of the process to be checked.
     * @return {Promise<boolean>} True if the given process is running.
     * @throws {Error} If the given process name is not a valid class name.
     */
    function processExists(this: ADB$1, processName: string): Promise<boolean>;
    /**
     * Get TCP port forwarding with adb on the device under test.
     *
     * @this {import('../adb.js').ADB}
     * @return {Promise<string[]>} The output of the corresponding adb command.
     * An array contains each forwarding line of output
     */
    function getForwardList(this: ADB$1): Promise<string[]>;
    /**
     * Setup TCP port forwarding with adb on the device under test.
     *
     * @this {import('../adb.js').ADB}
     * @param {string|number} systemPort - The number of the local system port.
     * @param {string|number} devicePort - The number of the remote device port.
     */
    function forwardPort(this: ADB$1, systemPort: string | number, devicePort: string | number): Promise<void>;
    /**
     * Remove TCP port forwarding with adb on the device under test. The forwarding
     * for the given port should be setup with {@link #forwardPort} first.
     *
     * @this {import('../adb.js').ADB}
     * @param {string|number} systemPort - The number of the local system port
     *                                     to remove forwarding on.
     */
    function removePortForward(this: ADB$1, systemPort: string | number): Promise<void>;
    /**
     * Get TCP port forwarding with adb on the device under test.
     *
     * @this {import('../adb.js').ADB}
     * @return {Promise<string[]>} The output of the corresponding adb command.
     * An array contains each forwarding line of output
     */
    function getReverseList(this: ADB$1): Promise<string[]>;
    /**
     * Setup TCP port forwarding with adb on the device under test.
     * Only available for API 21+.
     *
     * @this {import('../adb.js').ADB}
     * @param {string|number} devicePort - The number of the remote device port.
     * @param {string|number} systemPort - The number of the local system port.
     */
    function reversePort(this: ADB$1, devicePort: string | number, systemPort: string | number): Promise<void>;
    /**
     * Remove TCP port forwarding with adb on the device under test. The forwarding
     * for the given port should be setup with {@link #forwardPort} first.
     *
     * @this {import('../adb.js').ADB}
     * @param {string|number} devicePort - The number of the remote device port
     *                                     to remove forwarding on.
     */
    function removePortReverse(this: ADB$1, devicePort: string | number): Promise<void>;
    /**
     * Setup TCP port forwarding with adb on the device under test. The difference
     * between {@link #forwardPort} is that this method does setup for an abstract
     * local port.
     *
     * @this {import('../adb.js').ADB}
     * @param {string|number} systemPort - The number of the local system port.
     * @param {string|number} devicePort - The number of the remote device port.
     */
    function forwardAbstractPort(this: ADB$1, systemPort: string | number, devicePort: string | number): Promise<void>;
    /**
     * Execute ping shell command on the device under test.
     *
     * @this {import('../adb.js').ADB}
     * @return {Promise<boolean>} True if the command output contains 'ping' substring.
     * @throws {Error} If there was an error while executing 'ping' command on the
     *                 device under test.
     */
    function ping(this: ADB$1): Promise<boolean>;
    /**
     * Restart the device under test using adb commands.
     *
     * @this {import('../adb.js').ADB}
     * @throws {Error} If start fails.
     */
    function restart(this: ADB$1): Promise<void>;
    /**
     * Start the logcat process to gather logs.
     *
     * @this {import('../adb.js').ADB}
     * @param {import('../logcat.js').LogcatOpts} [opts={}]
     * @throws {Error} If restart fails.
     */
    function startLogcat(this: ADB$1, opts?: LogcatOpts | undefined): Promise<void>;
    /**
     * Stop the active logcat process which gathers logs.
     * The call will be ignored if no logcat process is running.
     * @this {import('../adb.js').ADB}
     */
    function stopLogcat(this: ADB$1): Promise<void>;
    /**
     * Retrieve the output from the currently running logcat process.
     * The logcat process should be executed by {2link #startLogcat} method.
     *
     * @this {import('../adb.js').ADB}
     * @return {string[]} The collected logcat output.
     * @throws {Error} If logcat process is not running.
     */
    function getLogcatLogs(this: ADB$1): string[];
    /**
     * Listener function, which accepts one argument.
     *
     * The argument is a log record object with `timestamp`, `level` and `message` properties.
     * @callback LogcatListener
     * @param {LogcatRecord} record
     */
    /**
     * @typedef LogcatRecord
     * @property {number} timestamp
     * @property {string} level
     * @property {string} message
     */
    /**
     * Set the callback for the logcat output event.
     *
     * @this {import('../adb.js').ADB}
     * @param {LogcatListener} listener - Listener function
     * @throws {Error} If logcat process is not running.
     */
    function setLogcatListener(this: ADB$1, listener: LogcatListener): void;
    /**
     * Removes the previously set callback for the logcat output event.
     *
     * @this {import('../adb.js').ADB}
     * @param {LogcatListener} listener - The listener function, which has been previously
     *                              passed to `setLogcatListener`
     * @throws {Error} If logcat process is not running.
     */
    function removeLogcatListener(this: ADB$1, listener: LogcatListener): void;
    /**
     * At some point of time Google has changed the default `ps` behaviour, so it only
     * lists processes that belong to the current shell user rather to all
     * users. It is necessary to execute ps with -A command line argument
     * to mimic the previous behaviour.
     *
     * @this {import('../adb.js').ADB}
     * @returns {Promise<string>} the output of `ps` command where all processes are included
     */
    function listProcessStatus(this: ADB$1): Promise<string>;
    /**
     * Returns process name for the given process identifier
     *
     * @this {import('../adb.js').ADB}
     * @param {string|number} pid - The valid process identifier
     * @throws {Error} If the given PID is either invalid or is not present
     * in the active processes list
     * @returns {Promise<string>} The process name
     */
    function getNameByPid(this: ADB$1, pid: string | number): Promise<string>;
    /**
     * Get the list of process ids for the particular process on the device under test.
     *
     * @this {import('../adb.js').ADB}
     * @param {string} name - The part of process name.
     * @return {Promise<number[]>} The list of matched process IDs or an empty list.
     * @throws {Error} If the passed process name is not a valid one
     */
    function getPIDsByName(this: ADB$1, name: string): Promise<number[]>;
    /**
     * Get the list of process ids for the particular process on the device under test.
     *
     * @this {import('../adb.js').ADB}
     * @param {string} name - The part of process name.
     */
    function killProcessesByName(this: ADB$1, name: string): Promise<void>;
    /**
     * Kill the particular process on the device under test.
     * The current user is automatically switched to root if necessary in order
     * to properly kill the process.
     *
     * @this {import('../adb.js').ADB}
     * @param {string|number} pid - The ID of the process to be killed.
     * @throws {Error} If the process cannot be killed.
     */
    function killProcessByPID(this: ADB$1, pid: string | number): Promise<void>;
    /**
     * Broadcast process killing on the device under test.
     *
     * @this {import('../adb.js').ADB}
     * @param {string} intent - The name of the intent to broadcast to.
     * @param {string} processName - The name of the killed process.
     * @throws {error} If the process was not killed.
     */
    function broadcastProcessEnd(this: ADB$1, intent: string, processName: string): Promise<void>;
    /**
     * Broadcast a message to the given intent.
     *
     * @this {import('../adb.js').ADB}
     * @param {string} intent - The name of the intent to broadcast to.
     * @throws {error} If intent name is not a valid class name.
     */
    function broadcast(this: ADB$1, intent: string): Promise<void>;
    /**
     * Get the particular property of the device under test.
     *
     * @this {import('../adb.js').ADB}
     * @param {string} property - The name of the property. This name should
     *                            be known to _adb shell getprop_ tool.
     *
     * @return {Promise<string>} The value of the given property.
     */
    function getDeviceProperty(this: ADB$1, property: string): Promise<string>;
    /**
     * @typedef {Object} SetPropOpts
     * @property {boolean} [privileged=true] - Do we run setProp as a privileged command? Default true.
     */
    /**
     * Set the particular property of the device under test.
     *
     * @this {import('../adb.js').ADB}
     * @param {string} prop - The name of the property. This name should
     *                            be known to _adb shell setprop_ tool.
     * @param {string} val - The new property value.
     * @param {SetPropOpts} [opts={}]
     *
     * @throws {error} If _setprop_ utility fails to change property value.
     */
    function setDeviceProperty(this: ADB$1, prop: string, val: string, opts?: SetPropOpts | undefined): Promise<void>;
    /**
     * @this {import('../adb.js').ADB}
     * @return {Promise<string>} Current system language on the device under test.
     */
    function getDeviceSysLanguage(this: ADB$1): Promise<string>;
    /**
     * @this {import('../adb.js').ADB}
     * @return {Promise<string>} Current country name on the device under test.
     */
    function getDeviceSysCountry(this: ADB$1): Promise<string>;
    /**
     * @this {import('../adb.js').ADB}
     * @return {Promise<string>} Current system locale name on the device under test.
     */
    function getDeviceSysLocale(this: ADB$1): Promise<string>;
    /**
     * @this {import('../adb.js').ADB}
     * @return {Promise<string>} Current product language name on the device under test.
     */
    function getDeviceProductLanguage(this: ADB$1): Promise<string>;
    /**
     * @this {import('../adb.js').ADB}
     * @return {Promise<string>} Current product country name on the device under test.
     */
    function getDeviceProductCountry(this: ADB$1): Promise<string>;
    /**
     * @this {import('../adb.js').ADB}
     * @return {Promise<string>} Current product locale name on the device under test.
     */
    function getDeviceProductLocale(this: ADB$1): Promise<string>;
    /**
     * @this {import('../adb.js').ADB}
     * @return {Promise<string>} The model name of the device under test.
     */
    function getModel(this: ADB$1): Promise<string>;
    /**
     * @this {import('../adb.js').ADB}
     * @return {Promise<string>} The manufacturer name of the device under test.
     */
    function getManufacturer(this: ADB$1): Promise<string>;
    /**
     * Get the current screen size.
     *
     * @this {import('../adb.js').ADB}
     * @return {Promise<string?>} Device screen size as string in format 'WxH' or
     * _null_ if it cannot be determined.
     */
    function getScreenSize(this: ADB$1): Promise<string | null>;
    /**
     * Get the current screen density in dpi
     *
     * @this {import('../adb.js').ADB}
     * @return {Promise<number?>} Device screen density as a number or _null_ if it
     * cannot be determined
     */
    function getScreenDensity(this: ADB$1): Promise<number | null>;
    /**
     * Setup HTTP proxy in device global settings.
     * Read https://android.googlesource.com/platform/frameworks/base/+/android-9.0.0_r21/core/java/android/provider/Settings.java for each property
     *
     * @this {import('../adb.js').ADB}
     * @param {string} proxyHost - The host name of the proxy.
     * @param {string|number} proxyPort - The port number to be set.
     */
    function setHttpProxy(this: ADB$1, proxyHost: string, proxyPort: string | number): Promise<void>;
    /**
     * Delete HTTP proxy in device global settings.
     * Rebooting the test device is necessary to apply the change.
     * @this {import('../adb.js').ADB}
     */
    function deleteHttpProxy(this: ADB$1): Promise<void>;
    /**
     * Set device property.
     * [android.provider.Settings]{@link https://developer.android.com/reference/android/provider/Settings.html}
     *
     * @this {import('../adb.js').ADB}
     * @param {string} namespace - one of {system, secure, global}, case-insensitive.
     * @param {string} setting - property name.
     * @param {string|number} value - property value.
     * @return {Promise<string>} command output.
     */
    function setSetting(this: ADB$1, namespace: string, setting: string, value: string | number): Promise<string>;
    /**
     * Get device property.
     * [android.provider.Settings]{@link https://developer.android.com/reference/android/provider/Settings.html}
     *
     * @this {import('../adb.js').ADB}
     * @param {string} namespace - one of {system, secure, global}, case-insensitive.
     * @param {string} setting - property name.
     * @return {Promise<string>} property value.
     */
    function getSetting(this: ADB$1, namespace: string, setting: string): Promise<string>;
    /**
     * Retrieve the `adb bugreport` command output. This
     * operation may take up to several minutes.
     *
     * @this {import('../adb.js').ADB}
     * @param {number} [timeout=120000] - Command timeout in milliseconds
     * @returns {Promise<string>} Command stdout
     */
    function bugreport(this: ADB$1, timeout?: number | undefined): Promise<string>;
    /**
     * @typedef {Object} ScreenrecordOptions
     * @property {string} [videoSize] - The format is widthxheight.
     *                  The default value is the device's native display resolution (if supported),
     *                  1280x720 if not. For best results,
     *                  use a size supported by your device's Advanced Video Coding (AVC) encoder.
     *                  For example, "1280x720"
     * @property {boolean} [bugReport] - Set it to `true` in order to display additional information on the video overlay,
     *                                  such as a timestamp, that is helpful in videos captured to illustrate bugs.
     *                                  This option is only supported since API level 27 (Android P).
     * @property {string|number} [timeLimit] - The maximum recording time, in seconds.
     *                                        The default (and maximum) value is 180 (3 minutes).
     * @property {string|number} [bitRate] - The video bit rate for the video, in megabits per second.
     *                The default value is 4. You can increase the bit rate to improve video quality,
     *                but doing so results in larger movie files.
     */
    /**
     * Initiate screenrecord utility on the device
     *
     * @this {import('../adb.js').ADB}
     * @param {string} destination - Full path to the writable media file destination
     *                               on the device file system.
     * @param {ScreenrecordOptions} [options={}]
     * @returns {SubProcess} screenrecord process, which can be then controlled by the client code
     */
    function screenrecord(this: ADB$1, destination: string, options?: ScreenrecordOptions | undefined): typeof SubProcess;
    /**
     * Executes the given function with the given input method context
     * and then restores the IME to the original value
     *
     * @this {import('../adb.js').ADB}
     * @param {string} ime - Valid IME identifier
     * @param {Function} fn - Function to execute
     * @returns {Promise<any>} The result of the given function
     */
    function runInImeContext(this: ADB$1, ime: string, fn: Function): Promise<any>;
    /**
     * Get tz database time zone formatted timezone
     *
     * @this {import('../adb.js').ADB}
     * @returns {Promise<string>} TZ database Time Zones format
     * @throws {Error} If any exception is reported by adb shell.
     */
    function getTimeZone(this: ADB$1): Promise<string>;
    /**
     * Retrieves the list of features supported by the device under test
     *
     * @this {import('../adb.js').ADB}
     * @returns {Promise<string[]>} the list of supported feature names or an empty list.
     * An example adb command output:
     * ```
     * cmd
     * ls_v2
     * fixed_push_mkdir
     * shell_v2
     * abb
     * stat_v2
     * apex
     * abb_exec
     * remount_shell
     * fixed_push_symlink_timestamp
     * ```
     * @throws {Error} if there was an error while retrieving the list
     */
    function listFeatures(this: ADB$1): Promise<string[]>;
    /**
     * Checks the state of streamed install feature.
     * This feature allows to speed up apk installation
     * since it does not require the original apk to be pushed to
     * the device under test first, which also saves space.
     * Although, it is required that both the device under test
     * and the adb server have the mentioned functionality.
     * See https://github.com/aosp-mirror/platform_system_core/blob/master/adb/client/adb_install.cpp
     * for more details
     *
     * @this {import('../adb.js').ADB}
     * @returns {Promise<boolean>} `true` if the feature is supported by both adb and the
     * device under test
     */
    function isStreamedInstallSupported(this: ADB$1): Promise<boolean>;
    /**
     * Checks whether incremental install feature is supported by ADB.
     * Read https://developer.android.com/preview/features#incremental
     * for more details on it.
     *
     * @this {import('../adb.js').ADB}
     * @returns {Promise<boolean>} `true` if the feature is supported by both adb and the
     * device under test
     */
    function isIncrementalInstallSupported(this: ADB$1): Promise<boolean>;
    /**
     * Retrieves the list of packages from Doze whitelist on Android 8+
     *
     * @this {import('../adb.js').ADB}
     * @returns {Promise<string[]>} The list of whitelisted packages. An example output:
     * system,com.android.shell,2000
     * system,com.google.android.cellbroadcastreceiver,10143
     * user,com.ait.armorsettings,10157
     */
    function getDeviceIdleWhitelist(this: ADB$1): Promise<string[]>;
    /**
     * Adds an existing package(s) into the Doze whitelist on Android 8+
     *
     * @this {import('../adb.js').ADB}
     * @param  {...string} packages One or more packages to add. If the package
     * already exists in the whitelist then it is only going to be added once.
     * If the package with the given name is not installed/not known then an error
     * will be thrown.
     * @returns {Promise<boolean>} `true` if the command to add package(s) has been executed
     */
    function addToDeviceIdleWhitelist(this: ADB$1, ...packages: string[]): Promise<boolean>;
    /**
     * Takes a screenshot of the given display or the default display.
     *
     * @this {import('../adb.js').ADB}
     * @param {number|string?} displayId A valid display identifier. If
     * no identifier is provided then the screenshot of the default display is returned.
     * Note that only recent Android APIs provide multi-screen support.
     * @returns {Promise<Buffer>} PNG screenshot payload
     */
    function takeScreenshot(this: ADB$1, displayId: string | number | null): Promise<Buffer>;
}

type ConnectedDevicesOptions = {
    /**
     * - Whether to get long output, which includes extra properties in each device.
     * Akin to running `adb devices -l`.
     */
    verbose?: boolean | undefined;
};
type Device = {
    /**
     * - The device udid.
     */
    udid: string;
    /**
     * - Current device state, as it is visible in
     *   _adb devices -l_ output.
     */
    state: string;
    port?: number | undefined;
};
type ExecOutputFormat$1 = 'stdout' | 'full';
type SpecialAdbExecOptions = {
    exclusive?: boolean | undefined;
};
type ShellExecOptions$1 = {
    /**
     * - the name of the corresponding Armor's timeout capability
     * (used in the error messages).
     */
    timeoutCapName?: string | undefined;
    /**
     * - command execution timeout.
     */
    timeout?: number | undefined;
    /**
     * - Whether to run the given command as root.
     */
    privileged?: boolean | undefined;
    /**
     * - Whether response should include full exec output or just stdout.
     * Potential values are full or stdout.
     */
    outputFormat?: ExecOutputFormat$1 | undefined;
};
type TFullOutputOption = {
    outputFormat: 'full';
};
type AvdLaunchOptions = {
    /**
     * Additional emulator command line arguments
     */
    args?: string | string[] | undefined;
    /**
     * Additional emulator environment variables
     */
    env?: any;
    /**
     * Emulator system language
     */
    language?: string | undefined;
    /**
     * Emulator system country
     */
    country?: string | undefined;
    /**
     * Emulator startup timeout in milliseconds
     */
    launchTimeout?: number | undefined;
    /**
     * The maximum period of time to wait until Emulator
     * is ready for usage in milliseconds
     */
    readyTimeout?: number | undefined;
    /**
     * The maximum number of startup retries
     */
    retryTimes?: number | undefined;
};
type RootResult = {
    /**
     * True if the call to root/unroot was successful
     */
    isSuccessful: boolean;
    /**
     * True if the device was already rooted
     */
    wasAlreadyRooted: boolean;
};
type SystemCalls = typeof systemCallMethods;
declare namespace systemCallMethods {
    /**
     * Retrieve full path to the given binary.
     *
     * @this {import('../adb.js').ADB}
     * @param {string} binaryName - The name of the binary.
     * @return {Promise<string>} Full path to the given binary including current SDK root.
     */
    function getSdkBinaryPath(this: ADB$1, binaryName: string): Promise<string>;
    let getBinaryNameForOS: ((binaryName: any) => string) & _.MemoizedFunction;
    /**
     * Retrieve full path to the given binary and caches it into `binaries`
     * property of the current ADB instance.
     *
     * @param {string} binaryName - Simple name of a binary file.
     * @return {Promise<string>} Full path to the given binary. The method tries
     *                  to enumerate all the known locations where the binary
     *                  might be located and stops the search as soon as the first
     *                  match is found on the local file system.
     * @throws {Error} If the binary with given name is not present at any
     *                 of known locations or Android SDK is not installed on the
     *                 local file system.
     */
    function getBinaryFromSdkRoot(binaryName: string): Promise<string>;
    /**
     * Retrieve full path to a binary file using the standard system lookup tool.
     *
     * @param {string} binaryName - The name of the binary.
     * @return {Promise<string>} Full path to the binary received from 'which'/'where'
     *                  output.
     * @throws {Error} If lookup tool returns non-zero return code.
     */
    function getBinaryFromPath(binaryName: string): Promise<string>;
    /**
     * @typedef {Object} ConnectedDevicesOptions
     * @property {boolean} [verbose] - Whether to get long output, which includes extra properties in each device.
     * Akin to running `adb devices -l`.
     */
    /**
     * @typedef {Object} Device
     * @property {string} udid - The device udid.
     * @property {string} state - Current device state, as it is visible in
     *                            _adb devices -l_ output.
     * @property {number} [port]
     */
    /**
     * @typedef {Device} VerboseDevice Additional properties returned when `verbose` is true.
     * @property {string} product - The product codename of the device, such as "razor".
     * @property {string} model - The model name of the device, such as "Nexus_7".
     * @property {string} device - The device codename, such as "flow".
     * @property {?string} usb - Represents the USB port the device is connected to, such as "1-1".
     * @property {?string} transport_id - The Transport ID for the device, such as "1".
     */
    /**
     * Retrieve the list of devices visible to adb.
     *
     * @this {import('../adb.js').ADB}
     * @param {ConnectedDevicesOptions} [opts={}] - Additional options mapping.
     * @return {Promise<Device[]>} The list of devices or an empty list if
     *                          no devices are connected.
     * @throws {Error} If there was an error while listing devices.
     */
    function getConnectedDevices(this: ADB$1, opts?: ConnectedDevicesOptions | undefined): Promise<Device[]>;
    /**
     * Retrieve the list of devices visible to adb within the given timeout.
     *
     * @this {import('../adb.js').ADB}
     * @param {number} timeoutMs - The maximum number of milliseconds to get at least
     *                             one list item.
     * @return {Promise<Device[]>} The list of connected devices.
     * @throws {Error} If no connected devices can be detected within the given timeout.
     */
    function getDevicesWithRetry(this: ADB$1, timeoutMs?: number): Promise<Device[]>;
    /**
     * Kick current connection from host/device side and make it reconnect
     *
     * @this {import('../adb.js').ADB}
     * @param {string} [target=offline] One of possible targets to reconnect:
     * offline, device or null
     * Providing `null` will cause reconnection to happen from the host side.
     *
     * @throws {Error} If either ADB version is too old and does not support this
     * command or there was a failure during reconnect.
     */
    function reconnect(this: ADB$1, target?: string | undefined): Promise<void>;
    /**
     * Restart adb server, unless _this.suppressKillServer_ property is true.
     *
     * @this {import('../adb.js').ADB}
     */
    function restartAdb(this: ADB$1): Promise<void>;
    /**
     * Kill adb server.
     * @this {import('../adb.js').ADB}
     */
    function killServer(this: ADB$1): Promise<void>;
    let resetTelnetAuthToken: (() => Promise<boolean>) & _.MemoizedFunction;
    /**
     * Execute the given emulator command using _adb emu_ tool.
     *
     * @this {import('../adb.js').ADB}
     * @param {string[]} cmd - The array of rest command line parameters.
     */
    function adbExecEmu(this: ADB$1, cmd: string[]): Promise<void>;
    let EXEC_OUTPUT_FORMAT: {
        STDOUT: 'stdout';
        FULL: 'full';
    };
    /**
     * @typedef {Object} ExecResult
     * @property {string} stdout The stdout received from exec
     * @property {string} stderr The stderr received from exec
     */
    /**
     * @typedef {Object} SpecialAdbExecOptions
     * @property {boolean} [exclusive]
     */
    /**
     * @typedef {Object} ShellExecOptions
     * @property {string} [timeoutCapName] - the name of the corresponding Armor's timeout capability
     * (used in the error messages).
     * @property {number} [timeout] - command execution timeout.
     * @property {boolean} [privileged=false] - Whether to run the given command as root.
     * @property {ExecOutputFormat} [outputFormat='stdout'] - Whether response should include full exec output or just stdout.
     * Potential values are full or stdout.
     *
     */
    /**
     * @typedef {{outputFormat: 'full'}} TFullOutputOption
     */
    /**
     * Execute the given adb command.
     *
     * @template {import('ait-process').AITProcessExecOptions & ShellExecOptions & SpecialAdbExecOptions} TExecOpts
     * @this {import('../adb.js').ADB}
     * @param {string|string[]} cmd - The array of rest command line parameters
     *                      or a single string parameter.
     * @param {TExecOpts} [opts] Additional options mapping. See
     * for more details.
     * You can also set the additional `exclusive` param
     * to `true` that assures no other parallel adb commands
     * are going to be executed while the current one is running
     * You can set the `outputFormat` param to `stdout` to receive just the stdout
     * output (default) or `full` to receive the stdout and stderr response from a
     * command with a zero exit code
     * @return {Promise<TExecOpts extends TFullOutputOption ? import('ait-process').AITProcessExecResult : string>}
     * Command's stdout or an object containing stdout and stderr.
     * @throws {Error} If the command returned non-zero exit code.
     */
    function adbExec<TExecOpts extends child_process.SpawnOptions & AITProcessProps & ShellExecOptions$1 & SpecialAdbExecOptions>(this: ADB$1, cmd: string | string[], opts?: TExecOpts | undefined): Promise<TExecOpts extends TFullOutputOption ? any : string>;
    /**
     * Execute the given command using _adb shell_ prefix.
     *
     * @this {import('../adb.js').ADB}
     * @template {ShellExecOptions} TShellExecOpts
     * @param {string|string[]} cmd - The array of rest command line parameters or a single
     *                                      string parameter.
     * @param {TShellExecOpts} [opts] - Additional options mapping.
     * @return {Promise<TShellExecOpts extends TFullOutputOption ? import('ait-process').AITProcessExecResult : string>}
     * Command's stdout.
     * @throws {Error} If the command returned non-zero exit code.
     */
    function shell<TShellExecOpts extends ShellExecOptions$1>(this: ADB$1, cmd: string | string[], opts?: TShellExecOpts | undefined): Promise<TShellExecOpts extends TFullOutputOption ? any : string>;
    /**
     *
     * @this {import('../adb.js').ADB}
     * @param {string[]} [args=[]]
     * @returns {import('ait-process').SubProcess}
     */
    function createSubProcess(this: ADB$1, args?: string[] | undefined): SubProcess;
    /**
     * Retrieve the current adb port.
     * @todo can probably deprecate this now that the logic is just to read this.adbPort
     *
     * @this {import('../adb.js').ADB}
     * @return {number} The current adb port number.
     */
    function getAdbServerPort(this: ADB$1): number;
    /**
     * Retrieve the current emulator port from _adb devives_ output.
     *
     * @this {import('../adb.js').ADB}
     * @return {Promise<number>} The current emulator port.
     * @throws {Error} If there are no connected devices.
     */
    function getEmulatorPort(this: ADB$1): Promise<number>;
    /**
     * Retrieve the current emulator port by parsing emulator name string.
     *
     * @this {import('../adb.js').ADB}
     * @param {string} emStr - Emulator name string.
     * @return {number|false} Either the current emulator port or
     * _false_ if port number cannot be parsed.
     */
    function getPortFromEmulatorString(this: ADB$1, emStr: string): number | false;
    /**
     * Retrieve the list of currently connected emulators.
     *
     * @this {import('../adb.js').ADB}
     * @param {ConnectedDevicesOptions} [opts={}] - Additional options mapping.
     * @return {Promise<Device[]>} The list of connected devices.
     */
    function getConnectedEmulators(this: ADB$1, opts?: ConnectedDevicesOptions | undefined): Promise<Device[]>;
    /**
     * Set _emulatorPort_ property of the current class.
     *
     * @this {import('../adb.js').ADB}
     * @param {number} emPort - The emulator port to be set.
     */
    function setEmulatorPort(this: ADB$1, emPort: number): void;
    /**
     * Set the identifier of the current device (_this.curDeviceId_).
     *
     * @this {import('../adb.js').ADB}
     * @param {string} deviceId - The device identifier.
     */
    function setDeviceId(this: ADB$1, deviceId: string): void;
    /**
     * Set the the current device object.
     *
     * @this {import('../adb.js').ADB}
     * @param {Device} deviceObj - The device object to be set.
     */
    function setDevice(this: ADB$1, deviceObj: Device): void;
    /**
     * Get the object for the currently running emulator.
     * !!! This method has a side effect - it implicitly changes the
     * `deviceId` (only if AVD with a matching name is found)
     * and `emulatorPort` instance properties.
     *
     * @this {import('../adb.js').ADB}
     * @param {string} avdName - Emulator name.
     * @return {Promise<Device|null>} Currently running emulator or _null_.
     */
    function getRunningAVD(this: ADB$1, avdName: string): Promise<Device | null>;
    /**
     * Get the object for the currently running emulator.
     *
     * @this {import('../adb.js').ADB}
     * @param {string} avdName - Emulator name.
     * @param {number} [timeoutMs=20000] - The maximum number of milliseconds
     *                                     to wait until at least one running AVD object
     *                                     is detected.
     * @return {Promise<Device|null>} Currently running emulator or _null_.
     * @throws {Error} If no device has been detected within the timeout.
     */
    function getRunningAVDWithRetry(this: ADB$1, avdName: string, timeoutMs?: number | undefined): Promise<Device | null>;
    /**
     * Shutdown all running emulators by killing their processes.
     *
     * @this {import('../adb.js').ADB}
     * @throws {Error} If killing tool returned non-zero return code.
     */
    function killAllEmulators(this: ADB$1): Promise<void>;
    /**
     * Kill emulator with the given name. No error
     * is thrown is given avd does not exist/is not running.
     *
     * @this {import('../adb.js').ADB}
     * @param {string?} [avdName=null] - The name of the emulator to be killed. If empty,
     *                            the current emulator will be killed.
     * @param {number} [timeout=60000] - The amount of time to wait before throwing
     *                                    an exception about unsuccessful killing
     * @return {Promise<boolean>} - True if the emulator was killed, false otherwise.
     * @throws {Error} if there was a failure by killing the emulator
     */
    function killEmulator(this: ADB$1, avdName?: string | null | undefined, timeout?: number | undefined): Promise<boolean>;
    /**
     * @typedef {Object} AvdLaunchOptions
     * @property {string|string[]} [args] Additional emulator command line arguments
     * @property {Object} [env] Additional emulator environment variables
     * @property {string} [language] Emulator system language
     * @property {string} [country] Emulator system country
     * @property {number} [launchTimeout=60000] Emulator startup timeout in milliseconds
     * @property {number} [readyTimeout=60000] The maximum period of time to wait until Emulator
     * is ready for usage in milliseconds
     * @property {number} [retryTimes=1] The maximum number of startup retries
     */
    /**
     * Start an emulator with given parameters and wait until it is fully started.
     *
     * @this {import('../adb.js').ADB}
     * @param {string} avdName - The name of an existing emulator.
     * @param {AvdLaunchOptions} [opts={}]
     * @returns {Promise<SubProcess>} Emulator subprocess instance
     * @throws {Error} If the emulator fails to start within the given timeout.
     */
    function launchAVD(this: ADB$1, avdName: string, opts?: AvdLaunchOptions | undefined): Promise<typeof SubProcess>;
    let getVersion: (() => Promise<{
        binary: {
            version: semver.SemVer | null;
            build: number;
        };
        bridge: {
            version: semver.SemVer | null;
        };
    }>) & _.MemoizedFunction;
    /**
     * Check if the current emulator is ready to accept further commands (booting completed).
     *
     * @this {import('../adb.js').ADB}
     * @param {number} [timeoutMs=20000] - The maximum number of milliseconds to wait.
     * @returns {Promise<void>}
     * @throws {Error} If the emulator is not ready within the given timeout.
     */
    function waitForEmulatorReady(this: ADB$1, timeoutMs?: number | undefined): Promise<void>;
    /**
     * Check if the current device is ready to accept further commands (booting completed).
     *
     * @this {import('../adb.js').ADB}
     * @param {number} [appDeviceReadyTimeout=30] - The maximum number of seconds to wait.
     * @throws {Error} If the device is not ready within the given timeout.
     */
    function waitForDevice(this: ADB$1, appDeviceReadyTimeout?: number | undefined): Promise<void>;
    /**
     * Reboot the current device and wait until it is completed.
     *
     * @this {import('../adb.js').ADB}
     * @param {number} [retries=DEFAULT_ADB_REBOOT_RETRIES] - The maximum number of reboot retries.
     * @throws {Error} If the device failed to reboot and number of retries is exceeded.
     */
    function reboot(this: ADB$1, retries?: number | undefined): Promise<void>;
    /**
     * @typedef {Object} RootResult
     * @property {boolean} isSuccessful True if the call to root/unroot was successful
     * @property {boolean} wasAlreadyRooted True if the device was already rooted
     */
    /**
     * Switch adb server root privileges.
     *
     * @this {import('../adb.js').ADB}
     * @param {boolean} isElevated - Should we elevate to to root or unroot? (default true)
     * @return {Promise<RootResult>}
     */
    function changeUserPrivileges(this: ADB$1, isElevated: boolean): Promise<RootResult>;
    /**
     * Switch adb server to root mode
     *
     * @this {import('../adb.js').ADB}
     * @return {Promise<RootResult>}
     */
    function root(this: ADB$1): Promise<RootResult>;
    /**
     * Switch adb server to non-root mode.
     *
     * @this {import('../adb.js').ADB}
     * @return {Promise<RootResult>}
     */
    function unroot(this: ADB$1): Promise<RootResult>;
    /**
     * Checks whether the current user is root
     *
     * @this {import('../adb.js').ADB}
     * @return {Promise<boolean>} True if the user is root
     * @throws {Error} if there was an error while identifying
     * the user.
     */
    function isRoot(this: ADB$1): Promise<boolean>;
    /**
     * Verify whether a remote path exists on the device under test.
     *
     * @this {import('../adb.js').ADB}
     * @param {string} remotePath - The remote path to verify.
     * @return {Promise<boolean>} True if the given path exists on the device.
     */
    function fileExists(this: ADB$1, remotePath: string): Promise<boolean>;
    /**
     * Get the output of _ls_ command on the device under test.
     *
     * @this {import('../adb.js').ADB}
     * @param {string} remotePath - The remote path (the first argument to the _ls_ command).
     * @param {string[]} [opts] - Additional _ls_ options.
     * @return {Promise<string[]>} The _ls_ output as an array of split lines.
     *                          An empty array is returned of the given _remotePath_
     *                          does not exist.
     */
    function ls(this: ADB$1, remotePath: string, opts?: string[] | undefined): Promise<string[]>;
    /**
     * Get the size of the particular file located on the device under test.
     *
     * @this {import('../adb.js').ADB}
     * @param {string} remotePath - The remote path to the file.
     * @return {Promise<number>} File size in bytes.
     * @throws {Error} If there was an error while getting the size of the given file.
     */
    function fileSize(this: ADB$1, remotePath: string): Promise<number>;
    /**
     * Installs the given certificate on a rooted real device or
     * an emulator. The emulator must be executed with `-writable-system`
     * command line option and adb daemon should be running in root
     * mode for this method to work properly. The method also requires
     * openssl tool to be available on the destination system.
     * for more details on this topic
     *
     * @this {import('../adb.js').ADB}
     * @param {Buffer|string} cert - base64-decoded content of the actual certificate
     * represented as a string or a buffer
     * @throws {Error} If openssl tool is not available on the destination system
     * or if there was an error while installing the certificate
     */
    function installMitmCertificate(this: ADB$1, cert: string | Buffer): Promise<void>;
    /**
     * Verifies if the given root certificate is already installed on the device.
     *
     * @this {import('../adb.js').ADB}
     * @param {Buffer|string} cert - base64-decoded content of the actual certificate
     * represented as a string or a buffer
     * @throws {Error} If openssl tool is not available on the destination system
     * or if there was an error while checking the certificate
     * @returns {Promise<boolean>} true if the given certificate is already installed
     */
    function isMitmCertificateInstalled(this: ADB$1, cert: string | Buffer): Promise<boolean>;
}

type GsmSignalStrength = 0 | 1 | 2 | 3 | 4;
type ExecTelnetOptions = {
    /**
     * A timeout used to wait for a server
     * reply to the given command
     */
    execTimeout?: number | undefined;
    /**
     * Console connection timeout in milliseconds
     */
    connTimeout?: number | undefined;
    /**
     * Telnet console initialization timeout
     * in milliseconds (the time between connection happens and the command prompt
     * is available)
     */
    initTimeout?: number | undefined;
    /**
     * The emulator port number. The method will try to parse it
     * from the current device identifier if unset
     */
    port?: string | number | undefined;
};
type EmuVersionInfo = {
    /**
     * The actual revision number, for example '30.0.5'
     */
    revision?: string | undefined;
    /**
     * The build identifier, for example 6306047
     */
    buildId?: number | undefined;
};
type ADBEmuCommands = typeof emuMethods;
declare namespace emuMethods {
    let POWER_AC_STATES: Readonly<{
        POWER_AC_ON: "on";
        POWER_AC_OFF: "off";
    }>;
    let GSM_CALL_ACTIONS: Readonly<{
        GSM_CALL: "call";
        GSM_ACCEPT: "accept";
        GSM_CANCEL: "cancel";
        GSM_HOLD: "hold";
    }>;
    let GSM_VOICE_STATES: Readonly<{
        GSM_VOICE_UNREGISTERED: "unregistered";
        GSM_VOICE_HOME: "home";
        GSM_VOICE_ROAMING: "roaming";
        GSM_VOICE_SEARCHING: "searching";
        GSM_VOICE_DENIED: "denied";
        GSM_VOICE_OFF: "off";
        GSM_VOICE_ON: "on";
    }>;
    let GSM_SIGNAL_STRENGTHS: readonly number[];
    let NETWORK_SPEED: Readonly<{
        GSM: "gsm";
        SCSD: "scsd";
        GPRS: "gprs";
        EDGE: "edge";
        UMTS: "umts";
        HSDPA: "hsdpa";
        LTE: "lte";
        EVDO: "evdo";
        FULL: "full";
    }>;
    let SENSORS: Readonly<{
        ACCELERATION: "acceleration";
        GYROSCOPE: "gyroscope";
        MAGNETIC_FIELD: "magnetic-field";
        ORIENTATION: "orientation";
        TEMPERATURE: "temperature";
        PROXIMITY: "proximity";
        LIGHT: "light";
        PRESSURE: "pressure";
        HUMIDITY: "humidity";
        MAGNETIC_FIELD_UNCALIBRATED: "magnetic-field-uncalibrated";
        GYROSCOPE_UNCALIBRATED: "gyroscope-uncalibrated";
        HINGE_ANGLE0: "hinge-angle0";
        HINGE_ANGLE1: "hinge-angle1";
        HINGE_ANGLE2: "hinge-angle2";
        HEART_RATE: "heart-rate";
        RGBC_LIGHT: "rgbc-light";
    }>;
    /**
     * Check the emulator state.
     *
     * @this {import('../adb.js').ADB}
     * @return {Promise<boolean>} True if Emulator is visible to adb.
     */
    function isEmulatorConnected(this: ADB$1): Promise<boolean>;
    /**
     * Verify the emulator is connected.
     *
     * @this {import('../adb.js').ADB}
     * @throws {Error} If Emulator is not visible to adb.
     */
    function verifyEmulatorConnected(this: ADB$1): Promise<void>;
    /**
     * Emulate fingerprint touch event on the connected emulator.
     *
     * @this {import('../adb.js').ADB}
     * @param {string} fingerprintId - The ID of the fingerprint.
     */
    function fingerprint(this: ADB$1, fingerprintId: string): Promise<void>;
    /**
     * Change the display orientation on the connected emulator.
     * The orientation is changed (PI/2 is added) every time
     * this method is called.
     * @this {import('../adb.js').ADB}
     */
    function rotate(this: ADB$1): Promise<void>;
    /**
     * Emulate power state change on the connected emulator.
     *
     * @this {import('../adb.js').ADB}
     * @param {PowerAcStates} [state='on'] - Either 'on' or 'off'.
     */
    function powerAC(this: ADB$1, state?: ValueOf<Readonly<{
        POWER_AC_ON: "on";
        POWER_AC_OFF: "off";
    }>, "POWER_AC_ON" | "POWER_AC_OFF"> | undefined): Promise<void>;
    /**
     * Emulate sensors values on the connected emulator.
     *
     * @this {import('../adb.js').ADB}
     * @param {string} sensor - Sensor type declared in SENSORS items.
     * @param {Sensors} value  - Number to set as the sensor value.
     * @throws {TypeError} - If sensor type or sensor value is not defined
     */
    function sensorSet(this: ADB$1, sensor: string, value: ValueOf<Readonly<{
        ACCELERATION: "acceleration";
        GYROSCOPE: "gyroscope";
        MAGNETIC_FIELD: "magnetic-field";
        ORIENTATION: "orientation";
        TEMPERATURE: "temperature";
        PROXIMITY: "proximity";
        LIGHT: "light";
        PRESSURE: "pressure";
        HUMIDITY: "humidity";
        MAGNETIC_FIELD_UNCALIBRATED: "magnetic-field-uncalibrated";
        GYROSCOPE_UNCALIBRATED: "gyroscope-uncalibrated";
        HINGE_ANGLE0: "hinge-angle0";
        HINGE_ANGLE1: "hinge-angle1";
        HINGE_ANGLE2: "hinge-angle2";
        HEART_RATE: "heart-rate";
        RGBC_LIGHT: "rgbc-light";
    }>, "ACCELERATION" | "GYROSCOPE" | "MAGNETIC_FIELD" | "ORIENTATION" | "TEMPERATURE" | "PROXIMITY" | "LIGHT" | "PRESSURE" | "HUMIDITY" | "MAGNETIC_FIELD_UNCALIBRATED" | "GYROSCOPE_UNCALIBRATED" | "HINGE_ANGLE0" | "HINGE_ANGLE1" | "HINGE_ANGLE2" | "HEART_RATE" | "RGBC_LIGHT">): Promise<void>;
    /**
     * Emulate power capacity change on the connected emulator.
     *
     * @this {import('../adb.js').ADB}
     * @param {string|number} [percent=100] - Percentage value in range [0, 100].
     */
    function powerCapacity(this: ADB$1, percent?: string | number | undefined): Promise<void>;
    /**
     * Emulate power off event on the connected emulator.
     * @this {import('../adb.js').ADB}
     */
    function powerOFF(this: ADB$1): Promise<void>;
    /**
     * Emulate send SMS event on the connected emulator.
     *
     * @this {import('../adb.js').ADB}
     * @param {string|number} phoneNumber - The phone number of message sender.
     * @param {string} [message=''] - The message content.
     * @throws {TypeError} If phone number has invalid format.
     */
    function sendSMS(this: ADB$1, phoneNumber: string | number, message?: string | undefined): Promise<void>;
    /**
     * Emulate GSM call event on the connected emulator.
     *
     * @this {import('../adb.js').ADB}
     * @param {string|number} phoneNumber - The phone number of the caller.
     * @param {GsmCallActions} action - One of available GSM call actions.
     * @throws {TypeError} If phone number has invalid format.
     * @throws {TypeError} If _action_ value is invalid.
     */
    function gsmCall(this: ADB$1, phoneNumber: string | number, action: ValueOf<Readonly<{
        GSM_CALL: "call";
        GSM_ACCEPT: "accept";
        GSM_CANCEL: "cancel";
        GSM_HOLD: "hold";
    }>, "GSM_CALL" | "GSM_ACCEPT" | "GSM_CANCEL" | "GSM_HOLD">): Promise<void>;
    /**
     * Emulate GSM signal strength change event on the connected emulator.
     *
     * @this {import('../adb.js').ADB}
     * @param {GsmSignalStrength} [strength=4] - A number in range [0, 4];
     * @throws {TypeError} If _strength_ value is invalid.
     */
    function gsmSignal(this: ADB$1, strength?: GsmSignalStrength | undefined): Promise<void>;
    /**
     * Emulate GSM voice event on the connected emulator.
     *
     * @this {import('../adb.js').ADB}
     * @param {GsmVoiceStates} [state='on'] - Either 'on' or 'off'.
     * @throws {TypeError} If _state_ value is invalid.
     */
    function gsmVoice(this: ADB$1, state?: ValueOf<Readonly<{
        GSM_VOICE_UNREGISTERED: "unregistered";
        GSM_VOICE_HOME: "home";
        GSM_VOICE_ROAMING: "roaming";
        GSM_VOICE_SEARCHING: "searching";
        GSM_VOICE_DENIED: "denied";
        GSM_VOICE_OFF: "off";
        GSM_VOICE_ON: "on";
    }>, "GSM_VOICE_UNREGISTERED" | "GSM_VOICE_HOME" | "GSM_VOICE_ROAMING" | "GSM_VOICE_SEARCHING" | "GSM_VOICE_DENIED" | "GSM_VOICE_OFF" | "GSM_VOICE_ON"> | undefined): Promise<void>;
    /**
     * Emulate network speed change event on the connected emulator.
     *
     * @this {import('../adb.js').ADB}
     * @param {NetworkSpeed} [speed='full']
     *  One of possible NETWORK_SPEED values.
     * @throws {TypeError} If _speed_ value is invalid.
     */
    function networkSpeed(this: ADB$1, speed?: ValueOf<Readonly<{
        GSM: "gsm";
        SCSD: "scsd";
        GPRS: "gprs";
        EDGE: "edge";
        UMTS: "umts";
        HSDPA: "hsdpa";
        LTE: "lte";
        EVDO: "evdo";
        FULL: "full";
    }>, "GSM" | "SCSD" | "GPRS" | "EDGE" | "UMTS" | "HSDPA" | "LTE" | "EVDO" | "FULL"> | undefined): Promise<void>;
    /**
     * @typedef {Object} ExecTelnetOptions
     * @property {number} [execTimeout=60000] A timeout used to wait for a server
     * reply to the given command
     * @property {number} [connTimeout=5000] Console connection timeout in milliseconds
     * @property {number} [initTimeout=5000] Telnet console initialization timeout
     * in milliseconds (the time between connection happens and the command prompt
     * is available)
     * @property {number|string} [port] The emulator port number. The method will try to parse it
     * from the current device identifier if unset
     */
    /**
     * Executes a command through emulator telnet console interface and returns its output
     *
     * @this {import('../adb.js').ADB}
     * @param {string[]|string} cmd - The actual command to execute. See
     * https://developer.android.com/studio/run/emulator-console for more details
     * on available commands
     * @param {ExecTelnetOptions} [opts={}]
     * @returns {Promise<string>} The command output
     * @throws {Error} If there was an error while connecting to the Telnet console
     * or if the given command returned non-OK response
     */
    function execEmuConsoleCommand(this: ADB$1, cmd: string | string[], opts?: ExecTelnetOptions | undefined): Promise<string>;
    /**
     * @typedef {Object} EmuVersionInfo
     * @property {string} [revision] The actual revision number, for example '30.0.5'
     * @property {number} [buildId] The build identifier, for example 6306047
     */
    /**
     * Retrieves emulator version from the file system
     *
     * @this {import('../adb.js').ADB}
     * @returns {Promise<EmuVersionInfo>} If no version info could be parsed then an empty
     * object is returned
     */
    function getEmuVersionInfo(this: ADB$1): Promise<EmuVersionInfo>;
    /**
     * Retrieves emulator image properties from the local file system
     *
     * @this {import('../adb.js').ADB}
     * @param {string} avdName Emulator name. Should NOT start with '@' character
     * @throws {Error} if there was a failure while extracting the properties
     * Usually this configuration .ini file has the following content:
     *   avd.ini.encoding=UTF-8
     *   path=/Users/username/.android/avd/Pixel_XL_API_30.avd
     *   path.rel=avd/Pixel_XL_API_30.avd
     *   target=android-30
     */
    function getEmuImageProperties(this: ADB$1, avdName: string): Promise<{
        [key: string]: any;
    }>;
    /**
     * Check if given emulator exists in the list of available avds.
     *
     * @this {import('../adb.js').ADB}
     * @param {string} avdName - The name of emulator to verify for existence.
     * Should NOT start with '@' character
     * @throws {Error} If the emulator with given name does not exist.
     */
    function checkAvdExist(this: ADB$1, avdName: string): Promise<boolean>;
}

type CertCheckOptions = {
    /**
     * Whether to require that the destination APK
     * is signed with the default Armor certificate or any valid certificate. This option
     * only has effect if `useKeystore` property is unset.
     */
    requireDefaultCert?: boolean | undefined;
};
type KeystoreHash = {
    /**
     * the md5 hash value of the keystore
     */
    md5?: string | undefined;
    /**
     * the sha1 hash value of the keystore
     */
    sha1?: string | undefined;
    /**
     * the sha256 hash value of the keystore
     */
    sha256?: string | undefined;
    /**
     * the sha512 hash value of the keystore
     */
    sha512?: string | undefined;
};
type ApkSigningCommands = typeof apkSigningMethods;
declare namespace apkSigningMethods {
    /**
     * Execute apksigner utility with given arguments.
     *
     * @this {import('../adb.js').ADB}
     * @param {string[]} args - The list of tool arguments.
     * @return {Promise<string>} - Command stdout
     * @throws {Error} If apksigner binary is not present on the local file system
     *                 or the return code is not equal to zero.
     */
    function executeApksigner(this: ADB$1, args: string[]): Promise<string>;
    /**
     * (Re)sign the given apk file on the local file system with the default certificate.
     *
     * @this {import('../adb.js').ADB}
     * @param {string} apk - The full path to the local apk file.
     * @throws {Error} If signing fails.
     */
    function signWithDefaultCert(this: ADB$1, apk: string): Promise<void>;
    /**
     * (Re)sign the given apk file on the local file system with a custom certificate.
     *
     * @this {import('../adb.js').ADB}
     * @param {string} apk - The full path to the local apk file.
     * @throws {Error} If signing fails.
     */
    function signWithCustomCert(this: ADB$1, apk: string): Promise<void>;
    /**
     * (Re)sign the given apk file on the local file system with either
     * custom or default certificate based on _this.useKeystore_ property value
     * and Zip-aligns it after signing.
     *
     * @this {import('../adb.js').ADB}
     * @param {string} appPath - The full path to the local .apk(s) file.
     * @throws {Error} If signing fails.
     */
    function sign(this: ADB$1, appPath: string): Promise<void>;
    /**
     * Perform zip-aligning to the given local apk file.
     *
     * @param {string} apk - The full path to the local apk file.
     * @returns {Promise<boolean>} True if the apk has been successfully aligned
     * or false if the apk has been already aligned.
     * @throws {Error} If zip-align fails.
     */
    function zipAlignApk(apk: string): Promise<boolean>;
    /**
     * @typedef {Object} CertCheckOptions
     * @property {boolean} [requireDefaultCert=true] Whether to require that the destination APK
     * is signed with the default Armor certificate or any valid certificate. This option
     * only has effect if `useKeystore` property is unset.
     */
    /**
     * Check if the app is already signed with the default Armor certificate.
     *
     * @this {import('../adb.js').ADB}
     * @param {string} appPath - The full path to the local .apk(s) file.
     * @param {string} pkg - The name of application package.
     * @param {CertCheckOptions} [opts={}] - Certificate checking options
     * @return {Promise<boolean>} True if given application is already signed.
     */
    function checkApkCert(this: ADB$1, appPath: string, pkg: string, opts?: CertCheckOptions | undefined): Promise<boolean>;
    /**
     * @typedef {Object} KeystoreHash
     * @property {string} [md5] the md5 hash value of the keystore
     * @property {string} [sha1] the sha1 hash value of the keystore
     * @property {string} [sha256] the sha256 hash value of the keystore
     * @property {string} [sha512] the sha512 hash value of the keystore
     */
    /**
     * Retrieve the the hash of the given keystore.
     *
     * @this {import('../adb.js').ADB}
     * @return {Promise<KeystoreHash>}
     * @throws {Error} If getting keystore hash fails.
     */
    function getKeystoreHash(this: ADB$1): Promise<KeystoreHash>;
}

type ExecOutputFormat = 'stdout' | 'full';
type ShellExecOptions = {
    /**
     * - the name of the corresponding Armor's timeout capability
     * (used in the error messages).
     */
    timeoutCapName?: string | undefined;
    /**
     * - command execution timeout.
     */
    timeout?: number | undefined;
    /**
     * - Whether to run the given command as root.
     */
    privileged?: boolean | undefined;
    /**
     * - Whether response should include full exec output or just stdout.
     * Potential values are full or stdout.
     */
    outputFormat?: ExecOutputFormat | undefined;
};

type InstallState = 'unknown' | 'notInstalled' | 'newerVersionInstalled' | 'sameVersionInstalled' | 'olderVersionInstalled';
type StartUriOptions = {
    /**
     * - if `false` then adb won't wait
     * for the started activity to return the control
     */
    waitForLaunch?: boolean | undefined;
};
type StartAppOptions = {
    /**
     * - The name of the application package
     */
    pkg: string;
    /**
     * - The name of the main application activity.
     * This or action is required in order to be able to launch an app.
     */
    activity?: string | undefined;
    /**
     * - The name of the intent action that will launch the required app.
     * This or activity is required in order to be able to launch an app.
     */
    action?: string | undefined;
    /**
     * - If this property is set to `true`
     * and the activity name does not start with '.' then the method
     * will try to add the missing dot and start the activity once more
     * if the first startup try fails.
     */
    retry?: boolean | undefined;
    /**
     * - Set it to `true` in order to forcefully
     * stop the activity if it is already running.
     */
    stopApp?: boolean | undefined;
    /**
     * - The name of the package to wait to on
     * startup (this only makes sense if this name is different from the one, which is set as `pkg`)
     */
    waitPkg?: string | undefined;
    /**
     * - The name of the activity to wait to on
     * startup (this only makes sense if this name is different from the one, which is set as `activity`)
     */
    waitActivity?: string | undefined;
    /**
     * - The number of milliseconds to wait until the
     * `waitActivity` is focused
     */
    waitDuration?: number | undefined;
    /**
     * - The number of the user profile to start
     * the given activity with. The default OS user profile (usually zero) is used
     * when this property is unset
     */
    user?: string | number | undefined;
    /**
     * - if `false` then adb won't wait
     * for the started activity to return the control
     */
    waitForLaunch?: boolean | undefined;
    category?: string | undefined;
    flags?: string | undefined;
    optionalIntentArguments?: string | undefined;
};
type PackageActivityInfo = {
    /**
     * - The name of application package,
     *   for example 'com.acme.app'.
     */
    appPackage: string | null;
    /**
     * - The name of main application activity.
     */
    appActivity: string | null;
};
type UninstallOptions = {
    /**
     * - The count of milliseconds to wait until the
     *          app is uninstalled.
     */
    timeout?: number | undefined;
    /**
     * - Set to true in order to keep the
     *          application data and cache folders after uninstall.
     */
    keepData?: boolean | undefined;
};
type CachingOptions = {
    /**
     * - The count of milliseconds to wait until the
     * app is uploaded to the remote location.
     */
    timeout?: number | undefined;
};
type InstallOptions = {
    /**
     * - The count of milliseconds to wait until the
     *    app is installed.
     */
    timeout?: number | undefined;
    /**
     * - The timeout option name
     *    users can increase the timeout.
     */
    timeoutCapName?: string | undefined;
    /**
     * - Set to true in order to allow test
     *    packages installation.
     */
    allowTestPackages?: boolean | undefined;
    /**
     * - Set to true to install the app on sdcard
     *    instead of the device memory.
     */
    useSdcard?: boolean | undefined;
    /**
     * - Set to true in order to grant all the
     *    permissions requested in the application's manifest
     *    automatically after the installation is completed
     *    under Android 6+.
     */
    grantPermissions?: boolean | undefined;
    /**
     * - Set it to false if you don't want
     *    the application to be upgraded/reinstalled
     *    if it is already present on the device.
     */
    replace?: boolean | undefined;
    /**
     * - Forcefully disables incremental installs if set to `true`.
     *    Read https://developer.android.com/preview/features#incremental
     *    for more details.
     */
    noIncremental?: boolean | undefined;
};
type InstallOrUpgradeOptions = {
    /**
     * - The count of milliseconds to wait until the
     *    app is installed.
     */
    timeout?: number | undefined;
    /**
     * - Set to true in order to allow test
     *    packages installation.
     */
    allowTestPackages?: boolean | undefined;
    /**
     * - Set to true to install the app on SDCard
     *    instead of the device memory.
     */
    useSdcard?: boolean | undefined;
    /**
     * - Set to true in order to grant all the
     *    permissions requested in the application's manifest
     *    automatically after the installation is completed
     *    under Android 6+.
     */
    grantPermissions?: boolean | undefined;
    /**
     * - Set to `true` in order to always prefer
     *    the current build over any installed packages having
     *    the same identifier
     */
    enforceCurrentBuild?: boolean | undefined;
};
type InstallOrUpgradeResult = {
    /**
     * - Equals to `true` if the target app has been uninstalled
     *   before being installed
     */
    wasUninstalled: boolean;
    /**
     * - One of `adb.APP_INSTALL_STATE` states, which reflects
     * the state of the application before being installed.
     */
    appState: InstallState;
};
type AppInfo = {
    /**
     * - Package name, for example 'com.acme.app'.
     */
    name: string;
    /**
     * - Version code.
     */
    versionCode?: number | null | undefined;
    /**
     * - Version name, for example '1.0'.
     */
    versionName?: string | null | undefined;
    /**
     * - true if the app is installed on the device under test.
     */
    isInstalled?: boolean | null | undefined;
};
type ApkUtils = typeof apkUtilsMethods;
declare namespace apkUtilsMethods {
    let APP_INSTALL_STATE: Record<string, InstallState>;
    /**
     * Check whether the particular package is present on the device under test.
     *
     * @this {import('../adb.js').ADB}
     * @param {string} pkg - The name of the package to check.
     * @return {Promise<boolean>} True if the package is installed.
     */
    function isAppInstalled(this: ADB$1, pkg: string): Promise<boolean>;
    /**
     * @typedef {Object} StartUriOptions
     * @property {boolean} [waitForLaunch=true] - if `false` then adb won't wait
     * for the started activity to return the control
     */
    /**
     * Start the particular URI on the device under test.
     *
     * @this {import('../adb.js').ADB}
     * @param {string} uri - The name of URI to start.
     * @param {string} pkg - The name of the package to start the URI with.
     * @param {StartUriOptions} [opts={}]
     */
    function startUri(this: ADB$1, uri: string, pkg: string, opts?: StartUriOptions | undefined): Promise<void>;
    /**
     * @typedef {Object} StartAppOptions
     * @property {string} pkg - The name of the application package
     * @property {string} [activity] - The name of the main application activity.
     * This or action is required in order to be able to launch an app.
     * @property {string} [action] - The name of the intent action that will launch the required app.
     * This or activity is required in order to be able to launch an app.
     * @property {boolean} [retry=true] - If this property is set to `true`
     * and the activity name does not start with '.' then the method
     * will try to add the missing dot and start the activity once more
     * if the first startup try fails.
     * @property {boolean} [stopApp=true] - Set it to `true` in order to forcefully
     * stop the activity if it is already running.
     * @property {string} [waitPkg] - The name of the package to wait to on
     * startup (this only makes sense if this name is different from the one, which is set as `pkg`)
     * @property {string} [waitActivity] - The name of the activity to wait to on
     * startup (this only makes sense if this name is different from the one, which is set as `activity`)
     * @property {number} [waitDuration] - The number of milliseconds to wait until the
     * `waitActivity` is focused
     * @property {string|number} [user] - The number of the user profile to start
     * the given activity with. The default OS user profile (usually zero) is used
     * when this property is unset
     * @property {boolean} [waitForLaunch=true] - if `false` then adb won't wait
     * for the started activity to return the control
     * @property {string} [category]
     * @property {string} [flags]
     * @property {string} [optionalIntentArguments]
     */
    /**
     * Start the particular package/activity on the device under test.
     *
     * @this {import('../adb.js').ADB}
     * @param {StartAppOptions} startAppOptions - Startup options mapping.
     * @return {Promise<string>} The output of the corresponding adb command.
     * @throws {Error} If there is an error while executing the activity
     */
    function startApp(this: ADB$1, startAppOptions: StartAppOptions): Promise<string>;
    /**
     * Helper method to call `adb dumpsys window windows/displays`
     * @this {import('../adb.js').ADB}
     * @returns {Promise<string>}
     */
    function dumpWindows(this: ADB$1): Promise<string>;
    /**
     * @typedef {Object} PackageActivityInfo
     * @property {string?} appPackage - The name of application package,
     *                                  for example 'com.acme.app'.
     * @property {string?} appActivity - The name of main application activity.
     */
    /**
     * Get the name of currently focused package and activity.
     *
     * @this {import('../adb.js').ADB}
     * @return {Promise<PackageActivityInfo>} The mapping, where property names are 'appPackage' and 'appActivity'.
     * @throws {Error} If there is an error while parsing the data.
     */
    function getFocusedPackageAndActivity(this: ADB$1): Promise<PackageActivityInfo>;
    /**
     * Wait for the given activity to be focused/non-focused.
     *
     * @this {import('../adb.js').ADB}
     * @param {string} pkg - The name of the package to wait for.
     * @param {string} activity - The name of the activity, belonging to that package,
     *                            to wait for.
     * @param {boolean} waitForStop - Whether to wait until the activity is focused (true)
     *                                or is not focused (false).
     * @param {number} [waitMs=20000] - Number of milliseconds to wait before timeout occurs.
     * @throws {error} If timeout happens.
     */
    function waitForActivityOrNot(this: ADB$1, pkg: string, activity: string, waitForStop: boolean, waitMs?: number | undefined): Promise<void>;
    /**
     * Wait for the given activity to be focused
     *
     * @this {import('../adb.js').ADB}
     * @param {string} pkg - The name of the package to wait for.
     * @param {string} act - The name of the activity, belonging to that package,
     *                            to wait for.
     * @param {number} [waitMs=20000] - Number of milliseconds to wait before timeout occurs.
     * @throws {error} If timeout happens.
     */
    function waitForActivity(this: ADB$1, pkg: string, act: string, waitMs?: number | undefined): Promise<void>;
    /**
     * Wait for the given activity to be non-focused.
     *
     * @this {import('../adb.js').ADB}
     * @param {string} pkg - The name of the package to wait for.
     * @param {string} act - The name of the activity, belonging to that package,
     *                            to wait for.
     * @param {number} [waitMs=20000] - Number of milliseconds to wait before timeout occurs.
     * @throws {error} If timeout happens.
     */
    function waitForNotActivity(this: ADB$1, pkg: string, act: string, waitMs?: number | undefined): Promise<void>;
    /**
     * @typedef {Object} UninstallOptions
     * @property {number} [timeout] - The count of milliseconds to wait until the
     *                                      app is uninstalled.
     * @property {boolean} [keepData] - Set to true in order to keep the
     *                                        application data and cache folders after uninstall.
     */
    /**
     * Uninstall the given package from the device under test.
     *
     * @this {import('../adb.js').ADB}
     * @param {string} pkg - The name of the package to be uninstalled.
     * @param {UninstallOptions} [options={}] - The set of uninstall options.
     * @return {Promise<boolean>} True if the package was found on the device and
     *                   successfully uninstalled.
     */
    function uninstallApk(this: ADB$1, pkg: string, options?: UninstallOptions | undefined): Promise<boolean>;
    /**
     * Install the package after it was pushed to the device under test.
     *
     * @this {import('../adb.js').ADB}
     * @param {string} apkPathOnDevice - The full path to the package on the device file system.
     * @param {import('./system-calls.js').ShellExecOptions} [opts={}] Additional exec options.
     * @throws {error} If there was a failure during application install.
     */
    function installFromDevicePath(this: ADB$1, apkPathOnDevice: string, opts?: ShellExecOptions | undefined): Promise<void>;
    /**
     * @typedef {Object} CachingOptions
     * @property {number} [timeout] - The count of milliseconds to wait until the
     * app is uploaded to the remote location.
     */
    /**
     * Caches the given APK at a remote location to speed up further APK deployments.
     *
     * @this {import('../adb.js').ADB}
     * @param {string} apkPath - Full path to the apk on the local FS
     * @param {CachingOptions} [options={}] - Caching options
     * @returns {Promise<string>} - Full path to the cached apk on the remote file system
     * @throws {Error} if there was a failure while caching the app
     */
    function cacheApk(this: ADB$1, apkPath: string, options?: CachingOptions | undefined): Promise<string>;
    /**
     * @typedef {Object} InstallOptions
     * @property {number} [timeout=60000] - The count of milliseconds to wait until the
     *                                      app is installed.
     * @property {string} [timeoutCapName=androidInstallTimeout] - The timeout option name
     *                                                             users can increase the timeout.
     * @property {boolean} [allowTestPackages=false] - Set to true in order to allow test
     *                                                 packages installation.
     * @property {boolean} [useSdcard=false] - Set to true to install the app on sdcard
     *                                         instead of the device memory.
     * @property {boolean} [grantPermissions=false] - Set to true in order to grant all the
     *                                                permissions requested in the application's manifest
     *                                                automatically after the installation is completed
     *                                                under Android 6+.
     * @property {boolean} [replace=true] - Set it to false if you don't want
     *                                      the application to be upgraded/reinstalled
     *                                      if it is already present on the device.
     * @property {boolean} [noIncremental=false] - Forcefully disables incremental installs if set to `true`.
     *                                             Read https://developer.android.com/preview/features#incremental
     *                                             for more details.
     */
    /**
     * Install the package from the local file system.
     *
     * @this {import('../adb.js').ADB}
     * @param {string} appPath - The full path to the local package.
     * @param {InstallOptions} [options={}] - The set of installation options.
     * @throws {Error} If an unexpected error happens during install.
     */
    function install(this: ADB$1, appPath: string, options?: InstallOptions | undefined): Promise<void>;
    /**
     * Retrieves the current installation state of the particular application
     *
     * @this {import('../adb.js').ADB}
     * @param {string} appPath - Full path to the application
     * @param {string?} [pkg=null] - Package identifier. If omitted then the script will
     * try to extract it on its own
     * @returns {Promise<InstallState>} One of `APP_INSTALL_STATE` constants
     */
    function getApplicationInstallState(this: ADB$1, appPath: string, pkg?: string | null | undefined): Promise<InstallState>;
    /**
     * @typedef {Object} InstallOrUpgradeOptions
     * @property {number} [timeout=60000] - The count of milliseconds to wait until the
     *                                      app is installed.
     * @property {boolean} [allowTestPackages=false] - Set to true in order to allow test
     *                                                 packages installation.
     * @property {boolean} [useSdcard=false] - Set to true to install the app on SDCard
     *                                         instead of the device memory.
     * @property {boolean} [grantPermissions=false] - Set to true in order to grant all the
     *                                                permissions requested in the application's manifest
     *                                                automatically after the installation is completed
     *                                                under Android 6+.
     * @property {boolean} [enforceCurrentBuild=false] - Set to `true` in order to always prefer
     *                                                   the current build over any installed packages having
     *                                                   the same identifier
     */
    /**
     * @typedef {Object} InstallOrUpgradeResult
     * @property {boolean} wasUninstalled - Equals to `true` if the target app has been uninstalled
     *                                      before being installed
     * @property {InstallState} appState - One of `adb.APP_INSTALL_STATE` states, which reflects
     * the state of the application before being installed.
     */
    /**
     * Install the package from the local file system or upgrade it if an older
     * version of the same package is already installed.
     *
     * @this {import('../adb.js').ADB}
     * @param {string} appPath - The full path to the local package.
     * @param {string?} [pkg=null] - The name of the installed package. The method will
     * perform faster if it is set.
     * @param {InstallOrUpgradeOptions} [options={}] - Set of install options.
     * @throws {Error} If an unexpected error happens during install.
     * @returns {Promise<InstallOrUpgradeResult>}
     */
    function installOrUpgrade(this: ADB$1, appPath: string, pkg?: string | null | undefined, options?: InstallOrUpgradeOptions | undefined): Promise<InstallOrUpgradeResult>;
    /**
     * Extract string resources from the given package on local file system.
     *
     * @param {string} appPath - The full path to the .apk(s) package.
     * @param {string?} language - The name of the language to extract the resources for.
     * The default language is used if this equals to `null`
     * @param {string} out - The name of the destination folder on the local file system to
     *                       store the extracted file to.
     *                  parsed resource file represented as JSON object, and 'localPath',
     *                  containing the path to the extracted file on the local file system.
     */
    function extractStringsFromApk(appPath: string, language: string | null, out: string): Promise<{
        apkStrings: {};
        localPath: string;
    }>;
    /**
     * Get the language name of the device under test.
     *
     * @this {import('../adb.js').ADB}
     * @return {Promise<string>} The name of device language.
     */
    function getDeviceLanguage(this: ADB$1): Promise<string>;
    /**
     * Get the country name of the device under test.
     *
     * @this {import('../adb.js').ADB}
     * @return {Promise<string>} The name of device country.
     */
    function getDeviceCountry(this: ADB$1): Promise<string>;
    /**
     * Get the locale name of the device under test.
     *
     * @this {import('../adb.js').ADB}
     * @return {Promise<string>} The name of device locale.
     */
    function getDeviceLocale(this: ADB$1): Promise<string>;
    /**
     * Set the locale name of the device under test and the format of the locale is en-US, for example.
     * This method call setDeviceLanguageCountry, so, please use setDeviceLanguageCountry as possible.
     *
     * @this {import('../adb.js').ADB}
     * @param {string} locale - Names of the device language and the country connected with `-`. e.g. en-US.
     */
    function setDeviceLocale(this: ADB$1, locale: string): Promise<void>;
    /**
     * Make sure current device locale is expected or not.
     *
     * @this {import('../adb.js').ADB}
     * @privateRemarks FIXME: language or country is required
     * @param {string} [language] - Language. The language field is case insensitive, but Locale always canonicalizes to lower case.
     * @param {string} [country] - Country. The language field is case insensitive, but Locale always canonicalizes to lower case.
     * @param {string} [script] - Script. The script field is case insensitive but Locale always canonicalizes to title case.
     *
     * @return {Promise<boolean>} If current locale is language and country as arguments, return true.
     */
    function ensureCurrentLocale(this: ADB$1, language?: string | undefined, country?: string | undefined, script?: string | undefined): Promise<boolean>;
    /**
     * Set the locale name of the device under test.
     *
     * @this {import('../adb.js').ADB}
     * @privateRemarks FIXME: language or country is required
     * @param {string} [language] - Language. The language field is case insensitive, but Locale always canonicalizes to lower case.
     * format: [a-zA-Z]{2,8}. e.g. en, ja : https://developer.android.com/reference/java/util/Locale.html
     * @param {string} [country] - Country. The country (region) field is case insensitive, but Locale always canonicalizes to upper case.
     * format: [a-zA-Z]{2} | [0-9]{3}. e.g. US, JP : https://developer.android.com/reference/java/util/Locale.html
     * @param {string?} [script] - Script. The script field is case insensitive but Locale always canonicalizes to title case.
     * format: [a-zA-Z]{4}. e.g. Hans in zh-Hans-CN : https://developer.android.com/reference/java/util/Locale.html
     */
    function setDeviceLanguageCountry(this: ADB$1, language?: string | undefined, country?: string | undefined, script?: string | null | undefined): Promise<void>;
    /**
     * @typedef {Object} AppInfo
     * @property {string} name - Package name, for example 'com.acme.app'.
     * @property {number?} [versionCode] - Version code.
     * @property {string?} [versionName] - Version name, for example '1.0'.
     * @property {boolean?} [isInstalled] - true if the app is installed on the device under test.
     */
    /**
     * Get the package info from local apk file.
     *
     * @this {import('../adb.js').ADB}
     * @param {string} appPath - The full path to existing .apk(s) package on the local
     *                           file system.
     * @return {Promise<AppInfo|{}>} The parsed application information.
     */
    function getApkInfo(this: ADB$1, appPath: string): Promise<{} | AppInfo>;
    /**
     * Get the package info from the installed application.
     *
     * @this {import('../adb.js').ADB}
     * @param {string} pkg - The name of the installed package.
     * @return {Promise<AppInfo>} The parsed application information.
     */
    function getPackageInfo(this: ADB$1, pkg: string): Promise<AppInfo>;
    /**
     * Fetches base.apk of the given package to the local file system
     *
     * @this {import('../adb.js').ADB}
     * @param {string} pkg The package identifier (must be already installed on the device)
     * @param {string} tmpDir The destination folder path
     * @returns {Promise<string>} Full path to the downloaded file
     * @throws {Error} If there was an error while fetching the .apk
     */
    function pullApk(this: ADB$1, pkg: string, tmpDir: string): Promise<string>;
    /**
     * Activates the given application or launches it if necessary.
     * The action literally simulates
     * clicking the corresponding application icon on the dashboard.
     *
     * @this {import('../adb.js').ADB}
     * @param {string} appId - Application package identifier
     * @throws {Error} If the app cannot be activated
     */
    function activateApp(this: ADB$1, appId: string): Promise<void>;
}

type InstallMultipleApksOptions = {
    /**
     * - The number of milliseconds to wait until
     * the installation is completed
     */
    timeout?: string | number | null | undefined;
    /**
     * - The timeout option name
     * users can increase the timeout.
     */
    timeoutCapName?: string | undefined;
    /**
     * - Set to true in order to allow test
     * packages installation.
     */
    allowTestPackages?: boolean | undefined;
    /**
     * - Set to true to install the app on sdcard
     * instead of the device memory.
     */
    useSdcard?: boolean | undefined;
    /**
     * - Set to true in order to grant all the
     * permissions requested in the application's manifest automatically after the installation
     * is completed under Android 6+.
     */
    grantPermissions?: boolean | undefined;
    /**
     * - Install apks partially. It is used for 'install-multiple'.
     * https://android.stackexchange.com/questions/111064/what-is-a-partial-application-install-via-adb
     */
    partialInstall?: boolean | undefined;
};
type InstallApksOptions = {
    /**
     * - The number of milliseconds to wait until
     * the installation is completed
     */
    timeout?: string | number | undefined;
    /**
     * - The timeout option name
     * users can increase the timeout.
     */
    timeoutCapName?: string | undefined;
    /**
     * - Set to true in order to allow test
     * packages installation.
     */
    allowTestPackages?: boolean | undefined;
    /**
     * - Set to true in order to grant all the
     * permissions requested in the application's manifest automatically after the installation
     * is completed under Android 6+.
     */
    grantPermissions?: boolean | undefined;
};
type ApksUtils = typeof apksUtilsMethods;
declare namespace apksUtilsMethods {
    /**
     * Executes bundletool utility with given arguments and returns the actual stdout
     *
     * @this {import('../adb.js').ADB}
     * @param {Array<String>} args - the list of bundletool arguments
     * @param {string} errorMsg - The customized error message string
     * @throws {Error} If bundletool jar does not exist in PATH or there was an error while
     * executing it
     */
    function execBundletool(this: ADB$1, args: string[], errorMsg: string): Promise<void>;
    /**
     *
     * @this {import('../adb.js').ADB}
     * @param {string} specLocation - The full path to the generated device spec location
     * @returns {Promise<string>} The same `specLocation` value
     * @throws {Error} If it is not possible to retrieve the spec for the current device
     */
    function getDeviceSpec(this: ADB$1, specLocation: string): Promise<string>;
    /**
     * @typedef {Object} InstallMultipleApksOptions
     * @property {?number|string} [timeout=20000] - The number of milliseconds to wait until
     * the installation is completed
     * @property {string} [timeoutCapName=androidInstallTimeout] - The timeout option name
     * users can increase the timeout.
     * @property {boolean} [allowTestPackages=false] - Set to true in order to allow test
     * packages installation.
     * @property {boolean} [useSdcard=false] - Set to true to install the app on sdcard
     * instead of the device memory.
     * @property {boolean} [grantPermissions=false] - Set to true in order to grant all the
     * permissions requested in the application's manifest automatically after the installation
     * is completed under Android 6+.
     * @property {boolean} [partialInstall=false] - Install apks partially. It is used for 'install-multiple'.
     * https://android.stackexchange.com/questions/111064/what-is-a-partial-application-install-via-adb
     */
    /**
     * Installs the given apks into the device under test
     *
     * @this {import('../adb.js').ADB}
     * @param {Array<string>} apkPathsToInstall - The full paths to install apks
     * @param {InstallMultipleApksOptions} [options={}] - Installation options
     */
    function installMultipleApks(this: ADB$1, apkPathsToInstall: string[], options?: InstallMultipleApksOptions | undefined): Promise<string>;
    /**
     * @typedef {Object} InstallApksOptions
     * @property {number|string} [timeout=120000] - The number of milliseconds to wait until
     * the installation is completed
     * @property {string} [timeoutCapName=androidInstallTimeout] - The timeout option name
     * users can increase the timeout.
     * @property {boolean} [allowTestPackages=false] - Set to true in order to allow test
     * packages installation.
     * @property {boolean} [grantPermissions=false] - Set to true in order to grant all the
     * permissions requested in the application's manifest automatically after the installation
     * is completed under Android 6+.
     */
    /**
     * Installs the given .apks package into the device under test
     *
     * @this {import('../adb.js').ADB}
     * @param {string} apks - The full path to the .apks file
     * @param {InstallApksOptions} [options={}] - Installation options
     * @throws {Error} If the .apks bundle cannot be installed
     */
    function installApks(this: ADB$1, apks: string, options?: InstallApksOptions | undefined): Promise<void>;
    /**
     * Extracts and returns the full path to the master .apk file inside the bundle.
     *
     * @this {import('../adb.js').ADB}
     * @param {string} apks - The full path to the .apks file
     * @returns {Promise<string>} The full path to the master bundle .apk
     * @throws {Error} If there was an error while extracting/finding the file
     */
    function extractBaseApk(this: ADB$1, apks: string): Promise<string>;
    /**
     * Extracts and returns the full path to the .apk, which contains the corresponding
     * resources for the given language in the .apks bundle.
     *
     * @this {import('../adb.js').ADB}
     * @param {string} apks - The full path to the .apks file
     * @param {?string} [language=null] - The language abbreviation. The default language is
     * going to be selected if it is not set.
     * @returns {Promise<string>} The full path to the corresponding language .apk or the master .apk
     * if language split is not enabled for the bundle.
     * @throws {Error} If there was an error while extracting/finding the file
     */
    function extractLanguageApk(this: ADB$1, apks: string, language?: string | null | undefined): Promise<string>;
    /**
     *
     * @param {string} output
     * @returns {boolean}
     */
    function isTestPackageOnlyError(output: string): boolean;
}

type ApkCreationOptions = {
    /**
     * Specifies the path to the deployment keystore used
     * to sign the APKs. This flag is optional. If you don't include it,
     * bundletool attempts to sign your APKs with a debug signing key.
     * If the .apk has been already signed and cached then it is not going to be resigned
     * unless a different keystore or key alias is used.
     */
    keystore?: string | undefined;
    /**
     * Specifies your keystore’s password.
     * It is mandatory to provide this value if `keystore` one is assigned
     * otherwise it is going to be ignored.
     */
    keystorePassword?: string | undefined;
    /**
     * Specifies the alias of the signing key you want to use.
     * It is mandatory to provide this value if `keystore` one is assigned
     * otherwise it is going to be ignored.
     */
    keyAlias?: string | undefined;
    /**
     * Specifies the password for the signing key.
     * It is mandatory to provide this value if `keystore` one is assigned
     * otherwise it is going to be ignored.
     */
    keyPassword?: string | undefined;
};
type AabUtils = typeof aabUtilsMethods;
declare namespace aabUtilsMethods {
    /**
     * @typedef {Object} ApkCreationOptions
     * @property {string} [keystore] Specifies the path to the deployment keystore used
     * to sign the APKs. This flag is optional. If you don't include it,
     * bundletool attempts to sign your APKs with a debug signing key.
     * If the .apk has been already signed and cached then it is not going to be resigned
     * unless a different keystore or key alias is used.
     * @property {string} [keystorePassword] Specifies your keystore’s password.
     * It is mandatory to provide this value if `keystore` one is assigned
     * otherwise it is going to be ignored.
     * @property {string} [keyAlias] Specifies the alias of the signing key you want to use.
     * It is mandatory to provide this value if `keystore` one is assigned
     * otherwise it is going to be ignored.
     * @property {string} [keyPassword] Specifies the password for the signing key.
     * It is mandatory to provide this value if `keystore` one is assigned
     * otherwise it is going to be ignored.
     */
    /**
     * Builds a universal .apk from the given .aab package. See
     * https://developer.android.com/studio/command-line/bundletool#generate_apks
     * for more details.
     *
     * @param {string} aabPath Full path to the source .aab package
     * @param {ApkCreationOptions} [opts={}]
     * @returns The path to the resulting universal .apk. The .apk is stored in the internal cache
     * by default.
     * @throws {Error} If there was an error while creating the universal .apk
     */
    function extractUniversalApk(aabPath: string, opts?: ApkCreationOptions | undefined): Promise<string>;
}

type SettingsAppStartupOptions = {
    /**
     * The maximum number of milliseconds
     * to wait until the app has started
     */
    timeout?: number | undefined;
    /**
     * Whether to restore
     * the activity which was the current one before Settings startup
     */
    shouldRestoreCurrentApp?: boolean | undefined;
};
type Location = {
    /**
     * - Valid longitude value.
     */
    longitude: number | string;
    /**
     * - Valid latitude value.
     */
    latitude: number | string;
    /**
     * - Valid altitude value.
     */
    altitude?: string | number | null | undefined;
    /**
     * - Number of satellites being tracked (1-12).
     * This value is ignored on real devices.
     */
    satellites?: string | number | null | undefined;
    /**
     * - Valid speed value.
     * Should be greater than 0.0 meters/second for real devices or 0.0 knots
     * for emulators.
     */
    speed?: string | number | null | undefined;
};
type SmsListOptions = {
    /**
     * - The maximum count of recent messages
     * to retrieve
     */
    max?: number | undefined;
};
type SmsListResult = {
    items: SmsListResultItem[];
    total: number;
};
type SmsListResultItem = {
    id: string;
    address: string;
    person: string | null;
    date: string;
    read: string;
    status: string;
    type: string;
    subject: string | null;
    body: string;
    serviceCenter: string | null;
};
type SettingsClientCommands = typeof commands;
declare namespace commands {
    /**
     * @typedef {Object} SettingsAppStartupOptions
     * @property {number} [timeout=5000] The maximum number of milliseconds
     * to wait until the app has started
     * @property {boolean} [shouldRestoreCurrentApp=false] Whether to restore
     * the activity which was the current one before Settings startup
     */
    /**
     * Ensures that Armor Settings helper application is running
     * and starts it if necessary
     *
     * @this {import('../adb.js').ADB}
     * @param {SettingsAppStartupOptions} [opts={}]
     * @throws {Error} If Armor Settings has failed to start
     * @returns {Promise<import('../adb.js').ADB>} self instance for chaining
     */
    function requireRunningSettingsApp(this: ADB$1, opts?: SettingsAppStartupOptions | undefined): Promise<ADB$1>;
    /**
     * Change the state of WiFi on the device under test.
     *
     * @this {import('../adb.js').ADB}
     * @param {boolean} on - True to enable and false to disable it.
     * @param {boolean} [isEmulator=false] - Set it to true if the device under test
     *                                       is an emulator rather than a real device.
     */
    function setWifiState(this: ADB$1, on: boolean, isEmulator?: boolean | undefined): Promise<void>;
    /**
     * Change the state of Data transfer on the device under test.
     *
     * @this {import('../adb.js').ADB}
     * @param {boolean} on - True to enable and false to disable it.
     * @param {boolean} [isEmulator=false] - Set it to true if the device under test
     *                                       is an emulator rather than a real device.
     */
    function setDataState(this: ADB$1, on: boolean, isEmulator?: boolean | undefined): Promise<void>;
    /**
     * Change the state of animation on the device under test.
     * Animation on the device is controlled by the following global properties:
     * [ANIMATOR_DURATION_SCALE]{@link https://developer.android.com/reference/android/provider/Settings.Global.html#ANIMATOR_DURATION_SCALE},
     * [TRANSITION_ANIMATION_SCALE]{@link https://developer.android.com/reference/android/provider/Settings.Global.html#TRANSITION_ANIMATION_SCALE},
     * [WINDOW_ANIMATION_SCALE]{@link https://developer.android.com/reference/android/provider/Settings.Global.html#WINDOW_ANIMATION_SCALE}.
     * This method sets all this properties to 0.0 to disable (1.0 to enable) animation.
     *
     * Turning off animation might be useful to improve stability
     * and reduce tests execution time.
     *
     * @this {import('../adb.js').ADB}
     * @param {boolean} on - True to enable and false to disable it.
     */
    function setAnimationState(this: ADB$1, on: boolean): Promise<void>;
    /**
     * Change the locale on the device under test. Don't need to reboot the device after changing the locale.
     * This method sets an arbitrary locale following:
     *   https://developer.android.com/reference/java/util/Locale.html
     *   https://developer.android.com/reference/java/util/Locale.html#Locale(java.lang.String,%20java.lang.String)
     *
     * @this {import('../adb.js').ADB}
     * @param {string} language - Language. e.g. en, ja
     * @param {string} country - Country. e.g. US, JP
     * @param {string?} [script=null] - Script. e.g. Hans in `zh-Hans-CN`
     */
    function setDeviceSysLocaleViaSettingApp(this: ADB$1, language: string, country: string, script?: string | null | undefined): Promise<void>;
    /**
     * @typedef {Object} Location
     * @property {number|string} longitude - Valid longitude value.
     * @property {number|string} latitude - Valid latitude value.
     * @property {?number|string} [altitude] - Valid altitude value.
     * @property {?number|string} [satellites=12] - Number of satellites being tracked (1-12).
     * This value is ignored on real devices.
     * @property {?number|string} [speed] - Valid speed value.
     * Should be greater than 0.0 meters/second for real devices or 0.0 knots
     * for emulators.
     */
    /**
     * Emulate geolocation coordinates on the device under test.
     *
     * @this {import('../adb.js').ADB}
     * @param {Location} location - Location object. The `altitude` value is ignored
     * while mocking the position.
     * @param {boolean} [isEmulator=false] - Set it to true if the device under test
     *                                       is an emulator rather than a real device.
     */
    function setGeoLocation(this: ADB$1, location: Location, isEmulator?: boolean | undefined): Promise<void>;
    /**
     * Get the current cached GPS location from the device under test.
     *
     * @this {import('../adb.js').ADB}
     * @returns {Promise<Location>} The current location
     * @throws {Error} If the current location cannot be retrieved
     */
    function getGeoLocation(this: ADB$1): Promise<Location>;
    /**
     * Sends an async request to refresh the GPS cache.
     * This feature only works if the device under test has
     * Google Play Services installed. In case the vanilla
     * LocationManager is used the device API level must be at
     * version 30 (Android R) or higher.
     *
     * @this {import('../adb.js').ADB}
     * @param {number} timeoutMs The maximum number of milliseconds
     * to block until GPS cache is refreshed. Providing zero or a negative
     * value to it skips waiting completely.
     *
     * @throws {Error} If the GPS cache cannot be refreshed.
     */
    function refreshGeoLocationCache(this: ADB$1, timeoutMs?: number): Promise<void>;
    /**
     * Performs the given editor action on the focused input field.
     * This method requires Armor Settings helper to be installed on the device.
     * No exception is thrown if there was a failure while performing the action.
     * You must investigate the logcat output if something did not work as expected.
     *
     * @this {import('../adb.js').ADB}
     * @param {string|number} action - Either action code or name. The following action
     *                                 names are supported: `normal, unspecified, none,
     *                                 go, search, send, next, done, previous`
     */
    function performEditorAction(this: ADB$1, action: string | number): Promise<void>;
    /**
     * Retrieves the text content of the device's clipboard.
     * The method works for Android below and above 29.
     * It temorarily enforces the IME setting in order to workaround
     * security limitations if needed.
     * This method only works if Armor Settings v. 2.15+ is installed
     * on the device under test
     *
     * @this {import('../adb.js').ADB}
     * @returns {Promise<string>} The actual content of the main clipboard as
     * base64-encoded string or an empty string if the clipboard is empty
     * @throws {Error} If there was a problem while getting the
     * clipboard contant
     */
    function getClipboard(this: ADB$1): Promise<string>;
    /**
     * Retrieves Android notifications via Armor Settings helper.
     * Armor Settings app itself must be *manually* granted to access notifications
     * under device Settings in order to make this feature working.
     * Armor Settings helper keeps all the active notifications plus
     * notifications that appeared while it was running in the internal buffer,
     * but no more than 100 items altogether. Newly appeared notifications
     * are always added to the head of the notifications array.
     * The `isRemoved` flag is set to `true` for notifications that have been removed.
     *
     * See https://developer.android.com/reference/android/service/notification/StatusBarNotification
     * and https://developer.android.com/reference/android/app/Notification.html
     * for more information on available notification properties and their values.
     *
     * @this {import('../adb.js').ADB}
     * ```json
     * {
     *   "statusBarNotifications":[
     *     {
     *       "isGroup":false,
     *       "packageName":"com.ait.armorsettings",
     *       "isClearable":false,
     *       "isOngoing":true,
     *       "id":1,
     *       "tag":null,
     *       "notification":{
     *         "title":null,
     *         "bigTitle":"Armor Settings",
     *         "text":null,
     *         "bigText":"Keep this service running, so Armor for Android can properly interact with several system APIs",
     *         "tickerText":null,
     *         "subText":null,
     *         "infoText":null,
     *         "template":"android.app.Notification$BigTextStyle"
     *       },
     *       "userHandle":0,
     *       "groupKey":"0|com.ait.armorsettings|1|null|10133",
     *       "overrideGroupKey":null,
     *       "postTime":1576853518850,
     *       "key":"0|com.ait.armorsettings|1|null|10133",
     *       "isRemoved":false
     *     }
     *   ]
     * }
     * ```
     * @throws {Error} If there was an error while getting the notifications list
     */
    function getNotifications(this: ADB$1): Promise<any>;
    /**
     * @typedef {Object} SmsListOptions
     * @property {number} [max=100] - The maximum count of recent messages
     * to retrieve
     */
    /**
     * @typedef SmsListResult
     * @property {SmsListResultItem[]} items
     * @property {number} total
     */
    /**
     * @privateRemarks XXX: WAG
     * @typedef SmsListResultItem
     * @property {string} id
     * @property {string} address
     * @property {string|null} person
     * @property {string} date
     * @property {string} read
     * @property {string} status
     * @property {string} type
     * @property {string|null} subject
     * @property {string} body
     * @property {string|null} serviceCenter
     */
    /**
     * Retrieves the list of the most recent SMS
     * properties list via Armor Settings helper.
     * Messages are sorted by date in descending order.
     *
     * @this {import('../adb.js').ADB}
     * @param {SmsListOptions} opts
     * @returns {Promise<SmsListResult>} The example output is:
     * ```json
     * {
     *   "items":[
     *     {
     *       "id":"2",
     *       "address":"+123456789",
     *       "person":null,
     *       "date":"1581936422203",
     *       "read":"0",
     *       "status":"-1",
     *       "type":"1",
     *       "subject":null,
     *       "body":"\"text message2\"",
     *       "serviceCenter":null
     *     },
     *     {
     *       "id":"1",
     *       "address":"+123456789",
     *       "person":null,
     *       "date":"1581936382740",
     *       "read":"0",
     *       "status":"-1",
     *       "type":"1",
     *       "subject":null,
     *       "body":"\"text message\"",
     *       "serviceCenter":null
     *     }
     *   ],
     *   "total":2
     * }
     * ```
     * @throws {Error} If there was an error while getting the SMS list
     */
    function getSmsList(this: ADB$1, opts?: SmsListOptions): Promise<SmsListResult>;
    /**
     * Types the given Unicode string.
     * It is expected that the focus is already put
     * to the destination input field before this method is called.
     *
     * @this {import('../adb.js').ADB}
     * @param {string} text The string to type
     * @returns {Promise<boolean>} `true` if the input text has been successfully sent to adb
     */
    function typeUnicode(this: ADB$1, text: string): Promise<boolean>;
    /**
     * Performs recursive media scan at the given destination.
     * All successfully scanned items are being added to the device's
     * media library.
     *
     * @this {import('../adb.js').ADB}
     * @param {string} destination File/folder path on the remote device.
     * @throws {Error} If there was an unexpected error by scanning.
     */
    function scanMedia(this: ADB$1, destination: string): Promise<void>;
}

type APKInfo = {
    /**
     * - The name of application package, for example 'com.acme.app'.
     */
    apkPackage: string;
    /**
     * - The name of main application activity.
     */
    apkActivity?: string | undefined;
};
type ManifestMethods = typeof manifestMethods;
declare namespace manifestMethods {
    /**
     * @typedef {Object} APKInfo
     * @property {string} apkPackage - The name of application package, for example 'com.acme.app'.
     * @property {string} [apkActivity] - The name of main application activity.
     */
    /**
     * Extract package and main activity name from application manifest.
     *
     * @this {import('../adb.js').ADB}
     * @param {string} appPath - The full path to application .apk(s) package
     * @return {Promise<APKInfo>} The parsed application info.
     * @throws {error} If there was an error while getting the data from the given
     *                 application package.
     */
    function packageAndLaunchActivityFromManifest(this: ADB$1, appPath: string): Promise<APKInfo>;
    /**
     * Extract target SDK version from application manifest.
     *
     * @param {string} appPath - The full path to .apk(s) package.
     * @return {Promise<number>} The version of the target SDK.
     * @throws {error} If there was an error while getting the data from the given
     *                 application package.
     */
    function targetSdkVersionFromManifest(appPath: string): Promise<number>;
    /**
     * Extract target SDK version from package information.
     *
     * @this {import('../adb.js').ADB}
     * @param {string} pkg - The class name of the package installed on the device under test.
     * @param {string?} [cmdOutput=null] - Optional parameter containing the output of
     * _dumpsys package_ command. It may speed up the method execution.
     * @return {Promise<number>} The version of the target SDK.
     */
    function targetSdkVersionUsingPKG(this: ADB$1, pkg: string, cmdOutput?: string | null | undefined): Promise<number>;
    /**
     * Create binary representation of package manifest (usually AndroidManifest.xml).
     * `${manifest}.apk` file will be created as the result of this method
     * containing the compiled manifest.
     *
     * @param {string} manifest - Full path to the initial manifest template
     * @param {string} manifestPackage - The name of the manifest package
     * @param {string} targetPackage - The name of the destination package
     */
    function compileManifest(manifest: string, manifestPackage: string, targetPackage: string): Promise<void>;
    /**
     * Replace/insert the specially precompiled manifest file into the
     * particular package.
     *
     * @param {string} manifest - Full path to the precompiled manifest
     *                            created by `compileManifest` method call
     *                            without .apk extension
     * @param {string} srcApk - Full path to the existing valid application package, where
     *                          this manifest has to be insetred to. This package
     *                          will NOT be modified.
     * @param {string} dstApk - Full path to the resulting package.
     *                          The file will be overridden if it already exists.
     */
    function insertManifest(manifest: string, srcApk: string, dstApk: string): Promise<void>;
    /**
     * Check whether package manifest contains Internet permissions.
     *
     * @this {import('../adb.js').ADB}
     * @param {string} appPath - The full path to .apk(s) package.
     * @return {Promise<boolean>} True if the manifest requires Internet access permission.
     */
    function hasInternetPermissionFromManifest(this: ADB$1, appPath: string): Promise<boolean>;
}

type KeyboardState = {
    /**
     * - Whether soft keyboard is currently visible.
     */
    isKeyboardShown: boolean;
    /**
     * - Whether the keyboard can be closed.
     */
    canCloseKeyboard: boolean;
};
type KeyboardCommands = typeof keyboardCommands;
declare namespace keyboardCommands {
    /**
     * Hides software keyboard if it is visible.
     * Noop if the keyboard is already hidden.
     *
     * @this {import('../adb.js').ADB}
     * @param {number} [timeoutMs=1000] For how long to wait (in milliseconds)
     * until the keyboard is actually hidden.
     * @returns {Promise<boolean>} `false` if the keyboard was already hidden
     * @throws {Error} If the keyboard cannot be hidden.
     */
    function hideKeyboard(this: ADB$1, timeoutMs?: number | undefined): Promise<boolean>;
    /**
     * @typedef {Object} KeyboardState
     * @property {boolean} isKeyboardShown - Whether soft keyboard is currently visible.
     * @property {boolean} canCloseKeyboard - Whether the keyboard can be closed.
     */
    /**
     * Retrieve the state of the software keyboard on the device under test.
     *
     * @this {import('../adb.js').ADB}
     * @return {Promise<KeyboardState>} The keyboard state.
     */
    function isSoftKeyboardPresent(this: ADB$1): Promise<KeyboardState>;
}

type LockManagementCommands = typeof lockManagementMethods;
declare namespace lockManagementMethods {
    /**
     * Check whether the device supports lock settings management with `locksettings`
     * command line tool. This tool has been added to Android toolset since  API 27 Oreo
     *
     * @this {import('../adb.js').ADB}
     * @return {Promise<boolean>} True if the management is supported. The result is cached per ADB instance
     */
    function isLockManagementSupported(this: ADB$1): Promise<boolean>;
    /**
     * Check whether the given credential is matches to the currently set one.
     *
     * @this {import('../adb.js').ADB}
     * @param {string?} [credential=null] The credential value. It could be either
     * pin, password or a pattern. A pattern is specified by a non-separated list
     * of numbers that index the cell on the pattern in a 1-based manner in left
     * to right and top to bottom order, i.e. the top-left cell is indexed with 1,
     * whereas the bottom-right cell is indexed with 9. Example: 1234.
     * null/empty value assumes the device has no lock currently set.
     * @return {Promise<boolean>} True if the given credential matches to the device's one
     * @throws {Error} If the verification faces an unexpected error
     */
    function verifyLockCredential(this: ADB$1, credential?: string | null | undefined): Promise<boolean>;
    /**
     * Clears current lock credentials. Usually it takes several seconds for a device to
     * sync the credential state after this method returns.
     *
     * @this {import('../adb.js').ADB}
     * @param {string?} [credential=null] The credential value. It could be either
     * pin, password or a pattern. A pattern is specified by a non-separated list
     * of numbers that index the cell on the pattern in a 1-based manner in left
     * to right and top to bottom order, i.e. the top-left cell is indexed with 1,
     * whereas the bottom-right cell is indexed with 9. Example: 1234.
     * null/empty value assumes the device has no lock currently set.
     * @throws {Error} If operation faces an unexpected error
     */
    function clearLockCredential(this: ADB$1, credential?: string | null | undefined): Promise<void>;
    /**
     * Checks whether the device is locked with a credential (either pin or a password
     * or a pattern).
     *
     * @this {import('../adb.js').ADB}
     * @returns {Promise<boolean>} `true` if the device is locked
     * @throws {Error} If operation faces an unexpected error
     */
    function isLockEnabled(this: ADB$1): Promise<boolean>;
    /**
     * Sets the device lock.
     *
     * @this {import('../adb.js').ADB}
     * @param {string} credentialType One of: password, pin, pattern.
     * @param {string} credential A non-empty credential value to be set.
     * Make sure your new credential matches to the actual system security requirements,
     * e.g. a minimum password length. A pattern is specified by a non-separated list
     * of numbers that index the cell on the pattern in a 1-based manner in left
     * to right and top to bottom order, i.e. the top-left cell is indexed with 1,
     * whereas the bottom-right cell is indexed with 9. Example: 1234.
     * @param {string?} [oldCredential=null] An old credential string.
     * It is only required to be set in case you need to change the current
     * credential rather than to set a new one. Setting it to a wrong value will
     * make this method to fail and throw an exception.
     * @throws {Error} If there was a failure while verifying input arguments or setting
     * the credential
     */
    function setLockCredential(this: ADB$1, credentialType: string, credential: string, oldCredential?: string | null | undefined): Promise<void>;
    /**
     * Retrieve the screen lock state of the device under test.
     *
     * @this {import('../adb.js').ADB}
     * @return {Promise<boolean>} True if the device is locked.
     */
    function isScreenLocked(this: ADB$1): Promise<boolean>;
    /**
     * Dismisses keyguard overlay.
     * @this {import('../adb.js').ADB}
     */
    function dismissKeyguard(this: ADB$1): Promise<void>;
    /**
     * Presses the corresponding key combination to make sure the device's screen
     * is not turned off and is locked if the latter is enabled.
     * @this {import('../adb.js').ADB}
     */
    function cycleWakeUp(this: ADB$1): Promise<void>;
}

declare class ADB {
    adbHost?: string;
    adbPort?: number;
    _apiLevel: number | undefined;
    _logcatStartupParams: LogcatOpts | undefined;
    _doesPsSupportAOption: boolean | undefined;
    _isPgrepAvailable: boolean | undefined;
    _canPgrepUseFullCmdLineSearch: boolean | undefined;
    _isPidofAvailable: boolean | undefined;
    _memoizedFeatures: (() => Promise<string>) | undefined;
    _areExtendedLsOptionsSupported: boolean | undefined;
    remoteAppsCache: LRUCache<string, string> | undefined;
    _isLockManagementSupported: boolean | undefined;
    executable: ADBExecutable;
    constructor(opts?: Partial<ADBOptions>);
    /**
     * Create a new instance of `ADB` that inherits configuration from this `ADB` instance.
     * This avoids the need to call `ADB.createADB()` multiple times.
     * @param opts - Additional options mapping to pass to the `ADB` constructor.
     * @returns The resulting class instance.
     */
    clone(opts?: Partial<ADBOptions>): ADB;
    static createADB(opts: Partial<ADBOptions>): Promise<ADB>;
}

/**
 * @privateRemarks
 * This is just an interface which mixes methods into the root `ADB` class.
 *
 * @module
 */

declare module './adb' {
    interface ADB extends ADBCommands, AabUtils, ApkUtils, ApksUtils, SystemCalls, ADBOptions, SettingsClientCommands, ADBEmuCommands, LockManagementCommands, ManifestMethods, KeyboardCommands, ApkSigningCommands {
    }
}
//# sourceMappingURL=mixins.d.ts.map

declare class ProtocolConverter {
    constructor(proxyFunc: any, log?: null);
    proxyFunc: any;
    _downstreamProtocol: any;
    _log: any;
    get log(): any;
    set downstreamProtocol(value: any);
    get downstreamProtocol(): any;
    /**
     * W3C /timeouts can take as many as 3 timeout types at once, MJSONWP /timeouts only takes one
     * at a time. So if we're using W3C and proxying to MJSONWP and there's more than one timeout type
     * provided in the request, we need to do 3 proxies and combine the result
     *
     * @param {Object} body Request body
     * @return {Array} Array of W3C + MJSONWP compatible timeout objects
     */
    getTimeoutRequestObjects(body: any): any[];
    /**
     * Proxy an array of timeout objects and merge the result
     * @param {String} url Endpoint url
     * @param {String} method Endpoint method
     * @param {Object} body Request body
     */
    proxySetTimeouts(url: string, method: string, body: any): Promise<any[]>;
    proxySetWindow(url: any, method: any, body: any): Promise<any>;
    proxySetValue(url: any, method: any, body: any): Promise<any>;
    proxySetFrame(url: any, method: any, body: any): Promise<any>;
    proxyPerformActions(url: any, method: any, body: any): Promise<any>;
    proxyReleaseActions(url: any, method: any): Promise<any>;
    /**
     * Handle "crossing" endpoints for the case
     * when upstream and downstream drivers operate different protocols
     *
     * @param {string} commandName
     * @param {string} url
     * @param {string} method
     * @param {?string|object} body
     * @returns The proxyfying result as [response, responseBody] tuple
     */
    convertAndProxy(commandName: string, url: string, method: string, body: (string | object) | null): Promise<any>;
}

declare class JWProxy {
    constructor(opts?: {});
    /** @type {string} */
    scheme: string;
    /** @type {string} */
    server: string;
    /** @type {number} */
    port: number;
    /** @type {string} */
    base: string;
    /** @type {string} */
    reqBasePath: string;
    /** @type {string?} */
    sessionId: string | null;
    /** @type {number} */
    timeout: number;
    _activeRequests: any[];
    _downstreamProtocol: any;
    httpAgent: http.Agent;
    httpsAgent: https.Agent;
    protocolConverter: ProtocolConverter;
    _log: any;
    get log(): any;
    /**
     * Performs requests to the downstream server
     *
     * @private - Do not call this method directly,
     * it uses client-specific arguments and responses!
     *
     * @param {import('axios').AxiosRequestConfig} requestConfig
     * @returns {Promise<import('axios').AxiosResponse>}
     */
    private request;
    getActiveRequestsCount(): number;
    cancelActiveRequests(): void;
    endpointRequiresSessionId(endpoint: any): boolean;
    set downstreamProtocol(value: any);
    get downstreamProtocol(): any;
    getUrlForProxy(url: any): string;
    proxy(url: any, method: any, body?: null): Promise<any[]>;
    getProtocolFromResBody(resObj: any): "MJSONWP" | "W3C" | undefined;
    /**
     *
     * @param {string} url
     * @param {import('armor-types').HTTPMethod} method
     * @returns {string|undefined}
     */
    requestToCommandName(url: string, method: HTTPMethod): string | undefined;
    /**
     *
     * @param {string} url
     * @param {import('armor-types').HTTPMethod} method
     * @param {any?} body
     */
    proxyCommand(url: string, method: HTTPMethod, body?: any | null): Promise<any>;
    /**
     *
     * @param {string} url
     * @param {import('armor-types').HTTPMethod} method
     * @param {any?} body
     * @returns {Promise<unknown>}
     */
    command(url: string, method: HTTPMethod, body?: any | null): Promise<unknown>;
    getSessionIdFromUrl(url: any): any;
    proxyReqRes(req: any, res: any): Promise<void>;
}

declare module '../driver' {
    interface BaseDriver<C extends Constraints> extends IEventCommands {
    }
}
//# sourceMappingURL=event.d.ts.map

declare module '../driver' {
    interface BaseDriver<C extends Constraints> extends IFindCommands {
    }
}
//# sourceMappingURL=find.d.ts.map

declare module '../driver' {
    interface BaseDriver<C extends Constraints> extends ILogCommands {
    }
}
//# sourceMappingURL=log.d.ts.map

declare module '../driver' {
    interface BaseDriver<C extends Constraints> extends ITimeoutCommands {
    }
}
//# sourceMappingURL=timeout.d.ts.map

declare module '../driver' {
    interface BaseDriver<C extends Constraints> extends IExecuteCommands {
    }
}
//# sourceMappingURL=execute.d.ts.map

declare class UiAutomator2Server {
    constructor(log: any, opts?: {});
    /** @type {string} */
    host: string;
    /** @type {number} */
    systemPort: number;
    /** @type {import('ait-adb').ADB} */
    adb: ADB;
    /** @type {boolean} */
    disableWindowAnimation: boolean;
    /** @type {boolean|undefined} */
    disableSuppressAccessibilityService: boolean | undefined;
    log: any;
    jwproxy: UIA2Proxy;
    proxyReqRes: (req: any, res: any) => Promise<void>;
    proxyCommand: (url: string, method: HTTPMethod, body?: any) => Promise<unknown>;
    prepareServerPackage(appPath: any, appId: any, tmpRoot: any): Promise<{
        wasSigned: boolean;
        installState: InstallState;
        appPath: any;
        appId: any;
    }>;
    /**
     * Installs the apks on to the device or emulator.
     *
     * @param {number} installTimeout - Installation timeout
     */
    installServerApk(installTimeout?: number): Promise<void>;
    verifyServicesAvailability(): Promise<void>;
    startSession(caps: any): Promise<void>;
    startInstrumentationProcess(): Promise<void>;
    deleteSession(): Promise<void>;
    cleanupAutomationLeftovers(strictCleanup?: boolean): Promise<void>;
}
declare class UIA2Proxy extends JWProxy {
    /** @type {boolean} */
    didInstrumentationExit: boolean;
    proxyCommand(url: any, method: any, body?: null): Promise<any>;
}

declare class AndroidUiautomator2Driver extends AndroidDriver implements ExternalDriver<Uiautomator2Constraints, string, StringRecord> {
    static newMethodMap: {
        readonly '/session/:sessionId/armor/device/get_clipboard': {
            readonly POST: {
                readonly command: "getClipboard";
                readonly payloadParams: {
                    readonly optional: readonly ["contentType"];
                };
            };
        };
        readonly '/session/:sessionId/timeouts/implicit_wait': {
            readonly POST: {
                readonly command: "implicitWait";
                readonly payloadParams: {
                    readonly required: readonly ["ms"];
                };
            };
        };
        readonly '/session/:sessionId/ime/available_engines': {
            readonly GET: {
                readonly command: "availableIMEEngines";
            };
        };
        readonly '/session/:sessionId/ime/active_engine': {
            readonly GET: {
                readonly command: "getActiveIMEEngine";
            };
        };
        readonly '/session/:sessionId/ime/activated': {
            readonly GET: {
                readonly command: "isIMEActivated";
            };
        };
        readonly '/session/:sessionId/ime/deactivate': {
            readonly POST: {
                readonly command: "deactivateIMEEngine";
            };
        };
        readonly '/session/:sessionId/ime/activate': {
            readonly POST: {
                readonly command: "activateIMEEngine";
                readonly payloadParams: {
                    readonly required: readonly ["engine"];
                };
            };
        };
        readonly '/session/:sessionId/window/:windowhandle/size': {
            readonly GET: {
                readonly command: "getWindowSize";
            };
        };
        readonly '/session/:sessionId/keys': {
            readonly POST: {
                readonly command: "keys";
                readonly payloadParams: {
                    readonly required: readonly ["value"];
                };
            };
        };
        readonly '/session/:sessionId/element/:elementId/location': {
            readonly GET: {
                readonly command: "getLocation";
            };
        };
        readonly '/session/:sessionId/element/:elementId/location_in_view': {
            readonly GET: {
                readonly command: "getLocationInView";
            };
        };
        readonly '/session/:sessionId/element/:elementId/size': {
            readonly GET: {
                readonly command: "getSize";
            };
        };
        readonly '/session/:sessionId/touch/click': {
            readonly POST: {
                readonly command: "click";
                readonly payloadParams: {
                    readonly required: readonly ["element"];
                };
            };
        };
        readonly '/session/:sessionId/touch/down': {
            readonly POST: {
                readonly command: "touchDown";
                readonly payloadParams: {
                    readonly required: readonly ["x", "y"];
                };
            };
        };
        readonly '/session/:sessionId/touch/up': {
            readonly POST: {
                readonly command: "touchUp";
                readonly payloadParams: {
                    readonly required: readonly ["x", "y"];
                };
            };
        };
        readonly '/session/:sessionId/touch/move': {
            readonly POST: {
                readonly command: "touchMove";
                readonly payloadParams: {
                    readonly required: readonly ["x", "y"];
                };
            };
        };
        readonly '/session/:sessionId/touch/longclick': {
            readonly POST: {
                readonly command: "touchLongClick";
                readonly payloadParams: {
                    readonly required: readonly ["elements"];
                };
            };
        };
        readonly '/session/:sessionId/touch/flick': {
            readonly POST: {
                readonly command: "flick";
                readonly payloadParams: {
                    readonly optional: readonly ["element", "xspeed", "yspeed", "xoffset", "yoffset", "speed"];
                };
            };
        };
        readonly '/session/:sessionId/touch/perform': {
            readonly POST: {
                readonly command: "performTouch";
                readonly payloadParams: {
                    readonly wrap: "actions";
                    readonly required: readonly ["actions"];
                };
            };
        };
        readonly '/session/:sessionId/touch/multi/perform': {
            readonly POST: {
                readonly command: "performMultiAction";
                readonly payloadParams: {
                    readonly required: readonly ["actions"];
                    readonly optional: readonly ["elementId"];
                };
            };
        };
        readonly '/session/:sessionId/armor/device/lock': {
            readonly POST: {
                readonly command: "lock";
                readonly payloadParams: {
                    readonly optional: readonly ["seconds"];
                };
            };
        };
        readonly '/session/:sessionId/armor/device/unlock': {
            readonly POST: {
                readonly command: "unlock";
            };
        };
        readonly '/session/:sessionId/armor/device/is_locked': {
            readonly POST: {
                readonly command: "isLocked";
            };
        };
        readonly '/session/:sessionId/armor/start_recording_screen': {
            readonly POST: {
                readonly command: "startRecordingScreen";
                readonly payloadParams: {
                    readonly optional: readonly ["options"];
                };
            };
        };
        readonly '/session/:sessionId/armor/stop_recording_screen': {
            readonly POST: {
                readonly command: "stopRecordingScreen";
                readonly payloadParams: {
                    readonly optional: readonly ["options"];
                };
            };
        };
        readonly '/session/:sessionId/armor/performanceData/types': {
            readonly POST: {
                readonly command: "getPerformanceDataTypes";
            };
        };
        readonly '/session/:sessionId/armor/getPerformanceData': {
            readonly POST: {
                readonly command: "getPerformanceData";
                readonly payloadParams: {
                    readonly required: readonly ["packageName", "dataType"];
                    readonly optional: readonly ["dataReadTimeout"];
                };
            };
        };
        readonly '/session/:sessionId/armor/device/press_keycode': {
            readonly POST: {
                readonly command: "pressKeyCode";
                readonly payloadParams: {
                    readonly required: readonly ["keycode"];
                    readonly optional: readonly ["metastate", "flags"];
                };
            };
        };
        readonly '/session/:sessionId/armor/device/long_press_keycode': {
            readonly POST: {
                readonly command: "longPressKeyCode";
                readonly payloadParams: {
                    readonly required: readonly ["keycode"];
                    readonly optional: readonly ["metastate", "flags"];
                };
            };
        };
        readonly '/session/:sessionId/armor/device/finger_print': {
            readonly POST: {
                readonly command: "fingerprint";
                readonly payloadParams: {
                    readonly required: readonly ["fingerprintId"];
                };
            };
        };
        readonly '/session/:sessionId/armor/device/send_sms': {
            readonly POST: {
                readonly command: "sendSMS";
                readonly payloadParams: {
                    readonly required: readonly ["phoneNumber", "message"];
                };
            };
        };
        readonly '/session/:sessionId/armor/device/gsm_call': {
            readonly POST: {
                readonly command: "gsmCall";
                readonly payloadParams: {
                    readonly required: readonly ["phoneNumber", "action"];
                };
            };
        };
        readonly '/session/:sessionId/armor/device/gsm_signal': {
            readonly POST: {
                readonly command: "gsmSignal";
                readonly payloadParams: {
                    readonly required: readonly ["signalStrength"];
                };
            };
        };
        readonly '/session/:sessionId/armor/device/gsm_voice': {
            readonly POST: {
                readonly command: "gsmVoice";
                readonly payloadParams: {
                    readonly required: readonly ["state"];
                };
            };
        };
        readonly '/session/:sessionId/armor/device/power_capacity': {
            readonly POST: {
                readonly command: "powerCapacity";
                readonly payloadParams: {
                    readonly required: readonly ["percent"];
                };
            };
        };
        readonly '/session/:sessionId/armor/device/power_ac': {
            readonly POST: {
                readonly command: "powerAC";
                readonly payloadParams: {
                    readonly required: readonly ["state"];
                };
            };
        };
        readonly '/session/:sessionId/armor/device/network_speed': {
            readonly POST: {
                readonly command: "networkSpeed";
                readonly payloadParams: {
                    readonly required: readonly ["netspeed"];
                };
            };
        };
        readonly '/session/:sessionId/armor/device/keyevent': {
            readonly POST: {
                readonly command: "keyevent";
                readonly payloadParams: {
                    readonly required: readonly ["keycode"];
                    readonly optional: readonly ["metastate"];
                };
            };
        };
        readonly '/session/:sessionId/armor/device/current_activity': {
            readonly GET: {
                readonly command: "getCurrentActivity";
            };
        };
        readonly '/session/:sessionId/armor/device/current_package': {
            readonly GET: {
                readonly command: "getCurrentPackage";
            };
        };
        readonly '/session/:sessionId/armor/device/app_state': {
            readonly POST: {
                readonly command: "queryAppState";
                readonly payloadParams: {
                    readonly required: readonly [readonly ["appId"], readonly ["bundleId"]];
                };
            };
        };
        readonly '/session/:sessionId/armor/device/toggle_airplane_mode': {
            readonly POST: {
                readonly command: "toggleFlightMode";
            };
        };
        readonly '/session/:sessionId/armor/device/toggle_data': {
            readonly POST: {
                readonly command: "toggleData";
            };
        };
        readonly '/session/:sessionId/armor/device/toggle_wifi': {
            readonly POST: {
                readonly command: "toggleWiFi";
            };
        };
        readonly '/session/:sessionId/armor/device/toggle_location_services': {
            readonly POST: {
                readonly command: "toggleLocationServices";
            };
        };
        readonly '/session/:sessionId/armor/device/open_notifications': {
            readonly POST: {
                readonly command: "openNotifications";
            };
        };
        readonly '/session/:sessionId/armor/device/start_activity': {
            readonly POST: {
                readonly command: "startActivity";
                readonly payloadParams: {
                    readonly required: readonly ["appPackage", "appActivity"];
                    readonly optional: readonly ["appWaitPackage", "appWaitActivity", "intentAction", "intentCategory", "intentFlags", "optionalIntentArguments", "dontStopAppOnReset"];
                };
            };
        };
        readonly '/session/:sessionId/armor/device/system_bars': {
            readonly GET: {
                readonly command: "getSystemBars";
            };
        };
        readonly '/session/:sessionId/armor/device/display_density': {
            readonly GET: {
                readonly command: "getDisplayDensity";
            };
        };
        readonly '/session/:sessionId/armor/app/launch': {
            readonly POST: {
                readonly command: "launchApp";
            };
        };
        readonly '/session/:sessionId/armor/app/close': {
            readonly POST: {
                readonly command: "closeApp";
            };
        };
        readonly '/session/:sessionId/armor/app/reset': {
            readonly POST: {
                readonly command: "reset";
            };
        };
        readonly '/session/:sessionId/armor/app/background': {
            readonly POST: {
                readonly command: "background";
                readonly payloadParams: {
                    readonly required: readonly ["seconds"];
                };
            };
        };
        readonly '/session/:sessionId/armor/app/strings': {
            readonly POST: {
                readonly command: "getStrings";
                readonly payloadParams: {
                    readonly optional: readonly ["language", "stringFile"];
                };
            };
        };
        readonly '/session/:sessionId/armor/element/:elementId/value': {
            readonly POST: {
                readonly command: "setValueImmediate";
                readonly payloadParams: {
                    readonly required: readonly ["text"];
                };
            };
        };
        readonly '/session/:sessionId/armor/element/:elementId/replace_value': {
            readonly POST: {
                readonly command: "replaceValue";
                readonly payloadParams: {
                    readonly required: readonly ["text"];
                };
            };
        };
    };
    static executeMethodMap: {
        readonly 'mobile: shell': {
            readonly command: "mobileShell";
            readonly params: {
                readonly required: readonly ["command"];
                readonly optional: readonly ["args", "timeout", "includeStderr"];
            };
        };
        readonly 'mobile: execEmuConsoleCommand': {
            readonly command: "mobileExecEmuConsoleCommand";
            readonly params: {
                readonly required: readonly ["command"];
                readonly optional: readonly ["execTimeout", "connTimeout", "initTimeout"];
            };
        };
        readonly 'mobile: dragGesture': {
            readonly command: "mobileDragGesture";
            readonly params: {
                readonly optional: readonly ["elementId", "startX", "startY", "endX", "endY", "speed"];
            };
        };
        readonly 'mobile: flingGesture': {
            readonly command: "mobileFlingGesture";
            readonly params: {
                readonly required: readonly ["direction"];
                readonly optional: readonly ["elementId", "left", "top", "width", "height", "speed"];
            };
        };
        readonly 'mobile: doubleClickGesture': {
            readonly command: "mobileDoubleClickGesture";
            readonly params: {
                readonly optional: readonly ["elementId", "x", "y"];
            };
        };
        readonly 'mobile: clickGesture': {
            readonly command: "mobileClickGesture";
            readonly params: {
                readonly optional: readonly ["elementId", "x", "y"];
            };
        };
        readonly 'mobile: longClickGesture': {
            readonly command: "mobileLongClickGesture";
            readonly params: {
                readonly optional: readonly ["elementId", "x", "y", "duration"];
            };
        };
        readonly 'mobile: pinchCloseGesture': {
            readonly command: "mobilePinchCloseGesture";
            readonly params: {
                readonly required: readonly ["percent"];
                readonly optional: readonly ["elementId", "left", "top", "width", "height", "speed"];
            };
        };
        readonly 'mobile: pinchOpenGesture': {
            readonly command: "mobilePinchOpenGesture";
            readonly params: {
                readonly required: readonly ["percent"];
                readonly optional: readonly ["elementId", "left", "top", "width", "height", "speed"];
            };
        };
        readonly 'mobile: swipeGesture': {
            readonly command: "mobileSwipeGesture";
            readonly params: {
                readonly required: readonly ["direction", "percent"];
                readonly optional: readonly ["elementId", "left", "top", "width", "height", "speed"];
            };
        };
        readonly 'mobile: scrollGesture': {
            readonly command: "mobileScrollGesture";
            readonly params: {
                readonly required: readonly ["direction", "percent"];
                readonly optional: readonly ["elementId", "left", "top", "width", "height", "speed"];
            };
        };
        readonly 'mobile: scrollBackTo': {
            readonly command: "mobileScrollBackTo";
            readonly params: {
                readonly required: readonly ["elementId", "elementToId"];
            };
        };
        readonly 'mobile: scroll': {
            readonly command: "mobileScroll";
            readonly params: {
                readonly required: readonly ["strategy", "selector"];
                readonly optional: readonly ["elementId", "maxSwipes", "element"];
            };
        };
        readonly 'mobile: viewportScreenshot': {
            readonly command: "mobileViewportScreenshot";
        };
        readonly 'mobile: viewportRect': {
            readonly command: "mobileViewPortRect";
        };
        readonly 'mobile: deepLink': {
            readonly command: "mobileDeepLink";
            readonly params: {
                readonly required: readonly ["url", "package"];
                readonly optional: readonly ["waitForLaunch"];
            };
        };
        readonly 'mobile: startLogsBroadcast': {
            readonly command: "mobileStartLogsBroadcast";
        };
        readonly 'mobile: stopLogsBroadcast': {
            readonly command: "mobileStopLogsBroadcast";
        };
        readonly 'mobile: deviceidle': {
            readonly command: "mobileDeviceidle";
            readonly params: {
                readonly required: readonly ["action"];
                readonly optional: readonly ["packages"];
            };
        };
        readonly 'mobile: acceptAlert': {
            readonly command: "mobileAcceptAlert";
            readonly params: {
                readonly optional: readonly ["buttonLabel"];
            };
        };
        readonly 'mobile: dismissAlert': {
            readonly command: "mobileDismissAlert";
            readonly params: {
                readonly optional: readonly ["buttonLabel"];
            };
        };
        readonly 'mobile: batteryInfo': {
            readonly command: "mobileGetBatteryInfo";
        };
        readonly 'mobile: deviceInfo': {
            readonly command: "mobileGetDeviceInfo";
        };
        readonly 'mobile: getDeviceTime': {
            readonly command: "mobileGetDeviceTime";
            readonly params: {
                readonly optional: readonly ["format"];
            };
        };
        readonly 'mobile: changePermissions': {
            readonly command: "mobileChangePermissions";
            readonly params: {
                readonly required: readonly ["permissions"];
                readonly optional: readonly ["appPackage", "action", "target"];
            };
        };
        readonly 'mobile: getPermissions': {
            readonly command: "mobileGetPermissions";
            readonly params: {
                readonly optional: readonly ["type", "appPackage"];
            };
        };
        readonly 'mobile: performEditorAction': {
            readonly command: "mobilePerformEditorAction";
            readonly params: {
                readonly required: readonly ["action"];
            };
        };
        readonly 'mobile: startScreenStreaming': {
            readonly command: "mobileStartScreenStreaming";
            readonly params: {
                readonly optional: readonly ["width", "height", "bitrate", "host", "pathname", "tcpPort", "port", "quality", "considerRotation", "logPipelineDetails"];
            };
        };
        readonly 'mobile: stopScreenStreaming': {
            readonly command: "mobileStopScreenStreaming";
        };
        readonly 'mobile: getNotifications': {
            readonly command: "mobileGetNotifications";
        };
        readonly 'mobile: openNotifications': {
            readonly command: "openNotifications";
        };
        readonly 'mobile: listSms': {
            readonly command: "mobileListSms";
            readonly params: {
                readonly optional: readonly ["max"];
            };
        };
        readonly 'mobile: type': {
            readonly command: "mobileType";
            readonly params: {
                readonly required: readonly ["text"];
            };
        };
        readonly 'mobile: replaceElementValue': {
            readonly command: "mobileReplaceElementValue";
            readonly params: {
                readonly required: readonly ["elementId", "text"];
            };
        };
        readonly 'mobile: pushFile': {
            readonly command: "mobilePushFile";
            readonly params: {
                readonly required: readonly ["payload", "remotePath"];
            };
        };
        readonly 'mobile: pullFile': {
            readonly command: "mobilePullFile";
            readonly params: {
                readonly required: readonly ["remotePath"];
            };
        };
        readonly 'mobile: pullFolder': {
            readonly command: "mobilePullFolder";
            readonly params: {
                readonly required: readonly ["remotePath"];
            };
        };
        readonly 'mobile: deleteFile': {
            readonly command: "mobileDeleteFile";
            readonly params: {
                readonly required: readonly ["remotePath"];
            };
        };
        readonly 'mobile: isAppInstalled': {
            readonly command: "mobileIsAppInstalled";
            readonly params: {
                readonly required: readonly ["appId"];
            };
        };
        readonly 'mobile: queryAppState': {
            readonly command: "mobileQueryAppState";
            readonly params: {
                readonly required: readonly ["appId"];
            };
        };
        readonly 'mobile: activateApp': {
            readonly command: "mobileActivateApp";
            readonly params: {
                readonly required: readonly ["appId"];
            };
        };
        readonly 'mobile: removeApp': {
            readonly command: "mobileRemoveApp";
            readonly params: {
                readonly required: readonly ["appId"];
                readonly optional: readonly ["timeout", "keepData"];
            };
        };
        readonly 'mobile: terminateApp': {
            readonly command: "mobileTerminateApp";
            readonly params: {
                readonly required: readonly ["appId"];
                readonly optional: readonly ["timeout"];
            };
        };
        readonly 'mobile: installApp': {
            readonly command: "mobileInstallApp";
            readonly params: {
                readonly required: readonly ["appPath"];
                readonly optional: readonly ["timeout", "keepData"];
            };
        };
        readonly 'mobile: clearApp': {
            readonly command: "mobileClearApp";
            readonly params: {
                readonly required: readonly ["appId"];
            };
        };
        readonly 'mobile: backgroundApp': {
            readonly command: "mobileBackgroundApp";
            readonly params: {
                readonly optional: readonly ["seconds"];
            };
        };
        readonly 'mobile: getCurrentActivity': {
            readonly command: "getCurrentActivity";
        };
        readonly 'mobile: getCurrentPackage': {
            readonly command: "getCurrentPackage";
        };
        readonly 'mobile: startActivity': {
            readonly command: "mobileStartActivity";
            readonly params: {
                readonly optional: readonly ["wait", "stop", "windowingMode", "activityType", "display"];
            };
        };
        readonly 'mobile: startService': {
            readonly command: "mobileStartService";
            readonly params: {
                readonly optional: readonly ["user", "intent", "action", "package", "uri", "mimeType", "identifier", "component", "categories", "extras", "flags", "wait", "stop", "windowingMode", "activityType", "display"];
            };
        };
        readonly 'mobile: stopService': {
            readonly command: "mobileStopService";
            readonly params: {
                readonly optional: readonly ["user", "intent", "action", "package", "uri", "mimeType", "identifier", "component", "categories", "extras", "flags"];
            };
        };
        readonly 'mobile: broadcast': {
            readonly command: "mobileBroadcast";
            readonly params: {
                readonly optional: readonly ["user", "intent", "action", "package", "uri", "mimeType", "identifier", "component", "categories", "extras", "flags", "receiverPermission", "allowBackgroundActivityStarts"];
            };
        };
        readonly 'mobile: getContexts': {
            readonly command: "mobileGetContexts";
        };
        readonly 'mobile: getAppStrings': {
            readonly command: "mobileGetAppStrings";
            readonly params: {
                readonly optional: readonly ["language"];
            };
        };
        readonly 'mobile: installMultipleApks': {
            readonly command: "mobileInstallMultipleApks";
            readonly params: {
                readonly required: readonly ["apks"];
                readonly optional: readonly ["options"];
            };
        };
        readonly 'mobile: lock': {
            readonly command: "mobileLock";
            readonly params: {
                readonly optional: readonly ["seconds"];
            };
        };
        readonly 'mobile: unlock': {
            readonly command: "mobileUnlock";
            readonly params: {
                readonly optional: readonly ["key", "type", "strategy", "timeoutMs"];
            };
        };
        readonly 'mobile: isLocked': {
            readonly command: "isLocked";
        };
        readonly 'mobile: refreshGpsCache': {
            readonly command: "mobileRefreshGpsCache";
            readonly params: {
                readonly optional: readonly ["timeoutMs"];
            };
        };
        readonly 'mobile: startMediaProjectionRecording': {
            readonly command: "mobileStartMediaProjectionRecording";
            readonly params: {
                readonly optional: readonly ["resolution", "maxDurationSec", "priority", "filename"];
            };
        };
        readonly 'mobile: isMediaProjectionRecordingRunning': {
            readonly command: "mobileIsMediaProjectionRecordingRunning";
        };
        readonly 'mobile: stopMediaProjectionRecording': {
            readonly command: "mobileStopMediaProjectionRecording";
            readonly params: {
                readonly optional: readonly ["remotePath", "user", "pass", "method", "headers", "fileFieldName", "formFields", "uploadTimeout"];
            };
        };
        readonly 'mobile: getConnectivity': {
            readonly command: "mobileGetConnectivity";
            readonly params: {
                readonly optional: readonly ["services"];
            };
        };
        readonly 'mobile: setConnectivity': {
            readonly command: "mobileSetConnectivity";
            readonly params: {
                readonly optional: readonly ["wifi", "data", "airplaneMode"];
            };
        };
        readonly 'mobile: toggleGps': {
            readonly command: "toggleLocationServices";
        };
        readonly 'mobile: isGpsEnabled': {
            readonly command: "isLocationServicesEnabled";
        };
        readonly 'mobile: hideKeyboard': {
            readonly command: "hideKeyboard";
        };
        readonly 'mobile: isKeyboardShown': {
            readonly command: "isKeyboardShown";
        };
        readonly 'mobile: pressKey': {
            readonly command: "mobilePressKey";
            readonly params: {
                readonly required: readonly ["keycode"];
                readonly optional: readonly ["metastate", "flags", "isLongPress"];
            };
        };
        readonly 'mobile: getDisplayDensity': {
            readonly command: "getDisplayDensity";
        };
        readonly 'mobile: getSystemBars': {
            readonly command: "getSystemBars";
        };
        readonly 'mobile: fingerprint': {
            readonly command: "mobileFingerprint";
            readonly params: {
                readonly required: readonly ["fingerprintId"];
            };
        };
        readonly 'mobile: sendSms': {
            readonly command: "mobileSendSms";
            readonly params: {
                readonly required: readonly ["phoneNumber", "message"];
            };
        };
        readonly 'mobile: gsmCall': {
            readonly command: "mobileGsmCall";
            readonly params: {
                readonly required: readonly ["phoneNumber", "action"];
            };
        };
        readonly 'mobile: gsmSignal': {
            readonly command: "mobileGsmSignal";
            readonly params: {
                readonly required: readonly ["strength"];
            };
        };
        readonly 'mobile: gsmVoice': {
            readonly command: "mobileGsmVoice";
            readonly params: {
                readonly required: readonly ["state"];
            };
        };
        readonly 'mobile: powerAc': {
            readonly command: "mobilePowerAc";
            readonly params: {
                readonly required: readonly ["state"];
            };
        };
        readonly 'mobile: powerCapacity': {
            readonly command: "mobilePowerCapacity";
            readonly params: {
                readonly required: readonly ["percent"];
            };
        };
        readonly 'mobile: networkSpeed': {
            readonly command: "mobileNetworkSpeed";
            readonly params: {
                readonly required: readonly ["speed"];
            };
        };
        readonly 'mobile: sensorSet': {
            readonly command: "sensorSet";
            readonly params: {
                readonly required: readonly ["sensorType", "value"];
            };
        };
        readonly 'mobile: getPerformanceData': {
            readonly command: "mobileGetPerformanceData";
            readonly params: {
                readonly required: readonly ["packageName", "dataType"];
            };
        };
        readonly 'mobile: getPerformanceDataTypes': {
            readonly command: "getPerformanceDataTypes";
        };
        readonly 'mobile: statusBar': {
            readonly command: "mobilePerformStatusBarCommand";
            readonly params: {
                readonly required: readonly ["command"];
                readonly optional: readonly ["component"];
            };
        };
        readonly 'mobile: screenshots': {
            readonly command: "mobileScreenshots";
            readonly params: {
                readonly optional: readonly ["displayId"];
            };
        };
        readonly 'mobile: scheduleAction': {
            readonly command: "mobileScheduleAction";
            readonly params: {
                readonly optional: readonly ["opts"];
            };
        };
        readonly 'mobile: getActionHistory': {
            readonly command: "mobileGetActionHistory";
            readonly params: {
                readonly optional: readonly ["opts"];
            };
        };
        readonly 'mobile: unscheduleAction': {
            readonly command: "mobileUnscheduleAction";
            readonly params: {
                readonly optional: readonly ["opts"];
            };
        };
        readonly 'mobile: getUiMode': {
            readonly command: "mobileGetUiMode";
            readonly params: {
                readonly optional: readonly ["opts"];
            };
        };
        readonly 'mobile: setUiMode': {
            readonly command: "mobileSetUiMode";
            readonly params: {
                readonly optional: readonly ["opts"];
            };
        };
    };
    uiautomator2: UiAutomator2Server;
    systemPort: number | undefined;
    _originalIme: string | null;
    mjpegStream?: MJpegStream;
    caps: Uiautomator2DriverCaps;
    opts: Uiautomator2DriverOpts;
    desiredCapConstraints: Uiautomator2Constraints;
    constructor(opts?: InitialOpts, shouldValidateCaps?: boolean);
    validateDesiredCaps(caps: AndroidDriverCaps): caps is Uiautomator2DriverCaps;
    createSession(w3cCaps1: W3CUiautomator2DriverCaps, w3cCaps2?: W3CUiautomator2DriverCaps, w3cCaps3?: W3CUiautomator2DriverCaps, driverData?: DriverData[]): Promise<unknown>;
    getDeviceDetails(): Promise<Uiautomator2DeviceDetails>;
    getSession(): Promise<SingularSessionData<Uiautomator2Constraints>>;
    allocateSystemPort(): Promise<void>;
    releaseSystemPort(): Promise<void>;
    allocateMjpegServerPort(): Promise<void>;
    releaseMjpegServerPort(): Promise<void>;
    startUiAutomator2Session(caps: Uiautomator2StartSessionOpts): Promise<Uiautomator2SessionCaps>;
    initUiAutomator2Server(): Promise<UiAutomator2Server>;
    initAUT(): Promise<void>;
    ensureAppStarts(): Promise<void>;
    deleteSession(): Promise<void>;
    checkAppPresent(): Promise<void>;
    onSettingsUpdate(): Promise<void>;
    proxyActive(_sessionId: string): boolean;
    canProxy(_sessionId: string): boolean;
    getProxyAvoidList(): RouteMatcher[];
    updateSettings(settings: Uiautomator2Settings): Promise<void>;
    getSettings(): Promise<unknown>;
}

type automatorDriver_AndroidUiautomator2Driver = AndroidUiautomator2Driver;
declare const automatorDriver_AndroidUiautomator2Driver: typeof AndroidUiautomator2Driver;
declare namespace automatorDriver {
  export {
    automatorDriver_AndroidUiautomator2Driver as AndroidUiautomator2Driver,
    AndroidUiautomator2Driver as default,
  };
}

/*!
 * The code following this comment originates from:
 *   https://github.com/types/npm-bluebird
 *
 * Note for browser users: use bluebird-global typings instead of this one
 * if you want to use Bluebird via the global Promise symbol.
 *
 * Licensed under:
 *   The MIT License (MIT)
 *
 *   Copyright (c) 2016 unional
 *
 *   Permission is hereby granted, free of charge, to any person obtaining a copy
 *   of this software and associated documentation files (the "Software"), to deal
 *   in the Software without restriction, including without limitation the rights
 *   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *   copies of the Software, and to permit persons to whom the Software is
 *   furnished to do so, subject to the following conditions:
 *
 *   The above copyright notice and this permission notice shall be included in
 *   all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *   THE SOFTWARE.
 */

type Constructor<E> = new(...args: any[]) => E;
type CatchFilter<E> = ((error: E) => boolean) | (object & E);
type Resolvable<R> = R | PromiseLike<R>;
type IterateFunction<T, R> = (item: T, index: number, arrayLength: number) => Resolvable<R>;

type PromisifyAllKeys<T> = T extends string ? `${T}Async` : never;
type WithoutLast<T> = T extends [...infer A, any] ? A : [];
type Last<T> = T extends [...any[], infer L] ? L : never;
type ExtractCallbackValueType<T> = T extends (error: any, ...data: infer D) => any ? D : never;

type PromiseMethod<TArgs, TReturn> = TReturn extends never ? never : (...args: WithoutLast<TArgs>) => Promise<TReturn>;

type ExtractAsyncMethod<T> = T extends (...args: infer A) => any
    ? PromiseMethod<A, ExtractCallbackValueType<Last<Required<A>>>[0]>
    : never;

type PromisifyAllItems<T> = {
    [K in keyof T as PromisifyAllKeys<K>]: ExtractAsyncMethod<T[K]>;
};

type NonNeverValues<T> = {
    [K in keyof T as T[K] extends never ? never : K]: T[K];
};

// Drop `never` values
type PromisifyAll<T> = NonNeverValues<PromisifyAllItems<T>> & T;

declare class Bluebird<R> implements PromiseLike<R>, Bluebird.Inspection<R> {
    readonly [Symbol.toStringTag]: "Object";

    /**
     * Create a new promise. The passed in function will receive functions
     * `resolve` and `reject` as its arguments which can be called to seal the fate of the created promise.
     *
     * If promise cancellation is enabled, passed in function will receive
     * one more function argument `onCancel` that allows to register an optional cancellation callback.
     */
    constructor(
        callback: (
            resolve: (thenableOrResult?: Resolvable<R>) => void,
            reject: (error?: any) => void,
            onCancel?: (callback: () => void) => void,
        ) => void,
    );

    /**
     * Promises/A+ `.then()`. Returns a new promise chained from this promise.
     *
     * The new promise will be rejected or resolved depending on the passed `fulfilledHandler`, `rejectedHandler` and the state of this promise.
     */
    // Based on PromiseLike.then, but returns a Bluebird instance.
    then<U>(onFulfill?: (value: R) => Resolvable<U>, onReject?: (error: any) => Resolvable<U>): Bluebird<U>; // For simpler signature help.
    then<TResult1 = R, TResult2 = never>(
        onfulfilled?: ((value: R) => Resolvable<TResult1>) | null,
        onrejected?: ((reason: any) => Resolvable<TResult2>) | null,
    ): Bluebird<TResult1 | TResult2>;

    /**
     * This is a catch-all exception handler, shortcut for calling `.then(null, handler)` on this promise.
     *
     * Any exception happening in a `.then`-chain will propagate to nearest `.catch` handler.
     *
     * Alias `.caught();` for compatibility with earlier ECMAScript version.
     */
    catch<U = R>(onReject: ((error: any) => Resolvable<U>) | undefined | null): Bluebird<U | R>;

    /**
     * This extends `.catch` to work more like catch-clauses in languages like Java or C#.
     *
     * Instead of manually checking `instanceof` or `.name === "SomeError"`,
     * you may specify a number of error constructors which are eligible for this catch handler.
     * The catch handler that is first met that has eligible constructors specified, is the one that will be called.
     *
     * This method also supports predicate-based filters.
     * If you pass a predicate function instead of an error constructor, the predicate will receive the error as an argument.
     * The return result of the predicate will be used determine whether the error handler should be called.
     *
     * Alias `.caught();` for compatibility with earlier ECMAScript version.
     */
    catch<U, E1, E2, E3, E4, E5>(
        filter1: Constructor<E1>,
        filter2: Constructor<E2>,
        filter3: Constructor<E3>,
        filter4: Constructor<E4>,
        filter5: Constructor<E5>,
        onReject: (error: E1 | E2 | E3 | E4 | E5) => Resolvable<U>,
    ): Bluebird<U | R>;

    catch<U, E1, E2, E3, E4, E5>(
        filter1: Constructor<E1> | CatchFilter<E1>,
        filter2: Constructor<E2> | CatchFilter<E2>,
        filter3: Constructor<E3> | CatchFilter<E3>,
        filter4: Constructor<E4> | CatchFilter<E4>,
        filter5: Constructor<E5> | CatchFilter<E5>,
        onReject: (error: E1 | E2 | E3 | E4 | E5) => Resolvable<U>,
    ): Bluebird<U | R>;

    catch<U, E1, E2, E3, E4>(
        filter1: Constructor<E1>,
        filter2: Constructor<E2>,
        filter3: Constructor<E3>,
        filter4: Constructor<E4>,
        onReject: (error: E1 | E2 | E3 | E4) => Resolvable<U>,
    ): Bluebird<U | R>;

    catch<U, E1, E2, E3, E4>(
        filter1: Constructor<E1> | CatchFilter<E1>,
        filter2: Constructor<E2> | CatchFilter<E2>,
        filter3: Constructor<E3> | CatchFilter<E3>,
        filter4: Constructor<E4> | CatchFilter<E4>,
        onReject: (error: E1 | E2 | E3 | E4) => Resolvable<U>,
    ): Bluebird<U | R>;

    catch<U, E1, E2, E3>(
        filter1: Constructor<E1>,
        filter2: Constructor<E2>,
        filter3: Constructor<E3>,
        onReject: (error: E1 | E2 | E3) => Resolvable<U>,
    ): Bluebird<U | R>;

    catch<U, E1, E2, E3>(
        filter1: Constructor<E1> | CatchFilter<E1>,
        filter2: Constructor<E2> | CatchFilter<E2>,
        filter3: Constructor<E3> | CatchFilter<E3>,
        onReject: (error: E1 | E2 | E3) => Resolvable<U>,
    ): Bluebird<U | R>;

    catch<U, E1, E2>(
        filter1: Constructor<E1>,
        filter2: Constructor<E2>,
        onReject: (error: E1 | E2) => Resolvable<U>,
    ): Bluebird<U | R>;

    catch<U, E1, E2>(
        filter1: Constructor<E1> | CatchFilter<E1>,
        filter2: Constructor<E2> | CatchFilter<E2>,
        onReject: (error: E1 | E2) => Resolvable<U>,
    ): Bluebird<U | R>;

    catch<U, E1>(
        filter1: Constructor<E1>,
        onReject: (error: E1) => Resolvable<U>,
    ): Bluebird<U | R>;

    catch<U, E1>(
        // tslint:disable-next-line:unified-signatures
        filter1: Constructor<E1> | CatchFilter<E1>,
        onReject: (error: E1) => Resolvable<U>,
    ): Bluebird<U | R>;

    /**
     * This is a catch-all exception handler, shortcut for calling `.then(null, handler)` on this promise.
     *
     * Any exception happening in a `.then`-chain will propagate to nearest `.catch` handler.
     *
     * Alias `.caught();` for compatibility with earlier ECMAScript version.
     */
    caught: Bluebird<R>["catch"];

    /**
     * Like `.catch` but instead of catching all types of exceptions,
     * it only catches those that don't originate from thrown errors but rather from explicit rejections.
     */
    error<U>(onReject: (reason: any) => Resolvable<U>): Bluebird<U>;

    /**
     * Pass a handler that will be called regardless of this promise's fate. Returns a new promise chained from this promise.
     *
     * There are special semantics for `.finally()` in that the final value cannot be modified from the handler.
     *
     * Alias `.lastly();` for compatibility with earlier ECMAScript version.
     */
    finally(handler: () => Resolvable<any>): Bluebird<R>;

    lastly: Bluebird<R>["finally"];

    /**
     * Create a promise that follows this promise, but is bound to the given `thisArg` value.
     * A bound promise will call its handlers with the bound value set to `this`.
     *
     * Additionally promises derived from a bound promise will also be bound promises with the same `thisArg` binding as the original promise.
     */
    bind(thisArg: any): Bluebird<R>;

    /**
     * Like `.then()`, but any unhandled rejection that ends up here will be thrown as an error.
     */
    done<U>(onFulfilled?: (value: R) => Resolvable<U>, onRejected?: (error: any) => Resolvable<U>): void;

    /**
     * Like `.finally()`, but not called for rejections.
     */
    tap(onFulFill: (value: R) => Resolvable<any>): Bluebird<R>;

    /**
     * Like `.catch()` but rethrows the error
     */
    tapCatch(onReject: (error?: any) => Resolvable<any>): Bluebird<R>;

    tapCatch<E1, E2, E3, E4, E5>(
        filter1: Constructor<E1>,
        filter2: Constructor<E2>,
        filter3: Constructor<E3>,
        filter4: Constructor<E4>,
        filter5: Constructor<E5>,
        onReject: (error: E1 | E2 | E3 | E4 | E5) => Resolvable<any>,
    ): Bluebird<R>;
    tapCatch<E1, E2, E3, E4, E5>(
        filter1: Constructor<E1> | CatchFilter<E1>,
        filter2: Constructor<E2> | CatchFilter<E2>,
        filter3: Constructor<E3> | CatchFilter<E3>,
        filter4: Constructor<E4> | CatchFilter<E4>,
        filter5: Constructor<E5> | CatchFilter<E5>,
        onReject: (error: E1 | E2 | E3 | E4 | E5) => Resolvable<any>,
    ): Bluebird<R>;
    tapCatch<E1, E2, E3, E4>(
        filter1: Constructor<E1>,
        filter2: Constructor<E2>,
        filter3: Constructor<E3>,
        filter4: Constructor<E4>,
        onReject: (error: E1 | E2 | E3 | E4) => Resolvable<any>,
    ): Bluebird<R>;
    tapCatch<E1, E2, E3, E4>(
        filter1: Constructor<E1> | CatchFilter<E1>,
        filter2: Constructor<E2> | CatchFilter<E2>,
        filter3: Constructor<E3> | CatchFilter<E3>,
        filter4: Constructor<E4> | CatchFilter<E4>,
        onReject: (error: E1 | E2 | E3 | E4) => Resolvable<any>,
    ): Bluebird<R>;
    tapCatch<E1, E2, E3>(
        filter1: Constructor<E1>,
        filter2: Constructor<E2>,
        filter3: Constructor<E3>,
        onReject: (error: E1 | E2 | E3) => Resolvable<any>,
    ): Bluebird<R>;
    tapCatch<E1, E2, E3>(
        filter1: Constructor<E1> | CatchFilter<E1>,
        filter2: Constructor<E2> | CatchFilter<E2>,
        filter3: Constructor<E3> | CatchFilter<E3>,
        onReject: (error: E1 | E2 | E3) => Resolvable<any>,
    ): Bluebird<R>;
    tapCatch<E1, E2>(
        filter1: Constructor<E1>,
        filter2: Constructor<E2>,
        onReject: (error: E1 | E2) => Resolvable<any>,
    ): Bluebird<R>;
    tapCatch<E1, E2>(
        filter1: Constructor<E1> | CatchFilter<E1>,
        filter2: Constructor<E2> | CatchFilter<E2>,
        onReject: (error: E1 | E2) => Resolvable<any>,
    ): Bluebird<R>;
    tapCatch<E1>(
        filter1: Constructor<E1>,
        onReject: (error: E1) => Resolvable<any>,
    ): Bluebird<R>;
    tapCatch<E1>(
        // tslint:disable-next-line:unified-signatures
        filter1: Constructor<E1> | CatchFilter<E1>,
        onReject: (error: E1) => Resolvable<any>,
    ): Bluebird<R>;

    /**
     * Same as calling `Promise.delay(ms, this)`.
     */
    delay(ms: number): Bluebird<R>;

    /**
     * Returns a promise that will be fulfilled with this promise's fulfillment value or rejection reason.
     *  However, if this promise is not fulfilled or rejected within ms milliseconds, the returned promise
     *  is rejected with a TimeoutError or the error as the reason.
     *
     * You may specify a custom error message with the `message` parameter.
     */
    timeout(ms: number, message?: string | Error): Bluebird<R>;

    /**
     * Register a node-style callback on this promise.
     *
     * When this promise is is either fulfilled or rejected,
     * the node callback will be called back with the node.js convention
     * where error reason is the first argument and success value is the second argument.
     *
     * The error argument will be `null` in case of success.
     * If the `callback` argument is not a function, this method does not do anything.
     */
    nodeify(callback: (err: any, value?: R) => void, options?: Bluebird.SpreadOption): this;
    nodeify(...sink: any[]): this;
    asCallback(callback: (err: any, value?: R) => void, options?: Bluebird.SpreadOption): this;
    asCallback(...sink: any[]): this;

    /**
     * See if this `promise` has been fulfilled.
     */
    isFulfilled(): boolean;

    /**
     * See if this `promise` has been rejected.
     */
    isRejected(): boolean;

    /**
     * See if this `promise` is still defer.
     */
    isPending(): boolean;

    /**
     * See if this `promise` has been cancelled.
     */
    isCancelled(): boolean;

    /**
     * See if this `promise` is resolved -> either fulfilled or rejected.
     */
    isResolved(): boolean;

    /**
     * Get the fulfillment value of the underlying promise. Throws if the promise isn't fulfilled yet.
     *
     * throws `TypeError`
     */
    value(): R;

    /**
     * Get the rejection reason for the underlying promise. Throws if the promise isn't rejected yet.
     *
     * throws `TypeError`
     */
    reason(): any;

    /**
     * Synchronously inspect the state of this `promise`. The `PromiseInspection` will represent the state of
     * the promise as snapshotted at the time of calling `.reflect()`.
     */
    reflect(): Bluebird<Bluebird.Inspection<R>>;

    /**
     * This is a convenience method for doing:
     *
     * <code>
     * promise.then(function(obj){
     *     return obj[propertyName].call(obj, arg...);
     * });
     * </code>
     */
    call<U extends keyof Q, Q>(
        this: Bluebird<Q>,
        propertyName: U,
        ...args: any[]
    ): Bluebird<Q[U] extends (...args: any[]) => any ? ReturnType<Q[U]> : never>;

    /**
     * This is a convenience method for doing:
     *
     * <code>
     * promise.then(function(obj){
     *     return obj[propertyName];
     * });
     * </code>
     */
    get<U extends keyof R>(key: U): Bluebird<R[U]>;

    /**
     * Convenience method for:
     *
     * <code>
     * .then(function() {
     *    return value;
     * });
     * </code>
     *
     * in the case where `value` doesn't change its value. That means `value` is bound at the time of calling `.return()`
     *
     * Alias `.thenReturn();` for compatibility with earlier ECMAScript version.
     */
    return(): Bluebird<void>;
    return<U>(value: U): Bluebird<U>;
    thenReturn(): Bluebird<void>;
    thenReturn<U>(value: U): Bluebird<U>;

    /**
     * Convenience method for:
     *
     * <code>
     * .then(function() {
     *    throw reason;
     * });
     * </code>
     * Same limitations apply as with `.return()`.
     *
     * Alias `.thenThrow();` for compatibility with earlier ECMAScript version.
     */
    throw(reason: Error): Bluebird<never>;
    thenThrow(reason: Error): Bluebird<never>;

    /**
     * Convenience method for:
     *
     * <code>
     * .catch(function() {
     *    return value;
     * });
     * </code>
     *
     * in the case where `value` doesn't change its value. That means `value` is bound at the time of calling `.catchReturn()`
     */
    catchReturn<U>(value: U): Bluebird<R | U>;

    // No need to be specific about Error types in these overrides, since there's no handler function
    catchReturn<U>(
        filter1: Constructor<Error>,
        filter2: Constructor<Error>,
        filter3: Constructor<Error>,
        filter4: Constructor<Error>,
        filter5: Constructor<Error>,
        value: U,
    ): Bluebird<R | U>;
    catchReturn<U>(
        filter1: Constructor<Error> | CatchFilter<Error>,
        filter2: Constructor<Error> | CatchFilter<Error>,
        filter3: Constructor<Error> | CatchFilter<Error>,
        filter4: Constructor<Error> | CatchFilter<Error>,
        filter5: Constructor<Error> | CatchFilter<Error>,
        value: U,
    ): Bluebird<R | U>;
    catchReturn<U>(
        filter1: Constructor<Error>,
        filter2: Constructor<Error>,
        filter3: Constructor<Error>,
        filter4: Constructor<Error>,
        value: U,
    ): Bluebird<R | U>;
    catchReturn<U>(
        filter1: Constructor<Error> | CatchFilter<Error>,
        filter2: Constructor<Error> | CatchFilter<Error>,
        filter3: Constructor<Error> | CatchFilter<Error>,
        filter4: Constructor<Error> | CatchFilter<Error>,
        value: U,
    ): Bluebird<R | U>;
    catchReturn<U>(
        filter1: Constructor<Error>,
        filter2: Constructor<Error>,
        filter3: Constructor<Error>,
        value: U,
    ): Bluebird<R | U>;
    catchReturn<U>(
        filter1: Constructor<Error> | CatchFilter<Error>,
        filter2: Constructor<Error> | CatchFilter<Error>,
        filter3: Constructor<Error> | CatchFilter<Error>,
        value: U,
    ): Bluebird<R | U>;
    catchReturn<U>(
        filter1: Constructor<Error>,
        filter2: Constructor<Error>,
        value: U,
    ): Bluebird<R | U>;
    catchReturn<U>(
        filter1: Constructor<Error> | CatchFilter<Error>,
        filter2: Constructor<Error> | CatchFilter<Error>,
        value: U,
    ): Bluebird<R | U>;
    catchReturn<U>(
        filter1: Constructor<Error>,
        value: U,
    ): Bluebird<R | U>;
    catchReturn<U>(
        // tslint:disable-next-line:unified-signatures
        filter1: Constructor<Error> | CatchFilter<Error>,
        value: U,
    ): Bluebird<R | U>;

    /**
     * Convenience method for:
     *
     * <code>
     * .catch(function() {
     *    throw reason;
     * });
     * </code>
     * Same limitations apply as with `.catchReturn()`.
     */
    catchThrow(reason: Error): Bluebird<R>;

    // No need to be specific about Error types in these overrides, since there's no handler function
    catchThrow(
        filter1: Constructor<Error>,
        filter2: Constructor<Error>,
        filter3: Constructor<Error>,
        filter4: Constructor<Error>,
        filter5: Constructor<Error>,
        reason: Error,
    ): Bluebird<R>;
    catchThrow(
        filter1: Constructor<Error> | CatchFilter<Error>,
        filter2: Constructor<Error> | CatchFilter<Error>,
        filter3: Constructor<Error> | CatchFilter<Error>,
        filter4: Constructor<Error> | CatchFilter<Error>,
        filter5: Constructor<Error> | CatchFilter<Error>,
        reason: Error,
    ): Bluebird<R>;
    catchThrow(
        filter1: Constructor<Error>,
        filter2: Constructor<Error>,
        filter3: Constructor<Error>,
        filter4: Constructor<Error>,
        reason: Error,
    ): Bluebird<R>;
    catchThrow(
        filter1: Constructor<Error> | CatchFilter<Error>,
        filter2: Constructor<Error> | CatchFilter<Error>,
        filter3: Constructor<Error> | CatchFilter<Error>,
        filter4: Constructor<Error> | CatchFilter<Error>,
        reason: Error,
    ): Bluebird<R>;
    catchThrow(
        filter1: Constructor<Error>,
        filter2: Constructor<Error>,
        filter3: Constructor<Error>,
        reason: Error,
    ): Bluebird<R>;
    catchThrow(
        filter1: Constructor<Error> | CatchFilter<Error>,
        filter2: Constructor<Error> | CatchFilter<Error>,
        filter3: Constructor<Error> | CatchFilter<Error>,
        reason: Error,
    ): Bluebird<R>;
    catchThrow(
        filter1: Constructor<Error>,
        filter2: Constructor<Error>,
        reason: Error,
    ): Bluebird<R>;
    catchThrow(
        filter1: Constructor<Error> | CatchFilter<Error>,
        filter2: Constructor<Error> | CatchFilter<Error>,
        reason: Error,
    ): Bluebird<R>;
    catchThrow(
        filter1: Constructor<Error>,
        reason: Error,
    ): Bluebird<R>;
    catchThrow(
        // tslint:disable-next-line:unified-signatures
        filter1: Constructor<Error> | CatchFilter<Error>,
        reason: Error,
    ): Bluebird<R>;

    /**
     * Convert to String.
     */
    toString(): string;

    /**
     * This is implicitly called by `JSON.stringify` when serializing the object. Returns a serialized representation of the `Promise`.
     */
    toJSON(): object;

    /**
     * Like calling `.then`, but the fulfillment value or rejection reason is assumed to be an array, which is flattened to the formal parameters of the handlers.
     */
    spread<U, Q>(this: Bluebird<R & Iterable<Q>>, fulfilledHandler: (...values: Q[]) => Resolvable<U>): Bluebird<U>;

    /**
     * Same as calling `Promise.all(thisPromise)`. With the exception that if this promise is bound to a value, the returned promise is bound to that value too.
     */
    all<T1, T2, T3, T4, T5>(
        this: Bluebird<[Resolvable<T1>, Resolvable<T2>, Resolvable<T3>, Resolvable<T4>, Resolvable<T5>]>,
    ): Bluebird<[T1, T2, T3, T4, T5]>;
    all<T1, T2, T3, T4>(
        this: Bluebird<[Resolvable<T1>, Resolvable<T2>, Resolvable<T3>, Resolvable<T4>]>,
    ): Bluebird<[T1, T2, T3, T4]>;
    all<T1, T2, T3>(this: Bluebird<[Resolvable<T1>, Resolvable<T2>, Resolvable<T3>]>): Bluebird<[T1, T2, T3]>;
    all<T1, T2>(this: Bluebird<[Resolvable<T1>, Resolvable<T2>]>): Bluebird<[T1, T2]>;
    all<T1>(this: Bluebird<[Resolvable<T1>]>): Bluebird<[T1]>;
    all<R>(this: Bluebird<Iterable<Resolvable<R>>>): Bluebird<R[]>;

    /**
     * Same as calling `Promise.all(thisPromise)`. With the exception that if this promise is bound to a value, the returned promise is bound to that value too.
     */
    all(): Bluebird<never>;

    /**
     * Same as calling `Promise.props(thisPromise)`. With the exception that if this promise is bound to a value, the returned promise is bound to that value too.
     */
    props<K, V>(this: PromiseLike<Map<K, Resolvable<V>>>): Bluebird<Map<K, V>>;
    props<T>(this: PromiseLike<Bluebird.ResolvableProps<T>>): Bluebird<T>;

    /**
     * Same as calling `Promise.any(thisPromise)`. With the exception that if this promise is bound to a value, the returned promise is bound to that value too.
     */
    any<Q>(this: Bluebird<R & Iterable<Q>>): Bluebird<Q>;

    /**
     * Same as calling `Promise.any(thisPromise)`. With the exception that if this promise is bound to a value, the returned promise is bound to that value too.
     */
    any(): Bluebird<never>;

    /**
     * Same as calling `Promise.some(thisPromise)`. With the exception that if this promise is bound to a value, the returned promise is bound to that value too.
     * Same as calling `Promise.some(thisPromise)`. With the exception that if this promise is bound to a value, the returned promise is bound to that value too.
     */
    some<Q>(this: Bluebird<R & Iterable<Q>>, count: number): Bluebird<R>;

    /**
     * Same as calling `Promise.some(thisPromise)`. With the exception that if this promise is bound to a value, the returned promise is bound to that value too.
     * Same as calling `Promise.some(thisPromise)`. With the exception that if this promise is bound to a value, the returned promise is bound to that value too.
     */
    some(count: number): Bluebird<never>;

    /**
     * Same as calling `Promise.race(thisPromise, count)`. With the exception that if this promise is bound to a value, the returned promise is bound to that value too.
     */
    race<Q>(this: Bluebird<R & Iterable<Q>>): Bluebird<Q>;

    /**
     * Same as calling `Promise.race(thisPromise, count)`. With the exception that if this promise is bound to a value, the returned promise is bound to that value too.
     */
    race(): Bluebird<never>;

    /**
     * Same as calling `Bluebird.map(thisPromise, mapper)`. With the exception that if this promise is bound to a value, the returned promise is bound to that value too.
     */
    map<U, Q>(
        this: Bluebird<R & Iterable<Q>>,
        mapper: IterateFunction<Q, U>,
        options?: Bluebird.ConcurrencyOption,
    ): Bluebird<U[]>;

    /**
     * Same as calling `Promise.reduce(thisPromise, Function reducer, initialValue)`. With the exception that if this promise is bound to a value, the returned promise is bound to that value too.
     */
    reduce<U, Q>(
        this: Bluebird<R & Iterable<Q>>,
        reducer: (memo: U, item: Q, index: number, arrayLength: number) => Resolvable<U>,
        initialValue?: Resolvable<U>,
    ): Bluebird<U>;

    /**
     * Same as calling ``Promise.filter(thisPromise, filterer)``. With the exception that if this promise is bound to a value, the returned promise is bound to that value too.
     */
    filter<Q>(
        this: Bluebird<R & Iterable<Q>>,
        filterer: IterateFunction<Q, boolean>,
        options?: Bluebird.ConcurrencyOption,
    ): Bluebird<R>;

    /**
     * Same as calling ``Bluebird.each(thisPromise, iterator)``. With the exception that if this promise is bound to a value, the returned promise is bound to that value too.
     */
    each<Q>(this: Bluebird<R & Iterable<Q>>, iterator: IterateFunction<Q, any>): Bluebird<R>;

    /**
     * Same as calling ``Bluebird.mapSeries(thisPromise, iterator)``. With the exception that if this promise is bound to a value, the returned promise is bound to that value too.
     */
    mapSeries<U, Q>(this: Bluebird<R & Iterable<Q>>, iterator: IterateFunction<Q, U>): Bluebird<U[]>;

    /**
     * Cancel this `promise`. Will not do anything if this promise is already settled or if the cancellation feature has not been enabled
     */
    cancel(): void;

    /**
     * Basically sugar for doing: somePromise.catch(function(){});
     *
     * Which is needed in case error handlers are attached asynchronously to the promise later, which would otherwise result in premature unhandled rejection reporting.
     */
    suppressUnhandledRejections(): void;

    /**
     * Start the chain of promises with `Promise.try`. Any synchronous exceptions will be turned into rejections on the returned promise.
     *
     * Note about second argument: if it's specifically a true array, its values become respective arguments for the function call.
     * Otherwise it is passed as is as the first argument for the function call.
     *
     * Alias for `attempt();` for compatibility with earlier ECMAScript version.
     */
    static try<R>(fn: () => Resolvable<R>): Bluebird<R>;
    static attempt<R>(fn: () => Resolvable<R>): Bluebird<R>;

    /**
     * Returns a new function that wraps the given function `fn`.
     * The new function will always return a promise that is fulfilled with the original functions return values or rejected with thrown exceptions from the original function.
     * This method is convenient when a function can sometimes return synchronously or throw synchronously.
     */
    static method<R>(fn: () => Resolvable<R>): () => Bluebird<R>;
    static method<R, A1>(fn: (arg1: A1) => Resolvable<R>): (arg1: A1) => Bluebird<R>;
    static method<R, A1, A2>(fn: (arg1: A1, arg2: A2) => Resolvable<R>): (arg1: A1, arg2: A2) => Bluebird<R>;
    static method<R, A1, A2, A3>(
        fn: (arg1: A1, arg2: A2, arg3: A3) => Resolvable<R>,
    ): (arg1: A1, arg2: A2, arg3: A3) => Bluebird<R>;
    static method<R, A1, A2, A3, A4>(
        fn: (arg1: A1, arg2: A2, arg3: A3, arg4: A4) => Resolvable<R>,
    ): (arg1: A1, arg2: A2, arg3: A3, arg4: A4) => Bluebird<R>;
    static method<R, A1, A2, A3, A4, A5>(
        fn: (arg1: A1, arg2: A2, arg3: A3, arg4: A4, arg5: A5) => Resolvable<R>,
    ): (arg1: A1, arg2: A2, arg3: A3, arg4: A4, arg5: A5) => Bluebird<R>;
    static method<R>(fn: (...args: any[]) => Resolvable<R>): (...args: any[]) => Bluebird<R>;

    /**
     * Create a promise that is resolved with the given `value`. If `value` is a thenable or promise, the returned promise will assume its state.
     */
    static resolve(): Bluebird<void>;
    static resolve<R>(value: Resolvable<R>): Bluebird<R>;

    /**
     * Create a promise that is rejected with the given `reason`.
     */
    static reject(reason: any): Bluebird<never>;

    /**
     * @deprecated
     * Create a promise with undecided fate and return a `PromiseResolver` to control it. See resolution?: Promise(#promise-resolution).
     * @see http://bluebirdjs.com/docs/deprecated-apis.html#promise-resolution
     */
    static defer<R>(): Bluebird.Resolver<R>;

    /**
     * Cast the given `value` to a trusted promise.
     *
     * If `value` is already a trusted `Promise`, it is returned as is. If `value` is not a thenable, a fulfilled is: Promise returned with `value` as its fulfillment value.
     * If `value` is a thenable (Promise-like object, like those returned by jQuery's `$.ajax`), returns a trusted that: Promise assimilates the state of the thenable.
     */
    static cast<R>(value: Resolvable<R>): Bluebird<R>;

    /**
     * Sugar for `Promise.resolve(undefined).bind(thisArg);`. See `.bind()`.
     */
    static bind(thisArg: any): Bluebird<void>;

    /**
     * See if `value` is a trusted Promise.
     */
    static is(value: any): boolean;

    /**
     * Call this right after the library is loaded to enabled long stack traces.
     *
     * Long stack traces cannot be disabled after being enabled, and cannot be enabled after promises have already been created.
     * Long stack traces imply a substantial performance penalty, around 4-5x for throughput and 0.5x for latency.
     */
    static longStackTraces(): void;

    /**
     * Returns a promise that will be resolved with value (or undefined) after given ms milliseconds.
     * If value is a promise, the delay will start counting down when it is fulfilled and the returned
     *  promise will be fulfilled with the fulfillment value of the value promise.
     */
    static delay<R>(ms: number, value: Resolvable<R>): Bluebird<R>;
    static delay(ms: number): Bluebird<void>;

    /**
     * Returns a function that will wrap the given `nodeFunction`.
     *
     * Instead of taking a callback, the returned function will return a promise whose fate is decided by the callback behavior of the given node function.
     * The node function should conform to node.js convention of accepting a callback as last argument and
     * calling that callback with error as the first argument and success value on the second argument.
     *
     * If the `nodeFunction` calls its callback with multiple success values, the fulfillment value will be an array of them.
     *
     * If you pass a `receiver`, the `nodeFunction` will be called as a method on the `receiver`.
     */
    static promisify<T>(
        func: (callback: (err: any, result?: T) => void) => void,
        options?: Bluebird.PromisifyOptions,
    ): () => Bluebird<T>;
    static promisify<T, A1>(
        func: (arg1: A1, callback: (err: any, result?: T) => void) => void,
        options?: Bluebird.PromisifyOptions,
    ): (arg1: A1) => Bluebird<T>;
    static promisify<T, A1, A2>(
        func: (arg1: A1, arg2: A2, callback: (err: any, result?: T) => void) => void,
        options?: Bluebird.PromisifyOptions,
    ): (arg1: A1, arg2: A2) => Bluebird<T>;
    static promisify<T, A1, A2, A3>(
        func: (arg1: A1, arg2: A2, arg3: A3, callback: (err: any, result?: T) => void) => void,
        options?: Bluebird.PromisifyOptions,
    ): (arg1: A1, arg2: A2, arg3: A3) => Bluebird<T>;
    static promisify<T, A1, A2, A3, A4>(
        func: (arg1: A1, arg2: A2, arg3: A3, arg4: A4, callback: (err: any, result?: T) => void) => void,
        options?: Bluebird.PromisifyOptions,
    ): (arg1: A1, arg2: A2, arg3: A3, arg4: A4) => Bluebird<T>;
    static promisify<T, A1, A2, A3, A4, A5>(
        func: (arg1: A1, arg2: A2, arg3: A3, arg4: A4, arg5: A5, callback: (err: any, result?: T) => void) => void,
        options?: Bluebird.PromisifyOptions,
    ): (arg1: A1, arg2: A2, arg3: A3, arg4: A4, arg5: A5) => Bluebird<T>;
    static promisify(
        nodeFunction: (...args: any[]) => void,
        options?: Bluebird.PromisifyOptions,
    ): (...args: any[]) => Bluebird<any>;

    /**
     * Promisifies the entire object by going through the object's properties and creating an async equivalent of each function on the object and its prototype chain.
     *
     * The promisified method name will be the original method name postfixed with `Async`. Returns the input object.
     *
     * Note that the original methods on the object are not overwritten but new methods are created with the `Async`-postfix. For example,
     * if you `promisifyAll()` the node.js `fs` object use `fs.statAsync()` to call the promisified `stat` method.
     */
    // TODO how to model promisifyAll?
    static promisifyAll<T extends object>(target: T, options?: Bluebird.PromisifyAllOptions<T>): PromisifyAll<T>;

    /**
     * Returns a promise that is resolved by a node style callback function.
     */
    static fromNode<T>(
        resolver: (callback: (err: any, result?: T) => void) => void,
        options?: Bluebird.FromNodeOptions,
    ): Bluebird<T>;
    static fromCallback<T>(
        resolver: (callback: (err: any, result?: T) => void) => void,
        options?: Bluebird.FromNodeOptions,
    ): Bluebird<T>;

    /**
     * Returns a function that can use `yield` to run asynchronous code synchronously.
     *
     * This feature requires the support of generators which are drafted in the next version of the language.
     * Node version greater than `0.11.2` is required and needs to be executed with the `--harmony-generators` (or `--harmony`) command-line switch.
     */
    // TODO: After https://github.com/Microsoft/TypeScript/issues/2983 is implemented, we can use
    // the return type propagation of generators to automatically infer the return type T.
    static coroutine<T>(
        generatorFunction: () => IterableIterator<any>,
        options?: Bluebird.CoroutineOptions,
    ): () => Bluebird<T>;
    static coroutine<T, A1>(
        generatorFunction: (a1: A1) => IterableIterator<any>,
        options?: Bluebird.CoroutineOptions,
    ): (a1: A1) => Bluebird<T>;
    static coroutine<T, A1, A2>(
        generatorFunction: (a1: A1, a2: A2) => IterableIterator<any>,
        options?: Bluebird.CoroutineOptions,
    ): (a1: A1, a2: A2) => Bluebird<T>;
    static coroutine<T, A1, A2, A3>(
        generatorFunction: (a1: A1, a2: A2, a3: A3) => IterableIterator<any>,
        options?: Bluebird.CoroutineOptions,
    ): (a1: A1, a2: A2, a3: A3) => Bluebird<T>;
    static coroutine<T, A1, A2, A3, A4>(
        generatorFunction: (a1: A1, a2: A2, a3: A3, a4: A4) => IterableIterator<any>,
        options?: Bluebird.CoroutineOptions,
    ): (a1: A1, a2: A2, a3: A3, a4: A4) => Bluebird<T>;
    static coroutine<T, A1, A2, A3, A4, A5>(
        generatorFunction: (a1: A1, a2: A2, a3: A3, a4: A4, a5: A5) => IterableIterator<any>,
        options?: Bluebird.CoroutineOptions,
    ): (a1: A1, a2: A2, a3: A3, a4: A4, a5: A5) => Bluebird<T>;
    static coroutine<T, A1, A2, A3, A4, A5, A6>(
        generatorFunction: (a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6) => IterableIterator<any>,
        options?: Bluebird.CoroutineOptions,
    ): (a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6) => Bluebird<T>;
    static coroutine<T, A1, A2, A3, A4, A5, A6, A7>(
        generatorFunction: (a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7) => IterableIterator<any>,
        options?: Bluebird.CoroutineOptions,
    ): (a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7) => Bluebird<T>;
    static coroutine<T, A1, A2, A3, A4, A5, A6, A7, A8>(
        generatorFunction: (a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7, a8: A8) => IterableIterator<any>,
        options?: Bluebird.CoroutineOptions,
    ): (a1: A1, a2: A2, a3: A3, a4: A4, a5: A5, a6: A6, a7: A7, a8: A8) => Bluebird<T>;

    /**
     * Add `handler` as the handler to call when there is a possibly unhandled rejection. The default handler logs the error stack to stderr or `console.error` in browsers.
     *
     * Passing no value or a non-function will have the effect of removing any kind of handling for possibly unhandled rejections.
     */
    static onPossiblyUnhandledRejection(handler: (reason: any) => any): void;

    /**
     * Add handler as the handler to call when there is a possibly unhandled rejection.
     * The default handler logs the error stack to stderr or console.error in browsers.
     *
     * Passing no value or a non-function will have the effect of removing any kind of handling for possibly unhandled rejections.
     *
     * Note: this hook is specific to the bluebird instance its called on, application developers should use global rejection events.
     */
    static onPossiblyUnhandledRejection(handler?: (error: Error, promise: Bluebird<any>) => void): void;

    /**
     * Given an array, or a promise of an array, which contains promises (or a mix of promises and values) return a promise that is fulfilled when all the items in the array are fulfilled.
     * The promise's fulfillment value is an array with fulfillment values at respective positions to the original array.
     * If any promise in the array rejects, the returned promise is rejected with the rejection reason.
     */
    // TODO enable more overloads
    // array with promises of different types
    static all<T1, T2, T3, T4, T5>(
        values: [Resolvable<T1>, Resolvable<T2>, Resolvable<T3>, Resolvable<T4>, Resolvable<T5>],
    ): Bluebird<[T1, T2, T3, T4, T5]>;
    static all<T1, T2, T3, T4>(
        values: [Resolvable<T1>, Resolvable<T2>, Resolvable<T3>, Resolvable<T4>],
    ): Bluebird<[T1, T2, T3, T4]>;
    static all<T1, T2, T3>(values: [Resolvable<T1>, Resolvable<T2>, Resolvable<T3>]): Bluebird<[T1, T2, T3]>;
    static all<T1, T2>(values: [Resolvable<T1>, Resolvable<T2>]): Bluebird<[T1, T2]>;
    static all<T1>(values: [Resolvable<T1>]): Bluebird<[T1]>;
    // array with values
    static all<R>(values: Resolvable<Iterable<Resolvable<R>>>): Bluebird<R[]>;

    static allSettled<T1, T2, T3, T4, T5>(
        values: [Resolvable<T1>, Resolvable<T2>, Resolvable<T3>, Resolvable<T4>, Resolvable<T5>],
    ): Bluebird<
        [
            Bluebird.Inspection<T1>,
            Bluebird.Inspection<T2>,
            Bluebird.Inspection<T3>,
            Bluebird.Inspection<T4>,
            Bluebird.Inspection<T5>,
        ]
    >;
    static allSettled<T1, T2, T3, T4>(
        values: [Resolvable<T1>, Resolvable<T2>, Resolvable<T3>, Resolvable<T4>],
    ): Bluebird<[Bluebird.Inspection<T1>, Bluebird.Inspection<T2>, Bluebird.Inspection<T3>, Bluebird.Inspection<T4>]>;
    static allSettled<T1, T2, T3>(
        values: [Resolvable<T1>, Resolvable<T2>, Resolvable<T3>],
    ): Bluebird<[Bluebird.Inspection<T1>, Bluebird.Inspection<T2>, Bluebird.Inspection<T3>]>;
    static allSettled<T1, T2>(
        values: [Resolvable<T1>, Resolvable<T2>],
    ): Bluebird<[Bluebird.Inspection<T1>, Bluebird.Inspection<T2>]>;
    static allSettled<T1>(values: [Resolvable<T1>]): Bluebird<[Bluebird.Inspection<T1>]>;
    static allSettled<R>(values: Resolvable<Iterable<Resolvable<R>>>): Bluebird<Array<Bluebird.Inspection<R>>>;

    /**
     * Like ``Promise.all`` but for object properties instead of array items. Returns a promise that is fulfilled when all the properties of the object are fulfilled.
     *
     * The promise's fulfillment value is an object with fulfillment values at respective keys to the original object.
     * If any promise in the object rejects, the returned promise is rejected with the rejection reason.
     *
     * If `object` is a trusted `Promise`, then it will be treated as a promise for object rather than for its properties.
     * All other objects are treated for their properties as is returned by `Object.keys` - the object's own enumerable properties.
     *
     * *The original object is not modified.*
     */
    // map
    static props<K, V>(map: Resolvable<Map<K, Resolvable<V>>>): Bluebird<Map<K, V>>;
    // trusted promise for object
    static props<T>(object: PromiseLike<Bluebird.ResolvableProps<T>>): Bluebird<T>;
    // object
    static props<T>(object: Bluebird.ResolvableProps<T>): Bluebird<T>; // tslint:disable-line:unified-signatures

    /**
     * Like `Promise.some()`, with 1 as `count`. However, if the promise fulfills, the fulfillment value is not an array of 1 but the value directly.
     */
    static any<R>(values: Resolvable<Iterable<Resolvable<R>>>): Bluebird<R>;

    /**
     * Given an array, or a promise of an array, which contains promises (or a mix of promises and values) return a promise that is
     * fulfilled or rejected as soon as a promise in the array is fulfilled or rejected with the respective rejection reason or fulfillment value.
     *
     * **Note** If you pass empty array or a sparse array with no values, or a promise/thenable for such, it will be forever pending.
     */
    static race<R>(values: Resolvable<Iterable<Resolvable<R>>>): Bluebird<R>;

    /**
     * Initiate a competitive race between multiple promises or values (values will become immediately fulfilled promises).
     * When `count` amount of promises have been fulfilled, the returned promise is fulfilled with an array that contains the fulfillment values of
     * the winners in order of resolution.
     *
     * If too many promises are rejected so that the promise can never become fulfilled,
     * it will be immediately rejected with an array of rejection reasons in the order they were thrown in.
     *
     * *The original array is not modified.*
     */
    static some<R>(values: Resolvable<Iterable<Resolvable<R>>>, count: number): Bluebird<R[]>;

    /**
     * Promise.join(
     *   Promise<any>|any values...,
     *   function handler
     * ) -> Promise
     * For coordinating multiple concurrent discrete promises.
     *
     * Note: In 1.x and 0.x Promise.join used to be a Promise.all that took the values in as arguments instead in an array.
     * This behavior has been deprecated but is still supported partially - when the last argument is an immediate function value the new semantics will apply
     */
    static join<R, A1>(
        arg1: Resolvable<A1>,
        handler: (arg1: A1) => Resolvable<R>,
    ): Bluebird<R>;
    static join<R, A1, A2>(
        arg1: Resolvable<A1>,
        arg2: Resolvable<A2>,
        handler: (arg1: A1, arg2: A2) => Resolvable<R>,
    ): Bluebird<R>;
    static join<R, A1, A2, A3>(
        arg1: Resolvable<A1>,
        arg2: Resolvable<A2>,
        arg3: Resolvable<A3>,
        handler: (arg1: A1, arg2: A2, arg3: A3) => Resolvable<R>,
    ): Bluebird<R>;
    static join<R, A1, A2, A3, A4>(
        arg1: Resolvable<A1>,
        arg2: Resolvable<A2>,
        arg3: Resolvable<A3>,
        arg4: Resolvable<A4>,
        handler: (arg1: A1, arg2: A2, arg3: A3, arg4: A4) => Resolvable<R>,
    ): Bluebird<R>;
    static join<R, A1, A2, A3, A4, A5>(
        arg1: Resolvable<A1>,
        arg2: Resolvable<A2>,
        arg3: Resolvable<A3>,
        arg4: Resolvable<A4>,
        arg5: Resolvable<A5>,
        handler: (arg1: A1, arg2: A2, arg3: A3, arg4: A4, arg5: A5) => Resolvable<R>,
    ): Bluebird<R>;

    // variadic array
    /** @deprecated use .all instead */
    static join<R>(...values: Array<Resolvable<R>>): Bluebird<R[]>;

    /**
     * Map an array, or a promise of an array,
     * which contains a promises (or a mix of promises and values) with the given `mapper` function with the signature `(item, index, arrayLength)`
     * where `item` is the resolved value of a respective promise in the input array.
     * If any promise in the input array is rejected the returned promise is rejected as well.
     *
     * If the `mapper` function returns promises or thenables, the returned promise will wait for all the mapped results to be resolved as well.
     *
     * *The original array is not modified.*
     */
    static map<R, U>(
        values: Resolvable<Iterable<Resolvable<R>>>,
        mapper: IterateFunction<R, U>,
        options?: Bluebird.ConcurrencyOption,
    ): Bluebird<U[]>;

    /**
     * Reduce an array, or a promise of an array,
     * which contains a promises (or a mix of promises and values) with the given `reducer` function with the signature `(total, current, index, arrayLength)`
     * where `item` is the resolved value of a respective promise in the input array.
     * If any promise in the input array is rejected the returned promise is rejected as well.
     *
     * If the reducer function returns a promise or a thenable, the result for the promise is awaited for before continuing with next iteration.
     *
     * *The original array is not modified. If no `initialValue` is given and the array doesn't contain at least 2 items,
     * the callback will not be called and `undefined` is returned.
     *
     * If `initialValue` is given and the array doesn't have at least 1 item, `initialValue` is returned.*
     */
    static reduce<R, U>(
        values: Resolvable<Iterable<Resolvable<R>>>,
        reducer: (total: U, current: R, index: number, arrayLength: number) => Resolvable<U>,
        initialValue?: Resolvable<U>,
    ): Bluebird<U>;

    /**
     * Filter an array, or a promise of an array,
     * which contains a promises (or a mix of promises and values) with the given `filterer` function with the signature `(item, index, arrayLength)`
     * where `item` is the resolved value of a respective promise in the input array.
     * If any promise in the input array is rejected the returned promise is rejected as well.
     *
     * The return values from the filtered functions are coerced to booleans, with the exception of promises and thenables which are awaited for their eventual result.
     *
     * *The original array is not modified.
     */
    static filter<R>(
        values: Resolvable<Iterable<Resolvable<R>>>,
        filterer: IterateFunction<R, boolean>,
        option?: Bluebird.ConcurrencyOption,
    ): Bluebird<R[]>;

    /**
     * Iterate over an array, or a promise of an array,
     * which contains promises (or a mix of promises and values) with the given iterator function with the signature `(item, index, value)`
     * where item is the resolved value of a respective promise in the input array.
     * Iteration happens serially. If any promise in the input array is rejected the returned promise is rejected as well.
     *
     * Resolves to the original array unmodified, this method is meant to be used for side effects.
     * If the iterator function returns a promise or a thenable, the result for the promise is awaited for before continuing with next iteration.
     */
    static each<R>(
        values: Resolvable<Iterable<Resolvable<R>>>,
        iterator: IterateFunction<R, any>,
    ): Bluebird<R[]>;

    /**
     * Given an Iterable(arrays are Iterable), or a promise of an Iterable, which produces promises (or a mix of promises and values),
     * iterate over all the values in the Iterable into an array and iterate over the array serially, in-order.
     *
     * Returns a promise for an array that contains the values returned by the iterator function in their respective positions.
     * The iterator won't be called for an item until its previous item, and the promise returned by the iterator for that item are fulfilled.
     * This results in a mapSeries kind of utility but it can also be used simply as a side effect iterator similar to Array#forEach.
     *
     * If any promise in the input array is rejected or any promise returned by the iterator function is rejected, the result will be rejected as well.
     */
    static mapSeries<R, U>(
        values: Resolvable<Iterable<Resolvable<R>>>,
        iterator: IterateFunction<R, U>,
    ): Bluebird<U[]>;

    /**
     * A meta method used to specify the disposer method that cleans up a resource when using `Promise.using`.
     *
     * Returns a Disposer object which encapsulates both the resource as well as the method to clean it up.
     *  The user can pass this object to `Promise.using` to get access to the resource when it becomes available,
     *  as well as to ensure its automatically cleaned up.
     *
     * The second argument passed to a disposer is the result promise of the using block, which you can
     *  inspect synchronously.
     */
    disposer(disposeFn: (arg: R, promise: Bluebird<R>) => Resolvable<void>): Bluebird.Disposer<R>;

    /**
     * In conjunction with `.disposer`, using will make sure that no matter what, the specified disposer
     *  will be called when the promise returned by the callback passed to using has settled. The disposer is
     *  necessary because there is no standard interface in node for disposing resources.
     */
    static using<R, T>(
        disposer: Bluebird.Disposer<R>,
        executor: (transaction: R) => PromiseLike<T>,
    ): Bluebird<T>;
    static using<R1, R2, T>(
        disposer: Bluebird.Disposer<R1>,
        disposer2: Bluebird.Disposer<R2>,
        executor: (transaction1: R1, transaction2: R2) => PromiseLike<T>,
    ): Bluebird<T>;
    static using<R1, R2, R3, T>(
        disposer: Bluebird.Disposer<R1>,
        disposer2: Bluebird.Disposer<R2>,
        disposer3: Bluebird.Disposer<R3>,
        executor: (transaction1: R1, transaction2: R2, transaction3: R3) => PromiseLike<T>,
    ): Bluebird<T>;

    /**
     * Configure long stack traces, warnings, monitoring and cancellation.
     * Note that even though false is the default here, a development environment might be detected which automatically
     *  enables long stack traces and warnings.
     */
    static config(options: {
        /** Enable warnings */
        warnings?: boolean | {
            /** Enables all warnings except forgotten return statements. */
            wForgottenReturn: boolean;
        } | undefined;
        /** Enable long stack traces */
        longStackTraces?: boolean | undefined;
        /** Enable cancellation */
        cancellation?: boolean | undefined;
        /** Enable monitoring */
        monitoring?: boolean | undefined;
        /** Enable async hooks */
        asyncHooks?: boolean | undefined;
    }): void;

    /**
     * Create a new promise. The passed in function will receive functions `resolve` and `reject` as its arguments which can be called to seal the fate of the created promise.
     * If promise cancellation is enabled, passed in function will receive one more function argument `onCancel` that allows to register an optional cancellation callback.
     */
    static Promise: typeof Bluebird;

    /**
     * The version number of the library
     */
    static version: string;
}

declare namespace Bluebird {
    interface ConcurrencyOption {
        concurrency: number;
    }
    interface SpreadOption {
        spread: boolean;
    }
    interface FromNodeOptions {
        multiArgs?: boolean | undefined;
    }
    interface PromisifyOptions {
        context?: any;
        multiArgs?: boolean | undefined;
    }
    interface PromisifyAllOptions<T> extends PromisifyOptions {
        suffix?: string | undefined;
        filter?(name: string, func: (...args: any[]) => any, target?: any, passesDefaultFilter?: boolean): boolean;
        // The promisifier gets a reference to the original method and should return a function which returns a promise
        promisifier?(
            this: T,
            originalMethod: (...args: any[]) => any,
            defaultPromisifer: (...args: any[]) => (...args: any[]) => Bluebird<any>,
        ): () => PromiseLike<any>;
    }
    interface CoroutineOptions {
        yieldHandler(value: any): any;
    }

    /**
     * Represents an error is an explicit promise rejection as opposed to a thrown error.
     *  For example, if an error is errbacked by a callback API promisified through undefined or undefined
     *  and is not a typed error, it will be converted to a `OperationalError` which has the original error in
     *  the `.cause` property.
     *
     * `OperationalError`s are caught in `.error` handlers.
     */
    class OperationalError extends Error {}

    /**
     * Signals that an operation has timed out. Used as a custom cancellation reason in `.timeout`.
     */
    class TimeoutError extends Error {}

    /**
     * Signals that an operation has been aborted or cancelled. The default reason used by `.cancel`.
     */
    class CancellationError extends Error {}

    /**
     * A collection of errors. `AggregateError` is an array-like object, with numeric indices and a `.length` property.
     *  It supports all generic array methods such as `.forEach` directly.
     *
     * `AggregateError`s are caught in `.error` handlers, even if the contained errors are not operational.
     *
     * `Promise.some` and `Promise.any` use `AggregateError` as rejection reason when they fail.
     */
    class AggregateError extends Error implements ArrayLike<Error> {
        length: number;
        [index: number]: Error;
        join(separator?: string): string;
        pop(): Error;
        push(...errors: Error[]): number;
        shift(): Error;
        unshift(...errors: Error[]): number;
        slice(begin?: number, end?: number): AggregateError;
        filter(
            callback: (element: Error, index: number, array: AggregateError) => boolean,
            thisArg?: any,
        ): AggregateError;
        forEach(callback: (element: Error, index: number, array: AggregateError) => void, thisArg?: any): undefined;
        some(callback: (element: Error, index: number, array: AggregateError) => boolean, thisArg?: any): boolean;
        every(callback: (element: Error, index: number, array: AggregateError) => boolean, thisArg?: any): boolean;
        map(callback: (element: Error, index: number, array: AggregateError) => boolean, thisArg?: any): AggregateError;
        indexOf(searchElement: Error, fromIndex?: number): number;
        lastIndexOf(searchElement: Error, fromIndex?: number): number;
        reduce(
            callback: (accumulator: any, element: Error, index: number, array: AggregateError) => any,
            initialValue?: any,
        ): any;
        reduceRight(
            callback: (previousValue: any, element: Error, index: number, array: AggregateError) => any,
            initialValue?: any,
        ): any;
        sort(compareFunction?: (errLeft: Error, errRight: Error) => number): AggregateError;
        reverse(): AggregateError;
    }

    /**
     * returned by `Bluebird.disposer()`.
     */
    class Disposer<R> {}

    /** @deprecated Use PromiseLike<T> directly. */
    type Thenable<T> = PromiseLike<T>;

    type ResolvableProps<T> = object & { [K in keyof T]: Resolvable<T[K]> };

    interface Resolver<R> {
        /**
         * Returns a reference to the controlled promise that can be passed to clients.
         */
        promise: Bluebird<R>;

        /**
         * Resolve the underlying promise with `value` as the resolution value. If `value` is a thenable or a promise, the underlying promise will assume its state.
         */
        resolve(value: R): void;
        resolve(): void;

        /**
         * Reject the underlying promise with `reason` as the rejection reason.
         */
        reject(reason: any): void;

        /**
         * Gives you a callback representation of the `PromiseResolver`. Note that this is not a method but a property.
         * The callback accepts error object in first argument and success values on the 2nd parameter and the rest, I.E. node js conventions.
         *
         * If the the callback is called with multiple success values, the resolver fulfills its promise with an array of the values.
         */
        // TODO specify resolver callback
        callback(err: any, value: R, ...values: R[]): void;
    }

    interface Inspection<R> {
        /**
         * See if the underlying promise was fulfilled at the creation time of this inspection object.
         */
        isFulfilled(): boolean;

        /**
         * See if the underlying promise was rejected at the creation time of this inspection object.
         */
        isRejected(): boolean;

        /**
         * See if the underlying promise was cancelled at the creation time of this inspection object.
         */
        isCancelled(): boolean;

        /**
         * See if the underlying promise was defer at the creation time of this inspection object.
         */
        isPending(): boolean;

        /**
         * Get the fulfillment value of the underlying promise. Throws if the promise wasn't fulfilled at the creation time of this inspection object.
         *
         * throws `TypeError`
         */
        value(): R;

        /**
         * Get the rejection reason for the underlying promise. Throws if the promise wasn't rejected at the creation time of this inspection object.
         *
         * throws `TypeError`
         */
        reason(): any;
    }

    /**
     * Returns a new independent copy of the Bluebird library.
     *
     * This method should be used before you use any of the methods which would otherwise alter the global Bluebird object - to avoid polluting global state.
     */
    function getNewLibraryCopy(): typeof Bluebird;

    /**
     * This is relevant to browser environments with no module loader.
     *
     * Release control of the Promise namespace to whatever it was before this library was loaded.
     * Returns a reference to the library namespace so you can attach it to something else.
     */
    function noConflict(): typeof Bluebird;

    /**
     * Changes how bluebird schedules calls a-synchronously.
     *
     * @param scheduler Should be a function that asynchronously schedules
     *                  the calling of the passed in function
     */
    function setScheduler(scheduler: (callback: (...args: any[]) => void) => void): void;
}

/**
 * Options for {@link waitForCondition }
 */
type WaitForConditionOptions = {
    waitMs?: number | undefined;
    intervalMs?: number | undefined;
    logger?: {
        debug: (...args: any[]) => void;
    } | undefined;
    error?: string | Error | undefined;
};
/**
 * Options for {@link longSleep }
 */
type LongSleepOptions = {
    thresholdMs?: number | undefined;
    intervalMs?: number | undefined;
    progressCb?: ProgressCallback | null | undefined;
};
/**
 * Parameter provided to a {@link ProgressCallback }
 */
type Progress = {
    elapsedMs: number;
    timeLeft: number;
    progress: number;
};
/**
 * Progress callback for {@link longSleep }
 */
type ProgressCallback = (progress: Progress) => void;
/**
 * An async/await version of setTimeout
 * @param {number} ms
 * @returns {Promise<void>}
 */
declare function sleep(ms: number): Promise<void>;
/**
 * An async/await way of running a method until it doesn't throw an error
 * @template [T=any]
 * @param {number} times
 * @param {(...args: any[]) => Promise<T>} fn
 * @param  {...any} args
 * @returns {Promise<T?>}
 */
declare function retry<T = any>(times: number, fn: (...args: any[]) => Promise<T>, ...args: any[]): Promise<T | null>;
/**
 * Export async functions (Promises) and import this with your ES5 code to use
 * it with Node.
 * @template [R=any]
 * @param {any} promisey
 * @param {(err: any, value?: R) => void} cb
 * @returns {Promise<R>}
 */
declare function nodeify<R = any>(promisey: any, cb: (err: any, value?: R | undefined) => void): Promise<R>;
/**
 * Node-ify an entire object of `Promise`-returning functions
 * @param {Record<string,(...args: any[]) => any>} promiseyMap
 * @returns {Record<string,(...args: any[])=>void>}
 */
declare function nodeifyAll(promiseyMap: Record<string, (...args: any[]) => any>): Record<string, (...args: any[]) => void>;
/**
 * You can also use `retryInterval` to add a sleep in between retries. This can
 * be useful if you want to throttle how fast we retry.
 * @template [T=any]
 * @param {number} times
 * @param {number} sleepMs
 * @param {(...args: any[]) => Promise<T>} fn
 * @param  {...any} args
 * @returns {Promise<T?>}
 */
declare function retryInterval<T = any>(times: number, sleepMs: number, fn: (...args: any[]) => Promise<T>, ...args: any[]): Promise<T | null>;
/**
 * @param {(...args: any[]) => any|Promise<any>} fn
 * @param  {...any} args
 */
declare function asyncify(fn: (...args: any[]) => any | Promise<any>, ...args: any[]): void;
declare const parallel: typeof Bluebird.all;
/**
 * Similar to `Array.prototype.map`; runs in serial
 * @param {any[]} coll
 * @param {(value: any) => any|Promise<any>} mapper
 * @returns {Promise<any[]>}
 */
declare function asyncmap(coll: any[], mapper: (value: any) => any | Promise<any>, runInParallel?: boolean): Promise<any[]>;
/**
 * Similar to `Array.prototype.filter`
 * @param {any[]} coll
 * @param {(value: any) => any|Promise<any>} filter
 * @param {boolean} runInParallel
 * @returns {Promise<any[]>}
 */
declare function asyncfilter(coll: any[], filter: (value: any) => any | Promise<any>, runInParallel?: boolean): Promise<any[]>;
/**
 * Takes a condition (a function returning a boolean or boolean promise), and
 * waits until the condition is true.
 *
 * Throws a `/Condition unmet/` error if the condition has not been satisfied
 * within the allocated time, unless an error is provided in the options, as the
 * `error` property, which is either thrown itself, or used as the message.
 *
 * The condition result is returned if it is not falsy. If the condition throws an
 * error then this exception will be immediately passed through.
 *
 * The default options are: `{ waitMs: 5000, intervalMs: 500 }`
 * @template T
 * @param {() => Promise<T>|T} condFn
 * @param {WaitForConditionOptions} [options]
 * @returns {Promise<T>}
 */
declare function waitForCondition<T>(condFn: () => T | Promise<T>, options?: WaitForConditionOptions | undefined): Promise<T>;
/**
 * Sometimes `Promise.delay` or `setTimeout` are inaccurate for large wait
 * times. To safely wait for these long times (e.g. in the 5+ minute range), you
 * can use `longSleep`.
 *
 * sYou can also pass a `progressCb` option which is a callback function that
 * receives an object with the properties `elapsedMs`, `timeLeft`, and
 * `progress`. This will be called on every wait interval so you can do your
 * wait logging or whatever.
 * @param {number} ms
 * @param {LongSleepOptions} [opts]
 * @returns {Promise<void>}
 */
declare function longSleep(ms: number, { thresholdMs, intervalMs, progressCb, }?: LongSleepOptions | undefined): Promise<void>;

type async_LongSleepOptions = LongSleepOptions;
type async_Progress = Progress;
type async_ProgressCallback = ProgressCallback;
type async_WaitForConditionOptions = WaitForConditionOptions;
declare const async_asyncfilter: typeof asyncfilter;
declare const async_asyncify: typeof asyncify;
declare const async_asyncmap: typeof asyncmap;
declare const async_longSleep: typeof longSleep;
declare const async_nodeify: typeof nodeify;
declare const async_nodeifyAll: typeof nodeifyAll;
declare const async_parallel: typeof parallel;
declare const async_retry: typeof retry;
declare const async_retryInterval: typeof retryInterval;
declare const async_sleep: typeof sleep;
declare const async_waitForCondition: typeof waitForCondition;
declare namespace async {
  export {
    async_LongSleepOptions as LongSleepOptions,
    async_Progress as Progress,
    async_ProgressCallback as ProgressCallback,
    async_WaitForConditionOptions as WaitForConditionOptions,
    async_asyncfilter as asyncfilter,
    async_asyncify as asyncify,
    async_asyncmap as asyncmap,
    async_longSleep as longSleep,
    async_nodeify as nodeify,
    async_nodeifyAll as nodeifyAll,
    async_parallel as parallel,
    async_retry as retry,
    async_retryInterval as retryInterval,
    async_sleep as sleep,
    async_waitForCondition as waitForCondition,
  };
}

declare class AITAndroidLibs extends AITModule {
    automatorDriver: typeof automatorDriver | undefined;
    support: typeof support | undefined;
    async: typeof async | undefined;
    process: typeof process | undefined;
    constructor();
    static newInstance(): AITAndroidLibs;
}

export { AITAndroidLibs as default };
